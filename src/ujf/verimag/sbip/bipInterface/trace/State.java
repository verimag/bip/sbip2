package ujf.verimag.sbip.bipInterface.trace;
import java.util.ArrayList;




public class State {
	private Double clockValue;
	private int id;
	private ArrayList<StateVariable> v_states;

	public State() {
		super();
		this.id = 0;
		this.clockValue = 0.0;
		this.v_states = new ArrayList<StateVariable>();
	}
	
	public State(int id) {
		super();
		this.clockValue = 0.0;
		this.v_states = new ArrayList<StateVariable>();
		this.id = id;
	}

	public State(ArrayList<StateVariable> v_states) {
		super();
		this.id = 0;
		this.clockValue = 0.0;
		this.v_states = v_states;
	}
	
	public State(ArrayList<StateVariable> v_states, Double d) {
		super();
		this.id = 0;
		this.clockValue = d;
		this.v_states = v_states;
	}

	
	public State(int id, ArrayList<StateVariable> v_states, Double d) {
		super();
		this.id = id;
		this.clockValue = d;
		this.v_states = v_states;
	}
	
	public State(State s)
	{
		this(s.id, s.v_states, s.clockValue);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ArrayList<StateVariable> getStateVariables() {
		return v_states;
	}

	public StateVariable getStateVariable(int i) {
		return v_states.get(i);
	}

	public void setStateVariables(ArrayList<StateVariable> v_states) {
		this.v_states = v_states;
	}
	
	public void setClockValue(Double d){
		this.clockValue = d;
	}
	
	public Double getClockValue(){
		return this.clockValue;
	}


	public String toString() {
		String tmp="@GlobalTime (ns) : "+ clockValue + "\n";
		for(int i = 0; i< this.v_states.size();i++)
		{
			tmp+=v_states.get(i).toString() + "\n";
		}
		return tmp;
	}

	public StateVariable getVariable(String text) {
		for (StateVariable stateVariable : v_states) {
			
			if(stateVariable.getVariable().equals(text)) {
//				System.out.println(stateVariable.toString());
				return stateVariable;
			}
		}
		return null;
	}

	public void addVariable(StateVariable parametricVariable) {
		if(parametricVariable != null){
			this.v_states.add(parametricVariable);
		}
		
	}

	public String toBip(boolean initial) {
		String S;
		if(initial)
			S = "[BIP ENGINE]: initialize components...\n" + 
				"[BIP ENGINE]: random scheduling based on seed=103424086\n";
		else
			S = "[BIP ENGINE]: -> choose [0] one thing\n";
		for (StateVariable stateVariable : v_states) {
			S+=stateVariable.toBip()+"\n";
		}
		S+="[BIP ENGINE]: state #0: 1 interaction:\n" + 
				"[BIP ENGINE]:   [0] things to do\n";
		return S;
	}

	
}