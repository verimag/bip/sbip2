package ujf.verimag.sbip.bipInterface.trace;


public class StateVariable {
	
	private String variable;
	private String value;
	private String type;
	
	public StateVariable(String variable, String value, String type) {
		super();
		this.variable = variable;
		this.value = value;
		this.type = type;
	}
	
	public StateVariable(StateVariable sv) {
		this(sv.getVariable(), sv.getValue(),sv.getType());
	}
	
	public String getVariable() {
		return variable;
	}
	public void setVariable(String variable) {
		this.variable = variable;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "\t " + type + " "+ variable + " = " + value;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StateVariable other = (StateVariable) obj;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (variable == null) {
			if (other.variable != null)
				return false;
		} else if (!variable.equals(other.variable))
			return false;
		return true;
	}

	public String toBip() {
		return "var "+this.type+" "+ this.variable+" "+ this.value;
	}	
}