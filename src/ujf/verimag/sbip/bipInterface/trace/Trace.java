package ujf.verimag.sbip.bipInterface.trace;

import java.util.ArrayList;

import javax.swing.JOptionPane;

public class Trace {
	
	private ArrayList<State> states;
	private String bipTrace;

	public Trace(ArrayList<State> states) {
		super();
		this.states = states;
	}
	
	public Trace(ArrayList<State> states, String t) {
		super();
		this.states = states;
		this.bipTrace = t;
	}

	public Trace() {
	}

	public State getState(int index) {
		return states.get(index);
	}
	
	public ArrayList<State> getStates() {
		return states;
	}

	public void setStates(ArrayList<State> states) {
		this.states = states;
	}
	
	public String getBipTrace()
	{
		return bipTrace;
	}
	
	public void setBiptrace(String t)
	{
		bipTrace = t;
	}

	@Override
	public String toString() {
		double time = 0;
		StringBuilder tmp = new StringBuilder();
		for(int i = 0; i < states.size(); i++) {
			//System.out.println(tmp);
			if(states.get(i).getClockValue() < time) {
				JOptionPane.showMessageDialog(null, "Error at state "+i, "err", JOptionPane.CANCEL_OPTION);
			}
			else time = states.get(i).getClockValue();
			tmp.append("\nState" + i + ":\n" + states.get(i).toString());
		}
		
		return tmp.toString();
	}
	

	public String toString(int nb_symbols) {
		double time = 0;
		StringBuilder tmp = new StringBuilder();
		for(int i = 0; i < (states.size()<nb_symbols? states.size():nb_symbols); i++) {
			//System.out.println(tmp);
			if(states.get(i).getClockValue() < time) {
				JOptionPane.showMessageDialog(null, "Error at state "+i, "err", JOptionPane.CANCEL_OPTION);
			}
			else time = states.get(i).getClockValue();
			tmp.append("\nState" + i + ":\n" + states.get(i).toString());
		}
		
		return tmp.toString();
	}


	public String toBip() {
		String S = "[BIP ENGINE]: BIP Engine (version 2017.12.172218-DEV )\n" ;
		boolean initial = true;
		for (State state : states) {
			S+= state.toBip(initial);
			
			initial = false;
		}
		return S;
	}
	
	

}