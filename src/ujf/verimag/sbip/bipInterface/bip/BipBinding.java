package ujf.verimag.sbip.bipInterface.bip;

import ujf.verimag.sbip.bipInterface.trace.State;

/**
 * This interface defines the methods that a simulator must expose in order to be plugged to PlasmaLab
 *
 */
public interface BipBinding {

	/**
	 * Test whether the file at path exists and is a valid BIP executable.
	 * @param path A string indicating the path to the file.
	 * @param seed A seed for initializing the simulator
	 * @param extra_parameters Some extra parameters to pass to the BIP engine.
	 * @return true if a valid BIP executable was initialized, false otherwise.
	 */
	public boolean init(String path);
	public boolean init(String path, long seed);
	public boolean init(String path, String extra_parameters);
	public boolean init(String path, String extra_parameters, long seed);
	
	/**
	 * Restore a state from its index.
	 * @param state_index the index of the state to restore.
	 */
	public void setState(int state_index);
	
	/**
	 * Save the current state.
	 * @return the id of the saved state.
	 */
	public int saveState(int state_index);
	
	/**
	 * Advance the simulation
	 * @param nb_steps number of steps to execute.
	 */
	public void step(int nb_steps);
	
	/**
	 *  Get a representation of the state at index index.
	 */
	public State getState(int index);
}
