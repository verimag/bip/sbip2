package ujf.verimag.sbip.bipInterface.bip;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import ujf.verimag.sbip.bipInterface.trace.*;

public class TraceGenerator {
	
	String URL = "";
	int ID = 0;
	int limit;
	boolean logvar = true;
	boolean saveTrace = true;
	String URL_traceRoot;
	
	public TraceGenerator(String url, int limit){
		this.URL = url;
		this.limit = limit;

	}

	public TraceGenerator(String url, int limit, boolean logVariables){
		this.URL = url;
		this.limit = limit;
		this.logvar = logVariables;

	}
	
	public TraceGenerator(String url, int limit, boolean logVariables, boolean saveTrace, String URL_traceRoot){
		this.URL = url;
		this.limit = limit;
		this.logvar = logVariables;
		this.saveTrace = saveTrace;
		this.URL_traceRoot = URL_traceRoot;

	}

	public Trace generate() throws IOException, InterruptedException{
//		System.out.println("start trace sim");
		Utilities u = new Utilities();
		String trace = generate(ID);
//		System.out.println("Sim time : "+(midTime));
		Trace t = u.extractTrace(trace);
//		System.out.println("Trace building time : "+(System.currentTimeMillis()-midTime-startTime));
		ID ++;
		return t;
	}
	
	private String generate(int id) throws IOException, InterruptedException{
				
		
		/// End make sim visible
		
		String raw = "";
		StringBuilder builder = new StringBuilder();
		long seed = Math.round(Math.random()*(Integer.MAX_VALUE-1)+1);
		File modelFile = new File(URL);
		String cmd = "./"+modelFile.getName()  +" --seed "+ seed  + (logvar? " --log-variables" : "") + " --limit "+ limit;
//		System.out.println(cmd);
		
//		Process p = Runtime.getRuntime().exec();
		Process p = Runtime.getRuntime().exec(new String[]{"bash","-c",cmd}, null, modelFile.getParentFile());
		
//		Process p = Runtime.getRuntime().exec(new String[]{"bash","-c",cmd}, null, new File(URL).getParentFile());
		try (final BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
	        String line;
	        while ((line = b.readLine()) != null) {
	        	
//	        	System.out.println(line);
//	            raw += line+"\n";
	        	builder.append(line+ "\n");
	        }
	        
	        raw = builder.toString();
			
	        
	    }
		try
		{
			p.waitFor();
		}		
		catch(InterruptedException e){
//			System.out.println("Aborted trace generator!");
		}
		
		if(p.exitValue() != 0){
			JOptionPane.showMessageDialog(null, "Error : The trace generation process exited with "+p.exitValue(),"Trace generation failed", JOptionPane.ERROR_MESSAGE);
		}
		
		if(saveTrace) saveTrace(raw);
		return raw;
		

		
		
		
	}
	
	public void saveTrace(String trace){
		
		PrintWriter out;
		try {
			out = new PrintWriter(this.URL_traceRoot+"trace_"+ID+".t");			
			out.print(trace);
			out.close();
			} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public Trace generate(ArrayList<String> varsList) throws IOException, InterruptedException {
		Utilities u = new Utilities();
		String trace = generate(ID);
//		System.out.println("Sim time : "+(midTime));
		Trace t = u.extractTrace(trace, varsList);
//		System.out.println("Trace building time : "+ endTime);
		ID ++;
		return t;
	}
}
