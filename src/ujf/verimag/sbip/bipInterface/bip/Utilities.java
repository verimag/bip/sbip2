package ujf.verimag.sbip.bipInterface.bip;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import ujf.verimag.sbip.bipInterface.trace.State;
import ujf.verimag.sbip.bipInterface.trace.StateVariable;
import ujf.verimag.sbip.bipInterface.trace.Trace;


public class Utilities {
	
//	public static void main(String[] args){
//		Trace tr = transformBip2("trace.txt");
//		System.out.println(tr);
//	}


	/**
	 * Method that transform New BIP (BIP2) output to a Trace Object.
	 * 
	 * @param fname 
	 * 			Name of the file in which we store generated Traces
	 * @return Trace as Object
	 * @see Trace
	 */
	public Trace transformBip2(String fname)
	{
		BufferedReader f = null;
		String line;
		boolean marker;
		int nbStateVars = 0;
		int vars_count = 0;
		int state_count = 0;

		//System.out.println("Start to build a trace \n");
		/** Instatiation d'un vecteur d'etats**/
		ArrayList<State> states = new ArrayList<State>();
		try {
			f = new BufferedReader(new FileReader(fname));

			// avancer jusqu'a la ligne d'initilisation
			do {
				line = f.readLine();
			} while(!line.startsWith("[BIP ENGINE]: initialize components"));

			//System.out.println(line.toString());

			// initialization step 
			marker = true;
			ArrayList<StateVariable> statesVarsInit = new ArrayList<StateVariable>();
			line = f.readLine();	// lire la ligne suivante
			while(line != null && marker)
			{
				if(line.startsWith("var"))
				{
					String[] tokens = line.split(" ");
					//System.out.println(line.toString());
					//System.out.println(tokens[1] + ", " + tokens[2] + ", " + tokens[3]);
					StateVariable var = new StateVariable(tokens[2], tokens[3], tokens[1]);
					statesVarsInit.add(var);
					line = f.readLine();
				}
				else if(line.startsWith("[BIP ENGINE]: -> choose")) {
					marker = false;
				}
				else
					line = f.readLine();
			}
			// Je sais que pour l'initialisation, j'aurais surement des valeurs pour chaque variable
			nbStateVars = statesVarsInit.size();
			//System.out.println("nb vars to consider = " + nbStateVars);
			State stInit = new State(statesVarsInit, 0.0);
			states.add(stInit);
			state_count++;
			//System.out.println(states.size());
			//System.out.println(states.get(0).toString());

			while(line != null)
			{
				if(line.startsWith("[BIP ENGINE]: -> choose"))
				{
					Double time = 0.0;
					if(line.contains("at global time"))
						time = getTimeValue(line);
					marker = true;
					vars_count=0;
					ArrayList<StateVariable> statesVars = new ArrayList<StateVariable>();
					//System.out.println("Find a new state, number of state vars = " + statesVars.size());
					line = f.readLine();
					while(line != null && marker)
					{
						if(line.startsWith("var"))
						{
							String[] tokens = line.split(" ");
							//System.out.println(line.toString());
							//System.out.println(tokens[1] + ", " + tokens[2] + ", " + tokens[3]);
							StateVariable var = new StateVariable(tokens[2], tokens[3], tokens[1]);
							statesVars.add(var);
							vars_count++ ;
							line = f.readLine();
						}
						else if(line.startsWith("[BIP ENGINE]: -> choose"))
							marker = false;
						else
							line = f.readLine();

					}
					// si etat vide ==> pas dechangement sur les variables observees
					//if(vars_count == 0) {
					//System.out.println("Empty state");
					//st = new State(states.get(state_count - 1));
					//} else {
					if(vars_count > 0) {
						if(vars_count < nbStateVars) {
							//System.out.println("----- nb vars in the current state = " + vars_count);
							//System.out.println("predecessor state contains " + states.get(state_count-1).getStates().size() + " vars");
							for(int i = 0; i < states.get(state_count-1).getStateVariables().size(); i++) {
								if(!statesVars.contains(states.get(state_count-1).getStateVariables().get(i))) {
									statesVars.add(new StateVariable(states.get(state_count-1).getStateVariables().get(i)));
								}
							}
						}
						State st = new State(statesVars, time);
						states.add(st);
						state_count++;
					}
				} else
					line = f.readLine();
			}
			f.close();
			//System.out.println("number of states in the trace= " + states.size()+"\n");
			//System.out.println("number of states in the trace= " + state_count+"\n");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if(states.size() == 0)
		{
			System.err.println("Error : Empty Trace Found\n");
			System.exit(-1);
		}
		//System.out.println("Trace built and returned \n");
		return new Trace(states);
	}
	
	public  Double getTimeValue(String line) {
//		System.out.println("Parsing "+ line);
		String[] ss = line.split(" ");
		String strTime = ss[ss.length-1];
//		System.out.print(strTime);
		String val, unit;
		int splitIndex = 0;
		
		Double timeValue = 0.0;

		ArrayList<String> timeDigits = splitTimeString(strTime);
		
		for (int i = 0; i < timeDigits.size(); i++) {
			String sDigit = timeDigits.get(i);
//			System.out.println(sDigit);
			Boolean foundUnit = false;
			for (int j = 0; j < sDigit.length(); j++) {
				if(sDigit.charAt(j)>'9' && !foundUnit){
					splitIndex = j;
					foundUnit = true;
				}
			}
			
			val =  sDigit.substring(0, splitIndex);
			unit = sDigit.substring(splitIndex , sDigit.length());
			timeValue += getTimeValue(val, unit);
		}
		
//		System.out.println(" => "+ timeValue);
		return timeValue;
	}

	private  ArrayList<String> splitTimeString(String strTime) {
		ArrayList<String> ss= new ArrayList<String>();
		String s = "";
		Boolean foundUnit = false;
		int begIndex = 0, endIndex = 0;
		while(endIndex < strTime.length() - 1){
			if(foundUnit){
				if(strTime.charAt(endIndex)<='9'){
					
					foundUnit = false;
					s = strTime.substring(begIndex,endIndex);
					ss.add(s);
					begIndex = endIndex;
				}
			}
			if(!foundUnit){
				if(strTime.charAt(endIndex)>'9'){
					foundUnit = true;
				}
					
			}
			endIndex ++;
		}
		s = strTime.substring(begIndex,endIndex+1);
		ss.add(s);
		return ss;
	}


	private  Double getTimeValue(String val, String unit) {
//		System.out.println("Extracted :"+val +" "+unit);
		Double value = 0.0;
		if(!unit.equals("0"))
			value = Double.parseDouble(val);
		
		Double convertUnit = 1.0;
		
		switch (unit) {
		case "ns":
			convertUnit = 1.0;
			break;
		
		case "us":
			convertUnit = 1000.0;
			break;
			
		case "ms":
			convertUnit = 1000000.0;
			break;
			
		case "s":
			convertUnit = 1000000000.0;
			break;
			
		case "min":
			convertUnit = 60000000000.0;
			break;
			
		case "hr":
			convertUnit = 3600000000000.0;
			break;
			
			
		case "day":
			convertUnit = 24*3600000000000.0;
			break;

		default:
			convertUnit = 1.0;
			break;
		}
		
		return value * convertUnit;
		
	}

//	public static Trace Bip2Output2Trace(BufferedReader f)
//	{
//		String line;
//		boolean stateMarker;
//		int nbStateVars = 0;
//		int varsCount = 0;
//		int stateCount = 0;
//		
//		/** An attempt to store the whole trace
//		 * I Think it is really costly 
//		 * I need to optimize it: return it only when needed **/
//		String trace="";
//
//		//System.out.println("Start to build a trace \n");
//		/** Instatiation d'un vecteur d'etats**/
//		ArrayList<State> states = new ArrayList<State>();
//		try {
//
//			// avancer jusqu'a la ligne d'initilisation
//			do {
//				line = f.readLine();
//				
//				/** accumulate the read lines in trace **/
//				trace += line + "\n";
//				
//				//System.out.println(line);
//			} while(line != null && !line.startsWith("[BIP ENGINE]: random scheduling based on seed"));
//
//			if(line == null)
//				return null;
//			
//			//System.out.println(line);
//
//			// initialization step 
//			stateMarker = true;
//			ArrayList<StateVariable> statesVarsInit = new ArrayList<StateVariable>();
//			line = f.readLine();	// lire la ligne suivante
//			
//			/** accumulate the read lines in trace **/
//			trace += line + "\n";
//			
//			//System.out.println(line);
//			while(line != null && stateMarker)
//			{
//				if(line.startsWith("var"))
//				{
//					String[] tokens = line.split(" ");
//					//System.out.println(line.toString());
//					//System.out.println(tokens[1] + ", " + tokens[2] + ", " + tokens[3]);
//					StateVariable var = new StateVariable(tokens[2], tokens[3], tokens[1]);
//					statesVarsInit.add(var);
//					line = f.readLine();
//					
//					/** accumulate the read lines in trace **/
//					trace += line + "\n";
//				}
//				else if(line.startsWith("[BIP ENGINE]: -> choose")) {
//					stateMarker = false;
//				}
//				else{
//					line = f.readLine();
//					
//					/** accumulate the read lines in trace **/
//					trace += line + "\n";
//				}
//				
//			}
//			// Je sais que pour l'initialisation, j'aurais surement des valeurs pour chaque variable
//			nbStateVars = statesVarsInit.size();
//			//System.out.println("nb vars to consider = " + nbStateVars);
//			State stInit = new State(statesVarsInit);
//			states.add(stInit);
//			stateCount++;
//			//System.out.println(states.size());
//			//System.out.println(states.get(0).toString());
//
//			while(line != null)
//			{
//				if(line.startsWith("[BIP ENGINE]: -> choose"))
//				{
//					stateMarker = true;
//					varsCount=0;
//					ArrayList<StateVariable> statesVars = new ArrayList<StateVariable>();
//					//System.out.println("Find a new state, number of state vars = " + statesVars.size());
//					line = f.readLine();
//					
//					/** accumulate the read lines in trace **/
//					trace += line + "\n";
//					
//					while(line != null && stateMarker)
//					{
//						if(line.startsWith("var"))
//						{
//							String[] tokens = line.split(" ");
//							//System.out.println(line.toString());
//							//System.out.println(tokens[1] + ", " + tokens[2] + ", " + tokens[3]);
//							StateVariable var = new StateVariable(tokens[2], tokens[3], tokens[1]);
//							statesVars.add(var);
//							varsCount++ ;
//							line = f.readLine();
//							
//							/** accumulate the read lines in trace **/
//							trace += line + "\n";
//						}
//						else if(line.startsWith("[BIP ENGINE]: -> choose"))
//							stateMarker = false;
//						else{
//							line = f.readLine();
//							
//							/** accumulate the read lines in trace **/
//							trace += line + "\n";
//						}
//						
//						
//
//					}
//					// si etat vide ==> pas dechangement sur les variables observees
//					//if(vars_count == 0) {
//					//System.out.println("Empty state");
//					//st = new State(states.get(state_count - 1));
//					//} else {
//					if(varsCount > 0) {
//						if(varsCount < nbStateVars) {
//							//System.out.println("----- nb vars in the current state = " + vars_count);
//							//System.out.println("predecessor state contains " + states.get(state_count-1).getStates().size() + " vars");
//							for(int i = 0; i < states.get(stateCount-1).getStates().size(); i++) {
//								if(!statesVars.contains(states.get(stateCount-1).getStates().get(i))) {
//									statesVars.add(new StateVariable(states.get(stateCount-1).getStates().get(i)));
//								}
//							}
//						}
//						State st = new State(statesVars);
//						states.add(st);
//						stateCount++;
//					}
//				} else{
//					line = f.readLine();
//					
//					/** accumulate the read lines in trace **/
//					trace += line + "\n";
//				}
//			}
//			f.close();
//			
//			/** accumulate the read lines in trace **/
//			//System.out.println(trace);
//			
//			//System.out.println("number of states in the trace= " + states.size()+"\n");
//			//System.out.println("number of states in the trace= " + state_count+"\n");
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//
//		if(states.size() == 0)
//		{
//			System.err.println("Error : Empty Trace Found\n");
//			return null;
//		}
//		//System.out.println("Trace built and returned \n");
//		return new Trace(states, trace);
//	}
	
	public  void bipTrace2File(String trace, String filePath) throws IOException
	{
		BufferedWriter writer;
		
		writer = new BufferedWriter(new FileWriter(filePath));
		writer.write(trace);
		writer.close();
	}


	public Trace transformBip2(String fname, ArrayList<String> formulaVars) {
		BufferedReader f = null;
		String line;
		boolean marker;
		int nbStateVars = 0;
		int vars_count = 0;
		int state_count = 0;

		//System.out.println("Start to build a trace \n");
		/** Instatiation d'un vecteur d'etats**/
		ArrayList<State> states = new ArrayList<State>();
		try {
			f = new BufferedReader(new FileReader(fname));

			// avancer jusqu'a la ligne d'initilisation
			do {
				line = f.readLine();
			} while(!line.startsWith("[BIP ENGINE]: initialize components"));

			//System.out.println(line.toString());

			// initialization step 
			marker = true;
			ArrayList<StateVariable> statesVarsInit = new ArrayList<StateVariable>();
			line = f.readLine();	// lire la ligne suivante
			while(line != null && marker)
			{
				if(line.startsWith("var"))
				{
					String[] tokens = line.split(" ");
					//System.out.println(line.toString());
					//System.out.println(tokens[1] + ", " + tokens[2] + ", " + tokens[3]);
					StateVariable var = new StateVariable(tokens[2], tokens[3], tokens[1]);
					if(formulaVars.contains(tokens[2]))
						statesVarsInit.add(var);
					line = f.readLine();
				}
				else if(line.startsWith("[BIP ENGINE]: -> choose")) {
					marker = false;
				}
				else
					line = f.readLine();
			}
			// Je sais que pour l'initialisation, j'aurais surement des valeurs pour chaque variable
			nbStateVars = statesVarsInit.size();
			//System.out.println("nb vars to consider = " + nbStateVars);
			State stInit = new State(statesVarsInit, 0.0);
			states.add(stInit);
			state_count++;
			//System.out.println(states.size());
			//System.out.println(states.get(0).toString());

			while(line != null)
			{
				if(line.startsWith("[BIP ENGINE]: -> choose"))
				{
					Double time = 0.0;
					if(line.contains("at global time"))
						time = getTimeValue(line);
					marker = true;
					vars_count=0;
					ArrayList<StateVariable> statesVars = new ArrayList<StateVariable>();
//					System.out.println("Find a new state, number of state vars = " + statesVars.size());
					line = f.readLine();
					while(line != null && marker)
					{
						if(line.startsWith("var"))
						{
							String[] tokens = line.split(" ");
							//System.out.println(line.toString());
							//System.out.println(tokens[1] + ", " + tokens[2] + ", " + tokens[3]);
							StateVariable var = new StateVariable(tokens[2], tokens[3], tokens[1]);
							if(formulaVars.contains(tokens[2]))
							{
								statesVars.add(var);
								vars_count++ ;
							}
							line = f.readLine();
						}
						else if(line.startsWith("[BIP ENGINE]: -> choose"))
							marker = false;
						else
							line = f.readLine();

					}
					
					if(vars_count > 0) 
					{
						if(vars_count < nbStateVars) {
//							System.out.println("----- nb vars in the current state = " + vars_count);
//							System.out.println("predecessor state contains " + states.get(state_count-1).getStateVariables().size() + " vars");
							for(int i = 0; i < states.get(state_count-1).getStateVariables().size(); i++) {
								if(!statesVars.contains(states.get(state_count-1).getStateVariables().get(i))) {
									statesVars.add(new StateVariable(states.get(state_count-1).getStateVariables().get(i)));
								}
							}
						}
						State st = new State(statesVars, time);
						states.add(st);
						state_count++;
					}
				} else
					line = f.readLine();
			}
			f.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if(states.size() == 0)
		{
			System.err.println("Error : Empty Trace Found\n");
			System.exit(-1);
		}
		//System.out.println("Trace built and returned \n");
		return new Trace(states);
	}


	public Trace extractTrace(String trace, ArrayList<String> formulaVars) {
		
		String[] ss = trace.split("\n");
		int index = 0;
		String line;
		boolean marker;
		int nbStateVars = 0;
		int vars_count = 0;
		int state_count = 0;

		//System.out.println("Start to build a trace \n");
		/** Instatiation d'un vecteur d'etats**/
		ArrayList<State> states = new ArrayList<State>();
						// avancer jusqu'a la ligne d'initilisation
			do {
			line = ss[index++];
			} while(!line.startsWith("[BIP ENGINE]: initialize components"));

			//System.out.println(line.toString());

			// initialization step 
			marker = true;
			ArrayList<StateVariable> statesVarsInit = new ArrayList<StateVariable>();
			line = ss[index++];	// lire la ligne suivante
			while(index < ss.length && marker)
			{
				if(line.startsWith("var"))
				{
					String[] tokens = line.split(" ");
					//System.out.println(line.toString());
					//System.out.println(tokens[1] + ", " + tokens[2] + ", " + tokens[3]);
					StateVariable var = new StateVariable(tokens[2], tokens[3], tokens[1]);
					if(formulaVars.contains(tokens[2]))
						statesVarsInit.add(var);
					line = ss[index++];
				}
				else if(line.startsWith("[BIP ENGINE]: -> choose")) {
					marker = false;
				}
				else
					line = ss[index++];
			}
			// Je sais que pour l'initialisation, j'aurais surement des valeurs pour chaque variable
			nbStateVars = statesVarsInit.size();
			//System.out.println("nb vars to consider = " + nbStateVars);
			State stInit = new State(statesVarsInit, 0.0);
			states.add(stInit);
			state_count++;
			//System.out.println(states.size());
			//System.out.println(states.get(0).toString());

			while(index < ss.length)
			{
				if(line.startsWith("[BIP ENGINE]: -> choose"))
				{
					Double time = getTimeValue(line);
					marker = true;
					vars_count=0;
					ArrayList<StateVariable> statesVars = new ArrayList<StateVariable>();
					//System.out.println("Find a new state, number of state vars = " + statesVars.size());
					line = ss[index++];
					while(index< ss.length && marker)
					{
						if(line.startsWith("var"))
						{
							String[] tokens = line.split(" ");
							//System.out.println(line.toString());
							//System.out.println(tokens[1] + ", " + tokens[2] + ", " + tokens[3]);
							StateVariable var = new StateVariable(tokens[2], tokens[3], tokens[1]);
							if(formulaVars.contains(tokens[2]))
								statesVars.add(var);
							vars_count++ ;
							line = ss[index++];
						}
						else if(line.startsWith("[BIP ENGINE]: -> choose"))
							marker = false;
						else
							line = ss[index++];

					}
					// si etat vide ==> pas de changement sur les variables observees
					//if(vars_count == 0) {
					//System.out.println("Empty state");
					//st = new State(states.get(state_count - 1));
					//} else {
					if(vars_count > 0) 
					{
						if(vars_count < nbStateVars) {
							//System.out.println("----- nb vars in the current state = " + vars_count);
							//System.out.println("predecessor state contains " + states.get(state_count-1).getStates().size() + " vars");
							for(int i = 0; i < states.get(state_count-1).getStateVariables().size(); i++) {
								if(!statesVars.contains(states.get(state_count-1).getStateVariables().get(i))) {
									statesVars.add(new StateVariable(states.get(state_count-1).getStateVariables().get(i)));
								}
							}
						}
						State st = new State(statesVars, time);
						states.add(st);
						state_count++;
					}
				} else
					line = ss[index++];
			}
			
		 

		if(states.size() == 0)
		{
			System.err.println("Error : Empty Trace Found\n");
			System.exit(-1);
		}
		//System.out.println("Trace built and returned \n");
		return new Trace(states);
	}
	
public Trace extractTrace(String trace) {
		
		String[] ss = trace.split("\n");
		int index = 0;
		String line;
		boolean marker;
		int nbStateVars = 0;
		int vars_count = 0;
		int state_count = 0;

		//System.out.println("Start to build a trace \n");
		/** Instatiation d'un vecteur d'etats**/
		ArrayList<State> states = new ArrayList<State>();
						// avancer jusqu'a la ligne d'initilisation
			do {
			line = ss[index++];
			} while(!line.startsWith("[BIP ENGINE]: initialize components"));

			//System.out.println(line.toString());

			// initialization step 
			marker = true;
			ArrayList<StateVariable> statesVarsInit = new ArrayList<StateVariable>();
			line = ss[index++];	// lire la ligne suivante
			while(index < ss.length && marker)
			{
				if(line.startsWith("var"))
				{
					String[] tokens = line.split(" ");
					//System.out.println(line.toString());
//					System.out.println(tokens[1] + ", " + tokens[2] + ", " + tokens[3]);
					StateVariable var = new StateVariable(tokens[2], tokens[3], tokens[1]);
					statesVarsInit.add(var);
					line = ss[index++];
				}
				else if(line.startsWith("[BIP ENGINE]: -> choose")) {
					marker = false;
				}
				else
					line = ss[index++];
			}
			// Je sais que pour l'initialisation, j'aurais surement des valeurs pour chaque variable
			nbStateVars = statesVarsInit.size();
			//System.out.println("nb vars to consider = " + nbStateVars);
			State stInit = new State(statesVarsInit, 0.0);
			states.add(stInit);
			state_count++;
			//System.out.println(states.size());
			//System.out.println(states.get(0).toString());

			while(index < ss.length)
			{
				if(line.startsWith("[BIP ENGINE]: -> choose"))
				{
					Double time = 0.0;
					if(line.contains("at global time"))
						time = getTimeValue(line);
					marker = true;
					vars_count=0;
					ArrayList<StateVariable> statesVars = new ArrayList<StateVariable>();
					//System.out.println("Find a new state, number of state vars = " + statesVars.size());
					line = ss[index++];
					while(index< ss.length && marker)
					{
						if(line.startsWith("var"))
						{
							String[] tokens = line.split(" ");
							//System.out.println(line.toString());
							//System.out.println(tokens[1] + ", " + tokens[2] + ", " + tokens[3]);
							StateVariable var = new StateVariable(tokens[2], tokens[3], tokens[1]);
							statesVars.add(var);
							vars_count++ ;
							line = ss[index++];
						}
						else if(line.startsWith("[BIP ENGINE]: -> choose"))
							marker = false;
						else
							line = ss[index++];

					}
					// si etat vide ==> pas de changement sur les variables observees
					//if(vars_count == 0) {
					//System.out.println("Empty state");
					//st = new State(states.get(state_count - 1));
					//} else {
					if(vars_count > 0) 
					{
						if(vars_count < nbStateVars) {
							//System.out.println("----- nb vars in the current state = " + vars_count);
							//System.out.println("predecessor state contains " + states.get(state_count-1).getStates().size() + " vars");
							for(int i = 0; i < states.get(state_count-1).getStateVariables().size(); i++) {
								if(!statesVars.contains(states.get(state_count-1).getStateVariables().get(i))) {
									statesVars.add(new StateVariable(states.get(state_count-1).getStateVariables().get(i)));
								}
							}
						}
						State st = new State(statesVars, time);
						states.add(st);
						state_count++;
					}
				} else
					line = ss[index++];
			}
			
		 

		if(states.size() == 0)
		{
			System.err.println("Error : Empty Trace Found\n");
			System.exit(-1);
		}
		//System.out.println("Trace built and returned \n");
		return new Trace(states);
	}


ArrayList<StateVariable> statesVarsMemory;
int nbStateVar = 0;

public State extractState(String trace, boolean initial) {
	
	String[] ss = trace.split("\n");
	int index = 0;
	String line;
	
	int vars_count = 0;

	/** Instatiation d'un vecteur d'etats**/
	if(initial)
	{
		statesVarsMemory = new ArrayList<StateVariable>();
		nbStateVar = 0;
		do {
			line = ss[index++];
		} while(!line.startsWith("[BIP ENGINE]: initialize components"));

		
		ArrayList<StateVariable> statesVarsInit = new ArrayList<StateVariable>();
		line = ss[index++];	// lire la ligne suivante
		while(index < ss.length)
		{
			if(line.startsWith("var"))
			{
				String[] tokens = line.split(" ");
				StateVariable var = new StateVariable(tokens[2], tokens[3], tokens[1]);
				if(!inMemory(var))
				{
					statesVarsInit.add(var);
					statesVarsMemory.add(new StateVariable(tokens[2], tokens[3], tokens[1]));
				}
				line = ss[index++];
			}
			else 
				line = ss[index++];
		}
		nbStateVar = statesVarsMemory.size();
		State stInit = new State(statesVarsInit, 0.0);
		return stInit;
	}
	else
	{
		line = ss[index++];
		if(line.startsWith("[BIP ENGINE]: -> choose"))
		{
//			System.out.println(line);
			Double time = 0.0;
			if(line.contains("at global time"))
				time = getTimeValue(line);
//			System.out.println(time);
			vars_count=0;
			ArrayList<StateVariable> statesVars = new ArrayList<StateVariable>();
			line = ss[index++];
			while(index< ss.length)
			{
				if(line.startsWith("var"))
				{
					String[] tokens = line.split(" ");
					StateVariable var = new StateVariable(tokens[2], tokens[3], tokens[1]);
//					System.out.println(var);
					statesVars.add(var);
					updateMemory(var);
//					for(int i = statesVarsMemory.size()-1; i >= 0; i--) {
//						if(statesVars.contains(statesVarsMemory.get(i))) {
//							
//						}					
//					}
					vars_count++ ;
				}
				
				line = ss[index++];
			}
			
			if(vars_count > 0) 
			{
				if(vars_count < nbStateVar) 
				{
//					System.out.println("Incomplete state");
					for(int i = 0; i < statesVarsMemory.size(); i++) 
					{
						if(!statesVars.contains(statesVarsMemory.get(i))) 
						{
//							System.out.println("Completing state");							
							statesVars.add(new StateVariable(statesVarsMemory.get(i)));
							vars_count ++;
						}
						
					}
				}
				if(vars_count < nbStateVar) {
					System.err.println("Error : state not completed");
					System.err.println("State variables");					
					for (StateVariable stateVariable : statesVars) {
						System.err.println(stateVariable);
					}
					System.err.println("Memory state variables");					
					for (StateVariable stateVariable : statesVarsMemory) {
						System.err.println(stateVariable);
					}
					System.exit(-1);
				}
				State st = new State(statesVars, time);
				return st;
				
			} 
			
		}
	}
		
	 

	return null;
}

private boolean inMemory(StateVariable var) {
	for (StateVariable stateVariable : statesVarsMemory) {
		if(stateVariable.getVariable().equals(var.getVariable())) {
			return true;
		}
	}
	return false;
}

private void updateMemory(StateVariable var) {
	for (StateVariable stateVariable : statesVarsMemory) {
		if(stateVariable.getVariable().equals(var.getVariable())) {
			stateVariable.setValue(var.getValue());
		}
	}
}

public Trace reconstituteTrace(String structured_trace) {
	return null;
}
	

}