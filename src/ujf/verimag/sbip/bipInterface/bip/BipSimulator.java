package ujf.verimag.sbip.bipInterface.bip;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import ujf.verimag.sbip.bipInterface.trace.State;
import ujf.verimag.sbip.bipInterface.trace.StateVariable;



/**
 * Java interface for the BIP engine
 * @author mediouni
 *
 */
public class BipSimulator implements BipBinding {

	private String executable_URL;
	private Process process = null;
	private HashMap<Integer, State> states_map;
	private OutputStream processCommandWriter = null;
	private InputStream processConsoleReader = null;
	private BufferedReader bufferedReader = null;
	private BufferedReader errbuffer = null;
	private State currentState = null;
	// Memorize the initialized variables to complete the intermediate states if the variables are not systematically printed  
	private ArrayList<StateVariable> stateVarsMemory;
	private int nbStateVars = 0;

	
	

	////////////////////////////////////////////////////////////////
	//////////////         Interface methods            ////////////
	////////////////////////////////////////////////////////////////
	@Override
	public boolean init(String path) {

		long seed = Math.round(Math.random()*(Integer.MAX_VALUE - 1 ) + 1);
		return init(path, "", seed);
	}

	@Override
	public boolean init(String path, long seed) {
		return init(path, "", seed);
	}

	@Override
	public boolean init(String path, String extra_parameters) {

		long seed = Math.round(Math.random()*(Integer.MAX_VALUE - 1 ) + 1);
		return init(path, extra_parameters, seed);
	}

	@Override
	public boolean init(String path, String extra_parameters, long seed) {
		stateVarsMemory = new ArrayList<StateVariable>();
		nbStateVars = 0;
		states_map = new HashMap<Integer, State>();
		StringBuilder builder = new StringBuilder();
		executable_URL = path;
		
		File modelFile = new File(path);
		String cmd = "./"+modelFile.getName()  +" --interactive --seed "+ seed + " " + extra_parameters;
		try {
			process = Runtime.getRuntime().exec(new String[]{"bash","-c",cmd}, null, modelFile.getParentFile());
			processCommandWriter = process.getOutputStream ();
			processConsoleReader = process.getInputStream() ;
			

		
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		if(process == null) return false;
		
		boolean initialized = false;
		
		try {
			bufferedReader = new BufferedReader(new InputStreamReader(processConsoleReader));
			errbuffer= new BufferedReader(new InputStreamReader(process.getErrorStream()));
			
	        String line;
	        // At the last iteration, the next line is consumed
	        // that line contains "[BIP ENGINE]: random scheduling based on seed=xxxxxx"
	        while ((line = bufferedReader.readLine()) != null && !initialized) { 
//				System.out.println(line);       
	        	// check if the initialization is correctly performed
	        	if(line.startsWith("[BIP ENGINE]: initialize components..."))
	        	{
	        		initialized = true;
	        	}
			}
	        if(initialized)
        		currentState = parseState(true);
	        
		}
		catch(IOException e){
			e.printStackTrace();
		}
			

		return initialized;
	}

	@Override
	public void setState(int state_index) {		
		executeCommand("l "+state_index);
		currentState = parseState(false);
		State savedState = new State(getState(state_index));
		savedState.setId(currentState.getId());
		currentState = savedState;
		stateVarsMemory = clone(currentState.getStateVariables());
		
	}
	

	@Override
	public int saveState(int state_index) {
		executeCommand("s");
		states_map.put(state_index, currentState);
		return state_index;
	}



	@Override
	public void step(int nb_steps) {
		executeCommand("r");
		currentState = parseState(false);
	}

	@Override
	public State getState(int index) {
		return states_map.get(index);
	}
	
	

	////////////////////////////////////////////////////////////////
	//////////////            Public methods            ////////////
	////////////////////////////////////////////////////////////////

	public int saveState() {
		return saveState(currentState.getId());
	}
	
	public State getCurrentState() {
		return this.currentState;
	}
	
	
	public void stopSimulation() {
		endProcess();
		System.err.println(getProcessErrors());
		process.destroy();
		try {
			processCommandWriter.close();
			processConsoleReader.close();
			bufferedReader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void delete(int id) {
		executeCommand("d "+id);
	}
	

	
	
	////////////////////////////////////////////////////////////////
	//////////////           Private methods            ////////////
	////////////////////////////////////////////////////////////////

	/**
	 * Parses the process inputStream and builds a representation of the current state
	 * @return current state of the engine
	 */
	
	private State parseState(boolean initial) {
//		System.out.println("parsing current state");
		ArrayList<StateVariable> stateVars = new ArrayList<StateVariable>();
		State state = new State();
		    String line;
		    boolean done = false;
		    int vars_count = 0;
		    boolean count_interactions = false;
	    	int nb_interactions = -1;
	    	int nb_counted_interactions = 0;
	        try {
				while (!done && (line = bufferedReader.readLine()) != null ) { 
//					System.out.println(line);   
					// Ignore the choose line
					if(!line.startsWith("[BIP ENGINE]: -> choose")) {
						// Parse variables and state id
						if(line.startsWith("var")) {
							String[] tokens = line.split(" ");
							StateVariable var = new StateVariable(tokens[2], tokens[3], tokens[1]);
							stateVars.add(var);
							vars_count++ ;
							if(initial) {
								stateVarsMemory.add(var);
								nbStateVars ++;
							}
							else {
								for(int i = stateVarsMemory.size()-1; i >= 0; i--) {
									if(stateVars.contains(stateVarsMemory.get(i))) {
										stateVarsMemory.remove(i) ;
										stateVarsMemory.add(new StateVariable(tokens[2], tokens[3], tokens[1]));
									}					
								}
							}
						} else {
							if(line.startsWith("[BIP ENGINE]: state ")) {
								String[] s = line.substring(line.indexOf("#")+1, line.length()).split(":");
//								System.out.println("ID of state : '" + s[0]+ "'");
//								System.out.println("Nb interactions : '" + s[1].split(" ")[1] + "'");
								nb_interactions = Integer.parseInt(s[1].split(" ")[1]);
								count_interactions = true;
								
								// Complete non-printed variables
								if(vars_count < nbStateVars) 
								{
									for(int i = 0; i < stateVarsMemory.size(); i++) 
									{
										if(!stateVars.contains(stateVarsMemory.get(i))) 
										{
											stateVars.add(new StateVariable(stateVarsMemory.get(i)));
										}
										
									}
								}
								state.setStateVariables(stateVars);
								state.setId(Integer.parseInt(s[0]));
								
							}
							else {
								if(count_interactions) {
									nb_counted_interactions ++;
									if(nb_counted_interactions == nb_interactions)
										done = true;
								}
							}
						}
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
	        
		
		return state;
	}
	
//	/**
//	 * Reads the inputStream and return the ID of the state that was just saved
//	 * @return the ID of the saved state
//	 */
//	private int parseID() {
//		// TODO Auto-generated method stub
//		return 0;
//	}
//	
	/**
	 * Execute the command cmd in the process
	 * @param cmd input parameters
	 * @return true if nothing went wrong, false otherwise
	 */
	private boolean executeCommand(String cmd) {
		cmd+="\n";
		byte [] bytes = cmd.getBytes ();
		try {
			processCommandWriter.write(bytes);
			processCommandWriter.flush();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	
	private boolean isProcessInitiated() {
		return (process != null);
	}
	
	/**
	 * check the exit code of a process
	 * @return
	 */
	private String getProcessErrors() {
		
		try {		
			
				System.out.println("Process exited with value: " + process.exitValue());
				String line = "", error = new String();
					
				try {
					while ((line = errbuffer.readLine()) != null ) {
						error+=line+"\n";
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
	
				return error;
			
			
		}
		catch(Exception e) {
			// Catch any exception
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Ends the simulation process
	 */
	private void endProcess() {
		executeCommand("q");
		try {
			process.waitFor();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Creates a clone of a list of variables
	 * @param stateVariables the list to clone
	 * @return a clone
	 */
	private ArrayList<StateVariable> clone(ArrayList<StateVariable> stateVariables) {
		ArrayList<StateVariable> clone = new ArrayList<StateVariable>();
		for (StateVariable var : stateVariables) {
			clone.add(new StateVariable(var));
		}
		return clone;
	}

	
	

}
