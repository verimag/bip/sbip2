package ujf.verimag.sbip.bipInterface.bip;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import ujf.verimag.sbip.bipInterface.bip.Utilities;
import ujf.verimag.sbip.bipInterface.trace.*;

public class ThreadedTraceGenerator implements Runnable{
	
	String URL = "";
	int ID = -1;
	int limit;
	boolean logvar = true;
	boolean saveTrace = true;
	String URL_traceRoot;
	volatile ArrayList< State> states = new ArrayList< State>();	
	private volatile boolean running = true;
	StringBuilder builder;
	StringBuilder traceToRaw ;
	int nb_consumed_states = 0;
	
	Process p;
	

	String raw = "";
	 
	public synchronized boolean isRunning(){
		 return running;
	}
	
	 
	public void run() {

		traceToRaw = new StringBuilder();
//		System.out.println("start generator");
		running = true;  
	    try {
	    	generate();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	    running = false;
	}
	 
	public synchronized State getModelState(int i ) {
		if(this.states != null)
		if(this.states.size()>i) 
			return this.states.get(i);
		return null;
	}

	
	public   synchronized  State consumeModelState() {
		 State st = getModelState(0);
		if(st!= null) {   
			this.states.remove(0);
		}
		return st;
	}
	
	private synchronized  void addState( State state){
		this.states.add(state);
	}


	public synchronized void arreter() {
		if(running){
			running = false;
			states = new ArrayList< State>();
			p.destroy();
//			System.err.println("Generator killed by monitor!");		
			
		}
	}
	
	public  synchronized int getStatesSize(){
		return this.states.size();
	}
	
	public ThreadedTraceGenerator(String url, int limit){
		this.URL = url;
		this.limit = limit;
		this.saveTrace = true;
	}

	public ThreadedTraceGenerator(String url, int limit, boolean logVariables){
		this.URL = url;
		this.limit = limit;
		this.logvar = logVariables;
		this.saveTrace = true;

	}
	
	public ThreadedTraceGenerator(String url, int limit, boolean logVariables, boolean saveTrace, String URL_traceRoot){
		this.URL = url;
		this.limit = limit;
		this.logvar = logVariables;
		this.saveTrace = saveTrace;
		this.URL_traceRoot = URL_traceRoot;

	}

	public void generate() throws IOException, InterruptedException{
		ID ++;
		generate(ID);
//		System.out.println("Sim time : "+(midTime));
//		System.out.println("Remaining symbols to check : "+states.size());
		

	}
	
	private void generate(int id) throws IOException, InterruptedException{

		states = new ArrayList<State>();
		nb_consumed_states = 0;
        Utilities u = new Utilities();
		builder = new StringBuilder();
		traceToRaw = new StringBuilder();
		long seed = Math.round(Math.random()*(Integer.MAX_VALUE - 1 ) + 1);
		
		File modelFile = new File(URL);
		String cmd = "./"+modelFile.getName() +" --seed "+ seed  + (logvar? " --log-variables" : "") ;
		
		
//		Process p = Runtime.getRuntime().exec();
		p = Runtime.getRuntime().exec(new String[]{"bash","-c",cmd}, null, modelFile.getParentFile());
		
		try (final BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
	        String line;
	        boolean initialization = true;
			while (running && (line = b.readLine()) != null) {
      	
//				System.out.println(line);
	        	if(line.startsWith("[BIP ENGINE]: -> choose")){
//	        		System.out.println("!!!!!!!!!!!!!!!!!   State detected ");
		        	State state = u.extractState(builder.toString(), initialization);
		        	if(initialization)	initialization = false;		 

		        	if(saveTrace) traceToRaw.append(builder.toString());
		        	builder = new StringBuilder();
		        	
		        	if(state != null){
//		        		System.out.println("added "+ state );
	        			this.addState(state);
	        		}
		        	
	           	}
	        	
	        	
	        	builder.append(line+ "\n");
	        	
	        }			
	        
//	         traiter le dernier state ici
	        if(running && !initialization)
	        {
	        	 State state = u.extractState(builder.toString(), initialization);
	        	if(state != null){
	        		this.addState(state);
	        		if(saveTrace) traceToRaw.append(builder.toString());
	        	}
	        }
			
	        
	    }
		catch(IOException e){
//			System.out.println("Closed stream : running is "+running);
			// It can happen , if running is false then no problem
			if(running) {
				System.err.println("Stream closed but thread still running!");
			}
		}
		
		try
		{
			p.waitFor();
		}		
		catch(InterruptedException e){
			System.out.println("Aborted trace generator!");
		}
		
		
		
//		System.out.println(p.exitValue());
		if(p.exitValue() != 143 && p.exitValue() != 141){
			raw = traceToRaw.toString();
			String line = "", error = new String();
			BufferedReader errbuffer= new BufferedReader(new InputStreamReader(p.getErrorStream()));
			
//			try{
				
			while ((line = errbuffer.readLine()) != null ) {
//				System.out.println(line);
				error+=line+"\n";
			}
//			}
//			catch(IOException e){
//				System.err.println("Stream is closed "+p.exitValue());
//			}

			JOptionPane.showMessageDialog(null, "Error : The trace generation process exited with wrong exit code : "+p.exitValue()+"\n"+error,"Trace generation failed", JOptionPane.ERROR_MESSAGE);
//			System.err.println("Error "+p.exitValue());  
//			System.exit(-1);
			
//			System.out.println(raw);
		}
		
		if(saveTrace){
//			System.out.println("trace saving "+this.ID);
			raw = traceToRaw.toString();
			traceToRaw = new StringBuilder();
			saveTrace(raw);
		}
		raw = null;	
//		System.out.println(raw);
		
		
	}
	
	public void saveTrace(String trace){
		PrintWriter out = null;		
		try {
			File f = new File(this.URL_traceRoot+"trace_"+ID+".t");
//			if(!f.getParentFile().exists())
//				f.getParentFile().mkdir();
			out = new PrintWriter(f);			
			out.print(trace);
			out.close();
			} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}


	
	
}