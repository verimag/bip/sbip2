package ujf.verimag.sbip.analysis;

import org.apache.commons.math3.distribution.BinomialDistribution;

import ujf.verimag.sbip.bipInterface.trace.Trace;

public class SST extends Algorithm {
	
	private double beta;
	SamplingPlan sp;
	
	@SuppressWarnings("unused")
	private double teta;
	@SuppressWarnings("unused")
	private OrderRelation relation;
	@SuppressWarnings("unused")
	private Hypothesis H0;
	@SuppressWarnings("unused")
	private Hypothesis H1;

	public SST(double teta, OrderRelation relation, double delta, double alpha, double beta) {
		super(teta,delta, alpha);
		this.beta = beta;
		this.sp = ComputeSingleSamplingPlan();
		this.teta = teta;
		this.relation = relation;
		this.H0 = new Hypothesis("H0", relation, bounds.getP0());
		this.H1 = new Hypothesis("H1", relation.getOpposite(),bounds.getP1());
		
//		System.out.println(sp);
	}
	
	public SamplingPlan ComputeSingleSamplingPlan()
	{	
//		System.out.println("P0: "+bounds.getP0());
//		System.out.println("P1: "+bounds.getP1());
		/***** Cas particuliers : 1er cas du tableau P22 thèse de Younes *****/
		if (bounds.getP1() == 0 && bounds.getP0() == 1) {
		    return new SamplingPlan(1, 0);
		}
		/***** Cas particuliers : 2eme cas du tableau P22 thèse de Younes *****/
		if (bounds.getP1() == 0) {
			System.out.println(">>" + Math.log(alpha) / Math.log(1 - bounds.getP0()));
		    return new SamplingPlan((long)(Math.ceil(Math.log(alpha) / Math.log(1 - bounds.getP0()))), 0);
		}
		/***** Cas particuliers : 3eme cas du tableau P22 thèse de Younes *****/
		if (bounds.getP0() == 1 && bounds.getP1()>0) {
			long n = (long)((Math.ceil(Math.log(beta) / Math.log(bounds.getP1()))));
//			System.out.println("N is :  "+n);
		    return new SamplingPlan(n, n - 1);
		}

		/***** Cas générale *****/
		int n = 0;
		int c0 = -1;
		int c1 = 0;
		BinomialDistribution bdist; 
		while (c0 < c1) {
			++n;
			bdist = new BinomialDistribution(null, n, 1 - bounds.getP0());
			c0 = n - bdist.inverseCumulativeProbability(1 - alpha) - 1;
			bdist = new BinomialDistribution(null, n, bounds.getP1());
			c1 = bdist.inverseCumulativeProbability(1 - beta);
//			System.out.println(n + "\t" + c0 + "\t" + c1);
		}
		return new SamplingPlan(n, (c0 + c1) / 2);
	}
	
	

	
	public boolean runStep(Trace trace, boolean t_verdict) {
		this.addTrace(trace, t_verdict);

		Object[] params = new Object[2];
		params[0] = sp.getC();
		params[1] = sp.getN();
		conclude(params);

		
		return concluded;
	}

	
	public void conclude(Object[] params) {
		long c = (long) params[0];
		long n = (long) params[1];
//		System.out.println("C: "+c+" N: "+n);
		long sum1 = nb_traces_true + n;
		long sum2 = sum1 - nb_traces; 

		if(sum1 < 0 && sum2 > 0)
			sum2 = Long.MIN_VALUE;
		
		if ((nb_traces_true <= c) && (((sum2)) > c)){
			concluded = false;
		}
		else{
			if(nb_traces_true > c){
//				System.out.println("SSP : H0 is true");
				verdict = true;
				concluded = true;
			}
			else{
//				System.out.println("SSP : H1 is true");
				verdict = false;
				concluded = true;
			}
		}
		

	}

}
