package ujf.verimag.sbip.analysis;

import ujf.verimag.sbip.bipInterface.trace.Trace;

public class PEstimation extends Algorithm {

	private int N;
	public PEstimation(double delta, double alpha) {
		super(delta, alpha);
//		From ayoub's tool
//		N =(int)Math.ceil(4*Math.log(2/alpha)/(Math.pow(this.bounds.getDelta(), 2))); 
		
//		From Plasma tool
		N =(int)Math.ceil(Math.log(2/alpha)/(2*Math.pow(this.bounds.getDelta(), 2))); 
		
	}

	@Override
	public boolean runStep(Trace trace, boolean t_verdict) {
		addTrace(trace, t_verdict);
		conclude(new Object[0]);
		
		return concluded;
	}

	protected void conclude(Object[] params) {
		if(N == nb_traces) {
			verdict = (nb_traces_true+0.0)/(N+0.0);
			concluded = true;
		}
		
	}

	public int getN(){
		return N;
	}

}
