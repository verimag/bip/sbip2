package ujf.verimag.sbip.analysis;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.apache.commons.math3.distribution.NormalDistribution;


/**
 * Confidence interval for the estimated probability of a property using Importance Splitting
 * @author mediouni
 *
 */
public class IPConfidenceInterval {
	private double lower_bound;
	private double upper_bound;
	
	public IPConfidenceInterval(double estimate, double alpha, double variance, int nb_traces) {
		
		double percentile = 1 - alpha/2;
		
		NormalDistribution NDist = new NormalDistribution(0,1);
		double critical_value = NDist.inverseCumulativeProbability(percentile);
		

		lower_bound = estimate / (1 + (critical_value * Math.sqrt(variance / nb_traces)));
		upper_bound = estimate / (1 - (critical_value * Math.sqrt(variance / nb_traces)));
		
	}

	public IPConfidenceInterval() {
		this.lower_bound = 0.0;
		this.upper_bound = 0.0;
	}
	
	public String toString() {
		return "["+ round(lower_bound, 10)+","+round(upper_bound,10)+"]";
		
	}

	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();
	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
}
