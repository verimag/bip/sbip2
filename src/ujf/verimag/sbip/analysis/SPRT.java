package ujf.verimag.sbip.analysis;



import ujf.verimag.sbip.bipInterface.trace.Trace;
import ujf.verimag.sbip.parser.formula.Interval;

public class SPRT extends Algorithm {

	@SuppressWarnings("unused")
	private double beta;
	private SST ssp;
	private double fm;
	private double A;
	private double B;

	@SuppressWarnings("unused")
	private double teta;
	@SuppressWarnings("unused")
	private OrderRelation relation;
	@SuppressWarnings("unused")
	private Hypothesis H1;
	@SuppressWarnings("unused")
	private Hypothesis H0;

	public SPRT(double teta, OrderRelation relation, double delta, double alpha, double beta) {
		super(teta, delta, alpha);
		this.beta = beta;
		this.teta = teta;
		this.relation = relation;
		ssp = new SST(teta, relation, delta, alpha, beta);
		fm = 0;
//		System.out.println(beta + " ***** " +alpha);
		// Stabilisation des doubles
		double val1 = Interval.round((1-beta)/alpha, 6);
		double val2 = Interval.round(beta/(1-alpha), 6);
		
		A = Math.log(val1);
		B = Math.log(val2);
		
		H0 = new Hypothesis("H0", relation, bounds.getP0());
		H1 = new Hypothesis("H1", relation.getOpposite(), bounds.getP1());
		
//		System.out.println(A+ " ==== " + B);
	}


	public boolean runStep(Trace trace, boolean t_verdict) {
		this.addTrace(trace, t_verdict);
		if(bounds.getP0() == 1 || (bounds.getP1() == 0)){
//			System.out.println("Using SSP");
			concluded = ssp.runStep(trace, t_verdict);
			verdict = ssp.getVerdict();
			return concluded;
		}
		
		double xm;
		
//		System.out.println("runStep");
		
		// What to do if A==B==0
		if (fm > B && fm < A)
		{	
			//Can be commented because the check is done in the calling method
//			if(trace != null)
			{
				xm = (((boolean)t_verdict)? 1:0);
				fm += xm*(Math.log(bounds.getP1()/bounds.getP0()))
						+(1-xm)*(Math.log((1-bounds.getP1())/(1-bounds.getP0())));				
//				System.out.println("fm == "+fm);
				
			}
//			else
//			{
//				System.err.println("\nERROR : when executing the system to verify");
//			}
		}

//		System.out.println("try to conclude");
		conclude(new Object[0]);
//		System.out.println("gonna return");
		return concluded;
		
	}

	
	protected void conclude(Object[] params) {
		
		if(fm <= B)
		{
//			System.out.println("H0 is true");
			verdict = true;
			concluded = true;
		}
		
		if(fm >= A)
		{
//			System.out.println("H1 is true");
			verdict = false;
			concluded = true;
		}
	}


	

}
