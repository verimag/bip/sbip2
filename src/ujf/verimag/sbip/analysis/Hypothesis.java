package ujf.verimag.sbip.analysis;

import java.text.DecimalFormat;

public class Hypothesis {

	private String name;
	private OrderRelation order;
	private double  threshold;
	
	public Hypothesis(String name, OrderRelation order, double threshold) {
		super();
		this.name = name;
		this.order = order;
		this.threshold = threshold;
	}
	
	public Hypothesis()
	{
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public OrderRelation getOrder() {
		return order;
	}

	public void setOrder(OrderRelation order) {
		this.order = order;
	}

	public double getThreshold() {
		return threshold;
	}

	public void setThreshold(double threshold) {
		this.threshold = threshold;
	}
	
	public boolean equal(Hypothesis h)
	{
		if(this.name.equalsIgnoreCase(h.getName()) && this.order.equals(h.getOrder())  && this.threshold == h.getThreshold())
			return true;
		else
			return false;
	}

	@Override
	public String toString() {
		DecimalFormat df = new DecimalFormat("0.##########");
		return this.name +" : p "+ this.order + " " +df.format((double)this.threshold);
	}
	
	
	
	
}
