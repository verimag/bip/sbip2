package ujf.verimag.sbip.analysis;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import ujf.verimag.sbip.bipInterface.bip.ThreadedTraceGenerator;
import ujf.verimag.sbip.bipInterface.bip.TraceGenerator;
import ujf.verimag.sbip.bipInterface.trace.StateVariable;
import ujf.verimag.sbip.bipInterface.trace.Trace;
import ujf.verimag.sbip.gui.TabbedPane;
import ujf.verimag.sbip.gui.dialogs.ProgressionViewer;
import ujf.verimag.sbip.monitor.mtl.MtlMonitor;
import ujf.verimag.sbip.parser.formula.Interval;
import ujf.verimag.sbip.parser.formula.MtlFormula;

public class MtlSimulator implements Simulator {
	
	private String project;
	private String model;
	private MtlFormula formula;
	private boolean log_vars ;
	private boolean save_traces ;
	@SuppressWarnings("unused")
	private int indexEngine;
	private String sim_type;
	private int limit ;
	private String smc_type;
	private double alpha;
	private double beta;
	private double delta;
	private double teta;
	private OrderRelation relation;
	
	private Object verdict;
	private boolean aborted = false;
	private int traces_number;
	private long executiontime;
	private long overallExecutiontime = 0;
	
	private ProgressionViewer pviewer;
	private TabbedPane tabbedpane;
	private StateVariable paramVar = null;
	
	public StateVariable getParamVar() {
		return paramVar;
	}


	private ArrayList<TraceVerdict> data;	
	private ArrayList<SmcVerdict> parametricVerdicts;
		
	public MtlSimulator(String selectedProject, String selectedModel, MtlFormula mtlFormula, boolean logVars,
			boolean saveTraces, int indexEngine, int indexSimulationMode, int limit, int indexSmcType, double alpha,
			double beta, double delta, double teta, OrderRelation relation, ProgressionViewer pviewer, TabbedPane tp) {

			this.project = selectedProject;
			this.model = selectedModel;
			this.formula = mtlFormula;
			this.log_vars = logVars;
			this.save_traces = saveTraces;
			this.indexEngine = indexEngine;
			this.sim_type = (indexSimulationMode == 0? "trace" : "symbol");
			this.limit = limit;
			this.smc_type = ((indexSmcType == 1)? "Hypothesis testing" : "Probability estimation");
			this.alpha = alpha;
			this.beta = beta;
			this.delta = delta;
			this.teta = teta;
			this.relation = relation;
			this.pviewer = pviewer;
			this.verdict = new Object();
			this.parametricVerdicts = new ArrayList<SmcVerdict>();
			this.tabbedpane = tp;
			this.paramVar = null;
			
	}

	public MtlSimulator(String selectedProject, String selectedModel, MtlFormula mtlFormula, boolean logVars,
			boolean saveTraces, int indexEngine, int indexSimulationMode, int limit, int indexSmcType, double alpha,
			double beta, double delta, double teta, OrderRelation relation, ProgressionViewer pviewer, StateVariable paramVar, TabbedPane tp) {

		this.traces_number = 0;
		this.project = selectedProject;
		this.model = selectedModel;
		this.formula = mtlFormula;
		this.log_vars = logVars;
		this.save_traces = saveTraces;
		this.indexEngine = indexEngine;
		this.sim_type = (indexSimulationMode == 0? "trace" : "symbol");
		this.limit = limit;
		this.smc_type = (indexSmcType == 1? "Hypothesis testing" : "Probability estimation");
		this.alpha = alpha;
		this.beta = beta;
		this.delta = delta;
		this.teta = teta;
		this.relation = relation;
		this.pviewer = pviewer;
		this.verdict = new Object();
		this.paramVar = paramVar;
		this.parametricVerdicts = new ArrayList<SmcVerdict>();
		this.tabbedpane = tp;
		
}
	
	public ArrayList<TraceVerdict> runSimulation(String paramValue){
		
		
		  paramVar.setValue(paramValue);
		  pviewer.updateLog("\n\nSimulating with param value : "+ paramValue);
		  Long startTime = System.currentTimeMillis();
		  

			try {
				if(sim_type.equals("trace"))
					data = getSimData(isSave_traces()) ;
				else
					data = getThreadedSimData(isSave_traces()) ;
			} catch (IOException | InterruptedException e1) {
				e1.printStackTrace();
			}	
			
//			traces_number = data.size();
			executiontime = System.currentTimeMillis() - startTime;
			
			
			//For parametric ...
			overallExecutiontime += executiontime;
			parametricVerdicts.add(new SmcVerdict(paramVar.getType(), paramValue, data.size(), getPercentageTracesEvaluatedTo(true), executiontime, verdict));
			return data;
	}

	public double getPercentageTracesEvaluatedTo(Boolean b){
		int cpt = 0;
		for (int i = 0; i < data.size(); i++) {
			if(data.get(i).getEvaluation() == 1) cpt ++;
		}
		
		Double d = Interval.round((cpt + 0.0)/ (data.size() + 0.0) * 100, 1);
		return d;
	}
	
	public ArrayList<TraceVerdict> runSimulation(){
		  Long startTime = System.currentTimeMillis();
			
			

			try {
				if(sim_type.equals("trace"))
					data = getSimData(isSave_traces()) ;
				else
					data = getThreadedSimData(isSave_traces()) ;
			} catch (IOException | InterruptedException e1) {
				e1.printStackTrace();
			}	
			
//			traces_number = data.size();
			executiontime = System.currentTimeMillis() - startTime;
		
			return data;
	}
	
	private ArrayList<TraceVerdict> getSimData(boolean save_traces) throws IOException, InterruptedException {
		ArrayList<TraceVerdict> data = new ArrayList<TraceVerdict>();
		
		
		TraceGenerator generator = new TraceGenerator(model, 
				limit,
				log_vars,
				save_traces, 
				"./Workspace/"+project+"/Outputs/");
		MtlMonitor monitor = (paramVar == null? new MtlMonitor(this) : new MtlMonitor(paramVar, this));
		Algorithm smc = (this.smc_type.equals("Hypothesis testing")?
							new SPRT(this.teta, this.relation, this.delta, this.alpha, this.beta):
							new PEstimation(this.delta, this.alpha));
		
		if(smc instanceof PEstimation){
			pviewer.activateProgressBar(((PEstimation) smc).getN()); 
		}
		while (!smc.hasConcluded() && !aborted) {
			long startTime = System.currentTimeMillis();
			Trace trace = generator.generate();
//			System.out.println(trace.getStates().size());
			pviewer.updateLog("\tTrace_"+traces_number+" has been generated .");
			
			
			MtlFormula f= (paramVar == null? formula : formula.instantiateInterval(paramVar));
//			System.out.println("Instantiated f : ");
			int m_verdict = monitor.check(f	, trace);
			
			String s_verdict ="True";
			if(m_verdict == 0) s_verdict = "False";
			else if(m_verdict == 2) s_verdict = "Not able to conclude";
			
			pviewer.updateLog("\t -> Trace_"+traces_number+" has been evaluated to "+ s_verdict+".\n");
			
			boolean verdict = (m_verdict == 1? true : false);
			smc.runStep(trace, verdict);
//			System.out.println("<><> has concluded? " + smc.hasConcluded());
			long endTime = System.currentTimeMillis() - startTime;
			if(save_traces){
				File traceFile = new File("./Workspace/"+project+"/Outputs/trace_"+traces_number+".t");
				tabbedpane.insertNodeInTree(traceFile.getAbsolutePath());
				
				data.add(new TraceVerdict(null, monitor.getTraceLength(), "trace_"+traces_number,
						"./Workspace/"+project+"/Outputs/trace_"+traces_number+".t", endTime,
						m_verdict));
			}
			else{
//				System.out.println("add");
				data.add(new TraceVerdict(trace, monitor.getTraceLength(), "trace_"+traces_number, 
						"not saved", endTime,
						m_verdict));
//				System.out.println("added");
						
			}
			traces_number++;
			if(paramVar==null) 	pviewer.incrementProgressBar();
		}
		verdict = smc.getVerdict();
		pviewer.updateLog("\nSMC solver concluded : "
				+ verdict
				);
		pviewer.updateLog("\n");
		return data;
	}
	
	@SuppressWarnings("deprecation")
	private ArrayList<TraceVerdict> getThreadedSimData(boolean save_traces) throws IOException, InterruptedException {
		ArrayList<TraceVerdict> data = new ArrayList<TraceVerdict>();
		
			
		ThreadedTraceGenerator generator = new ThreadedTraceGenerator(model, 
				limit,
				log_vars,
				save_traces, 
				"./Workspace/"+project+"/Outputs/");
		
		Algorithm smc = (this.smc_type.equals("Hypothesis testing")?
				new SPRT(this.teta, this.relation, this.delta, this.alpha, this.beta):
				new PEstimation(this.delta, this.alpha));
		
		if(smc instanceof PEstimation){
			pviewer.activateProgressBar(((PEstimation) smc).getN()); 
		}
		
		
		//Empty trace
		Trace trace = new Trace();
		
		while(!smc.hasConcluded() && !aborted){
			long startTime = System.currentTimeMillis();
			MtlFormula f = (paramVar == null)? this.formula : this.formula.instantiateInterval(paramVar);
			MtlMonitor monitor = ((paramVar == null)? 
					new MtlMonitor(generator, f, this) : 
					new MtlMonitor(generator, f, paramVar, this));

//			System.out.println("!!!! again here formula is "+formula);
			

			Thread thread_m = new Thread(monitor);
			Thread thread_tg = new Thread(generator);
			
			thread_tg.start();
			while(!thread_tg.isAlive() || !generator.isRunning()){
//				System.out.print("W.");
			}
//			System.out.println();
			thread_m.start();
			
			
			
			try {
				thread_m.join();
				thread_tg.join();
//				thread_tg.stop();
				
			} catch (InterruptedException e) {
				System.err.println("aborted");
				generator.arreter();
			} finally {
				thread_m.stop();
				thread_tg.stop();
			}

//			System.out.println("getVerdict");
			int m_verdict = monitor.getVerdict();
			boolean verdict = (m_verdict == 1? true : false);

//			System.out.println("SMC step to do");
			smc.runStep(trace, verdict);
//			System.out.println("SMC step done");
			long endTime = System.currentTimeMillis() - startTime;
			if(save_traces){
				File traceFile = new File("./Workspace/"+project+"/Outputs/trace_"+traces_number+".t");
				tabbedpane.insertNodeInTree(traceFile.getAbsolutePath());
				data.add(new TraceVerdict(null, monitor.getTraceLength(), "trace_"+traces_number, "./Workspace/"
						+project+"/Outputs/trace_"+traces_number+".t", endTime, m_verdict));
				
			}
			else{
				data.add(new TraceVerdict(null, monitor.getTraceLength(), "trace_"+traces_number, "not saved", endTime, m_verdict));
			}
			String s_verdict = "";
			if(m_verdict == 0) s_verdict = "False";
			else if(m_verdict == 1) s_verdict = "True";
			else if(m_verdict == 2) s_verdict = "Not able to conclude";

//			System.out.println("Verdict : "+m_verdict);
			pviewer.updateLog("\tTrace_"+traces_number+" generated and evaluated to "+ s_verdict+".");
			traces_number++;
			if(paramVar==null) pviewer.incrementProgressBar();
		}
		
		
		verdict = smc.getVerdict();

		pviewer.updateLog("\nSMC solver concluded : " + verdict	);
		pviewer.updateLog("\n");
		return data;
	}
	
	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public boolean isLog_vars() {
		return log_vars;
	}

	public void setLog_vars(boolean log_vars) {
		this.log_vars = log_vars;
	}

	public boolean isSave_traces() {
		return save_traces;
	}

	public void setSave_traces(boolean save_traces) {
		this.save_traces = save_traces;
	}

	public String getSmc_type() {
		return smc_type;
	}

	public void setSmc_type(String smc_type) {
		this.smc_type = smc_type;
	}

	public double getAlpha() {
		return alpha;
	}

	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}

	public double getBeta() {
		return beta;
	}

	public void setBeta(double beta) {
		this.beta = beta;
	}

	public double getDelta() {
		return delta;
	}

	public void setDelta(double delta) {
		this.delta = delta;
	}


	public void setExecutiontime(long executiontime) {
		this.executiontime = executiontime;
	}

	public ArrayList<TraceVerdict> getData() {
		return data;
	}

	public void setData(ArrayList<TraceVerdict> data) {
		this.data = data;
	}
	
	public int getTraces_number(){
		return traces_number;
	}

	public void clearData() {
		data.clear();
	}

	public String getSimType() {
		return this.sim_type;
	}

	public Object getVerdict() {
		return verdict;
	}

	public ArrayList<SmcVerdict> getSmcData() {
		return parametricVerdicts;
	}
	
	public void abort(){
		System.out.println("abort mission!!");
		this.aborted = true;
	}

	public boolean isAborted() {
		return aborted;
	}
	
	public OrderRelation getRelation(){
		return relation;
	}
	
	public double getTeta(){
		return teta;
	}
	

	public String getExecutiontime() {
		
		return convertTime(executiontime);
		
	}
	
	public String getOverallExecutiontime() {
		
		return convertTime(overallExecutiontime);
		
	}
	
	private String convertTime(long time){
		int milliseconds = (int) (time  % 1000);
		int seconds = (int) (time  / 1000) % 60 ;
		int minutes = (int) ((time  / (1000*60)) % 60);
		int hours = (int) (time / (1000*60*60)) ;
		String dateFormatted  = String.format("%02d:%02d:%02d:%03d",hours,minutes,seconds,milliseconds);
		return dateFormatted ;
	}
	

}
