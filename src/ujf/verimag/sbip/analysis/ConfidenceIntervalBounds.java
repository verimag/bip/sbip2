package ujf.verimag.sbip.analysis;

import javax.swing.JOptionPane;

public class ConfidenceIntervalBounds {
	
	private double delta;
	private double p0;
	private double p1;
	
	public ConfidenceIntervalBounds(double teta, double delta)
	{			
			this.delta = delta;
			this.p0 = teta + this.delta;
			this.p1 = teta - this.delta;;
//			System.out.println(p0 + " aaaaaaaaaaaaaaa "+p1);
			
			if(p0 > 1 || p0 < 0 || p1 > 1 || p1 < 0)
			{
				System.out.println("Error : Confidence interval bounds are incorrects (>1 OR <0)");
				JOptionPane.showConfirmDialog(null, "Error : Confidence interval ("+p1+","+p0+") bounds are incorrects. \n Values should not be >1 OR <0." , "Error",  JOptionPane.CANCEL_OPTION);
			}
	}
	
	public ConfidenceIntervalBounds(double delta)
	{							
		
		if(delta <= 1 && delta >= 0)
		{
			this.delta = delta;
//			System.out.println(p0 + " aaaaaaaaaaaaaaa "+p1);
		}
		else
		{
			System.out.println("Error : delta should be in [0 1]");
			JOptionPane.showConfirmDialog(null, "Error : delta should be in [0 1]" , "Error",  JOptionPane.CANCEL_OPTION);
			
		}
	}
	
	
	public double getDelta() {
		return delta;
	}


	public void setDelta(double delta) {
		this.delta = delta;
	}


	public double getP0() {
		return p0;
	}
	public void setP0(double p0) {
		this.p0 = p0;
	}
	public double getP1() {
		return p1;
	}
	public void setP1(double p1) {
		this.p1 = p1;
	}
}
