package ujf.verimag.sbip.analysis;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import ujf.verimag.sbip.bipInterface.bip.ThreadedTraceGenerator;
import ujf.verimag.sbip.bipInterface.bip.TraceGenerator;
import ujf.verimag.sbip.bipInterface.trace.StateVariable;
import ujf.verimag.sbip.bipInterface.trace.Trace;
import ujf.verimag.sbip.gui.TabbedPane;
import ujf.verimag.sbip.gui.dialogs.ProgressionViewer;
import ujf.verimag.sbip.monitor.mtl.MtlMonitor;
import ujf.verimag.sbip.parser.formula.Formula;
import ujf.verimag.sbip.parser.formula.Interval;
import ujf.verimag.sbip.parser.formula.MtlFormula;

public interface Simulator {

	
	public StateVariable getParamVar() ;
	
	public ArrayList<TraceVerdict> runSimulation(String paramValue);

	public double getPercentageTracesEvaluatedTo(Boolean b);
	
	public ArrayList<TraceVerdict> runSimulation();

	public int getLimit();

	public void setLimit(int limit);

	public boolean isLog_vars() ;

	public void setLog_vars(boolean log_vars);

	public boolean isSave_traces() ;

	public void setSave_traces(boolean save_traces) ;

	public String getSmc_type() ;

	public void setSmc_type(String smc_type) ;

	public double getAlpha() ;

	public void setAlpha(double alpha) ;

	public double getBeta() ;

	public void setBeta(double beta) ;

	public double getDelta() ;

	public void setDelta(double delta) ;

	public void setExecutiontime(long executiontime) ;

	public ArrayList<TraceVerdict> getData() ;

	public void setData(ArrayList<TraceVerdict> data) ;
	
	public int getTraces_number();

	public void clearData() ;

	public String getSimType() ;

	public Object getVerdict() ;

	public ArrayList<SmcVerdict> getSmcData() ;
	
	public void abort();

	public boolean isAborted() ;
	
	public OrderRelation getRelation();

	public double getTeta();

	public String getExecutiontime() ;

	public String getOverallExecutiontime() ;
	
	

}
