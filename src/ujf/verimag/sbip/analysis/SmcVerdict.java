package ujf.verimag.sbip.analysis;

public class SmcVerdict {

	private String varType;
	private String paramValue;
	private int nbTraces;
	private double true_percentage;
	private long processTime;
	private Object verdict;
	
	
	public SmcVerdict(String varType, String paramValue, int nbTraces, double true_percentage, long processTime,
			Object verdict) {
		super();
		this.varType = varType;
		this.paramValue = paramValue;
		this.nbTraces = nbTraces;
		this.true_percentage = true_percentage;
		this.processTime = processTime;
		this.verdict = verdict;
	}

	public Class<?> getVarType(){
		if(varType == "int") return Integer.class;
		else return Boolean.class;
	}
	
	public String getParamValue(){
		return paramValue;
	}
	
	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public int getNbTraces() {
		return nbTraces;
	}

	public void setNbTraces(int nbTraces) {
		this.nbTraces = nbTraces;
	}

	public long getProcessTime() {
		return processTime;
	}

	public void setProcessTime(long processTime) {
		this.processTime = processTime;
	}

	public Object getVerdict() {
		return verdict;
	}

	public void setVerdict(Object verdict) {
		this.verdict = verdict;
	}

	public void setVarType(String varType) {
		this.varType = varType;
	}

	public double getTrue_percentage() {
		return true_percentage;
	}

	public void setTrue_percentage(double true_percentage) {
		this.true_percentage = true_percentage;
	}

}
