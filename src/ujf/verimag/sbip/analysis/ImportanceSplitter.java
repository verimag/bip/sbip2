package ujf.verimag.sbip.analysis;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import ujf.verimag.sbip.bipInterface.bip.BipSimulator;
import ujf.verimag.sbip.gui.dialogs.ProgressionViewer;
import ujf.verimag.sbip.gui.tabs.PanelRareEvent;
import ujf.verimag.sbip.monitor.ScoreFunction;

/**
 * SMC algorithm for rare properties
 * @author mediouni
 *
 */
public class ImportanceSplitter {

	private int traceNumber = 0;
	private double alpha = 0.0;
	private double estimate = 1.0;
	private double variance = 0.0;
	private int stop_condition = 10;
	private IPConfidenceInterval confidenceInterval;
	private ProgressionViewer pviewer;
	private boolean isRunning;
	
	/**
	 * Constructor
	 * @param traceNumber number of traces
	 * @param alpha SMC parameter
	 * @param stop_condition max nb states to reach the next level
	 */
	public ImportanceSplitter(int traceNumber, double alpha, int stop_condition, ProgressionViewer pviewer) {
		super();
		this.traceNumber = traceNumber;
		this.alpha = alpha;
		this.estimate = 1.0;
		this.variance = 0.0;
		this.stop_condition = stop_condition;
		this.confidenceInterval = new IPConfidenceInterval();
		this.pviewer = pviewer;
		this.isRunning = false;
	}
	
	////////////////////////////
	////////  Getters //////////
	////////////////////////////

	public IPConfidenceInterval getConfidenceInterval() {
		return this.confidenceInterval;
	}
	
	public double getEstimate() {
		return this.estimate;
	}
	
	public double getVariance() {
		return variance;
	}
	

	////////////////////////////
	////////  Methods //////////
	////////////////////////////
	
	/**
	 * Launch the rare event smc algorithm
	 * @param bip_system the URL of the bip system
	 * @param score the scoring function
	 * @return the probability estimation
	 */
	public double runFixedLevelAlgorithm(String bip_system, ScoreFunction score) {
		this.isRunning = true;
				
		ArrayList<Integer> states_id = new ArrayList<Integer>();
		this.estimate = 1.0;
		this.variance = 0.0;
		BipSimulator simulator = new BipSimulator();
		simulator.init(bip_system);
		simulator.saveState();
		boolean initial = true;
		
		for (int k = 1; k <= score.getNbLevels() && this.isRunning; k++) {
			
			// Generate 'traceNumber' traces
			// Only save the states IDs that reach the next level
			if(initial) {
				if(score.evaluate(simulator.getCurrentState()) == -1) {

					JOptionPane.showMessageDialog(null, "Importance splitting failed : Unadequate score function!", "Simulation error", JOptionPane.ERROR_MESSAGE);
					return -1;
				}
				initial = false;
				
				// repeat traceNumber times
				for (int i = 0; i < traceNumber && this.isRunning; i++) {
					// Set the simulator state to 0
					simulator.setState(0);
				    // go further until reaching the level 1 or reach limit
					int nb_steps = 0;
					while(score.evaluate(simulator.getCurrentState())<k && nb_steps < stop_condition && score.evaluate(simulator.getCurrentState())!=-1 && this.isRunning) {
						simulator.step(1);
						nb_steps ++;
					}
					
					if(score.evaluate(simulator.getCurrentState()) == -1) {

						System.err.println("Unadequate score function!");
						return -1;
					}
					
					if(score.evaluate(simulator.getCurrentState()) >= k) {
						int id = simulator.saveState();
						states_id.add(id);
					}
				}
			}
			else {
				ArrayList<Integer> new_states_id = new ArrayList<Integer>();
				// Progress in traces
				for (int i = 0; i < states_id.size()&& this.isRunning; i++) {
					// Set the simulator to a frontier point
					simulator.setState(states_id.get(i));
					// go further until reaching the level k or reach limit
					int nb_steps = 0;
					while(score.evaluate(simulator.getCurrentState())<k && nb_steps < stop_condition && score.evaluate(simulator.getCurrentState())!=-1 && this.isRunning) {
						simulator.step(0);
						nb_steps ++;
					}

					if(score.evaluate(simulator.getCurrentState()) == -1) {

						System.err.println("Unadequate score function!");
						return -1;
					}
					
					if(score.evaluate(simulator.getCurrentState()) >= k) {
						int id = simulator.saveState();
						new_states_id.add(id);
					}
				}
				
				// Remove old states if not in new list
				for (int i = 0; i < states_id.size() && this.isRunning; i++) {
					if(!new_states_id.contains(states_id.get(i))) {
						simulator.delete(states_id.get(i));
					}
				}
				
				// Update the list
				states_id = new_states_id;
				
			}
			
			// If no trace is found then return 0
			if(states_id.size() == 0) {
				this.estimate = 0.0;
				return this.estimate;				
			}
						
			// Update estimate
			double p = (states_id.size() + 0.0) / traceNumber;
//			System.out.println(p);

			 pviewer.updateLog("\n\tProbability to reach level ("+ k +") being at level ("+(k-1)+")  : "+ p);
			 pviewer.incrementProgressBar();
			 
			 this.variance += (1-p) / p;
			this.estimate *= p;
			
			// Randomly complete the states_id to 
			states_id = complete(states_id);
			
			
		}
		
		if(!this.isRunning) {
			return -1;
		}
		
		if(states_id.size() == 0) {
			this.estimate = 0.0;
			return this.estimate;				
		}
		
		this.confidenceInterval = new IPConfidenceInterval(estimate, alpha, variance, traceNumber);
		return this.estimate;
	}

	/**
	 * Uniformly randomly complete the list of BIP states reaching the considered level
	 * @param states_id
	 * @return a list of states of size 'traceNumber'
	 */
	private ArrayList<Integer> complete(ArrayList<Integer> states_id) {
		ArrayList<Integer> randomly_selected_states = new ArrayList<Integer>();
		
		for(int i = 0; i < (traceNumber - states_id.size()) ; i++) {
			int x = (int) Math.floor(Math.random() * states_id.size());
			randomly_selected_states.add(states_id.get(x));
		}
		states_id.addAll(randomly_selected_states);
		if(states_id.size() != traceNumber)
			System.err.println("Error in the number of traces!");
		return states_id;
		
	}

	public void abort() {
		isRunning = false;
		
	}

	
	
	
}
