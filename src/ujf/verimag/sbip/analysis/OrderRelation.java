package ujf.verimag.sbip.analysis;

public enum OrderRelation {

	Gr,		/** >  **/ 
	GrE, 	/** >= **/
	Lw, 	/** <  **/
	LwE, 	/** <= **/
	Eq,		/** == **/
	Estim;	/** =? **/
	
	public OrderRelation getOpposite()
	{
		if(this.name().equalsIgnoreCase("Gr"))
			return Lw;
		else
		{
			if(this.name().equalsIgnoreCase("Lw"))
				return Gr;
			else
			{
				if(this.name().equalsIgnoreCase("GrE"))
					return LwE;
				else
				{
					if(this.name().equalsIgnoreCase("LwE"))
						return GrE;
					else
					{
						if(this.name().equalsIgnoreCase("Eq"))
							return Eq;
						else
						{
							if(this.name().equalsIgnoreCase("Estim"))
								return Estim;
							else
							{
								System.out.println("problem in OrderRelation : Unknown Relation !!");
								System.exit(-1);
								return Eq;
							}
						}						
					}	
				}
			}
		}
		
	}
	
	public String toString()
	{
		if(this.name().equalsIgnoreCase("Gr"))
			return ">";
		if(this.name().equalsIgnoreCase("GrE"))
			return ">=";
		if(this.name().equalsIgnoreCase("Lw"))
			return "<";
		if(this.name().equalsIgnoreCase("LwE"))
			return "<=";
		if(this.name().equalsIgnoreCase("Eq"))
			return "==";
		if(this.name().equalsIgnoreCase("Estim"))
			return "=?";
		else
			return "";
		
	}
	
	
	public static OrderRelation fromString(String s){
		
		switch (s) {
		case ">":
			return Gr;
		case ">=":
			return GrE;
		case "<":
			return Lw;
		case "<=":
			return LwE;
		case "==":
			return Eq;
		case "=?":
			return Estim;

		default:
			return Estim;
		}
	}
		
}
