package ujf.verimag.sbip.analysis;

import ujf.verimag.sbip.bipInterface.trace.Trace;

public class TraceVerdict {
	Trace trace;
	int size;
	String name;
	String URL;
	long time;
	int Evaluation;

	
	
	
	public TraceVerdict(Trace trace, int size, String name , String uRL, long time, int evaluation) {
		super();
		this.trace = trace;
		this.size = size;
		this.name = name;
		this.URL = uRL;
		this.time = time;
		this.Evaluation = evaluation;
	}
	
	
	public Trace getTrace() {
		return trace;
	}
	public void setTrace(Trace trace) {
		this.trace = trace;
	}
	public String getURL() {
		return URL;
	}
	public void setURL(String uRL) {
		URL = uRL;
	}
	public String getName() {
		return name;
	}
	public void setName(String _name) {
		name = _name;
	}
	public int getEvaluation() {
		return Evaluation;
	}
	public void setEvaluation(int evaluation) {
		Evaluation = evaluation;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	
}
