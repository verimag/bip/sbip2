package ujf.verimag.sbip.analysis;

public class SamplingPlan {

	private long n;
	private long c;
	
	public SamplingPlan() {
		this.n = 0;
		this.c = 0;
	}
	
	public SamplingPlan(long n, long c) {
		this.n = n;
		this.c = c;
	}
	public long getN() {
		return n;
	}
	public void setN(long n) {
		this.n = n;
	}
	public long getC() {
		return c;
	}
	public void setC(long c) {
		this.c = c;
	}

	
	public String toString() {
		return "SamplingPlan [n=" + n + ", c=" + c + "]";
	}
}
