package ujf.verimag.sbip.analysis;

import ujf.verimag.sbip.bipInterface.trace.Trace;

public abstract class Algorithm {

	protected ConfidenceIntervalBounds bounds;
	protected double alpha;
	protected int nb_traces = 0;
	protected int nb_traces_true = 0;
	protected boolean concluded = false;
	protected Object verdict = null;
	
	public Algorithm(double delta, double alpha){
		this.bounds = new ConfidenceIntervalBounds( delta);
		this.alpha = alpha;
		concluded = false;
		
	}
	public Algorithm(double teta, double delta, double alpha){
		this.bounds = new ConfidenceIntervalBounds(teta, delta);
		this.alpha = alpha;
		concluded = false;
		
	}
	public void addTrace(Trace trace, boolean t_verdict){
//		if(trace != null)
		{
			nb_traces ++;
			if(t_verdict) nb_traces_true ++;
		}		
	}
	public Object getVerdict() {
		return verdict;
	}
	public abstract boolean runStep(Trace trace, boolean t_verdict);
	protected abstract void conclude(Object[] params);
	public boolean hasConcluded(){
		return concluded;
	}
	public String getStats() {
		return "Verdict : "+verdict+".\nNb traces : " + nb_traces+".\nNb true traces : " + nb_traces_true+".";
	}
	
	
}
