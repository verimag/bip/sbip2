package ujf.verimag.sbip;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;


import ujf.verimag.sbip.analysis.Algorithm;
import ujf.verimag.sbip.analysis.ImportanceSplitter;
import ujf.verimag.sbip.analysis.PEstimation;
import ujf.verimag.sbip.bipInterface.bip.BipSimulator;
import ujf.verimag.sbip.bipInterface.bip.ThreadedTraceGenerator;
import ujf.verimag.sbip.bipInterface.bip.TraceGenerator;
import ujf.verimag.sbip.bipInterface.bip.Utilities;
import ujf.verimag.sbip.bipInterface.trace.State;
import ujf.verimag.sbip.bipInterface.trace.StateVariable;
import ujf.verimag.sbip.bipInterface.trace.Trace;
import ujf.verimag.sbip.gui.model.Score_Formula;
import ujf.verimag.sbip.monitor.ScoreFunction;
import ujf.verimag.sbip.monitor.ltl.LtlMonitor;
import ujf.verimag.sbip.monitor.mtl.Characterizer;
import ujf.verimag.sbip.monitor.mtl.Filter;
import ujf.verimag.sbip.monitor.mtl.MtlMonitor;
import ujf.verimag.sbip.parser.formula.LtlFormula;
import ujf.verimag.sbip.parser.formula.MtlFormula;
import ujf.verimag.sbip.parser.ltl.LtlExprParser;
import ujf.verimag.sbip.parser.mtl.MtlExprParser;

public class TestFormulaModel {

	public static String readFile(String path, Charset encoding) 
	{		  
			  byte[] encoded;
			try {
				encoded = Files.readAllBytes(Paths.get(path));
				  return new String(encoded, encoding);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
	}
	
	public static void main(String[] args) {
		
		
		long startTime = System.currentTimeMillis();


		testTraceParser();
//		testSimulator();
//		testImportanceSplitting();


		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		System.out.println("Overall time : "+totalTime);
		
		
	}
	
	public static void testTraceParser(){
	

		Utilities u = new Utilities();
		Trace trace = u.transformBip2("trace_1000.t");

		System.out.println(trace);
	}
	
//	public static void test18(){
//		/** Building a test trace */
//		ArrayList<StateVariable> sv0 = new ArrayList<>();
//		sv0.add(new StateVariable("a", "true", "bool"));
//		sv0.add(new StateVariable("b", "false", "bool"));
//		sv0.add(new StateVariable("c", "false", "bool"));
//		State s0 = new State(sv0, 0.0);
//		
//		ArrayList<StateVariable> sv1 = new ArrayList<>();
//		sv1.add(new StateVariable("a", "true", "bool"));
//		sv1.add(new StateVariable("b", "false", "bool"));
//		sv1.add(new StateVariable("c", "false", "bool"));
//		State s1 = new State(sv1, 1.0);
//		
//		ArrayList<StateVariable> sv2 = new ArrayList<>();
//		sv2.add(new StateVariable("a", "false", "bool"));
//		sv2.add(new StateVariable("b", "true", "bool"));
//		sv2.add(new StateVariable("c", "false", "bool"));
//		//sv2.add(new StateVariable("b", "10", "int"));
//		State s2 = new State(sv2, 5.0);
//		
//		ArrayList<StateVariable> sv3 = new ArrayList<>();
//		sv3.add(new StateVariable("a", "true", "bool"));
//		sv3.add(new StateVariable("b", "false", "bool"));
//		sv3.add(new StateVariable("c", "false", "bool"));
//		State s3 = new State(sv3, 6.0);
//		
//		ArrayList<StateVariable> sv4 = new ArrayList<>();
//		sv4.add(new StateVariable("a", "false", "bool"));
//		sv4.add(new StateVariable("b", "false", "bool"));
//		sv4.add(new StateVariable("c", "true", "bool"));
//		State s4 = new State(sv4, 7.0);
//		
//		ArrayList<StateVariable> sv5 = new ArrayList<>();
//		sv5.add(new StateVariable("a", "false", "bool"));
//		sv5.add(new StateVariable("b", "false", "bool"));
//		sv5.add(new StateVariable("c", "true", "bool"));
//		State s5 = new State(sv5, 8.0);
//		
//		ArrayList<State> states = new ArrayList<>();
//		states.add(s0);
//		states.add(s1);
//		states.add(s2);
//		states.add(s3);
//		states.add(s4);
//		states.add(s5);
//		
//		Trace t = new Trace(states);
//		//System.out.print(t.toString());
//		
//		MtlExprParser parser = new MtlExprParser();
//		MtlFormula phi = parser.parse("(a || b) U[0,30] c");		
//		//System.out.println("Formula () : "+ phi);
//		
//		MtlMonitor monitor =  new MtlMonitor(null);
//		int m_verdict = monitor.check(phi, t);
//		
//		String s_verdict = "";
//		if(m_verdict == 0) s_verdict = "False";
//		else if(m_verdict == 1) s_verdict = "True";
//		else if(m_verdict == 2) s_verdict = "Not able to conclude";
//	
//    	System.out.println("Verdict : " + s_verdict);
//		
//		
//	}
	
	private static void testImportanceSplitting() {
		// Parameters
		// URL of the bip system
		String url = "./system";
		double alpha = 0.1;
		// Number of traces
		int N = 1000;
		// Nb max of step to reach the next level
		int stop_condition = 10;
		
		
		String s = "./testsc.sf";
		if(s.endsWith(".sf")){
			File file = new File(s);
			FileInputStream in;
			try {
				in = new FileInputStream(file);
				 @SuppressWarnings("resource")
				ObjectInputStream ois = new ObjectInputStream(in);
        		ScoreFunction score = (ScoreFunction) ois.readObject();
        		ImportanceSplitter algo = new ImportanceSplitter(N, alpha, stop_condition, null);
        		
        		// Run the test and print results out
        		System.out.println("Probability estimation : " + algo.runFixedLevelAlgorithm(url, score) );
        		System.out.println(algo.getConfidenceInterval());
        		
    
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		  
    	}
		
		
	}
	
	private static void testSimulator() {
		BipSimulator simulator = new BipSimulator();
		String url = "./system";
		System.out.println("The file '"+url+ ((new File(url)).exists() ? "' exists!": " does not exist!") );
		
		Boolean initialized = simulator.init(url);
		if(initialized) {
			simulator.saveState();
			System.out.println("(saved)Parsed state " + simulator.getCurrentState());
			simulator.step(0);
			System.out.println("Parsed state " + simulator.getCurrentState());
			simulator.step(0);
			System.out.println("Parsed state " + simulator.getCurrentState());
			simulator.step(0);
			System.out.println("(saved) Parsed state " + simulator.getCurrentState());
			simulator.saveState();
			simulator.step(0);
			System.out.println("Parsed state " + simulator.getCurrentState());
			simulator.setState(0);
			System.out.println("Loaded state " + simulator.getCurrentState());
			simulator.step(0);
			System.out.println("Parsed state " + simulator.getCurrentState());
			simulator.step(0);
			System.out.println("Parsed state " + simulator.getCurrentState());
			simulator.setState(3);
			System.out.println("Loaded state " + simulator.getCurrentState());
			simulator.setState(0);
			System.out.println("Loaded state " + simulator.getCurrentState());			
			simulator.stopSimulation();	
		}
		
	}
	
	public static void testSF() {
		String s = "./testsc.sf";
		if(s.endsWith(".sf")){
			File file = new File(s);
			FileInputStream in;
			try {
				in = new FileInputStream(file);
				 @SuppressWarnings("resource")
				ObjectInputStream ois = new ObjectInputStream(in);
        		ScoreFunction sf = (ScoreFunction) ois.readObject();
        		ArrayList<StateVariable> sv0 = new ArrayList<>();					
        		sv0.add(new StateVariable("a", "16", "int"));
        		sv0.add(new StateVariable("b", "20", "int"));
        		sv0.add(new StateVariable("c", "16", "int"));
        		State s0 = new State(sv0);
        		
        		System.out.println(sf.evaluate(s0));
        		
    
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		  
    	}
	}
	
	public static void test18(){
		/** Building a test trace */
		ArrayList<StateVariable> sv0 = new ArrayList<>();
		sv0.add(new StateVariable("a", "true", "bool"));
		sv0.add(new StateVariable("b", "false", "bool"));
		sv0.add(new StateVariable("c", "false", "bool"));
		State s0 = new State(sv0, 0.0);
		
		ArrayList<StateVariable> sv1 = new ArrayList<>();
		sv1.add(new StateVariable("a", "true", "bool"));
		sv1.add(new StateVariable("b", "false", "bool"));
		sv1.add(new StateVariable("c", "false", "bool"));
		State s1 = new State(sv1, 1.0);
		
		ArrayList<StateVariable> sv2 = new ArrayList<>();
		sv2.add(new StateVariable("a", "false", "bool"));
		sv2.add(new StateVariable("b", "true", "bool"));
		sv2.add(new StateVariable("c", "false", "bool"));
		//sv2.add(new StateVariable("b", "10", "int"));
		State s2 = new State(sv2, 5.0);
		
		ArrayList<StateVariable> sv3 = new ArrayList<>();
		sv3.add(new StateVariable("a", "true", "bool"));
		sv3.add(new StateVariable("b", "false", "bool"));
		sv3.add(new StateVariable("c", "false", "bool"));
		State s3 = new State(sv3, 6.0);
		
		ArrayList<StateVariable> sv4 = new ArrayList<>();
		sv4.add(new StateVariable("a", "false", "bool"));
		sv4.add(new StateVariable("b", "false", "bool"));
		sv4.add(new StateVariable("c", "true", "bool"));
		State s4 = new State(sv4, 7.0);
		
		ArrayList<StateVariable> sv5 = new ArrayList<>();
		sv5.add(new StateVariable("a", "false", "bool"));
		sv5.add(new StateVariable("b", "false", "bool"));
		sv5.add(new StateVariable("c", "true", "bool"));
		State s5 = new State(sv5, 8.0);
		
		ArrayList<State> states = new ArrayList<>();
		states.add(s0);
		states.add(s1);
		states.add(s2);
		states.add(s3);
		states.add(s4);
		states.add(s5);
		
		Trace t = new Trace(states);
		//System.out.print(t.toString());
		
		MtlExprParser parser = new MtlExprParser();
		MtlFormula phi = parser.parse("(a || b) U[0,30] c");		
		//System.out.println("Formula () : "+ phi);
		
		MtlMonitor monitor =  new MtlMonitor(null);
		int m_verdict = monitor.check(phi, t);
		
		String s_verdict = "";
		if(m_verdict == 0) s_verdict = "False";
		else if(m_verdict == 1) s_verdict = "True";
		else if(m_verdict == 2) s_verdict = "Not able to conclude";
	
    	System.out.println("Verdict : " + s_verdict);
		
		
	}

	
	public static void test17() {
		ArrayList<StateVariable> sv0 = new ArrayList<>();
		sv0.add(new StateVariable("a", "10", "int"));
		sv0.add(new StateVariable("b", "20", "int"));
		State s0 = new State(sv0);
		
		ArrayList<StateVariable> sv1 = new ArrayList<>();
		State s1 = new State(sv1);
		
		ArrayList<StateVariable> sv2 = new ArrayList<>();
		sv2.add(new StateVariable("a", "105", "int"));
		//sv2.add(new StateVariable("b", "10", "int"));
		State s2 = new State(sv2);
		
		ArrayList<StateVariable> sv3 = new ArrayList<>();
		sv3.add(new StateVariable("b", "3", "int"));
		State s3 = new State(sv3);
		
		ArrayList<State> states = new ArrayList<>();
		states.add(s0);
		states.add(s1);
		states.add(s2);
		states.add(s3);
		
		Trace t = new Trace(states);
		System.out.println(t.toBip());
		
		Utilities u = new Utilities();
		boolean initial = true;
		
		for (State state : states) {
//			System.out.println(state.toBip(initial));
			System.out.println(u.extractState( state.toBip(initial), initial));
			initial = false;
		}
	}
	
	
	public static void test16(){
		
		
		LtlExprParser parser = new LtlExprParser();
		LtlFormula phi = parser.parse("P=? [G{500} (abs( Master.tm - Slave.ts)<15)]");		
		System.out.println("Formula () : "+ phi);
		
		String url = "./Workspace/Untimed_PTP/Models/system";
		ThreadedTraceGenerator generator =new ThreadedTraceGenerator(url, 
				100,
				false,
				true, 
				"./traces/");
		
		LtlMonitor monitor =  new LtlMonitor(generator, phi);

//		System.out.println("and again here formula is "+phi);
		

		Thread thread_m = new Thread(monitor);
		Thread thread_tg = new Thread(generator);
		
		thread_tg.start();
		while(!thread_tg.isAlive() || !generator.isRunning()){
			System.out.print("W.");
		}
		System.out.println();
		thread_m.start();
		
		
		
		try {
			thread_m.join();
			thread_tg.join();
		} catch (InterruptedException e) {
			System.err.println("aborted");
		} finally {
			thread_m.stop();
			thread_tg.stop();
		}
		
		int m_verdict = monitor.getVerdict();
		boolean verdict = (m_verdict == 1? true : false);
		System.out.println("Verdict : "+verdict);
		
		
	
	}
	
	public static void test15() {
		Utilities u = new Utilities();
		double d = u.getTimeValue("time 1us97ns");
		System.out.println(d);
	}

	public static void test14() {
		Utilities u = new Utilities();
		String content = readFile("./Workspace/Untimed_PTP/Outputs/trace_4.t", StandardCharsets.UTF_8);
		Trace t = u.extractTrace(content);
		System.out.println(t);
	}
	
	public static void test13(){
		
		
		try {
			String url = "./Workspace/BIP2_test/Models/system";
			url = "./Workspace/Firewire_3/Models/system";
			TraceGenerator tg = new TraceGenerator(url, 100, true, true, "./");
			Trace t =tg.generate();
			t.toString();
//			System.out.println(t);
			
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}	
		
	}
	
	@SuppressWarnings("deprecation")
	public static void test12(){
		MtlExprParser parser = new MtlExprParser();
		MtlFormula phi = parser.parse("F[0,1000] ( node1.s == 7 )"); 
		System.out.println("Formula () : "+ phi);


		
		ThreadedTraceGenerator generator = new ThreadedTraceGenerator("./Workspace/Firewire_2/Models/system", 
				1000,
				true,
				true, 
				"./Workspace/Firewire_2/Outputs/");
		
		Algorithm smc = new PEstimation(0.3, 0.4);		
		
	
		
		
		while(!smc.hasConcluded()){
			MtlMonitor monitor =  new MtlMonitor(generator, phi, null);
//			System.out.println("loop");
//			System.out.println("and again here formula is "+phi);
			

			Thread thread_m = new Thread(monitor);
			Thread thread_tg = new Thread(generator);
			
			thread_tg.start();
			while(!thread_tg.isAlive() || !generator.isRunning()){
//				System.out.print("W.");
			}
//			System.out.println();
			thread_m.start();
			
			
			
			try {
				thread_m.join();
//				System.out.println("Done");
			} catch (InterruptedException e) {
				System.err.println("aborted");
			} finally {
				thread_m.stop();
				if(thread_tg.isAlive()) thread_tg.stop();
			}
			
			int m_verdict = monitor.getVerdict();
//			System.out.println("Verdict : "+m_verdict);
			boolean verdict = (m_verdict == 1? true : false);

			smc.runStep(new Trace(), verdict);
			
			@SuppressWarnings("unused")
			String s_verdict = "";
			if(m_verdict == 0) s_verdict = "False";
			else if(m_verdict == 1) s_verdict = "True";
			else if(m_verdict == 2) s_verdict = "Not able to conclude";
			
		
			
		}		
		
		System.out.println("Verdict : " + smc.getVerdict());
	}

	
	public static void test11(){
		String text = "// comment ";
		System.out.println(text.matches("(\\s)*//(.)*"));
	}
	
	public static void test10(){
		
		
		MtlExprParser parser = new MtlExprParser();
		MtlFormula phi = parser.parse("F[0.0, 300.0] (((node1.s==7.0) && (node2.s==8.0)) || ((node1.s==8.0) && (node2.s==7.0)))");
//		MtlFormula phi = parser.parse("G[0.0,15000] ( abs(master.tm_val - slave.ts_val)< 120 || (!master.synchro && !slave.synchro) )");  System.out.println("Formula () : "+ phi);
		
		System.out.println("Formula () : "+ phi);
//		String url = "/home/mediouni/BIP-RT/Test/PTP/system";
		String url = "./Workspace/Projet_firewire_2/Models/system";
		TraceGenerator generator =new TraceGenerator(url, 
				20,
				true,
				true, 
				"./traces/");

		try {
			Trace t = generator.generate();
			System.out.println(t);

			MtlMonitor monitor = new MtlMonitor(  null);
			int verdict = monitor.check(phi, t);
			System.out.println("Verdict : "+ verdict);
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		
	
	}
	
	@SuppressWarnings("deprecation")
	public static void test9(){
		MtlExprParser parser = new MtlExprParser();
		MtlFormula phi = parser.parse("G[0.0,1500000] ( abs(master.tm_val - slave.ts_val)< 10 || (!master.synchro && !slave.synchro) )"); 
		System.out.println("Formula () : "+ phi);


		
		ThreadedTraceGenerator generator = new ThreadedTraceGenerator("./Workspace/Projet_ptp_customD/Models/system", 
				1000,
				true,
				true, 
				"./traces/");
		
		Algorithm smc = new PEstimation(0.5, 0.5);		
		
		//Empty trace
		Trace trace = new Trace();
		boolean problem = false;
		
		while(!smc.hasConcluded() && !problem){
			MtlMonitor monitor =  new MtlMonitor(generator, phi, null);

			System.out.println("and again here formula is "+phi);
			

			Thread thread_m = new Thread(monitor);
			Thread thread_tg = new Thread(generator);
			
			thread_tg.start();
			while(!thread_tg.isAlive() || !generator.isRunning()){
				System.out.print("W.");
			}
			System.out.println();
			thread_m.start();
			
			
			
			try {
				thread_m.join();
//				thread_tg.join();
			} catch (InterruptedException e) {
				System.err.println("aborted");
			} finally {
				thread_m.stop();
				thread_tg.stop();
			}
			
			int m_verdict = monitor.getVerdict();
			boolean verdict = (m_verdict == 1? true : false);

			smc.runStep(trace, verdict);
			
			@SuppressWarnings("unused")
			String s_verdict = "";
			if(m_verdict == 0) s_verdict = "False";
			else if(m_verdict == 1) s_verdict = "True";
			else if(m_verdict == 2) s_verdict = "Not able to conclude";
			
			if(monitor.getTraceLength() == 1) {
				problem = true;
				System.err.println("trace monitored length 1!");
			}
			
		}		
		
		System.out.println("Verdict : " + smc.getVerdict());
	}

	public static void test7(){

		Utilities u = new Utilities();
		State s;
		String trace;
		trace = "[BIP ENGINE]: BIP Engine (version 2015.04-RC7 )\n"
				+"[BIP ENGINE]:\n"
				+"[BIP ENGINE]: initialize components...\n"
				+"[BIP ENGINE]: random scheduling based on seed=152430920\n"
				+"\n"
				+"var bool a true\n"
				+"var bool b false\n"
				+"var bool c false\n"
				+"[BIP ENGINE]: state #0: 1 interaction:\n"
				+"[BIP ENGINE]:   [0] ROOT.TimeClose: time.time(delta=00;)\n";
		s = u.extractState( trace, true);
		System.out.println(s);
		
		
		
		
		
		trace = "[BIP ENGINE]: -> choose [0] ROOT.TimeClose: time.time(delta=00;) 1ns\n"
				+"var bool a false\n"
				+"var bool c true\n"
				+"[BIP ENGINE]: state #0: 1 interaction:\n"
				+"[BIP ENGINE]:   [0] ROOT.TimeClose: time.time(delta=00;)\n";
		s = u.extractState( trace, false);
		System.out.println(s);
		
		trace = "[BIP ENGINE]: -> choose [0] ROOT.TimeClose: time.time(delta=00;) 10ns\n"
				+"var bool a true\n"
				+"[BIP ENGINE]: state #0: 1 interaction:\n"
				+"[BIP ENGINE]:   [0] ROOT.TimeClose: time.time(delta=00;)\n";
		s = u.extractState( trace, false);
		System.out.println(s);
		
		trace = "[BIP ENGINE]: -> choose [0] ROOT.TimeClose: time.time(delta=00;) 10ns\n"
				+"[BIP ENGINE]: state #0: 1 interaction:\n"
				+"[BIP ENGINE]:   [0] ROOT.TimeClose: time.time(delta=00;)\n";
		s = u.extractState( trace, false);
		System.out.println(s);
		
		trace = "[BIP ENGINE]: -> choose [0] ROOT.TimeClose: time.time(delta=00;) 10ns\n"
				+"var bool a false\n"
				+"[BIP ENGINE]: state #0: 1 interaction:\n"
				+"[BIP ENGINE]:   [0] ROOT.TimeClose: time.time(delta=00;)\n";
		s = u.extractState( trace, false);
		System.out.println(s);
		
		
	}

	public static void test8(){
		
		
		MtlExprParser parser = new MtlExprParser();
		MtlFormula phi = parser.parse("F[0,200] ( sender.rec =1 )");  System.out.println("Formula () : "+ phi);
			
		String url = "./Workspace/Bluetooth/Models/system";
		ThreadedTraceGenerator tg = new ThreadedTraceGenerator(url,0);
		MtlMonitor m = new MtlMonitor(tg , phi, null);

		Thread thread_m = new Thread(m);
		Thread thread_tg = new Thread(tg);

		thread_m.start();
		thread_tg.start();
		
		
		try {
			thread_m.join();
			System.out.println("thread monitor joins");
			thread_tg.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
		
	}
	
	public static void test6(){
		// Disable log variable option in generate()
			
			try {
				MtlExprParser parser = new MtlExprParser();
				MtlFormula phi = parser.parse("G[0.0,150000] ( abs(master.tm_val - slave.ts_val)< 120 )");  System.out.println("Formula () : "+ phi);
			
				String url = "/home/mediouni/BIP-RT/Test/PTP/system";
				TraceGenerator tg = new TraceGenerator(url,200000);
				Trace t =tg.generate(phi.getDataVariablesList());
//				System.out.println(t);			
				long  startMonitor = System.currentTimeMillis();
				MtlMonitor m = new MtlMonitor( null);
				m.check(phi, t);
				System.out.println("Monitoring time : "+ (System.currentTimeMillis()- startMonitor));
				
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
			}	
			
		}
	
	public static void test5(){
		
//		try {
			long startTime = System.currentTimeMillis();
			
			MtlExprParser parser = new MtlExprParser();
			MtlFormula phi = parser.parse("G[0, 500] ((! (cmd_received)) || ((F[1, 100] (cmd_received)) || (F[110, 200] (is_timeout))))");  System.out.println("Formula () : "+ phi);
			long totalTime = System.currentTimeMillis() - startTime;
			System.out.println("MTL parsing time : "+totalTime);
			
//			String url = "/home/mediouni/BIP-RT/Test/PTP/system";
//			TraceGenerator tg = new TraceGenerator(url,200000);
			Utilities u = new Utilities();
			Trace t = u.transformBip2("/home/mediouni/Desktop/tool-sbip-2.2.3/SMC_Solver/Workspace/ESROCOS-Phase_3/Outputs/trace_0.t");
//					tg.generate(phi.getDataVariablesList());
			long  startMonitor = System.currentTimeMillis();
			MtlMonitor m = new MtlMonitor( null);
			System.out.println("verdict: " + m.check(phi, t));
			System.out.println("Monitoring time : "+ (System.currentTimeMillis()- startMonitor));
			
//		} 
//		catch (IOException | InterruptedException e) {
//			e.printStackTrace();
//		}	
		
	}
	
	public static void test4(){
		
		MtlExprParser parser = new MtlExprParser();
		MtlFormula phi = parser.parse("G[0,10]a || F[1,10] b && (c || d) U[1,10] e"); 
		
		@SuppressWarnings("unused")
		StateVariable var = new StateVariable("x", "7", "int");
		
		
		System.out.println(phi);
//		MtlFormula phi2 = phi.instantiateInterval(var);
////		((TemporalIntervalOperator) phi.getOperator()).getInterval().instantiate(var);
//		System.out.println(phi);
//		System.out.println(phi2);
//		ArrayList<String> vars = phi.getDataVariablesList();
//		Utilities u = new Utilities();
//		Trace t = u.transformBip2("trace.txt", vars);
//		System.out.println(t);
		
	}
	
	public static void test3(){
		// F[0.0, x] (((node1.s==7.0) && (node2.s==8.0)) || ((node1.s==8.0) && (node2.s==7.0)))
		try {
			String url = "/home/mediouni/BIP-RT/Test/PTP/system";
			TraceGenerator tg = new TraceGenerator(url, 100);
			Trace t =tg.generate();
			StateVariable var = new StateVariable("x", "150000000", "int");
			
			MtlExprParser parser = new MtlExprParser();
			MtlFormula phi = parser.parse("G[0.0,x] ( abs(master.tm_val - slave.ts_val)< 120 || (!master.synchro && !slave.synchro) )");  
			System.out.println("Formula () : "+ phi);
			
			MtlMonitor m = new MtlMonitor( null);
			System.out.println(phi.instantiateInterval(var));
			m.check(phi.instantiateInterval(var), t);
			System.err.println();
			
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}	
		
	}
	
	public static void test1(){
		
		MtlExprParser parser = new MtlExprParser();
		Filter f = new Filter();


		MtlFormula phi = parser.parse("(F[1,3](a)||exp(x+5)<3)R[0.0,5.0] (b && c)");

		MtlFormula filtered_phi = f.replaceEventually(f.replaceGlobally(phi));
		
//		ArrayList<String> vars = filtered_phi.getDataVariablesList();
//		Trace trace = u.transformBip2("trace.txt", vars);

		System.out.println(phi);
		System.out.println(filtered_phi);
//		System.out.println(trace);
//		
//		
//		int verdict = m.check(phi, trace);
	}
	
	public static void test2(){
			
			Characterizer C = new Characterizer();
			MtlExprParser parser = new MtlExprParser();
			Filter f = new Filter();
			
			MtlFormula phi;
			
			// Parser validation
			System.out.println("Parser Validation :");
			phi = parser.parse("b U[0.0,30.0] c"); System.out.println("Formula ("+ C.computeSufficientTime(phi) +") : "+ phi);
			phi = parser.parse("(b) U[0.0,30.0] c"); System.out.println("Formula ("+ C.computeSufficientTime(phi) +") : "+ phi);
			phi = parser.parse("((((b)))) U[0.0,30.0] (b && c)"); System.out.println("Formula ("+ C.computeSufficientTime(phi) +") : "+ phi);
			
			phi = parser.parse("( a!=10 || b>100 ) U[0,10] c");  System.out.println("Formula ("+ C.computeSufficientTime(phi) +") : "+ phi);
			phi = parser.parse("(a) U[0.0,30.0] c"); System.out.println("Formula ("+ C.computeSufficientTime(phi) +") : "+ phi);
			
			phi = parser.parse("(((a)U[0.0,4.0](b))U[0.0,10.0](c))"); System.out.println("Formula ("+ C.computeSufficientTime(phi) +") : "+ phi);
			phi = parser.parse("((a)U[2.0,10.0](N(b)))"); System.out.println("Formula ("+ C.computeSufficientTime(phi) +") : "+ phi);
			phi = parser.parse("a U[2.0,10.0](G[3.0,5.0](b))"); System.out.println("Formula ("+ C.computeSufficientTime(phi) +") : "+ phi);
			phi = parser.parse("(G[0.3,3.0](a))U[0.0,5.0] (b)"); System.out.println("Formula ("+ C.computeSufficientTime(phi) +") : "+ phi);
			
			phi = parser.parse("(F[1,3](a)||exp(x+5)<3)R[0.0,5.0] (b && c)"); System.out.println("Formula ("+ C.computeSufficientTime(phi) +") : "+ phi);
			phi = parser.parse("G[0.0,30.0] (abs  (Master.val-Slave.val)< (5-3) || !z)");  System.out.println("Formula ("+ C.computeSufficientTime(phi) +") : "+ phi);
			
			phi = parser.parse("b && G[1,2]e"); System.out.println("Formula ("+ C.computeSufficientTime(phi) +") : "+ phi);
			
			System.out.println();
			
			// Filter validation
			System.out.println("Filter Validation :");
			phi = parser.parse("a U[2.0,10.0](G[3.0,5.0](b))"); System.out.println(f.replaceEventually(f.replaceGlobally(phi)));
			phi = parser.parse("(G[0.3,3.0](a))U[0.0,5.0] (b)"); System.out.println(f.replaceEventually(f.replaceGlobally(phi)));
			phi = parser.parse("(F[1,3](a)||exp(x+5)<3)R[0.0,5.0] (b && c)"); System.out.println(f.replaceEventually(f.replaceGlobally(phi)));
			phi = parser.parse("G[0.0,30.0] (abs  (Master.val-Slave.val)< (5-3) || !z)");  System.out.println(f.replaceEventually(f.replaceGlobally(phi)));
			System.out.println();
			
//			// Trace validation
//			System.out.println("Trace Validation :");
//			trace = u.transformBip2("trace.txt"); System.out.println("Trace :"+trace);
//			trace = u.transformBip2("trace1.txt"); System.out.println("Trace :"+trace);
//			trace = u.transformBip2("trace2.txt"); System.out.println("Trace :"+trace);
//			trace = u.transformBip2("trace3.txt"); System.out.println("Trace :"+trace);
//			trace = u.transformBip2("trace4.txt"); System.out.println("Trace :"+trace);
//			trace = u.transformBip2("trace5.txt"); System.out.println("Trace :"+trace);
//			trace = u.transformBip2("trace6.txt"); System.out.println("Trace :"+trace);
//			//add a test with '!=' in bool
//			System.out.println();
//			
////			// Tests validation
////			// PTP (eval : true)
//			trace = u.transformBip2("trace.txt");
//			phi = parser.parse("G[0.0,20.0] (abs  (Master.val-Slave.val)< (2) ||!sync)");  
//			m.check(phi, trace); System.out.println();
//			
////			// Test1 (eval : true)
//			trace = u.transformBip2("trace1.txt");
//			phi = parser.parse("(a||b) U[0.0,30.0] c");
//			m.check(phi, trace); System.out.println();
//			
//			// Test2 (eval : true)
//			trace = u.transformBip2("trace2.txt");
//			phi = parser.parse("(((a)U[0.0,4.0](b))U[0.0,10.0](c))");
//			m.check(phi, trace); System.out.println();
//			
//			// Test3 (eval : true)
//			trace = u.transformBip2("trace3.txt");
//			phi = parser.parse("((a)U[2.0,10.0](G[3.0,5.0](b)))");
//			m.check(phi, trace); System.out.println();
//			
//			// Test4 (eval : true)
//			trace = u.transformBip2("trace4.txt");
//			phi = parser.parse("((a)U[2.0,10.0](N(b)))");
//			m.check(phi, trace); System.out.println();
//			
//			// Test5 (eval : true)
//			trace = u.transformBip2("trace5.txt");
//			phi = parser.parse("(G[0.0,3.0](a))U[0.0,5.0] (b)");
//			m.check(phi, trace); System.out.println();
//			
//			// Test6 (eval : true)
//			trace = u.transformBip2("trace6.txt");
//			m.check(phi, trace); System.out.println();
//			
//			// Test7 (eval : not able to conclude)
//			trace = u.transformBip2("trace5.txt");
//			phi = parser.parse("(a||b) U[0.0,30.0] c");
//			m.check(phi, trace); System.out.println();
//			
//			// Test8 (eval : false)
//			trace = u.transformBip2("trace7.txt");
//			phi = parser.parse("(F[0.0,4.0](b))U[0.0,7.0] (c)");
//			m.check(phi, trace); System.out.println();
		
		
	}
	

}
