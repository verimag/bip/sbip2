package ujf.verimag.sbip.parser.bool;

public class BoolOutput {
	String expression;
	String evalExpression;
	boolean value;
	boolean isEvaluated;
	
	public String getExpression() {
		return expression;
	}
	public void setExpression(String expression) {
		this.expression = expression;
	}
	public String getEvalExpression() {
		return evalExpression;
	}
	public void setEvalExpression(String evalExpression) {
		this.evalExpression = evalExpression;
	}
	public boolean getValue() {
		return value;
	}
	public void setValue(boolean value) {
		this.value = value;
	}
	public boolean isEvaluated() {
		return isEvaluated;
	}
	public void setEvaluated(boolean isEvaluated) {
		this.isEvaluated = isEvaluated;
	}

}
