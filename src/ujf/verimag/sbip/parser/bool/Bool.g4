grammar Bool;
@header {
import ujf.verimag.sbip.parser.formula.Interval;
}
// productions for syntax analysis
program returns [ BoolOutput out]: e = bool EOF
{
	$out = new BoolOutput();
	$out.expression = $e.s;
	$out.evalExpression = $e.s2;
	$out.value = $e.b;
	$out.isEvaluated = $e.isEval;
}
;


bool returns [boolean b, String s, String s2, boolean isEval]
: e=expr LT e2=expr
{
	$s = $e.s + "<" + $e2.s;
	$s2 = ($e.isEval? $e.d : $e.s2 )+ "<" + ($e2.isEval? $e2.d : $e2.s2);
	if($e.isEval && $e2.isEval){
		$b = $e.d < $e2.d;
		$isEval=true;
	}
	else
	{ $isEval = false;}
}
| e=expr LEQ e2=expr
{
	$s = $e.s + "<=" + $e2.s;	
	$s2 = ($e.isEval? $e.d : $e.s2 )+ "<=" + ($e2.isEval? $e2.d : $e2.s2);
	
	if($e.isEval && $e2.isEval){
		$b = $e.d <= $e2.d;
		$isEval=true;
	}
	else
	{ $isEval = false;}
}
|e=expr GT e2=expr
{
	$s = $e.s + ">" + $e2.s;
	$s2 = ($e.isEval? $e.d : $e.s2 )+ ">" + ($e2.isEval? $e2.d : $e2.s2);
	if($e.isEval && $e2.isEval){
		$b = $e.d > $e2.d;
		$isEval=true;
	}
	else
	{ $isEval = false;}
}
| e=expr GEQ e2=expr
{
	$s = $e.s + ">=" + $e2.s;	
	$s2 = ($e.isEval? $e.d : $e.s2 )+ ">=" + ($e2.isEval? $e2.d : $e2.s2);
	if($e.isEval && $e2.isEval){
		$b = $e.d >= $e2.d;
		$isEval=true;
	}
	else
	{ $isEval = false;}
}
|e=expr EQ e2=expr
{
	$s = $e.s + "==" + $e2.s;
	$s2 = ($e.isEval? $e.d : $e.s2 )+ "==" + ($e2.isEval? $e2.d : $e2.s2);
	
	if($e.isEval && $e2.isEval){
		$b = (Interval.round($e.d, 15) == Interval.round($e2.d, 15));
//		System.out.println("Evaluating ::::: "+$e.d+"=="+$e2.d);
	
	
//		System.out.println("Evaluated ::::: "+$b);
		$isEval=true;
	}
	else
	{ $isEval = false;}
}
| e=expr NEQ e2=expr
{
	$s = $e.s + "!=" + $e2.s;	
	$s2 = ($e.isEval? $e.d : $e.s2 )+ "!=" + ($e2.isEval? $e2.d : $e2.s2);
	if($e.isEval && $e2.isEval){
		$b = $e.d != $e2.d;
		$isEval=true;
	}
	else
	{ $isEval = false;}
}
| e=expr
{
	$s = $e.s;
	$s2 = ""+ ($e.isEval? $e.d : $e.s2);
	if($e.isEval){
		
		double d = $e.d;
		$b = 0 < d;
		$isEval = true;
	}
	else
	{$isEval = false;}
}
| TRUE
{
	$b =true;
	$s = "#True"; 
	$s2 = $s;
	$isEval = true;
	
}
| FALSE
{
	$b = false;
	$s = "#False"; 
	$s2 = $s;
	$isEval = false;
	
}
;

variable returns [String s]
: STRING
{
	$s= $STRING.text;
}
;

operand returns [String s, String s2, boolean isEval, Double d]
: v = variable
{
	$s = $v.s;	
	$s2 = $s;
	$isEval = false;
}
| DOUBLE
{
	$s = $DOUBLE.text;
	$isEval = true;
	$d = Double.parseDouble($DOUBLE.text);
	$s2 = ""+ $d;
}
| f = function
{
	$s = $f.s;	
	$s2 = ""+ ($f.isEval? $f.d : $f.s2);
	if($f.isEval){
		$d = $f.d;
		$isEval = true;
	}
	else $isEval = false;
}
| '-' o = operand
{
	$s = "-" + $o.s;	
	$s2 = ""+ ($o.isEval? -$o.d : ("-" +  $o.s2));
	$isEval = $o.isEval;
	if($isEval)
		$d = -$o.d;
}
| '(' e=expr ')'
{
	$s = "(" + $e.s + ")";	
	$s2 = ""+  ($e.isEval? $e.d : ("(" +$e.s2+ ")"));
	$isEval = $e.isEval;
	if($isEval)
		$d = $e.d;
}
;

expr returns [String s, String s2, Double d, boolean isEval]
: t=term e=expr2
{
	$s = $t.s + $e.s;
	$s2 = ""+  ($t.isEval? $t.d : $t.s2  )+ ($e.isEval && $e.ID_op!=0? 
	($e.ID_op== 1? "+":"-")+ 
	 $e.d : $e.s2);
		
	if($e.isEval && $t.isEval)
	{
		$isEval = true;
		switch($e.ID_op){
			case 0:
				$d = $t.d;
				break;
			case 1:				
				$d = $t.d + $e.d;
				break;
			case 2:
				$d = $t.d - $e.d;
				break;
		}
		$s2 = ""+ $d;
	}
	else $isEval = false;

}
;

expr2 returns [String s, String s2, Double d, boolean isEval, int ID_op]
: '+' t=term e=expr2
{
	$ID_op = 1;
	$s = "+" +$t.s + $e.s;
	
	$s2 = "+" + ($t.isEval? $t.d : $t.s2  )+ ($e.isEval && $e.ID_op!=0? ($e.ID_op== 1? "+":"-")+ $e.d : $e.s2);
	if($e.isEval && $t.isEval)
	{
		$isEval = true;
		switch($e.ID_op){
			case 0:
				$d = $t.d;
				break;
			case 1:				
				$d = $t.d + $e.d;
				break;
			case 2:
				$d = $t.d - $e.d;
				break;
		}
		$s2 = "+" + $d;
	}
	else $isEval = false;
	
}
| '-' t=term e=expr2
{
	$ID_op = 2;
	$s = "-" +$t.s + $e.s;	
	$s2 = "-" + ($t.isEval? $t.d : $t.s2 ) + ($e.isEval && $e.ID_op!=0? ($e.ID_op== 1? "+":"-")+ $e.d : $e.s2);
	if($e.isEval && $t.isEval)
	{
		$isEval = true;
		switch($e.ID_op){
			case 0:
				$d = $t.d;
				break;
			case 1:				
				$d = $t.d + $e.d;
				break;
			case 2:
				$d = $t.d - $e.d;
				break;
		}
		$s2 = "-" + $d;
	}
	else $isEval = false;
	
}
|
{
	$s = "";
	$s2 = "";
	$ID_op = 0;
	$isEval = true;
}
;

term returns [String s, String s2, Double d, boolean isEval]
: f=operand t=term2
{
	$s = $f.s + $t.s;		
	$s2 = ""+  ( $f.isEval? $f.d : $f.s2 ) + ($t.isEval && $t.ID_op!=0? 
	($t.ID_op== 1? "*":"/")+ $t.d : $t.s2);
	if($f.isEval && $t.isEval)
	{
		$isEval = true;
		switch($t.ID_op){
			case 0:
				$d = $f.d;
				break;
			case 1:				
				$d = $f.d * $t.d;
				break;
			case 2:
				$d = $f.d / $t.d;
				break;
		}		
		$s2 = ""+ $d;
	}
	else $isEval = false;
}
;

term2 returns [String s, String s2, Double d, boolean isEval, int ID_op]
: '*' f=operand t=term2
{
	$s = "*" + $f.s + $t.s;	
	$s2 = "*" + ($f.isEval? $f.d : $f.s2  )+( $t.isEval && $t.ID_op!=0? ($t.ID_op== 1? "*":"/")+ $t.d : $t.s2);
	$ID_op = 1;
	if($f.isEval && $t.isEval)
	{
		$isEval = true;
		switch($t.ID_op){
			case 0:
				$d = $f.d;
				break;
			case 1:				
				$d = $f.d * $t.d;
				break;
			case 2:
				$d = $f.d / $t.d;
				break;
		}
		$s2 = "*" + $d;
	}
	else $isEval = false;	
}

| '/' f=operand t=term2
{
	$s = "/" + $f.s + $t.s;	
	$s2 = "/" +  ($f.isEval? $f.d : $f.s2 ) + ($t.isEval && $t.ID_op!=0? ($t.ID_op== 1? "*":"/")+ $t.d : $t.s2);
	$ID_op = 2;
	if($f.isEval && $t.isEval)
	{
		$isEval = true;
		switch($t.ID_op){
			case 0:
				$d = $f.d;
				break;
			case 1:				
				$d = $f.d * $t.d;
				break;
			case 2:
				$d = $f.d / $t.d;
				break;
		}
		$s2 = "/" + $d;
	}
	else $isEval = false;	
}
|
{
	$s = "";
	$s2 = "";
	$ID_op = 0;
	$isEval = true;
}
;







function returns [String s, String s2, Double d, boolean isEval]
: STRING '(' e=expr ')'
{
	switch($STRING.text){
		case "exp":
			$s = $STRING.text + "(" + $e.s+ ")";			
			if($e.isEval){
				$d = Math.exp($e.d);
				$isEval = true;
				$s2 = ""+ $d;
			}
			else{
				$isEval = false;
				$s2 = $STRING.text + "(" + $e.s2+ ")";
			}
			break;
			
		case "abs":
			$s = $STRING.text + "(" + $e.s+ ")";			
			if($e.isEval){
				$d = Math.abs($e.d);
				$isEval = true;
				$s2 = ""+ $d;
			}
			else{
				$isEval = false;
				$s2 = $STRING.text + "(" + $e.s2+ ")" ;
			}
			break;
	}
	
}
;


// Lexer
DOUBLE : [1-9]+[0-9]*('.' [0-9]+)? | ('0' ('.' [0-9]+)?);
STRING : ([a-zA-Z])+[0-9]*([a-zA-Z] | '.' | '_')*; 
TRUE : '#True';
FALSE : '#False';
NEQ           : '!=';
LEQ            : '<=';
GEQ            : '>=';
EQ              : '==';
LT              : '<';
GT              : '>';