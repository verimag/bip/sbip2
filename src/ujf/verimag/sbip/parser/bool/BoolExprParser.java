package ujf.verimag.sbip.parser.bool;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import ujf.verimag.sbip.bipInterface.trace.State;
import ujf.verimag.sbip.parser.bool.BoolParser.ProgramContext;


@SuppressWarnings("deprecation")
public class BoolExprParser
{
	
	
	public String instantiate(String BoolSentence , Double v){
		BoolLexer lexer = new BoolLexer(new ANTLRInputStream(BoolSentence));
		 
	    // Get a list of matched tokens
	    CommonTokenStream tokens = new CommonTokenStream(lexer);
	 
	    // Pass the tokens to the parser
	    BoolParser parser = new BoolParser(tokens);
	 
	    // Specify our entry point
	    ProgramContext BoolSentenceContext = parser.program();
	 
	    // Walk it and attach our listener
	    ParseTreeWalker walker = new ParseTreeWalker();
	    BoolVarInstanciator listener = new BoolVarInstanciator(v);
	    walker.walk(listener, BoolSentenceContext);
		return listener.instantiatedString;
	}
	
	public BoolOutput parse(String s){
		BoolLexer lexer = new BoolLexer(new ANTLRInputStream(s));
		BoolParser parser = new BoolParser(new CommonTokenStream(lexer));
		BoolOutput o = parser.program().out;
		return o;		
	}

	public String instantiate(String phi, State state) {
//		System.out.println("Instantiation of -"+phi+"-");
		BoolLexer lexer = new BoolLexer(new ANTLRInputStream(phi));
		 
	    // Get a list of matched tokens
	    CommonTokenStream tokens = new CommonTokenStream(lexer);
	 
	    // Pass the tokens to the parser
	    BoolParser parser = new BoolParser(tokens);
	 
	    // Specify our entry point
	    ProgramContext BoolSentenceContext = parser.program();
	 
	    // Walk it and attach our listener
	    ParseTreeWalker walker = new ParseTreeWalker();
	    BoolVarInstanciator listener = new BoolVarInstanciator(state);
	    walker.walk(listener, BoolSentenceContext);
	    if(listener.error == true) return null;
		return listener.instantiatedString;
		
	}
}
