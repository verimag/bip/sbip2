package ujf.verimag.sbip.parser.bool;

import javax.swing.JOptionPane;

import org.antlr.v4.runtime.tree.TerminalNode;

import ujf.verimag.sbip.bipInterface.trace.State;
import ujf.verimag.sbip.bipInterface.trace.StateVariable;


public class BoolVarInstanciator extends BoolBaseListener{
	
	String instantiatedString = new String();
	Double value;
	State state;
	public boolean error = false;
	public BoolVarInstanciator(Double v) {
		this.value = v;
	}

	
	public BoolVarInstanciator(State state) {
		this.state = state;
	}


	public void visitTerminal(TerminalNode node) { 
		
		if(node.getSymbol().getType() == 8 && node.getParent().getText().equals(node.getText())) {
//			System.out.println("variable : \""+ node.getText()+"\"");
			StateVariable var = state.getVariable(node.getText());
			if(var == null){
				System.err.println("Boolean expr instantiation error : Variable '"+node.getText()+"' not found in trace !!");
				System.err.println(state);
				if(!error)JOptionPane.showMessageDialog(null, "Boolean expr instantiation error : Variable '"+node.getText()+"' not found in trace !!", "Monitor error", JOptionPane.ERROR_MESSAGE);
				error = true;
//				 try {
//					 throw (new InterruptedException("Boolean expr instantiation error : Variable not found in trace !!")) ;
//					 
//					}
//					catch (InterruptedException vEx) { 
//					System.err.println(vEx.getMessage());
////						System.exit(0);
//					}
			}
//			System.out.print("Instantiating ...");
//			System.out.println(var.getType());
			else
			if(var.getType().equals("int") || var.getType().equals("double")){

//				System.out.println("int var : "+var.getValue());
				this.instantiatedString += var.getValue();
			}
			else{
				if(var.getType().equals("bool")){
//					System.out.println("!!!"+var.getValue()+"!!!");
					if(var.getValue().equals("true")){
						this.instantiatedString += "#True";
//						System.out.println("Found variable "+ node.getText()+" is true");
						
					}
					else 
//						if(var.getValue().equals("false")) 
						this.instantiatedString += "#False";
				}
			}
		}
		else
		if(!node.getText().equals("<EOF>") ) this.instantiatedString+=node.getText();
//		System.out.println("Returning : "+this.instantiatedString);
	}

}
