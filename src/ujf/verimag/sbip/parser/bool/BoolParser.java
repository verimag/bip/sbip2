// Generated from Bool.g4 by ANTLR 4.7
package ujf.verimag.sbip.parser.bool;

import ujf.verimag.sbip.parser.formula.Interval;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BoolParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, DOUBLE=7, STRING=8, TRUE=9, 
		FALSE=10, NEQ=11, LEQ=12, GEQ=13, EQ=14, LT=15, GT=16;
	public static final int
		RULE_program = 0, RULE_bool = 1, RULE_variable = 2, RULE_operand = 3, 
		RULE_expr = 4, RULE_expr2 = 5, RULE_term = 6, RULE_term2 = 7, RULE_function = 8;
	public static final String[] ruleNames = {
		"program", "bool", "variable", "operand", "expr", "expr2", "term", "term2", 
		"function"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'-'", "'('", "')'", "'+'", "'*'", "'/'", null, null, "'#True'", 
		"'#False'", "'!='", "'<='", "'>='", "'=='", "'<'", "'>'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, "DOUBLE", "STRING", "TRUE", 
		"FALSE", "NEQ", "LEQ", "GEQ", "EQ", "LT", "GT"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Bool.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public BoolParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public BoolOutput out;
		public BoolContext e;
		public TerminalNode EOF() { return getToken(BoolParser.EOF, 0); }
		public BoolContext bool() {
			return getRuleContext(BoolContext.class,0);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BoolListener ) ((BoolListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BoolListener ) ((BoolListener)listener).exitProgram(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(18);
			((ProgramContext)_localctx).e = bool();
			setState(19);
			match(EOF);

				((ProgramContext)_localctx).out =  new BoolOutput();
				_localctx.out.expression = ((ProgramContext)_localctx).e.s;
				_localctx.out.evalExpression = ((ProgramContext)_localctx).e.s2;
				_localctx.out.value = ((ProgramContext)_localctx).e.b;
				_localctx.out.isEvaluated = ((ProgramContext)_localctx).e.isEval;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BoolContext extends ParserRuleContext {
		public boolean b;
		public String s;
		public String s2;
		public boolean isEval;
		public ExprContext e;
		public ExprContext e2;
		public TerminalNode LT() { return getToken(BoolParser.LT, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode LEQ() { return getToken(BoolParser.LEQ, 0); }
		public TerminalNode GT() { return getToken(BoolParser.GT, 0); }
		public TerminalNode GEQ() { return getToken(BoolParser.GEQ, 0); }
		public TerminalNode EQ() { return getToken(BoolParser.EQ, 0); }
		public TerminalNode NEQ() { return getToken(BoolParser.NEQ, 0); }
		public TerminalNode TRUE() { return getToken(BoolParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(BoolParser.FALSE, 0); }
		public BoolContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bool; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BoolListener ) ((BoolListener)listener).enterBool(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BoolListener ) ((BoolListener)listener).exitBool(this);
		}
	}

	public final BoolContext bool() throws RecognitionException {
		BoolContext _localctx = new BoolContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_bool);
		try {
			setState(59);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(22);
				((BoolContext)_localctx).e = expr();
				setState(23);
				match(LT);
				setState(24);
				((BoolContext)_localctx).e2 = expr();

					((BoolContext)_localctx).s =  ((BoolContext)_localctx).e.s + "<" + ((BoolContext)_localctx).e2.s;
					((BoolContext)_localctx).s2 =  (((BoolContext)_localctx).e.isEval? ((BoolContext)_localctx).e.d : ((BoolContext)_localctx).e.s2 )+ "<" + (((BoolContext)_localctx).e2.isEval? ((BoolContext)_localctx).e2.d : ((BoolContext)_localctx).e2.s2);
					if(((BoolContext)_localctx).e.isEval && ((BoolContext)_localctx).e2.isEval){
						((BoolContext)_localctx).b =  ((BoolContext)_localctx).e.d < ((BoolContext)_localctx).e2.d;
						((BoolContext)_localctx).isEval = true;
					}
					else
					{ ((BoolContext)_localctx).isEval =  false;}

				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(27);
				((BoolContext)_localctx).e = expr();
				setState(28);
				match(LEQ);
				setState(29);
				((BoolContext)_localctx).e2 = expr();

					((BoolContext)_localctx).s =  ((BoolContext)_localctx).e.s + "<=" + ((BoolContext)_localctx).e2.s;	
					((BoolContext)_localctx).s2 =  (((BoolContext)_localctx).e.isEval? ((BoolContext)_localctx).e.d : ((BoolContext)_localctx).e.s2 )+ "<=" + (((BoolContext)_localctx).e2.isEval? ((BoolContext)_localctx).e2.d : ((BoolContext)_localctx).e2.s2);
					
					if(((BoolContext)_localctx).e.isEval && ((BoolContext)_localctx).e2.isEval){
						((BoolContext)_localctx).b =  ((BoolContext)_localctx).e.d <= ((BoolContext)_localctx).e2.d;
						((BoolContext)_localctx).isEval = true;
					}
					else
					{ ((BoolContext)_localctx).isEval =  false;}

				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(32);
				((BoolContext)_localctx).e = expr();
				setState(33);
				match(GT);
				setState(34);
				((BoolContext)_localctx).e2 = expr();

					((BoolContext)_localctx).s =  ((BoolContext)_localctx).e.s + ">" + ((BoolContext)_localctx).e2.s;
					((BoolContext)_localctx).s2 =  (((BoolContext)_localctx).e.isEval? ((BoolContext)_localctx).e.d : ((BoolContext)_localctx).e.s2 )+ ">" + (((BoolContext)_localctx).e2.isEval? ((BoolContext)_localctx).e2.d : ((BoolContext)_localctx).e2.s2);
					if(((BoolContext)_localctx).e.isEval && ((BoolContext)_localctx).e2.isEval){
						((BoolContext)_localctx).b =  ((BoolContext)_localctx).e.d > ((BoolContext)_localctx).e2.d;
						((BoolContext)_localctx).isEval = true;
					}
					else
					{ ((BoolContext)_localctx).isEval =  false;}

				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(37);
				((BoolContext)_localctx).e = expr();
				setState(38);
				match(GEQ);
				setState(39);
				((BoolContext)_localctx).e2 = expr();

					((BoolContext)_localctx).s =  ((BoolContext)_localctx).e.s + ">=" + ((BoolContext)_localctx).e2.s;	
					((BoolContext)_localctx).s2 =  (((BoolContext)_localctx).e.isEval? ((BoolContext)_localctx).e.d : ((BoolContext)_localctx).e.s2 )+ ">=" + (((BoolContext)_localctx).e2.isEval? ((BoolContext)_localctx).e2.d : ((BoolContext)_localctx).e2.s2);
					if(((BoolContext)_localctx).e.isEval && ((BoolContext)_localctx).e2.isEval){
						((BoolContext)_localctx).b =  ((BoolContext)_localctx).e.d >= ((BoolContext)_localctx).e2.d;
						((BoolContext)_localctx).isEval = true;
					}
					else
					{ ((BoolContext)_localctx).isEval =  false;}

				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(42);
				((BoolContext)_localctx).e = expr();
				setState(43);
				match(EQ);
				setState(44);
				((BoolContext)_localctx).e2 = expr();

					((BoolContext)_localctx).s =  ((BoolContext)_localctx).e.s + "==" + ((BoolContext)_localctx).e2.s;
					((BoolContext)_localctx).s2 =  (((BoolContext)_localctx).e.isEval? ((BoolContext)_localctx).e.d : ((BoolContext)_localctx).e.s2 )+ "==" + (((BoolContext)_localctx).e2.isEval? ((BoolContext)_localctx).e2.d : ((BoolContext)_localctx).e2.s2);
					
					if(((BoolContext)_localctx).e.isEval && ((BoolContext)_localctx).e2.isEval){
						((BoolContext)_localctx).b =  (Interval.round(((BoolContext)_localctx).e.d, 15) == Interval.round(((BoolContext)_localctx).e2.d, 15));
				//		System.out.println("Evaluating ::::: "+((BoolContext)_localctx).e.d+"=="+((BoolContext)_localctx).e2.d);
					
					
				//		System.out.println("Evaluated ::::: "+_localctx.b);
						((BoolContext)_localctx).isEval = true;
					}
					else
					{ ((BoolContext)_localctx).isEval =  false;}

				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(47);
				((BoolContext)_localctx).e = expr();
				setState(48);
				match(NEQ);
				setState(49);
				((BoolContext)_localctx).e2 = expr();

					((BoolContext)_localctx).s =  ((BoolContext)_localctx).e.s + "!=" + ((BoolContext)_localctx).e2.s;	
					((BoolContext)_localctx).s2 =  (((BoolContext)_localctx).e.isEval? ((BoolContext)_localctx).e.d : ((BoolContext)_localctx).e.s2 )+ "!=" + (((BoolContext)_localctx).e2.isEval? ((BoolContext)_localctx).e2.d : ((BoolContext)_localctx).e2.s2);
					if(((BoolContext)_localctx).e.isEval && ((BoolContext)_localctx).e2.isEval){
						((BoolContext)_localctx).b =  ((BoolContext)_localctx).e.d != ((BoolContext)_localctx).e2.d;
						((BoolContext)_localctx).isEval = true;
					}
					else
					{ ((BoolContext)_localctx).isEval =  false;}

				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(52);
				((BoolContext)_localctx).e = expr();

					((BoolContext)_localctx).s =  ((BoolContext)_localctx).e.s;
					((BoolContext)_localctx).s2 =  ""+ (((BoolContext)_localctx).e.isEval? ((BoolContext)_localctx).e.d : ((BoolContext)_localctx).e.s2);
					if(((BoolContext)_localctx).e.isEval){
						
						double d = ((BoolContext)_localctx).e.d;
						((BoolContext)_localctx).b =  0 < d;
						((BoolContext)_localctx).isEval =  true;
					}
					else
					{((BoolContext)_localctx).isEval =  false;}

				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(55);
				match(TRUE);

					((BoolContext)_localctx).b = true;
					((BoolContext)_localctx).s =  "#True"; 
					((BoolContext)_localctx).s2 =  _localctx.s;
					((BoolContext)_localctx).isEval =  true;
					

				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(57);
				match(FALSE);

					((BoolContext)_localctx).b =  false;
					((BoolContext)_localctx).s =  "#False"; 
					((BoolContext)_localctx).s2 =  _localctx.s;
					((BoolContext)_localctx).isEval =  false;
					

				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableContext extends ParserRuleContext {
		public String s;
		public Token STRING;
		public TerminalNode STRING() { return getToken(BoolParser.STRING, 0); }
		public VariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BoolListener ) ((BoolListener)listener).enterVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BoolListener ) ((BoolListener)listener).exitVariable(this);
		}
	}

	public final VariableContext variable() throws RecognitionException {
		VariableContext _localctx = new VariableContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_variable);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(61);
			((VariableContext)_localctx).STRING = match(STRING);

				((VariableContext)_localctx).s =  (((VariableContext)_localctx).STRING!=null?((VariableContext)_localctx).STRING.getText():null);

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OperandContext extends ParserRuleContext {
		public String s;
		public String s2;
		public boolean isEval;
		public Double d;
		public VariableContext v;
		public Token DOUBLE;
		public FunctionContext f;
		public OperandContext o;
		public ExprContext e;
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public TerminalNode DOUBLE() { return getToken(BoolParser.DOUBLE, 0); }
		public FunctionContext function() {
			return getRuleContext(FunctionContext.class,0);
		}
		public OperandContext operand() {
			return getRuleContext(OperandContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public OperandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BoolListener ) ((BoolListener)listener).enterOperand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BoolListener ) ((BoolListener)listener).exitOperand(this);
		}
	}

	public final OperandContext operand() throws RecognitionException {
		OperandContext _localctx = new OperandContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_operand);
		try {
			setState(81);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(64);
				((OperandContext)_localctx).v = variable();

					((OperandContext)_localctx).s =  ((OperandContext)_localctx).v.s;	
					((OperandContext)_localctx).s2 =  _localctx.s;
					((OperandContext)_localctx).isEval =  false;

				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(67);
				((OperandContext)_localctx).DOUBLE = match(DOUBLE);

					((OperandContext)_localctx).s =  (((OperandContext)_localctx).DOUBLE!=null?((OperandContext)_localctx).DOUBLE.getText():null);
					((OperandContext)_localctx).isEval =  true;
					((OperandContext)_localctx).d =  Double.parseDouble((((OperandContext)_localctx).DOUBLE!=null?((OperandContext)_localctx).DOUBLE.getText():null));
					((OperandContext)_localctx).s2 =  ""+ _localctx.d;

				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(69);
				((OperandContext)_localctx).f = function();

					((OperandContext)_localctx).s =  ((OperandContext)_localctx).f.s;	
					((OperandContext)_localctx).s2 =  ""+ (((OperandContext)_localctx).f.isEval? ((OperandContext)_localctx).f.d : ((OperandContext)_localctx).f.s2);
					if(((OperandContext)_localctx).f.isEval){
						((OperandContext)_localctx).d =  ((OperandContext)_localctx).f.d;
						((OperandContext)_localctx).isEval =  true;
					}
					else ((OperandContext)_localctx).isEval =  false;

				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(72);
				match(T__0);
				setState(73);
				((OperandContext)_localctx).o = operand();

					((OperandContext)_localctx).s =  "-" + ((OperandContext)_localctx).o.s;	
					((OperandContext)_localctx).s2 =  ""+ (((OperandContext)_localctx).o.isEval? -((OperandContext)_localctx).o.d : ("-" +  ((OperandContext)_localctx).o.s2));
					((OperandContext)_localctx).isEval =  ((OperandContext)_localctx).o.isEval;
					if(_localctx.isEval)
						((OperandContext)_localctx).d =  -((OperandContext)_localctx).o.d;

				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(76);
				match(T__1);
				setState(77);
				((OperandContext)_localctx).e = expr();
				setState(78);
				match(T__2);

					((OperandContext)_localctx).s =  "(" + ((OperandContext)_localctx).e.s + ")";	
					((OperandContext)_localctx).s2 =  ""+  (((OperandContext)_localctx).e.isEval? ((OperandContext)_localctx).e.d : ("(" +((OperandContext)_localctx).e.s2+ ")"));
					((OperandContext)_localctx).isEval =  ((OperandContext)_localctx).e.isEval;
					if(_localctx.isEval)
						((OperandContext)_localctx).d =  ((OperandContext)_localctx).e.d;

				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public String s;
		public String s2;
		public Double d;
		public boolean isEval;
		public TermContext t;
		public Expr2Context e;
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public Expr2Context expr2() {
			return getRuleContext(Expr2Context.class,0);
		}
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BoolListener ) ((BoolListener)listener).enterExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BoolListener ) ((BoolListener)listener).exitExpr(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		ExprContext _localctx = new ExprContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_expr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(83);
			((ExprContext)_localctx).t = term();
			setState(84);
			((ExprContext)_localctx).e = expr2();

				((ExprContext)_localctx).s =  ((ExprContext)_localctx).t.s + ((ExprContext)_localctx).e.s;
				((ExprContext)_localctx).s2 =  ""+  (((ExprContext)_localctx).t.isEval? ((ExprContext)_localctx).t.d : ((ExprContext)_localctx).t.s2  )+ (((ExprContext)_localctx).e.isEval && ((ExprContext)_localctx).e.ID_op!=0? 
				(((ExprContext)_localctx).e.ID_op== 1? "+":"-")+ 
				 ((ExprContext)_localctx).e.d : ((ExprContext)_localctx).e.s2);
					
				if(((ExprContext)_localctx).e.isEval && ((ExprContext)_localctx).t.isEval)
				{
					((ExprContext)_localctx).isEval =  true;
					switch(((ExprContext)_localctx).e.ID_op){
						case 0:
							((ExprContext)_localctx).d =  ((ExprContext)_localctx).t.d;
							break;
						case 1:				
							((ExprContext)_localctx).d =  ((ExprContext)_localctx).t.d + ((ExprContext)_localctx).e.d;
							break;
						case 2:
							((ExprContext)_localctx).d =  ((ExprContext)_localctx).t.d - ((ExprContext)_localctx).e.d;
							break;
					}
					((ExprContext)_localctx).s2 =  ""+ _localctx.d;
				}
				else ((ExprContext)_localctx).isEval =  false;


			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr2Context extends ParserRuleContext {
		public String s;
		public String s2;
		public Double d;
		public boolean isEval;
		public int ID_op;
		public TermContext t;
		public Expr2Context e;
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public Expr2Context expr2() {
			return getRuleContext(Expr2Context.class,0);
		}
		public Expr2Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr2; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BoolListener ) ((BoolListener)listener).enterExpr2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BoolListener ) ((BoolListener)listener).exitExpr2(this);
		}
	}

	public final Expr2Context expr2() throws RecognitionException {
		Expr2Context _localctx = new Expr2Context(_ctx, getState());
		enterRule(_localctx, 10, RULE_expr2);
		try {
			setState(98);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__3:
				enterOuterAlt(_localctx, 1);
				{
				setState(87);
				match(T__3);
				setState(88);
				((Expr2Context)_localctx).t = term();
				setState(89);
				((Expr2Context)_localctx).e = expr2();

					((Expr2Context)_localctx).ID_op =  1;
					((Expr2Context)_localctx).s =  "+" +((Expr2Context)_localctx).t.s + ((Expr2Context)_localctx).e.s;
					
					((Expr2Context)_localctx).s2 =  "+" + (((Expr2Context)_localctx).t.isEval? ((Expr2Context)_localctx).t.d : ((Expr2Context)_localctx).t.s2  )+ (((Expr2Context)_localctx).e.isEval && ((Expr2Context)_localctx).e.ID_op!=0? (((Expr2Context)_localctx).e.ID_op== 1? "+":"-")+ ((Expr2Context)_localctx).e.d : ((Expr2Context)_localctx).e.s2);
					if(((Expr2Context)_localctx).e.isEval && ((Expr2Context)_localctx).t.isEval)
					{
						((Expr2Context)_localctx).isEval =  true;
						switch(((Expr2Context)_localctx).e.ID_op){
							case 0:
								((Expr2Context)_localctx).d =  ((Expr2Context)_localctx).t.d;
								break;
							case 1:				
								((Expr2Context)_localctx).d =  ((Expr2Context)_localctx).t.d + ((Expr2Context)_localctx).e.d;
								break;
							case 2:
								((Expr2Context)_localctx).d =  ((Expr2Context)_localctx).t.d - ((Expr2Context)_localctx).e.d;
								break;
						}
						((Expr2Context)_localctx).s2 =  "+" + _localctx.d;
					}
					else ((Expr2Context)_localctx).isEval =  false;
					

				}
				break;
			case T__0:
				enterOuterAlt(_localctx, 2);
				{
				setState(92);
				match(T__0);
				setState(93);
				((Expr2Context)_localctx).t = term();
				setState(94);
				((Expr2Context)_localctx).e = expr2();

					((Expr2Context)_localctx).ID_op =  2;
					((Expr2Context)_localctx).s =  "-" +((Expr2Context)_localctx).t.s + ((Expr2Context)_localctx).e.s;	
					((Expr2Context)_localctx).s2 =  "-" + (((Expr2Context)_localctx).t.isEval? ((Expr2Context)_localctx).t.d : ((Expr2Context)_localctx).t.s2 ) + (((Expr2Context)_localctx).e.isEval && ((Expr2Context)_localctx).e.ID_op!=0? (((Expr2Context)_localctx).e.ID_op== 1? "+":"-")+ ((Expr2Context)_localctx).e.d : ((Expr2Context)_localctx).e.s2);
					if(((Expr2Context)_localctx).e.isEval && ((Expr2Context)_localctx).t.isEval)
					{
						((Expr2Context)_localctx).isEval =  true;
						switch(((Expr2Context)_localctx).e.ID_op){
							case 0:
								((Expr2Context)_localctx).d =  ((Expr2Context)_localctx).t.d;
								break;
							case 1:				
								((Expr2Context)_localctx).d =  ((Expr2Context)_localctx).t.d + ((Expr2Context)_localctx).e.d;
								break;
							case 2:
								((Expr2Context)_localctx).d =  ((Expr2Context)_localctx).t.d - ((Expr2Context)_localctx).e.d;
								break;
						}
						((Expr2Context)_localctx).s2 =  "-" + _localctx.d;
					}
					else ((Expr2Context)_localctx).isEval =  false;
					

				}
				break;
			case EOF:
			case T__2:
			case NEQ:
			case LEQ:
			case GEQ:
			case EQ:
			case LT:
			case GT:
				enterOuterAlt(_localctx, 3);
				{

					((Expr2Context)_localctx).s =  "";
					((Expr2Context)_localctx).s2 =  "";
					((Expr2Context)_localctx).ID_op =  0;
					((Expr2Context)_localctx).isEval =  true;

				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TermContext extends ParserRuleContext {
		public String s;
		public String s2;
		public Double d;
		public boolean isEval;
		public OperandContext f;
		public Term2Context t;
		public OperandContext operand() {
			return getRuleContext(OperandContext.class,0);
		}
		public Term2Context term2() {
			return getRuleContext(Term2Context.class,0);
		}
		public TermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_term; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BoolListener ) ((BoolListener)listener).enterTerm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BoolListener ) ((BoolListener)listener).exitTerm(this);
		}
	}

	public final TermContext term() throws RecognitionException {
		TermContext _localctx = new TermContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_term);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(100);
			((TermContext)_localctx).f = operand();
			setState(101);
			((TermContext)_localctx).t = term2();

				((TermContext)_localctx).s =  ((TermContext)_localctx).f.s + ((TermContext)_localctx).t.s;		
				((TermContext)_localctx).s2 =  ""+  ( ((TermContext)_localctx).f.isEval? ((TermContext)_localctx).f.d : ((TermContext)_localctx).f.s2 ) + (((TermContext)_localctx).t.isEval && ((TermContext)_localctx).t.ID_op!=0? 
				(((TermContext)_localctx).t.ID_op== 1? "*":"/")+ ((TermContext)_localctx).t.d : ((TermContext)_localctx).t.s2);
				if(((TermContext)_localctx).f.isEval && ((TermContext)_localctx).t.isEval)
				{
					((TermContext)_localctx).isEval =  true;
					switch(((TermContext)_localctx).t.ID_op){
						case 0:
							((TermContext)_localctx).d =  ((TermContext)_localctx).f.d;
							break;
						case 1:				
							((TermContext)_localctx).d =  ((TermContext)_localctx).f.d * ((TermContext)_localctx).t.d;
							break;
						case 2:
							((TermContext)_localctx).d =  ((TermContext)_localctx).f.d / ((TermContext)_localctx).t.d;
							break;
					}		
					((TermContext)_localctx).s2 =  ""+ _localctx.d;
				}
				else ((TermContext)_localctx).isEval =  false;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Term2Context extends ParserRuleContext {
		public String s;
		public String s2;
		public Double d;
		public boolean isEval;
		public int ID_op;
		public OperandContext f;
		public Term2Context t;
		public OperandContext operand() {
			return getRuleContext(OperandContext.class,0);
		}
		public Term2Context term2() {
			return getRuleContext(Term2Context.class,0);
		}
		public Term2Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_term2; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BoolListener ) ((BoolListener)listener).enterTerm2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BoolListener ) ((BoolListener)listener).exitTerm2(this);
		}
	}

	public final Term2Context term2() throws RecognitionException {
		Term2Context _localctx = new Term2Context(_ctx, getState());
		enterRule(_localctx, 14, RULE_term2);
		try {
			setState(115);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__4:
				enterOuterAlt(_localctx, 1);
				{
				setState(104);
				match(T__4);
				setState(105);
				((Term2Context)_localctx).f = operand();
				setState(106);
				((Term2Context)_localctx).t = term2();

					((Term2Context)_localctx).s =  "*" + ((Term2Context)_localctx).f.s + ((Term2Context)_localctx).t.s;	
					((Term2Context)_localctx).s2 =  "*" + (((Term2Context)_localctx).f.isEval? ((Term2Context)_localctx).f.d : ((Term2Context)_localctx).f.s2  )+( ((Term2Context)_localctx).t.isEval && ((Term2Context)_localctx).t.ID_op!=0? (((Term2Context)_localctx).t.ID_op== 1? "*":"/")+ ((Term2Context)_localctx).t.d : ((Term2Context)_localctx).t.s2);
					((Term2Context)_localctx).ID_op =  1;
					if(((Term2Context)_localctx).f.isEval && ((Term2Context)_localctx).t.isEval)
					{
						((Term2Context)_localctx).isEval =  true;
						switch(((Term2Context)_localctx).t.ID_op){
							case 0:
								((Term2Context)_localctx).d =  ((Term2Context)_localctx).f.d;
								break;
							case 1:				
								((Term2Context)_localctx).d =  ((Term2Context)_localctx).f.d * ((Term2Context)_localctx).t.d;
								break;
							case 2:
								((Term2Context)_localctx).d =  ((Term2Context)_localctx).f.d / ((Term2Context)_localctx).t.d;
								break;
						}
						((Term2Context)_localctx).s2 =  "*" + _localctx.d;
					}
					else ((Term2Context)_localctx).isEval =  false;	

				}
				break;
			case T__5:
				enterOuterAlt(_localctx, 2);
				{
				setState(109);
				match(T__5);
				setState(110);
				((Term2Context)_localctx).f = operand();
				setState(111);
				((Term2Context)_localctx).t = term2();

					((Term2Context)_localctx).s =  "/" + ((Term2Context)_localctx).f.s + ((Term2Context)_localctx).t.s;	
					((Term2Context)_localctx).s2 =  "/" +  (((Term2Context)_localctx).f.isEval? ((Term2Context)_localctx).f.d : ((Term2Context)_localctx).f.s2 ) + (((Term2Context)_localctx).t.isEval && ((Term2Context)_localctx).t.ID_op!=0? (((Term2Context)_localctx).t.ID_op== 1? "*":"/")+ ((Term2Context)_localctx).t.d : ((Term2Context)_localctx).t.s2);
					((Term2Context)_localctx).ID_op =  2;
					if(((Term2Context)_localctx).f.isEval && ((Term2Context)_localctx).t.isEval)
					{
						((Term2Context)_localctx).isEval =  true;
						switch(((Term2Context)_localctx).t.ID_op){
							case 0:
								((Term2Context)_localctx).d =  ((Term2Context)_localctx).f.d;
								break;
							case 1:				
								((Term2Context)_localctx).d =  ((Term2Context)_localctx).f.d * ((Term2Context)_localctx).t.d;
								break;
							case 2:
								((Term2Context)_localctx).d =  ((Term2Context)_localctx).f.d / ((Term2Context)_localctx).t.d;
								break;
						}
						((Term2Context)_localctx).s2 =  "/" + _localctx.d;
					}
					else ((Term2Context)_localctx).isEval =  false;	

				}
				break;
			case EOF:
			case T__0:
			case T__2:
			case T__3:
			case NEQ:
			case LEQ:
			case GEQ:
			case EQ:
			case LT:
			case GT:
				enterOuterAlt(_localctx, 3);
				{

					((Term2Context)_localctx).s =  "";
					((Term2Context)_localctx).s2 =  "";
					((Term2Context)_localctx).ID_op =  0;
					((Term2Context)_localctx).isEval =  true;

				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionContext extends ParserRuleContext {
		public String s;
		public String s2;
		public Double d;
		public boolean isEval;
		public Token STRING;
		public ExprContext e;
		public TerminalNode STRING() { return getToken(BoolParser.STRING, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public FunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BoolListener ) ((BoolListener)listener).enterFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BoolListener ) ((BoolListener)listener).exitFunction(this);
		}
	}

	public final FunctionContext function() throws RecognitionException {
		FunctionContext _localctx = new FunctionContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_function);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(117);
			((FunctionContext)_localctx).STRING = match(STRING);
			setState(118);
			match(T__1);
			setState(119);
			((FunctionContext)_localctx).e = expr();
			setState(120);
			match(T__2);

				switch((((FunctionContext)_localctx).STRING!=null?((FunctionContext)_localctx).STRING.getText():null)){
					case "exp":
						((FunctionContext)_localctx).s =  (((FunctionContext)_localctx).STRING!=null?((FunctionContext)_localctx).STRING.getText():null) + "(" + ((FunctionContext)_localctx).e.s+ ")";			
						if(((FunctionContext)_localctx).e.isEval){
							((FunctionContext)_localctx).d =  Math.exp(((FunctionContext)_localctx).e.d);
							((FunctionContext)_localctx).isEval =  true;
							((FunctionContext)_localctx).s2 =  ""+ _localctx.d;
						}
						else{
							((FunctionContext)_localctx).isEval =  false;
							((FunctionContext)_localctx).s2 =  (((FunctionContext)_localctx).STRING!=null?((FunctionContext)_localctx).STRING.getText():null) + "(" + ((FunctionContext)_localctx).e.s2+ ")";
						}
						break;
						
					case "abs":
						((FunctionContext)_localctx).s =  (((FunctionContext)_localctx).STRING!=null?((FunctionContext)_localctx).STRING.getText():null) + "(" + ((FunctionContext)_localctx).e.s+ ")";			
						if(((FunctionContext)_localctx).e.isEval){
							((FunctionContext)_localctx).d =  Math.abs(((FunctionContext)_localctx).e.d);
							((FunctionContext)_localctx).isEval =  true;
							((FunctionContext)_localctx).s2 =  ""+ _localctx.d;
						}
						else{
							((FunctionContext)_localctx).isEval =  false;
							((FunctionContext)_localctx).s2 =  (((FunctionContext)_localctx).STRING!=null?((FunctionContext)_localctx).STRING.getText():null) + "(" + ((FunctionContext)_localctx).e.s2+ ")" ;
						}
						break;
				}
				

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\22~\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\3\2\3\2\3\2"+
		"\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\5\3>\n\3\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5"+
		"\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5T\n\5\3\6\3\6\3\6\3\6\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7e\n\7\3\b\3\b\3\b\3\b\3\t\3\t\3\t"+
		"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\tv\n\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n"+
		"\2\2\13\2\4\6\b\n\f\16\20\22\2\2\2\u0084\2\24\3\2\2\2\4=\3\2\2\2\6?\3"+
		"\2\2\2\bS\3\2\2\2\nU\3\2\2\2\fd\3\2\2\2\16f\3\2\2\2\20u\3\2\2\2\22w\3"+
		"\2\2\2\24\25\5\4\3\2\25\26\7\2\2\3\26\27\b\2\1\2\27\3\3\2\2\2\30\31\5"+
		"\n\6\2\31\32\7\21\2\2\32\33\5\n\6\2\33\34\b\3\1\2\34>\3\2\2\2\35\36\5"+
		"\n\6\2\36\37\7\16\2\2\37 \5\n\6\2 !\b\3\1\2!>\3\2\2\2\"#\5\n\6\2#$\7\22"+
		"\2\2$%\5\n\6\2%&\b\3\1\2&>\3\2\2\2\'(\5\n\6\2()\7\17\2\2)*\5\n\6\2*+\b"+
		"\3\1\2+>\3\2\2\2,-\5\n\6\2-.\7\20\2\2./\5\n\6\2/\60\b\3\1\2\60>\3\2\2"+
		"\2\61\62\5\n\6\2\62\63\7\r\2\2\63\64\5\n\6\2\64\65\b\3\1\2\65>\3\2\2\2"+
		"\66\67\5\n\6\2\678\b\3\1\28>\3\2\2\29:\7\13\2\2:>\b\3\1\2;<\7\f\2\2<>"+
		"\b\3\1\2=\30\3\2\2\2=\35\3\2\2\2=\"\3\2\2\2=\'\3\2\2\2=,\3\2\2\2=\61\3"+
		"\2\2\2=\66\3\2\2\2=9\3\2\2\2=;\3\2\2\2>\5\3\2\2\2?@\7\n\2\2@A\b\4\1\2"+
		"A\7\3\2\2\2BC\5\6\4\2CD\b\5\1\2DT\3\2\2\2EF\7\t\2\2FT\b\5\1\2GH\5\22\n"+
		"\2HI\b\5\1\2IT\3\2\2\2JK\7\3\2\2KL\5\b\5\2LM\b\5\1\2MT\3\2\2\2NO\7\4\2"+
		"\2OP\5\n\6\2PQ\7\5\2\2QR\b\5\1\2RT\3\2\2\2SB\3\2\2\2SE\3\2\2\2SG\3\2\2"+
		"\2SJ\3\2\2\2SN\3\2\2\2T\t\3\2\2\2UV\5\16\b\2VW\5\f\7\2WX\b\6\1\2X\13\3"+
		"\2\2\2YZ\7\6\2\2Z[\5\16\b\2[\\\5\f\7\2\\]\b\7\1\2]e\3\2\2\2^_\7\3\2\2"+
		"_`\5\16\b\2`a\5\f\7\2ab\b\7\1\2be\3\2\2\2ce\b\7\1\2dY\3\2\2\2d^\3\2\2"+
		"\2dc\3\2\2\2e\r\3\2\2\2fg\5\b\5\2gh\5\20\t\2hi\b\b\1\2i\17\3\2\2\2jk\7"+
		"\7\2\2kl\5\b\5\2lm\5\20\t\2mn\b\t\1\2nv\3\2\2\2op\7\b\2\2pq\5\b\5\2qr"+
		"\5\20\t\2rs\b\t\1\2sv\3\2\2\2tv\b\t\1\2uj\3\2\2\2uo\3\2\2\2ut\3\2\2\2"+
		"v\21\3\2\2\2wx\7\n\2\2xy\7\4\2\2yz\5\n\6\2z{\7\5\2\2{|\b\n\1\2|\23\3\2"+
		"\2\2\6=Sdu";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}