package ujf.verimag.sbip.parser.ltl;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.BailErrorStrategy;
import org.antlr.v4.runtime.CommonTokenStream;

import ujf.verimag.sbip.parser.formula.LtlFormula;

public class LtlExprParser {
	
	public LtlFormula parse(String strFormula){
		
		BoundedLtlLexer lexer = new BoundedLtlLexer(new ANTLRInputStream(strFormula));
		BoundedLtlParser parser = new BoundedLtlParser(new CommonTokenStream(lexer));
		
		lexer.removeErrorListeners();
		parser.removeErrorListeners();
		parser.setErrorHandler(new BailErrorStrategy());
		parser.setBuildParseTree(false);
		
		LtlFormula phi = (LtlFormula)parser.pbltl().pf.getFormula();
		
		return phi;
	}

}