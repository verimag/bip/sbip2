grammar BoundedLtl;

@header{
	import ujf.verimag.sbip.parser.formula.*;
}

@members{
	private TemporalOperatorEnum getTempOperatorName(String op) {
		if(op.equalsIgnoreCase("U"))
			return TemporalOperatorEnum.U;
		if(op.equalsIgnoreCase("R")) 
			return TemporalOperatorEnum.R;
		if(op.equalsIgnoreCase("G"))
			return TemporalOperatorEnum.G;
		if(op.equalsIgnoreCase("F"))
			return TemporalOperatorEnum.F;
		if(op.equalsIgnoreCase("N"))
			return TemporalOperatorEnum.N;
		return null;
	}
	
	private BooleanOperatorEnum getBoolOperatorName(String op) {
		if(op.equalsIgnoreCase("!"))
			return BooleanOperatorEnum.NOT;
		if(op.equalsIgnoreCase("&&"))
			return BooleanOperatorEnum.AND;
		if(op.equalsIgnoreCase("||"))
			return BooleanOperatorEnum.OR;
		return null;
	}
	
	private ProbabilisticOperatorEnum getProbOperatorName(String op) {
		//System.out.println(op);
		if(op.equalsIgnoreCase("P>="))
			return ProbabilisticOperatorEnum.PHT_GE;
		if(op.equalsIgnoreCase("P<="))
			return ProbabilisticOperatorEnum.PHT_LE;
		if(op.equalsIgnoreCase("P=?"))
			return ProbabilisticOperatorEnum.PE;	
		return null;
		
	}
}

/*********************************************
*              Parser Rules
*********************************************/

pbltl returns [ProbabilisticFormula pf]
	: ht_op=HT_OP p=PROBA LBRCK a=bltl RBRCK EOF	{$pf = new ProbabilisticFormula(getProbOperatorName($ht_op.text), Double.valueOf($p.text), $a.f);}		
	| pe_op=PESTIM_OP LBRCK b=bltl RBRCK EOF		{$pf = new ProbabilisticFormula(getProbOperatorName($pe_op.text), $b.f);}
	;

bltl returns [Operator o, Formula f, String s]
	: ubo=unary_bounded_op a=bltl b=r_bo_or			{$o = new TemporalBoundedOperator($ubo.opn,$ubo.bound); $s = $ubo.text + $a.s + $b.s; if($b.s == "") $f = new LtlFormula(null, $a.f, $o); else $f = new LtlFormula($a.f, $b.f, new TemporalBoundedOperator($ubo.opn,$ubo.bound));}
	| c=conjunction d=r_bo_or						{$o = $d.o; $s = $c.s + $d.s; if($d.s == "") $f = $c.f; else $f = new LtlFormula($c.f, $d.f, $o);}
	;
	
	
r_bo_or returns [Operator o, Formula f, String s]
	: bbo=binary_bounded_op a=bltl b=r_bo_or		{$o = new TemporalBoundedOperator($bbo.opn,$bbo.bound); $s = $bbo.text + $a.s + $b.s; if($b.s == "") $f = $a.f; else $f = new LtlFormula($a.f, $b.f, $o);}
	| op=OR c=bltl d=r_bo_or						{$o = new BooleanOperator(getBoolOperatorName($op.text));$s = $op.text + $c.s + $d.s; if($d.s == "") $f = $c.f; else $f = new LtlFormula($c.f, $d.f, $o);}
	|												{$o = null; $s = ""; $f = null;}
	;
conjunction returns [Formula f, String s]
	: a=terminal b=r_and							{$s = $a.s + $b.s; if($b.s == "") $f = $a.f;else $f = new LtlFormula($a.f, $b.f, $b.o);}
	;
	
r_and returns [Operator o, Formula f, String s]
	: op=AND t=terminal r=r_and						{$o = new BooleanOperator(getBoolOperatorName($op.text)); $s = $op.text + $t.s + $r.s; if($r.s == "") $f = $t.f; else $f = new LtlFormula($t.f, $r.f, $o);}
	|												{$o = null; $s = ""; $f = null;}
	;
	
terminal returns [Formula f, String s]
	: LPAR a=bltl RPAR								{$s = "(" + $a.s + ")"; $f = $a.f;}
	| b=bool_atom									{$s = $b.text; $f = new LtlFormula(null, new StateFormula($b.text), null);}		
	;
	
bool_atom
	: ID
	| '!' ID
	| TRUTH_VAL
	| rel_expr
	;

rel_expr
	: arith_expr REL_OP arith_expr
	;

arith_expr
	: arith_expr ARITH_OP arith_expr
	| FUNCTION LPAR arith_expr RPAR
	| INT
  	| ID
  	| LPAR arith_expr RPAR
	;
  	
unary_bounded_op returns [TemporalOperatorEnum opn, int bound]
	: UNARY_T_OP LBRCE INT RBRCE			{$opn = getTempOperatorName($UNARY_T_OP.text); $bound = Integer.parseInt($INT.text);}
	;

binary_bounded_op returns [TemporalOperatorEnum opn, int bound]
	:	BINARY_T_OP LBRCE INT RBRCE			{$opn = getTempOperatorName($BINARY_T_OP.text); $bound = Integer.parseInt($INT.text);}
	;

/*********************************************
*              Lexer Rules
*********************************************/

HT_OP 			:	'P>=' | 'P<=';
PESTIM_OP 		:   'P=?';
PROBA			:	'0' POINT DIGIT+;

FUNCTION        :   'abs';

BINARY_T_OP		:	'U'| 'R';
UNARY_T_OP		:	'G' | 'F' | 'N';

REL_OP			:	'>' | '<' | '==' | '!=' | '>=' | '<=' ;
ARITH_OP        :   '+' | '-' | '*' | '/' | '%';

AND				:	'&&';
OR 				:	'||';
TRUTH_VAL		:	'true' | 'false';

INT 			:	[1-9] DIGIT* | '0';
ID				:	LETTER(LETTER | DIGIT | '_' | POINT)*;

LPAR			: 	'(';
RPAR			: 	')';
LBRCE			:	'{';
RBRCE			:	'}';
LBRCK			:	'[';
RBRCK			:	']';

WS  			:  	[ \t\r\n]+ -> skip;

fragment LETTER	:	[a-zA-Z];
fragment DIGIT	:	[0-9];
fragment POINT	: 	'.';