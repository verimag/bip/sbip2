package ujf.verimag.sbip.parser.mtl;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.BailErrorStrategy;
import org.antlr.v4.runtime.CommonTokenStream;

import ujf.verimag.sbip.parser.formula.*;

@SuppressWarnings("deprecation")
public class MtlExprParser
{

	
	public MtlFormula parse(String strFormula){
		//strFormula = removeSpaces(strFormula);
		MtlLexer lexer = new MtlLexer(new ANTLRInputStream(strFormula));
		MtlParser parser = new MtlParser(new CommonTokenStream(lexer));
		
		lexer.removeErrorListeners();
		parser.removeErrorListeners();
		parser.setErrorHandler(new BailErrorStrategy());
		parser.setBuildParseTree(false);
		
		MtlFormula phi = parser.program().f;
		return phi;
	}

	public static String removeSpaces(String strFormula) {
		strFormula = strFormula.replaceAll("\\s","");
		return strFormula;
	}
}
