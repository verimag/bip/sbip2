grammar Mtl;

@header {
import ujf.verimag.sbip.parser.formula.*;
import ujf.verimag.sbip.parser.formula.Interval;
}

// productions for syntax analysis
program returns [MtlFormula f]: e=phi EOF
{
	$f = $e.f;
}
;


factor returns [MtlFormula f, String s]
:TRUE 
{
	$s = "#True";
	$f = new MtlFormula(null, new StateFormula("#True"), null);
}
| FALSE 
{
	$s = "#False";
	$f = new MtlFormula(null, new StateFormula("#False"), null);
}
| b = bool 
{
	$s = $b.s2;
	if($b.isEval)
	{		
		$f = (MtlFormula) ($b.b? new MtlFormula(null, new StateFormula("#True"), null) : new MtlFormula(null, new StateFormula("#False"), null) );
		$s = (String) ($b.b? "#True" : "#False") ;
	}
	else $f = new MtlFormula(null, new StateFormula($b.s2), null);
}
| NOT fac = factor 
{
	$s = "!" + $fac.s;
	$f = new MtlFormula( null, $fac.f, new BooleanOperator(BooleanOperatorEnum.NOT));
}
| '(' e=phi ')' 
{
	$s = "(" + $e.s + ")" ;
	$f = $e.f;
}
;

term returns [MtlFormula f, String s]
: e4=factor e5=term2
{
	$s =  $e4.s+ $e5.s;
	if($e5.s !="")
		$f = new MtlFormula( $e4.f, $e5.f, $e5.o);
	else 
		$f = $e4.f;
} 
;

term2 returns [Operator o, MtlFormula f, String s]
: AND e=factor e2=term2
{
	$s = "&&"+ $e.s+ $e2.s;
	$o = new BooleanOperator(BooleanOperatorEnum.AND);
	if($e2.s !="")
		$f = new MtlFormula( $e.f, $e2.f, $e2.o);
	else 
		$f = $e.f;
}
|
{
	$s="";
}
;

phi returns [MtlFormula f, String s]
: GLOBALLY i=interval e=phi e2=phi2
{
	$s = "G"+ $i.inter +"("+ $e.s+ $e2.s +")";
	if($e2.s !="")
		$f = new MtlFormula( 
						null, 
						new MtlFormula( $e.f, $e2.f, $e2.o), 
						new TemporalIntervalOperator(TemporalOperatorEnum.G, new Interval($i.inter) )
		);
	else 
		$f = new MtlFormula( 
						null, 
						$e.f, 
						new TemporalIntervalOperator(TemporalOperatorEnum.G, new Interval($i.inter) )
		);
		
	
}
| EVENTUALLY i=interval e=phi e2=phi2
{
	$s = "F"+ $i.inter +"("+ $e.s+ $e2.s +")";
	if($e2.s !="")
		$f = new MtlFormula(
						null,
						new MtlFormula( $e.f, $e2.f, $e2.o),
						new TemporalIntervalOperator(TemporalOperatorEnum.F, new Interval($i.inter) )
		);
		
		
	else 
		$f = new MtlFormula(
						null,
						$e.f,
						new TemporalIntervalOperator(TemporalOperatorEnum.F, new Interval($i.inter) )
		);
		
}
| NEXT e=phi e2=phi2
{
	$s = "N"+ "("+ $e.s+ $e2.s +")";
	if($e2.s !="")
		$f = new MtlFormula(
						null,
						new MtlFormula( $e.f, $e2.f, $e2.o),
						new TemporalIntervalOperator(TemporalOperatorEnum.N)
		);
		
	else 
		$f = new MtlFormula(
						null,
						$e.f,
						new TemporalIntervalOperator(TemporalOperatorEnum.N)
		);
		
}
| e3=term e2=phi2
{
	$s =  $e3.s+ $e2.s;
	if($e2.s !="")
		$f = new MtlFormula( $e3.f, $e2.f, $e2.o);
	else 
		$f = $e3.f;
} 
;

phi2 returns [Operator o, MtlFormula f, String s]
: OR e3=phi e2=phi2 // replaced e3 to take phi instead of term
{
	$s = "||"+ $e3.s+ $e2.s;	
	$o = new BooleanOperator(BooleanOperatorEnum.OR);
	if($e2.s !="")
		$f = new MtlFormula( $e3.f, $e2.f, $e2.o);
	else 
		$f = $e3.f;
}
| UNTIL i=interval e=phi e2=phi2
{
	$s = "U"+ $i.inter + $e.s+ $e2.s;
	$o = new TemporalIntervalOperator(TemporalOperatorEnum.U, new Interval($i.inter) );
	if($e2.s !="")
		$f = new MtlFormula( $e.f, $e2.f, $e2.o);
	else 
		$f = $e.f;
}
| RELEASE i=interval e=phi e2=phi2
{
	$s = "R"+ $i.inter + $e.s+ $e2.s;
	$o = new TemporalIntervalOperator(TemporalOperatorEnum.R, new Interval($i.inter) );
	if($e2.s !="")
		$f = new MtlFormula( $e.f, $e2.f, $e2.o);
	else 
		$f = $e.f;
}
|
{
	$s = "";
}
;


interval returns [Interval inter]
: '[' e1=DOUBLE ',' e2=DOUBLE ']'
{
	$inter = new Interval( Double.parseDouble($e1.text) , Double.parseDouble($e2.text), 0 );
}
| '[' e3=STRING ',' e2=DOUBLE ']'
{
	$inter = new Interval( ($e3.text) , Double.parseDouble($e2.text), 1 );
}
| '[' e1=DOUBLE ',' e3=STRING ']'
{
	$inter = new Interval( Double.parseDouble($e1.text) , ($e3.text), 2 );
}
;



//////////////////////////////
/// Boolean expressions parser
//////////////////////////////


bool returns [boolean b, String s, String s2, boolean isEval]
: e=expr LT e2=expr
{
	$s = $e.s + "<" + $e2.s;
	$s2 = ($e.isEval? $e.d : $e.s2 )+ "<" + ($e2.isEval? $e2.d : $e2.s2);
	if($e.isEval && $e2.isEval){
		$b = $e.d < $e2.d;
		$isEval=true;
	}
	else
	{ $isEval = false;}
}
| e=expr LEQ e2=expr
{
	$s = $e.s + "<=" + $e2.s;	
	$s2 = ($e.isEval? $e.d : $e.s2 )+ "<=" + ($e2.isEval? $e2.d : $e2.s2);
	if($e.isEval && $e2.isEval){
		$b = $e.d <= $e2.d;
		$isEval=true;
	}
	else
	{ $isEval = false;}
}
|e=expr GT e2=expr
{
	$s = $e.s + ">" + $e2.s;
	$s2 = ($e.isEval? $e.d : $e.s2 )+ ">" + ($e2.isEval? $e2.d : $e2.s2);
	if($e.isEval && $e2.isEval){
		$b = $e.d > $e2.d;
		$isEval=true;
	}
	else
	{ $isEval = false;}
}
| e=expr GEQ e2=expr
{
	$s = $e.s + ">=" + $e2.s;	
	$s2 = ($e.isEval? $e.d : $e.s2 )+ ">=" + ($e2.isEval? $e2.d : $e2.s2);
	if($e.isEval && $e2.isEval){
		$b = $e.d >= $e2.d;
		$isEval=true;
	}
	else
	{ $isEval = false;}
}
|e=expr EQ e2=expr
{
	$s = $e.s + "==" + $e2.s;
	$s2 = ($e.isEval? $e.d : $e.s2 )+ "==" + ($e2.isEval? $e2.d : $e2.s2);
	if($e.isEval && $e2.isEval){
		
		$b = (Interval.round($e.d, 15) == Interval.round($e2.d, 15));
		$isEval=true;
	}
	else
	{ $isEval = false;}
}
| e=expr NEQ e2=expr
{
	$s = $e.s + "!=" + $e2.s;	
	$s2 = ($e.isEval? $e.d : $e.s2 )+ "!=" + ($e2.isEval? $e2.d : $e2.s2);
	if($e.isEval && $e2.isEval){
		$b = $e.d != $e2.d;
		$isEval=true;
	}
	else
	{ $isEval = false;}
}
| e=expr
{
	$s = $e.s;
	$s2 = ""+ ($e.isEval? $e.d : $e.s2);
	if($e.isEval){
		
		double d = $e.d;
		$b = 0 < d;
		$isEval = true;
	}
	else
	{$isEval = false;}
}
;

operand returns [String s, String s2, boolean isEval, Double d]
: STRING
{
	$s = $STRING.text;
	$s2 = $s;
	$isEval = false;
}
| DOUBLE
{
	$s = $DOUBLE.text;
	$isEval = true;
	$d = Double.parseDouble($DOUBLE.text);
	$s2 = ""+ $d;
}
| f = function
{
	$s = $f.s;	
	$s2 = ""+ ($f.isEval? $f.d : $f.s2);
	if($f.isEval){
		$d = $f.d;
		$isEval = true;
	}
	else $isEval = false;
}
| '-' o = operand
{
	$s = "-" + $o.s;	
	$s2 = ""+ ($o.isEval? -$o.d : ("-" +  $o.s2));
	$isEval = $o.isEval;
	if($isEval)
		$d = -$o.d;
}
| '(' e=expr ')'
{
	$s = "(" + $e.s + ")";	
	$s2 = ""+  ($e.isEval? $e.d : ("(" +$e.s2+ ")"));
	$isEval = $e.isEval;
	if($isEval)
		$d = $e.d;
}
;

expr returns [String s, String s2, Double d, boolean isEval]
: t=termT e=expr2
{
	$s = $t.s + $e.s;
	$s2 = ""+  ($t.isEval? $t.d : $t.s2  )+ ($e.isEval && $e.ID_op!=0? 
	($e.ID_op== 1? "+":"-")+ 
	 $e.d : $e.s2);
		
	if($e.isEval && $t.isEval)
	{
		$isEval = true;
		switch($e.ID_op){
			case 0:
				$d = $t.d;
				break;
			case 1:				
				$d = $t.d + $e.d;
				break;
			case 2:
				$d = $t.d - $e.d;
				break;
		}
		$s2 = ""+ $d;
	}
	else $isEval = false;

}
;

expr2 returns [String s, String s2, Double d, boolean isEval, int ID_op]
: '+' t=termT e=expr2
{
	$ID_op = 1;
	$s = "+" +$t.s + $e.s;
	
	$s2 = "+" + ($t.isEval? $t.d : $t.s2  )+ ($e.isEval && $e.ID_op!=0? ($e.ID_op== 1? "+":"-")+ $e.d : $e.s2);
	if($e.isEval && $t.isEval)
	{
		$isEval = true;
		switch($e.ID_op){
			case 0:
				$d = $t.d;
				break;
			case 1:				
				$d = $t.d + $e.d;
				break;
			case 2:
				$d = $t.d - $e.d;
				break;
		}
		$s2 = "+" + $d;
	}
	else $isEval = false;
	
}
| '-' t=termT e=expr2
{
	$ID_op = 2;
	$s = "-" +$t.s + $e.s;	
	$s2 = "-" + ($t.isEval? $t.d : $t.s2 ) + ($e.isEval && $e.ID_op!=0? ($e.ID_op== 1? "+":"-")+ $e.d : $e.s2);
	if($e.isEval && $t.isEval)
	{
		$isEval = true;
		switch($e.ID_op){
			case 0:
				$d = $t.d;
				break;
			case 1:				
				$d = $t.d + $e.d;
				break;
			case 2:
				$d = $t.d - $e.d;
				break;
		}
		$s2 = "-" + $d;
	}
	else $isEval = false;
	
}
|
{
	$s = "";
	$s2 = "";
	$ID_op = 0;
	$isEval = true;
}
;

termT returns [String s, String s2, Double d, boolean isEval]
: f=operand t=termT2
{
	$s = $f.s + $t.s;		
	$s2 = ""+  ( $f.isEval? $f.d : $f.s2 ) + ($t.isEval && $t.ID_op!=0? 
	($t.ID_op== 1? "*":"/")+ $t.d : $t.s2);
	if($f.isEval && $t.isEval)
	{
		$isEval = true;
		switch($t.ID_op){
			case 0:
				$d = $f.d;
				break;
			case 1:				
				$d = $f.d * $t.d;
				break;
			case 2:
				$d = $f.d / $t.d;
				break;
		}
	}
	else $isEval = false;
}
;

termT2 returns [String s, String s2, Double d, boolean isEval, int ID_op]
: '*' f=operand t=termT2
{
	$s = "*" + $f.s + $t.s;	
	$s2 = "*" + ($f.isEval? $f.d : $f.s2  )+( $t.isEval && $t.ID_op!=0? ($t.ID_op== 1? "*":"/")+ $t.d : $t.s2);
	$ID_op = 1;
	if($f.isEval && $t.isEval)
	{
		$isEval = true;
		switch($t.ID_op){
			case 0:
				$d = $f.d;
				break;
			case 1:				
				$d = $f.d * $t.d;
				break;
			case 2:
				$d = $f.d / $t.d;
				break;
		}
		$s2 = "*" + $d;
	}
	else $isEval = false;	
}

| '/' f=operand t=termT2
{
	$s = "/" + $f.s + $t.s;	
	$s2 = "/" +  ($f.isEval? $f.d : $f.s2 ) + ($t.isEval && $t.ID_op!=0? ($t.ID_op== 1? "*":"/")+ $t.d : $t.s2);
	$ID_op = 2;
	if($f.isEval && $t.isEval)
	{
		$isEval = true;
		switch($t.ID_op){
			case 0:
				$d = $f.d;
				break;
			case 1:				
				$d = $f.d * $t.d;
				break;
			case 2:
				$d = $f.d / $t.d;
				break;
		}
		$s2 = "/" + $d;
	}
	else $isEval = false;	
}
|
{
	$s = "";
	$s2 = "";
	$ID_op = 0;
	$isEval = true;
}
;







function returns [String s, String s2, Double d, boolean isEval]
: STRING '(' e=expr ')'
{
	switch($STRING.text){
		case "exp":
			$s = $STRING.text + "(" + $e.s+ ")";			
			if($e.isEval){
				$d = Math.exp($e.d);
				$isEval = true;
				$s2 = ""+ $d;
			}
			else{
				$isEval = false;
				$s2 = $STRING.text + "(" + $e.s2+ ")";
			}
			break;
			
		case "abs":
			$s = $STRING.text + "(" + $e.s+ ")";			
			if($e.isEval){
				$d = Math.abs($e.d);
				$isEval = true;
				$s2 = ""+ $d;
			}
			else{
				$isEval = false;
				$s2 = $STRING.text + "(" + $e.s2+ ")" ;
			}
			break;
	}
	
}
;






// Lexer //([a-zA-Z])+('.'([a-zA-Z])+)?;
TRUE : '#True';
FALSE : '#False';
NEQ           : '!=';
LEQ            : '<=';
GEQ            : '>=';
EQ              : '==';
LT              : '<';
GT              : '>';
UNTIL : 'U';
RELEASE : 'R';
EVENTUALLY : 'F';
GLOBALLY : 'G';
NEXT : 'N';
DOUBLE : [1-9]+[0-9]*('.' [0-9]+)? | ('0' ('.' [0-9]+)?);
LONG : [0-9]+;
STRING : ([a-zA-Z])+[0-9]*([a-zA-Z] | '.' | '_')*; 
NOT : '!';
AND : '&&';
OR : '||';
WS : [ \n\t\r]+ -> skip;