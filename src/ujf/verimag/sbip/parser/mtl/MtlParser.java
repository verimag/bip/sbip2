// Generated from Mtl.g4 by ANTLR 4.7
package ujf.verimag.sbip.parser.mtl;

import ujf.verimag.sbip.parser.formula.*;
import ujf.verimag.sbip.parser.formula.Interval;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MtlParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		TRUE=10, FALSE=11, NEQ=12, LEQ=13, GEQ=14, EQ=15, LT=16, GT=17, UNTIL=18, 
		RELEASE=19, EVENTUALLY=20, GLOBALLY=21, NEXT=22, DOUBLE=23, LONG=24, STRING=25, 
		NOT=26, AND=27, OR=28, WS=29;
	public static final int
		RULE_program = 0, RULE_factor = 1, RULE_term = 2, RULE_term2 = 3, RULE_phi = 4, 
		RULE_phi2 = 5, RULE_interval = 6, RULE_bool = 7, RULE_operand = 8, RULE_expr = 9, 
		RULE_expr2 = 10, RULE_termT = 11, RULE_termT2 = 12, RULE_function = 13;
	public static final String[] ruleNames = {
		"program", "factor", "term", "term2", "phi", "phi2", "interval", "bool", 
		"operand", "expr", "expr2", "termT", "termT2", "function"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'('", "')'", "'['", "','", "']'", "'-'", "'+'", "'*'", "'/'", "'#True'", 
		"'#False'", "'!='", "'<='", "'>='", "'=='", "'<'", "'>'", "'U'", "'R'", 
		"'F'", "'G'", "'N'", null, null, null, "'!'", "'&&'", "'||'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, "TRUE", "FALSE", 
		"NEQ", "LEQ", "GEQ", "EQ", "LT", "GT", "UNTIL", "RELEASE", "EVENTUALLY", 
		"GLOBALLY", "NEXT", "DOUBLE", "LONG", "STRING", "NOT", "AND", "OR", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Mtl.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public MtlParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public MtlFormula f;
		public PhiContext e;
		public TerminalNode EOF() { return getToken(MtlParser.EOF, 0); }
		public PhiContext phi() {
			return getRuleContext(PhiContext.class,0);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).exitProgram(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(28);
			((ProgramContext)_localctx).e = phi();
			setState(29);
			match(EOF);

				((ProgramContext)_localctx).f =  ((ProgramContext)_localctx).e.f;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FactorContext extends ParserRuleContext {
		public MtlFormula f;
		public String s;
		public BoolContext b;
		public FactorContext fac;
		public PhiContext e;
		public TerminalNode TRUE() { return getToken(MtlParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(MtlParser.FALSE, 0); }
		public BoolContext bool() {
			return getRuleContext(BoolContext.class,0);
		}
		public TerminalNode NOT() { return getToken(MtlParser.NOT, 0); }
		public FactorContext factor() {
			return getRuleContext(FactorContext.class,0);
		}
		public PhiContext phi() {
			return getRuleContext(PhiContext.class,0);
		}
		public FactorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_factor; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).enterFactor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).exitFactor(this);
		}
	}

	public final FactorContext factor() throws RecognitionException {
		FactorContext _localctx = new FactorContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_factor);
		try {
			setState(48);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(32);
				match(TRUE);

					((FactorContext)_localctx).s =  "#True";
					((FactorContext)_localctx).f =  new MtlFormula(null, new StateFormula("#True"), null);

				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(34);
				match(FALSE);

					((FactorContext)_localctx).s =  "#False";
					((FactorContext)_localctx).f =  new MtlFormula(null, new StateFormula("#False"), null);

				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(36);
				((FactorContext)_localctx).b = bool();

					((FactorContext)_localctx).s =  ((FactorContext)_localctx).b.s2;
					if(((FactorContext)_localctx).b.isEval)
					{		
						((FactorContext)_localctx).f =  (MtlFormula) (((FactorContext)_localctx).b.b? new MtlFormula(null, new StateFormula("#True"), null) : new MtlFormula(null, new StateFormula("#False"), null) );
						((FactorContext)_localctx).s =  (String) (((FactorContext)_localctx).b.b? "#True" : "#False") ;
					}
					else ((FactorContext)_localctx).f =  new MtlFormula(null, new StateFormula(((FactorContext)_localctx).b.s2), null);

				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(39);
				match(NOT);
				setState(40);
				((FactorContext)_localctx).fac = factor();

					((FactorContext)_localctx).s =  "!" + ((FactorContext)_localctx).fac.s;
					((FactorContext)_localctx).f =  new MtlFormula( null, ((FactorContext)_localctx).fac.f, new BooleanOperator(BooleanOperatorEnum.NOT));

				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(43);
				match(T__0);
				setState(44);
				((FactorContext)_localctx).e = phi();
				setState(45);
				match(T__1);

					((FactorContext)_localctx).s =  "(" + ((FactorContext)_localctx).e.s + ")" ;
					((FactorContext)_localctx).f =  ((FactorContext)_localctx).e.f;

				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TermContext extends ParserRuleContext {
		public MtlFormula f;
		public String s;
		public FactorContext e4;
		public Term2Context e5;
		public FactorContext factor() {
			return getRuleContext(FactorContext.class,0);
		}
		public Term2Context term2() {
			return getRuleContext(Term2Context.class,0);
		}
		public TermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_term; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).enterTerm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).exitTerm(this);
		}
	}

	public final TermContext term() throws RecognitionException {
		TermContext _localctx = new TermContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_term);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(50);
			((TermContext)_localctx).e4 = factor();
			setState(51);
			((TermContext)_localctx).e5 = term2();

				((TermContext)_localctx).s =   ((TermContext)_localctx).e4.s+ ((TermContext)_localctx).e5.s;
				if(((TermContext)_localctx).e5.s !="")
					((TermContext)_localctx).f =  new MtlFormula( ((TermContext)_localctx).e4.f, ((TermContext)_localctx).e5.f, ((TermContext)_localctx).e5.o);
				else 
					((TermContext)_localctx).f =  ((TermContext)_localctx).e4.f;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Term2Context extends ParserRuleContext {
		public Operator o;
		public MtlFormula f;
		public String s;
		public FactorContext e;
		public Term2Context e2;
		public TerminalNode AND() { return getToken(MtlParser.AND, 0); }
		public FactorContext factor() {
			return getRuleContext(FactorContext.class,0);
		}
		public Term2Context term2() {
			return getRuleContext(Term2Context.class,0);
		}
		public Term2Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_term2; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).enterTerm2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).exitTerm2(this);
		}
	}

	public final Term2Context term2() throws RecognitionException {
		Term2Context _localctx = new Term2Context(_ctx, getState());
		enterRule(_localctx, 6, RULE_term2);
		try {
			setState(60);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case AND:
				enterOuterAlt(_localctx, 1);
				{
				setState(54);
				match(AND);
				setState(55);
				((Term2Context)_localctx).e = factor();
				setState(56);
				((Term2Context)_localctx).e2 = term2();

					((Term2Context)_localctx).s =  "&&"+ ((Term2Context)_localctx).e.s+ ((Term2Context)_localctx).e2.s;
					((Term2Context)_localctx).o =  new BooleanOperator(BooleanOperatorEnum.AND);
					if(((Term2Context)_localctx).e2.s !="")
						((Term2Context)_localctx).f =  new MtlFormula( ((Term2Context)_localctx).e.f, ((Term2Context)_localctx).e2.f, ((Term2Context)_localctx).e2.o);
					else 
						((Term2Context)_localctx).f =  ((Term2Context)_localctx).e.f;

				}
				break;
			case EOF:
			case T__1:
			case UNTIL:
			case RELEASE:
			case OR:
				enterOuterAlt(_localctx, 2);
				{

					((Term2Context)_localctx).s = "";

				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PhiContext extends ParserRuleContext {
		public MtlFormula f;
		public String s;
		public IntervalContext i;
		public PhiContext e;
		public Phi2Context e2;
		public TermContext e3;
		public TerminalNode GLOBALLY() { return getToken(MtlParser.GLOBALLY, 0); }
		public IntervalContext interval() {
			return getRuleContext(IntervalContext.class,0);
		}
		public PhiContext phi() {
			return getRuleContext(PhiContext.class,0);
		}
		public Phi2Context phi2() {
			return getRuleContext(Phi2Context.class,0);
		}
		public TerminalNode EVENTUALLY() { return getToken(MtlParser.EVENTUALLY, 0); }
		public TerminalNode NEXT() { return getToken(MtlParser.NEXT, 0); }
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public PhiContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_phi; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).enterPhi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).exitPhi(this);
		}
	}

	public final PhiContext phi() throws RecognitionException {
		PhiContext _localctx = new PhiContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_phi);
		try {
			setState(83);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case GLOBALLY:
				enterOuterAlt(_localctx, 1);
				{
				setState(62);
				match(GLOBALLY);
				setState(63);
				((PhiContext)_localctx).i = interval();
				setState(64);
				((PhiContext)_localctx).e = phi();
				setState(65);
				((PhiContext)_localctx).e2 = phi2();

					((PhiContext)_localctx).s =  "G"+ ((PhiContext)_localctx).i.inter +"("+ ((PhiContext)_localctx).e.s+ ((PhiContext)_localctx).e2.s +")";
					if(((PhiContext)_localctx).e2.s !="")
						((PhiContext)_localctx).f =  new MtlFormula( 
										null, 
										new MtlFormula( ((PhiContext)_localctx).e.f, ((PhiContext)_localctx).e2.f, ((PhiContext)_localctx).e2.o), 
										new TemporalIntervalOperator(TemporalOperatorEnum.G, new Interval(((PhiContext)_localctx).i.inter) )
						);
					else 
						((PhiContext)_localctx).f =  new MtlFormula( 
										null, 
										((PhiContext)_localctx).e.f, 
										new TemporalIntervalOperator(TemporalOperatorEnum.G, new Interval(((PhiContext)_localctx).i.inter) )
						);
						
					

				}
				break;
			case EVENTUALLY:
				enterOuterAlt(_localctx, 2);
				{
				setState(68);
				match(EVENTUALLY);
				setState(69);
				((PhiContext)_localctx).i = interval();
				setState(70);
				((PhiContext)_localctx).e = phi();
				setState(71);
				((PhiContext)_localctx).e2 = phi2();

					((PhiContext)_localctx).s =  "F"+ ((PhiContext)_localctx).i.inter +"("+ ((PhiContext)_localctx).e.s+ ((PhiContext)_localctx).e2.s +")";
					if(((PhiContext)_localctx).e2.s !="")
						((PhiContext)_localctx).f =  new MtlFormula(
										null,
										new MtlFormula( ((PhiContext)_localctx).e.f, ((PhiContext)_localctx).e2.f, ((PhiContext)_localctx).e2.o),
										new TemporalIntervalOperator(TemporalOperatorEnum.F, new Interval(((PhiContext)_localctx).i.inter) )
						);
						
						
					else 
						((PhiContext)_localctx).f =  new MtlFormula(
										null,
										((PhiContext)_localctx).e.f,
										new TemporalIntervalOperator(TemporalOperatorEnum.F, new Interval(((PhiContext)_localctx).i.inter) )
						);
						

				}
				break;
			case NEXT:
				enterOuterAlt(_localctx, 3);
				{
				setState(74);
				match(NEXT);
				setState(75);
				((PhiContext)_localctx).e = phi();
				setState(76);
				((PhiContext)_localctx).e2 = phi2();

					((PhiContext)_localctx).s =  "N"+ "("+ ((PhiContext)_localctx).e.s+ ((PhiContext)_localctx).e2.s +")";
					if(((PhiContext)_localctx).e2.s !="")
						((PhiContext)_localctx).f =  new MtlFormula(
										null,
										new MtlFormula( ((PhiContext)_localctx).e.f, ((PhiContext)_localctx).e2.f, ((PhiContext)_localctx).e2.o),
										new TemporalIntervalOperator(TemporalOperatorEnum.N)
						);
						
					else 
						((PhiContext)_localctx).f =  new MtlFormula(
										null,
										((PhiContext)_localctx).e.f,
										new TemporalIntervalOperator(TemporalOperatorEnum.N)
						);
						

				}
				break;
			case T__0:
			case T__5:
			case TRUE:
			case FALSE:
			case DOUBLE:
			case STRING:
			case NOT:
				enterOuterAlt(_localctx, 4);
				{
				setState(79);
				((PhiContext)_localctx).e3 = term();
				setState(80);
				((PhiContext)_localctx).e2 = phi2();

					((PhiContext)_localctx).s =   ((PhiContext)_localctx).e3.s+ ((PhiContext)_localctx).e2.s;
					if(((PhiContext)_localctx).e2.s !="")
						((PhiContext)_localctx).f =  new MtlFormula( ((PhiContext)_localctx).e3.f, ((PhiContext)_localctx).e2.f, ((PhiContext)_localctx).e2.o);
					else 
						((PhiContext)_localctx).f =  ((PhiContext)_localctx).e3.f;

				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Phi2Context extends ParserRuleContext {
		public Operator o;
		public MtlFormula f;
		public String s;
		public PhiContext e3;
		public Phi2Context e2;
		public IntervalContext i;
		public PhiContext e;
		public TerminalNode OR() { return getToken(MtlParser.OR, 0); }
		public PhiContext phi() {
			return getRuleContext(PhiContext.class,0);
		}
		public Phi2Context phi2() {
			return getRuleContext(Phi2Context.class,0);
		}
		public TerminalNode UNTIL() { return getToken(MtlParser.UNTIL, 0); }
		public IntervalContext interval() {
			return getRuleContext(IntervalContext.class,0);
		}
		public TerminalNode RELEASE() { return getToken(MtlParser.RELEASE, 0); }
		public Phi2Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_phi2; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).enterPhi2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).exitPhi2(this);
		}
	}

	public final Phi2Context phi2() throws RecognitionException {
		Phi2Context _localctx = new Phi2Context(_ctx, getState());
		enterRule(_localctx, 10, RULE_phi2);
		try {
			setState(103);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(85);
				match(OR);
				setState(86);
				((Phi2Context)_localctx).e3 = phi();
				setState(87);
				((Phi2Context)_localctx).e2 = phi2();

					((Phi2Context)_localctx).s =  "||"+ ((Phi2Context)_localctx).e3.s+ ((Phi2Context)_localctx).e2.s;	
					((Phi2Context)_localctx).o =  new BooleanOperator(BooleanOperatorEnum.OR);
					if(((Phi2Context)_localctx).e2.s !="")
						((Phi2Context)_localctx).f =  new MtlFormula( ((Phi2Context)_localctx).e3.f, ((Phi2Context)_localctx).e2.f, ((Phi2Context)_localctx).e2.o);
					else 
						((Phi2Context)_localctx).f =  ((Phi2Context)_localctx).e3.f;

				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(90);
				match(UNTIL);
				setState(91);
				((Phi2Context)_localctx).i = interval();
				setState(92);
				((Phi2Context)_localctx).e = phi();
				setState(93);
				((Phi2Context)_localctx).e2 = phi2();

					((Phi2Context)_localctx).s =  "U"+ ((Phi2Context)_localctx).i.inter + ((Phi2Context)_localctx).e.s+ ((Phi2Context)_localctx).e2.s;
					((Phi2Context)_localctx).o =  new TemporalIntervalOperator(TemporalOperatorEnum.U, new Interval(((Phi2Context)_localctx).i.inter) );
					if(((Phi2Context)_localctx).e2.s !="")
						((Phi2Context)_localctx).f =  new MtlFormula( ((Phi2Context)_localctx).e.f, ((Phi2Context)_localctx).e2.f, ((Phi2Context)_localctx).e2.o);
					else 
						((Phi2Context)_localctx).f =  ((Phi2Context)_localctx).e.f;

				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(96);
				match(RELEASE);
				setState(97);
				((Phi2Context)_localctx).i = interval();
				setState(98);
				((Phi2Context)_localctx).e = phi();
				setState(99);
				((Phi2Context)_localctx).e2 = phi2();

					((Phi2Context)_localctx).s =  "R"+ ((Phi2Context)_localctx).i.inter + ((Phi2Context)_localctx).e.s+ ((Phi2Context)_localctx).e2.s;
					((Phi2Context)_localctx).o =  new TemporalIntervalOperator(TemporalOperatorEnum.R, new Interval(((Phi2Context)_localctx).i.inter) );
					if(((Phi2Context)_localctx).e2.s !="")
						((Phi2Context)_localctx).f =  new MtlFormula( ((Phi2Context)_localctx).e.f, ((Phi2Context)_localctx).e2.f, ((Phi2Context)_localctx).e2.o);
					else 
						((Phi2Context)_localctx).f =  ((Phi2Context)_localctx).e.f;

				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{

					((Phi2Context)_localctx).s =  "";

				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntervalContext extends ParserRuleContext {
		public Interval inter;
		public Token e1;
		public Token e2;
		public Token e3;
		public List<TerminalNode> DOUBLE() { return getTokens(MtlParser.DOUBLE); }
		public TerminalNode DOUBLE(int i) {
			return getToken(MtlParser.DOUBLE, i);
		}
		public TerminalNode STRING() { return getToken(MtlParser.STRING, 0); }
		public IntervalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_interval; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).enterInterval(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).exitInterval(this);
		}
	}

	public final IntervalContext interval() throws RecognitionException {
		IntervalContext _localctx = new IntervalContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_interval);
		try {
			setState(123);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(105);
				match(T__2);
				setState(106);
				((IntervalContext)_localctx).e1 = match(DOUBLE);
				setState(107);
				match(T__3);
				setState(108);
				((IntervalContext)_localctx).e2 = match(DOUBLE);
				setState(109);
				match(T__4);

					((IntervalContext)_localctx).inter =  new Interval( Double.parseDouble((((IntervalContext)_localctx).e1!=null?((IntervalContext)_localctx).e1.getText():null)) , Double.parseDouble((((IntervalContext)_localctx).e2!=null?((IntervalContext)_localctx).e2.getText():null)), 0 );

				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(111);
				match(T__2);
				setState(112);
				((IntervalContext)_localctx).e3 = match(STRING);
				setState(113);
				match(T__3);
				setState(114);
				((IntervalContext)_localctx).e2 = match(DOUBLE);
				setState(115);
				match(T__4);

					((IntervalContext)_localctx).inter =  new Interval( ((((IntervalContext)_localctx).e3!=null?((IntervalContext)_localctx).e3.getText():null)) , Double.parseDouble((((IntervalContext)_localctx).e2!=null?((IntervalContext)_localctx).e2.getText():null)), 1 );

				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(117);
				match(T__2);
				setState(118);
				((IntervalContext)_localctx).e1 = match(DOUBLE);
				setState(119);
				match(T__3);
				setState(120);
				((IntervalContext)_localctx).e3 = match(STRING);
				setState(121);
				match(T__4);

					((IntervalContext)_localctx).inter =  new Interval( Double.parseDouble((((IntervalContext)_localctx).e1!=null?((IntervalContext)_localctx).e1.getText():null)) , ((((IntervalContext)_localctx).e3!=null?((IntervalContext)_localctx).e3.getText():null)), 2 );

				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BoolContext extends ParserRuleContext {
		public boolean b;
		public String s;
		public String s2;
		public boolean isEval;
		public ExprContext e;
		public ExprContext e2;
		public TerminalNode LT() { return getToken(MtlParser.LT, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode LEQ() { return getToken(MtlParser.LEQ, 0); }
		public TerminalNode GT() { return getToken(MtlParser.GT, 0); }
		public TerminalNode GEQ() { return getToken(MtlParser.GEQ, 0); }
		public TerminalNode EQ() { return getToken(MtlParser.EQ, 0); }
		public TerminalNode NEQ() { return getToken(MtlParser.NEQ, 0); }
		public BoolContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bool; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).enterBool(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).exitBool(this);
		}
	}

	public final BoolContext bool() throws RecognitionException {
		BoolContext _localctx = new BoolContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_bool);
		try {
			setState(158);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(125);
				((BoolContext)_localctx).e = expr();
				setState(126);
				match(LT);
				setState(127);
				((BoolContext)_localctx).e2 = expr();

					((BoolContext)_localctx).s =  ((BoolContext)_localctx).e.s + "<" + ((BoolContext)_localctx).e2.s;
					((BoolContext)_localctx).s2 =  (((BoolContext)_localctx).e.isEval? ((BoolContext)_localctx).e.d : ((BoolContext)_localctx).e.s2 )+ "<" + (((BoolContext)_localctx).e2.isEval? ((BoolContext)_localctx).e2.d : ((BoolContext)_localctx).e2.s2);
					if(((BoolContext)_localctx).e.isEval && ((BoolContext)_localctx).e2.isEval){
						((BoolContext)_localctx).b =  ((BoolContext)_localctx).e.d < ((BoolContext)_localctx).e2.d;
						((BoolContext)_localctx).isEval = true;
					}
					else
					{ ((BoolContext)_localctx).isEval =  false;}

				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(130);
				((BoolContext)_localctx).e = expr();
				setState(131);
				match(LEQ);
				setState(132);
				((BoolContext)_localctx).e2 = expr();

					((BoolContext)_localctx).s =  ((BoolContext)_localctx).e.s + "<=" + ((BoolContext)_localctx).e2.s;	
					((BoolContext)_localctx).s2 =  (((BoolContext)_localctx).e.isEval? ((BoolContext)_localctx).e.d : ((BoolContext)_localctx).e.s2 )+ "<=" + (((BoolContext)_localctx).e2.isEval? ((BoolContext)_localctx).e2.d : ((BoolContext)_localctx).e2.s2);
					if(((BoolContext)_localctx).e.isEval && ((BoolContext)_localctx).e2.isEval){
						((BoolContext)_localctx).b =  ((BoolContext)_localctx).e.d <= ((BoolContext)_localctx).e2.d;
						((BoolContext)_localctx).isEval = true;
					}
					else
					{ ((BoolContext)_localctx).isEval =  false;}

				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(135);
				((BoolContext)_localctx).e = expr();
				setState(136);
				match(GT);
				setState(137);
				((BoolContext)_localctx).e2 = expr();

					((BoolContext)_localctx).s =  ((BoolContext)_localctx).e.s + ">" + ((BoolContext)_localctx).e2.s;
					((BoolContext)_localctx).s2 =  (((BoolContext)_localctx).e.isEval? ((BoolContext)_localctx).e.d : ((BoolContext)_localctx).e.s2 )+ ">" + (((BoolContext)_localctx).e2.isEval? ((BoolContext)_localctx).e2.d : ((BoolContext)_localctx).e2.s2);
					if(((BoolContext)_localctx).e.isEval && ((BoolContext)_localctx).e2.isEval){
						((BoolContext)_localctx).b =  ((BoolContext)_localctx).e.d > ((BoolContext)_localctx).e2.d;
						((BoolContext)_localctx).isEval = true;
					}
					else
					{ ((BoolContext)_localctx).isEval =  false;}

				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(140);
				((BoolContext)_localctx).e = expr();
				setState(141);
				match(GEQ);
				setState(142);
				((BoolContext)_localctx).e2 = expr();

					((BoolContext)_localctx).s =  ((BoolContext)_localctx).e.s + ">=" + ((BoolContext)_localctx).e2.s;	
					((BoolContext)_localctx).s2 =  (((BoolContext)_localctx).e.isEval? ((BoolContext)_localctx).e.d : ((BoolContext)_localctx).e.s2 )+ ">=" + (((BoolContext)_localctx).e2.isEval? ((BoolContext)_localctx).e2.d : ((BoolContext)_localctx).e2.s2);
					if(((BoolContext)_localctx).e.isEval && ((BoolContext)_localctx).e2.isEval){
						((BoolContext)_localctx).b =  ((BoolContext)_localctx).e.d >= ((BoolContext)_localctx).e2.d;
						((BoolContext)_localctx).isEval = true;
					}
					else
					{ ((BoolContext)_localctx).isEval =  false;}

				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(145);
				((BoolContext)_localctx).e = expr();
				setState(146);
				match(EQ);
				setState(147);
				((BoolContext)_localctx).e2 = expr();

					((BoolContext)_localctx).s =  ((BoolContext)_localctx).e.s + "==" + ((BoolContext)_localctx).e2.s;
					((BoolContext)_localctx).s2 =  (((BoolContext)_localctx).e.isEval? ((BoolContext)_localctx).e.d : ((BoolContext)_localctx).e.s2 )+ "==" + (((BoolContext)_localctx).e2.isEval? ((BoolContext)_localctx).e2.d : ((BoolContext)_localctx).e2.s2);
					if(((BoolContext)_localctx).e.isEval && ((BoolContext)_localctx).e2.isEval){
						
						((BoolContext)_localctx).b =  (Interval.round(((BoolContext)_localctx).e.d, 15) == Interval.round(((BoolContext)_localctx).e2.d, 15));
						((BoolContext)_localctx).isEval = true;
					}
					else
					{ ((BoolContext)_localctx).isEval =  false;}

				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(150);
				((BoolContext)_localctx).e = expr();
				setState(151);
				match(NEQ);
				setState(152);
				((BoolContext)_localctx).e2 = expr();

					((BoolContext)_localctx).s =  ((BoolContext)_localctx).e.s + "!=" + ((BoolContext)_localctx).e2.s;	
					((BoolContext)_localctx).s2 =  (((BoolContext)_localctx).e.isEval? ((BoolContext)_localctx).e.d : ((BoolContext)_localctx).e.s2 )+ "!=" + (((BoolContext)_localctx).e2.isEval? ((BoolContext)_localctx).e2.d : ((BoolContext)_localctx).e2.s2);
					if(((BoolContext)_localctx).e.isEval && ((BoolContext)_localctx).e2.isEval){
						((BoolContext)_localctx).b =  ((BoolContext)_localctx).e.d != ((BoolContext)_localctx).e2.d;
						((BoolContext)_localctx).isEval = true;
					}
					else
					{ ((BoolContext)_localctx).isEval =  false;}

				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(155);
				((BoolContext)_localctx).e = expr();

					((BoolContext)_localctx).s =  ((BoolContext)_localctx).e.s;
					((BoolContext)_localctx).s2 =  ""+ (((BoolContext)_localctx).e.isEval? ((BoolContext)_localctx).e.d : ((BoolContext)_localctx).e.s2);
					if(((BoolContext)_localctx).e.isEval){
						
						double d = ((BoolContext)_localctx).e.d;
						((BoolContext)_localctx).b =  0 < d;
						((BoolContext)_localctx).isEval =  true;
					}
					else
					{((BoolContext)_localctx).isEval =  false;}

				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OperandContext extends ParserRuleContext {
		public String s;
		public String s2;
		public boolean isEval;
		public Double d;
		public Token STRING;
		public Token DOUBLE;
		public FunctionContext f;
		public OperandContext o;
		public ExprContext e;
		public TerminalNode STRING() { return getToken(MtlParser.STRING, 0); }
		public TerminalNode DOUBLE() { return getToken(MtlParser.DOUBLE, 0); }
		public FunctionContext function() {
			return getRuleContext(FunctionContext.class,0);
		}
		public OperandContext operand() {
			return getRuleContext(OperandContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public OperandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).enterOperand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).exitOperand(this);
		}
	}

	public final OperandContext operand() throws RecognitionException {
		OperandContext _localctx = new OperandContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_operand);
		try {
			setState(176);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(160);
				((OperandContext)_localctx).STRING = match(STRING);

					((OperandContext)_localctx).s =  (((OperandContext)_localctx).STRING!=null?((OperandContext)_localctx).STRING.getText():null);
					((OperandContext)_localctx).s2 =  _localctx.s;
					((OperandContext)_localctx).isEval =  false;

				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(162);
				((OperandContext)_localctx).DOUBLE = match(DOUBLE);

					((OperandContext)_localctx).s =  (((OperandContext)_localctx).DOUBLE!=null?((OperandContext)_localctx).DOUBLE.getText():null);
					((OperandContext)_localctx).isEval =  true;
					((OperandContext)_localctx).d =  Double.parseDouble((((OperandContext)_localctx).DOUBLE!=null?((OperandContext)_localctx).DOUBLE.getText():null));
					((OperandContext)_localctx).s2 =  ""+ _localctx.d;

				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(164);
				((OperandContext)_localctx).f = function();

					((OperandContext)_localctx).s =  ((OperandContext)_localctx).f.s;	
					((OperandContext)_localctx).s2 =  ""+ (((OperandContext)_localctx).f.isEval? ((OperandContext)_localctx).f.d : ((OperandContext)_localctx).f.s2);
					if(((OperandContext)_localctx).f.isEval){
						((OperandContext)_localctx).d =  ((OperandContext)_localctx).f.d;
						((OperandContext)_localctx).isEval =  true;
					}
					else ((OperandContext)_localctx).isEval =  false;

				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(167);
				match(T__5);
				setState(168);
				((OperandContext)_localctx).o = operand();

					((OperandContext)_localctx).s =  "-" + ((OperandContext)_localctx).o.s;	
					((OperandContext)_localctx).s2 =  ""+ (((OperandContext)_localctx).o.isEval? -((OperandContext)_localctx).o.d : ("-" +  ((OperandContext)_localctx).o.s2));
					((OperandContext)_localctx).isEval =  ((OperandContext)_localctx).o.isEval;
					if(_localctx.isEval)
						((OperandContext)_localctx).d =  -((OperandContext)_localctx).o.d;

				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(171);
				match(T__0);
				setState(172);
				((OperandContext)_localctx).e = expr();
				setState(173);
				match(T__1);

					((OperandContext)_localctx).s =  "(" + ((OperandContext)_localctx).e.s + ")";	
					((OperandContext)_localctx).s2 =  ""+  (((OperandContext)_localctx).e.isEval? ((OperandContext)_localctx).e.d : ("(" +((OperandContext)_localctx).e.s2+ ")"));
					((OperandContext)_localctx).isEval =  ((OperandContext)_localctx).e.isEval;
					if(_localctx.isEval)
						((OperandContext)_localctx).d =  ((OperandContext)_localctx).e.d;

				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public String s;
		public String s2;
		public Double d;
		public boolean isEval;
		public TermTContext t;
		public Expr2Context e;
		public TermTContext termT() {
			return getRuleContext(TermTContext.class,0);
		}
		public Expr2Context expr2() {
			return getRuleContext(Expr2Context.class,0);
		}
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).enterExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).exitExpr(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		ExprContext _localctx = new ExprContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_expr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(178);
			((ExprContext)_localctx).t = termT();
			setState(179);
			((ExprContext)_localctx).e = expr2();

				((ExprContext)_localctx).s =  ((ExprContext)_localctx).t.s + ((ExprContext)_localctx).e.s;
				((ExprContext)_localctx).s2 =  ""+  (((ExprContext)_localctx).t.isEval? ((ExprContext)_localctx).t.d : ((ExprContext)_localctx).t.s2  )+ (((ExprContext)_localctx).e.isEval && ((ExprContext)_localctx).e.ID_op!=0? 
				(((ExprContext)_localctx).e.ID_op== 1? "+":"-")+ 
				 ((ExprContext)_localctx).e.d : ((ExprContext)_localctx).e.s2);
					
				if(((ExprContext)_localctx).e.isEval && ((ExprContext)_localctx).t.isEval)
				{
					((ExprContext)_localctx).isEval =  true;
					switch(((ExprContext)_localctx).e.ID_op){
						case 0:
							((ExprContext)_localctx).d =  ((ExprContext)_localctx).t.d;
							break;
						case 1:				
							((ExprContext)_localctx).d =  ((ExprContext)_localctx).t.d + ((ExprContext)_localctx).e.d;
							break;
						case 2:
							((ExprContext)_localctx).d =  ((ExprContext)_localctx).t.d - ((ExprContext)_localctx).e.d;
							break;
					}
					((ExprContext)_localctx).s2 =  ""+ _localctx.d;
				}
				else ((ExprContext)_localctx).isEval =  false;


			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr2Context extends ParserRuleContext {
		public String s;
		public String s2;
		public Double d;
		public boolean isEval;
		public int ID_op;
		public TermTContext t;
		public Expr2Context e;
		public TermTContext termT() {
			return getRuleContext(TermTContext.class,0);
		}
		public Expr2Context expr2() {
			return getRuleContext(Expr2Context.class,0);
		}
		public Expr2Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr2; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).enterExpr2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).exitExpr2(this);
		}
	}

	public final Expr2Context expr2() throws RecognitionException {
		Expr2Context _localctx = new Expr2Context(_ctx, getState());
		enterRule(_localctx, 20, RULE_expr2);
		try {
			setState(193);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__6:
				enterOuterAlt(_localctx, 1);
				{
				setState(182);
				match(T__6);
				setState(183);
				((Expr2Context)_localctx).t = termT();
				setState(184);
				((Expr2Context)_localctx).e = expr2();

					((Expr2Context)_localctx).ID_op =  1;
					((Expr2Context)_localctx).s =  "+" +((Expr2Context)_localctx).t.s + ((Expr2Context)_localctx).e.s;
					
					((Expr2Context)_localctx).s2 =  "+" + (((Expr2Context)_localctx).t.isEval? ((Expr2Context)_localctx).t.d : ((Expr2Context)_localctx).t.s2  )+ (((Expr2Context)_localctx).e.isEval && ((Expr2Context)_localctx).e.ID_op!=0? (((Expr2Context)_localctx).e.ID_op== 1? "+":"-")+ ((Expr2Context)_localctx).e.d : ((Expr2Context)_localctx).e.s2);
					if(((Expr2Context)_localctx).e.isEval && ((Expr2Context)_localctx).t.isEval)
					{
						((Expr2Context)_localctx).isEval =  true;
						switch(((Expr2Context)_localctx).e.ID_op){
							case 0:
								((Expr2Context)_localctx).d =  ((Expr2Context)_localctx).t.d;
								break;
							case 1:				
								((Expr2Context)_localctx).d =  ((Expr2Context)_localctx).t.d + ((Expr2Context)_localctx).e.d;
								break;
							case 2:
								((Expr2Context)_localctx).d =  ((Expr2Context)_localctx).t.d - ((Expr2Context)_localctx).e.d;
								break;
						}
						((Expr2Context)_localctx).s2 =  "+" + _localctx.d;
					}
					else ((Expr2Context)_localctx).isEval =  false;
					

				}
				break;
			case T__5:
				enterOuterAlt(_localctx, 2);
				{
				setState(187);
				match(T__5);
				setState(188);
				((Expr2Context)_localctx).t = termT();
				setState(189);
				((Expr2Context)_localctx).e = expr2();

					((Expr2Context)_localctx).ID_op =  2;
					((Expr2Context)_localctx).s =  "-" +((Expr2Context)_localctx).t.s + ((Expr2Context)_localctx).e.s;	
					((Expr2Context)_localctx).s2 =  "-" + (((Expr2Context)_localctx).t.isEval? ((Expr2Context)_localctx).t.d : ((Expr2Context)_localctx).t.s2 ) + (((Expr2Context)_localctx).e.isEval && ((Expr2Context)_localctx).e.ID_op!=0? (((Expr2Context)_localctx).e.ID_op== 1? "+":"-")+ ((Expr2Context)_localctx).e.d : ((Expr2Context)_localctx).e.s2);
					if(((Expr2Context)_localctx).e.isEval && ((Expr2Context)_localctx).t.isEval)
					{
						((Expr2Context)_localctx).isEval =  true;
						switch(((Expr2Context)_localctx).e.ID_op){
							case 0:
								((Expr2Context)_localctx).d =  ((Expr2Context)_localctx).t.d;
								break;
							case 1:				
								((Expr2Context)_localctx).d =  ((Expr2Context)_localctx).t.d + ((Expr2Context)_localctx).e.d;
								break;
							case 2:
								((Expr2Context)_localctx).d =  ((Expr2Context)_localctx).t.d - ((Expr2Context)_localctx).e.d;
								break;
						}
						((Expr2Context)_localctx).s2 =  "-" + _localctx.d;
					}
					else ((Expr2Context)_localctx).isEval =  false;
					

				}
				break;
			case EOF:
			case T__1:
			case NEQ:
			case LEQ:
			case GEQ:
			case EQ:
			case LT:
			case GT:
			case UNTIL:
			case RELEASE:
			case AND:
			case OR:
				enterOuterAlt(_localctx, 3);
				{

					((Expr2Context)_localctx).s =  "";
					((Expr2Context)_localctx).s2 =  "";
					((Expr2Context)_localctx).ID_op =  0;
					((Expr2Context)_localctx).isEval =  true;

				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TermTContext extends ParserRuleContext {
		public String s;
		public String s2;
		public Double d;
		public boolean isEval;
		public OperandContext f;
		public TermT2Context t;
		public OperandContext operand() {
			return getRuleContext(OperandContext.class,0);
		}
		public TermT2Context termT2() {
			return getRuleContext(TermT2Context.class,0);
		}
		public TermTContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_termT; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).enterTermT(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).exitTermT(this);
		}
	}

	public final TermTContext termT() throws RecognitionException {
		TermTContext _localctx = new TermTContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_termT);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(195);
			((TermTContext)_localctx).f = operand();
			setState(196);
			((TermTContext)_localctx).t = termT2();

				((TermTContext)_localctx).s =  ((TermTContext)_localctx).f.s + ((TermTContext)_localctx).t.s;		
				((TermTContext)_localctx).s2 =  ""+  ( ((TermTContext)_localctx).f.isEval? ((TermTContext)_localctx).f.d : ((TermTContext)_localctx).f.s2 ) + (((TermTContext)_localctx).t.isEval && ((TermTContext)_localctx).t.ID_op!=0? 
				(((TermTContext)_localctx).t.ID_op== 1? "*":"/")+ ((TermTContext)_localctx).t.d : ((TermTContext)_localctx).t.s2);
				if(((TermTContext)_localctx).f.isEval && ((TermTContext)_localctx).t.isEval)
				{
					((TermTContext)_localctx).isEval =  true;
					switch(((TermTContext)_localctx).t.ID_op){
						case 0:
							((TermTContext)_localctx).d =  ((TermTContext)_localctx).f.d;
							break;
						case 1:				
							((TermTContext)_localctx).d =  ((TermTContext)_localctx).f.d * ((TermTContext)_localctx).t.d;
							break;
						case 2:
							((TermTContext)_localctx).d =  ((TermTContext)_localctx).f.d / ((TermTContext)_localctx).t.d;
							break;
					}
				}
				else ((TermTContext)_localctx).isEval =  false;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TermT2Context extends ParserRuleContext {
		public String s;
		public String s2;
		public Double d;
		public boolean isEval;
		public int ID_op;
		public OperandContext f;
		public TermT2Context t;
		public OperandContext operand() {
			return getRuleContext(OperandContext.class,0);
		}
		public TermT2Context termT2() {
			return getRuleContext(TermT2Context.class,0);
		}
		public TermT2Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_termT2; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).enterTermT2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).exitTermT2(this);
		}
	}

	public final TermT2Context termT2() throws RecognitionException {
		TermT2Context _localctx = new TermT2Context(_ctx, getState());
		enterRule(_localctx, 24, RULE_termT2);
		try {
			setState(210);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__7:
				enterOuterAlt(_localctx, 1);
				{
				setState(199);
				match(T__7);
				setState(200);
				((TermT2Context)_localctx).f = operand();
				setState(201);
				((TermT2Context)_localctx).t = termT2();

					((TermT2Context)_localctx).s =  "*" + ((TermT2Context)_localctx).f.s + ((TermT2Context)_localctx).t.s;	
					((TermT2Context)_localctx).s2 =  "*" + (((TermT2Context)_localctx).f.isEval? ((TermT2Context)_localctx).f.d : ((TermT2Context)_localctx).f.s2  )+( ((TermT2Context)_localctx).t.isEval && ((TermT2Context)_localctx).t.ID_op!=0? (((TermT2Context)_localctx).t.ID_op== 1? "*":"/")+ ((TermT2Context)_localctx).t.d : ((TermT2Context)_localctx).t.s2);
					((TermT2Context)_localctx).ID_op =  1;
					if(((TermT2Context)_localctx).f.isEval && ((TermT2Context)_localctx).t.isEval)
					{
						((TermT2Context)_localctx).isEval =  true;
						switch(((TermT2Context)_localctx).t.ID_op){
							case 0:
								((TermT2Context)_localctx).d =  ((TermT2Context)_localctx).f.d;
								break;
							case 1:				
								((TermT2Context)_localctx).d =  ((TermT2Context)_localctx).f.d * ((TermT2Context)_localctx).t.d;
								break;
							case 2:
								((TermT2Context)_localctx).d =  ((TermT2Context)_localctx).f.d / ((TermT2Context)_localctx).t.d;
								break;
						}
						((TermT2Context)_localctx).s2 =  "*" + _localctx.d;
					}
					else ((TermT2Context)_localctx).isEval =  false;	

				}
				break;
			case T__8:
				enterOuterAlt(_localctx, 2);
				{
				setState(204);
				match(T__8);
				setState(205);
				((TermT2Context)_localctx).f = operand();
				setState(206);
				((TermT2Context)_localctx).t = termT2();

					((TermT2Context)_localctx).s =  "/" + ((TermT2Context)_localctx).f.s + ((TermT2Context)_localctx).t.s;	
					((TermT2Context)_localctx).s2 =  "/" +  (((TermT2Context)_localctx).f.isEval? ((TermT2Context)_localctx).f.d : ((TermT2Context)_localctx).f.s2 ) + (((TermT2Context)_localctx).t.isEval && ((TermT2Context)_localctx).t.ID_op!=0? (((TermT2Context)_localctx).t.ID_op== 1? "*":"/")+ ((TermT2Context)_localctx).t.d : ((TermT2Context)_localctx).t.s2);
					((TermT2Context)_localctx).ID_op =  2;
					if(((TermT2Context)_localctx).f.isEval && ((TermT2Context)_localctx).t.isEval)
					{
						((TermT2Context)_localctx).isEval =  true;
						switch(((TermT2Context)_localctx).t.ID_op){
							case 0:
								((TermT2Context)_localctx).d =  ((TermT2Context)_localctx).f.d;
								break;
							case 1:				
								((TermT2Context)_localctx).d =  ((TermT2Context)_localctx).f.d * ((TermT2Context)_localctx).t.d;
								break;
							case 2:
								((TermT2Context)_localctx).d =  ((TermT2Context)_localctx).f.d / ((TermT2Context)_localctx).t.d;
								break;
						}
						((TermT2Context)_localctx).s2 =  "/" + _localctx.d;
					}
					else ((TermT2Context)_localctx).isEval =  false;	

				}
				break;
			case EOF:
			case T__1:
			case T__5:
			case T__6:
			case NEQ:
			case LEQ:
			case GEQ:
			case EQ:
			case LT:
			case GT:
			case UNTIL:
			case RELEASE:
			case AND:
			case OR:
				enterOuterAlt(_localctx, 3);
				{

					((TermT2Context)_localctx).s =  "";
					((TermT2Context)_localctx).s2 =  "";
					((TermT2Context)_localctx).ID_op =  0;
					((TermT2Context)_localctx).isEval =  true;

				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionContext extends ParserRuleContext {
		public String s;
		public String s2;
		public Double d;
		public boolean isEval;
		public Token STRING;
		public ExprContext e;
		public TerminalNode STRING() { return getToken(MtlParser.STRING, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public FunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).enterFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MtlListener ) ((MtlListener)listener).exitFunction(this);
		}
	}

	public final FunctionContext function() throws RecognitionException {
		FunctionContext _localctx = new FunctionContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_function);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(212);
			((FunctionContext)_localctx).STRING = match(STRING);
			setState(213);
			match(T__0);
			setState(214);
			((FunctionContext)_localctx).e = expr();
			setState(215);
			match(T__1);

				switch((((FunctionContext)_localctx).STRING!=null?((FunctionContext)_localctx).STRING.getText():null)){
					case "exp":
						((FunctionContext)_localctx).s =  (((FunctionContext)_localctx).STRING!=null?((FunctionContext)_localctx).STRING.getText():null) + "(" + ((FunctionContext)_localctx).e.s+ ")";			
						if(((FunctionContext)_localctx).e.isEval){
							((FunctionContext)_localctx).d =  Math.exp(((FunctionContext)_localctx).e.d);
							((FunctionContext)_localctx).isEval =  true;
							((FunctionContext)_localctx).s2 =  ""+ _localctx.d;
						}
						else{
							((FunctionContext)_localctx).isEval =  false;
							((FunctionContext)_localctx).s2 =  (((FunctionContext)_localctx).STRING!=null?((FunctionContext)_localctx).STRING.getText():null) + "(" + ((FunctionContext)_localctx).e.s2+ ")";
						}
						break;
						
					case "abs":
						((FunctionContext)_localctx).s =  (((FunctionContext)_localctx).STRING!=null?((FunctionContext)_localctx).STRING.getText():null) + "(" + ((FunctionContext)_localctx).e.s+ ")";			
						if(((FunctionContext)_localctx).e.isEval){
							((FunctionContext)_localctx).d =  Math.abs(((FunctionContext)_localctx).e.d);
							((FunctionContext)_localctx).isEval =  true;
							((FunctionContext)_localctx).s2 =  ""+ _localctx.d;
						}
						else{
							((FunctionContext)_localctx).isEval =  false;
							((FunctionContext)_localctx).s2 =  (((FunctionContext)_localctx).STRING!=null?((FunctionContext)_localctx).STRING.getText():null) + "(" + ((FunctionContext)_localctx).e.s2+ ")" ;
						}
						break;
				}
				

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\37\u00dd\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\3\2\3\2\3\2\3\2\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3\63\n\3\3\4\3"+
		"\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\5\5?\n\5\3\6\3\6\3\6\3\6\3\6\3\6\3"+
		"\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6V\n\6\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7"+
		"\5\7j\n\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\5\b~\n\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t"+
		"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3"+
		"\t\3\t\3\t\3\t\5\t\u00a1\n\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3"+
		"\n\3\n\3\n\3\n\3\n\3\n\5\n\u00b3\n\n\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3"+
		"\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u00c4\n\f\3\r\3\r\3\r\3\r\3\16\3\16"+
		"\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\5\16\u00d5\n\16\3\17\3\17"+
		"\3\17\3\17\3\17\3\17\3\17\2\2\20\2\4\6\b\n\f\16\20\22\24\26\30\32\34\2"+
		"\2\2\u00e9\2\36\3\2\2\2\4\62\3\2\2\2\6\64\3\2\2\2\b>\3\2\2\2\nU\3\2\2"+
		"\2\fi\3\2\2\2\16}\3\2\2\2\20\u00a0\3\2\2\2\22\u00b2\3\2\2\2\24\u00b4\3"+
		"\2\2\2\26\u00c3\3\2\2\2\30\u00c5\3\2\2\2\32\u00d4\3\2\2\2\34\u00d6\3\2"+
		"\2\2\36\37\5\n\6\2\37 \7\2\2\3 !\b\2\1\2!\3\3\2\2\2\"#\7\f\2\2#\63\b\3"+
		"\1\2$%\7\r\2\2%\63\b\3\1\2&\'\5\20\t\2\'(\b\3\1\2(\63\3\2\2\2)*\7\34\2"+
		"\2*+\5\4\3\2+,\b\3\1\2,\63\3\2\2\2-.\7\3\2\2./\5\n\6\2/\60\7\4\2\2\60"+
		"\61\b\3\1\2\61\63\3\2\2\2\62\"\3\2\2\2\62$\3\2\2\2\62&\3\2\2\2\62)\3\2"+
		"\2\2\62-\3\2\2\2\63\5\3\2\2\2\64\65\5\4\3\2\65\66\5\b\5\2\66\67\b\4\1"+
		"\2\67\7\3\2\2\289\7\35\2\29:\5\4\3\2:;\5\b\5\2;<\b\5\1\2<?\3\2\2\2=?\b"+
		"\5\1\2>8\3\2\2\2>=\3\2\2\2?\t\3\2\2\2@A\7\27\2\2AB\5\16\b\2BC\5\n\6\2"+
		"CD\5\f\7\2DE\b\6\1\2EV\3\2\2\2FG\7\26\2\2GH\5\16\b\2HI\5\n\6\2IJ\5\f\7"+
		"\2JK\b\6\1\2KV\3\2\2\2LM\7\30\2\2MN\5\n\6\2NO\5\f\7\2OP\b\6\1\2PV\3\2"+
		"\2\2QR\5\6\4\2RS\5\f\7\2ST\b\6\1\2TV\3\2\2\2U@\3\2\2\2UF\3\2\2\2UL\3\2"+
		"\2\2UQ\3\2\2\2V\13\3\2\2\2WX\7\36\2\2XY\5\n\6\2YZ\5\f\7\2Z[\b\7\1\2[j"+
		"\3\2\2\2\\]\7\24\2\2]^\5\16\b\2^_\5\n\6\2_`\5\f\7\2`a\b\7\1\2aj\3\2\2"+
		"\2bc\7\25\2\2cd\5\16\b\2de\5\n\6\2ef\5\f\7\2fg\b\7\1\2gj\3\2\2\2hj\b\7"+
		"\1\2iW\3\2\2\2i\\\3\2\2\2ib\3\2\2\2ih\3\2\2\2j\r\3\2\2\2kl\7\5\2\2lm\7"+
		"\31\2\2mn\7\6\2\2no\7\31\2\2op\7\7\2\2p~\b\b\1\2qr\7\5\2\2rs\7\33\2\2"+
		"st\7\6\2\2tu\7\31\2\2uv\7\7\2\2v~\b\b\1\2wx\7\5\2\2xy\7\31\2\2yz\7\6\2"+
		"\2z{\7\33\2\2{|\7\7\2\2|~\b\b\1\2}k\3\2\2\2}q\3\2\2\2}w\3\2\2\2~\17\3"+
		"\2\2\2\177\u0080\5\24\13\2\u0080\u0081\7\22\2\2\u0081\u0082\5\24\13\2"+
		"\u0082\u0083\b\t\1\2\u0083\u00a1\3\2\2\2\u0084\u0085\5\24\13\2\u0085\u0086"+
		"\7\17\2\2\u0086\u0087\5\24\13\2\u0087\u0088\b\t\1\2\u0088\u00a1\3\2\2"+
		"\2\u0089\u008a\5\24\13\2\u008a\u008b\7\23\2\2\u008b\u008c\5\24\13\2\u008c"+
		"\u008d\b\t\1\2\u008d\u00a1\3\2\2\2\u008e\u008f\5\24\13\2\u008f\u0090\7"+
		"\20\2\2\u0090\u0091\5\24\13\2\u0091\u0092\b\t\1\2\u0092\u00a1\3\2\2\2"+
		"\u0093\u0094\5\24\13\2\u0094\u0095\7\21\2\2\u0095\u0096\5\24\13\2\u0096"+
		"\u0097\b\t\1\2\u0097\u00a1\3\2\2\2\u0098\u0099\5\24\13\2\u0099\u009a\7"+
		"\16\2\2\u009a\u009b\5\24\13\2\u009b\u009c\b\t\1\2\u009c\u00a1\3\2\2\2"+
		"\u009d\u009e\5\24\13\2\u009e\u009f\b\t\1\2\u009f\u00a1\3\2\2\2\u00a0\177"+
		"\3\2\2\2\u00a0\u0084\3\2\2\2\u00a0\u0089\3\2\2\2\u00a0\u008e\3\2\2\2\u00a0"+
		"\u0093\3\2\2\2\u00a0\u0098\3\2\2\2\u00a0\u009d\3\2\2\2\u00a1\21\3\2\2"+
		"\2\u00a2\u00a3\7\33\2\2\u00a3\u00b3\b\n\1\2\u00a4\u00a5\7\31\2\2\u00a5"+
		"\u00b3\b\n\1\2\u00a6\u00a7\5\34\17\2\u00a7\u00a8\b\n\1\2\u00a8\u00b3\3"+
		"\2\2\2\u00a9\u00aa\7\b\2\2\u00aa\u00ab\5\22\n\2\u00ab\u00ac\b\n\1\2\u00ac"+
		"\u00b3\3\2\2\2\u00ad\u00ae\7\3\2\2\u00ae\u00af\5\24\13\2\u00af\u00b0\7"+
		"\4\2\2\u00b0\u00b1\b\n\1\2\u00b1\u00b3\3\2\2\2\u00b2\u00a2\3\2\2\2\u00b2"+
		"\u00a4\3\2\2\2\u00b2\u00a6\3\2\2\2\u00b2\u00a9\3\2\2\2\u00b2\u00ad\3\2"+
		"\2\2\u00b3\23\3\2\2\2\u00b4\u00b5\5\30\r\2\u00b5\u00b6\5\26\f\2\u00b6"+
		"\u00b7\b\13\1\2\u00b7\25\3\2\2\2\u00b8\u00b9\7\t\2\2\u00b9\u00ba\5\30"+
		"\r\2\u00ba\u00bb\5\26\f\2\u00bb\u00bc\b\f\1\2\u00bc\u00c4\3\2\2\2\u00bd"+
		"\u00be\7\b\2\2\u00be\u00bf\5\30\r\2\u00bf\u00c0\5\26\f\2\u00c0\u00c1\b"+
		"\f\1\2\u00c1\u00c4\3\2\2\2\u00c2\u00c4\b\f\1\2\u00c3\u00b8\3\2\2\2\u00c3"+
		"\u00bd\3\2\2\2\u00c3\u00c2\3\2\2\2\u00c4\27\3\2\2\2\u00c5\u00c6\5\22\n"+
		"\2\u00c6\u00c7\5\32\16\2\u00c7\u00c8\b\r\1\2\u00c8\31\3\2\2\2\u00c9\u00ca"+
		"\7\n\2\2\u00ca\u00cb\5\22\n\2\u00cb\u00cc\5\32\16\2\u00cc\u00cd\b\16\1"+
		"\2\u00cd\u00d5\3\2\2\2\u00ce\u00cf\7\13\2\2\u00cf\u00d0\5\22\n\2\u00d0"+
		"\u00d1\5\32\16\2\u00d1\u00d2\b\16\1\2\u00d2\u00d5\3\2\2\2\u00d3\u00d5"+
		"\b\16\1\2\u00d4\u00c9\3\2\2\2\u00d4\u00ce\3\2\2\2\u00d4\u00d3\3\2\2\2"+
		"\u00d5\33\3\2\2\2\u00d6\u00d7\7\33\2\2\u00d7\u00d8\7\3\2\2\u00d8\u00d9"+
		"\5\24\13\2\u00d9\u00da\7\4\2\2\u00da\u00db\b\17\1\2\u00db\35\3\2\2\2\13"+
		"\62>Ui}\u00a0\u00b2\u00c3\u00d4";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}