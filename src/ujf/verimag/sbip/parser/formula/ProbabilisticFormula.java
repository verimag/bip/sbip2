package ujf.verimag.sbip.parser.formula;

import java.util.ArrayList;

public class ProbabilisticFormula {
	
	ProbabilisticOperatorEnum probabilisticOp;
	double probabilty;
	Formula formula;
	
	public ProbabilisticFormula(ProbabilisticOperatorEnum probabilisticOp, double probabilty, Formula formula) {
		super();
		this.probabilisticOp = probabilisticOp;
		this.probabilty = probabilty;
		this.formula = formula;
	}
	
	public ProbabilisticFormula(ProbabilisticOperatorEnum probabilisticOp, Formula formula) {
		super();
		this.probabilisticOp = probabilisticOp;
		this.probabilty = 0;
		this.formula = formula;
	}
	
	

	@Override
	public String toString() {
		if(probabilisticOp.name().equalsIgnoreCase("PE"))
			return probabilisticOp.toString() + "[ "+ formula + "]";
		if(probabilisticOp.name().equalsIgnoreCase("PHT_GE") || probabilisticOp.name().equalsIgnoreCase("PHT_LE"))
			return probabilisticOp.toString() + probabilty + "[ "+ formula.toString() + "]";
		return null;
	}
	
	public Formula getFormula(){
		return formula;
	}

	public ArrayList<String> getDataVariblesList() {
		return formula.getDataVariablesList();		
	}
	
	
}
