package ujf.verimag.sbip.parser.formula;


public class TemporalIntervalOperator extends Operator{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	TemporalOperatorEnum opName;
	Interval interval;
	
	public TemporalIntervalOperator(TemporalOperatorEnum opName, Interval interval) {
		super();
		this.opName = opName;
		this.interval = interval;
	}
	
	public TemporalIntervalOperator(TemporalOperatorEnum opName) {
		super();
		this.opName = opName;
		this.interval = new Interval();
	}

	public TemporalOperatorEnum getOpName() {
		return opName;
	}

	public void setOpName(TemporalOperatorEnum opName) {
		this.opName = opName;
	}

	public Interval getInterval() {
		return interval;
	}

	public void setInterval(Interval interval) {
		this.interval = interval;
	}

	@Override
	public String toString() {
		return opName +""+ interval;
	}

	@Override
	public boolean isTimable() {
		switch (opName) {
		case N:
			return false;

		default:
			return true;
		}
	
	}
	
	public boolean isUnary() {
		switch (opName) {
			case U: return false;
			case R: return false;
			default: return true;
		}
		
	}

	public String getName() {
		switch (opName) {
		case U: return "U";
		case R: return "R";
		case N: return "N";
		case F: return "F";
		case G: return "G";
		default: return " ";
		}
	}
	
	
}
