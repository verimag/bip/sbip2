package ujf.verimag.sbip.parser.formula;

import ujf.verimag.sbip.bipInterface.trace.StateVariable;

public class MtlFormula extends Formula {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MtlFormula(Formula leftFormula, Formula rightFormula, Operator operator) {
		super(leftFormula, rightFormula, operator);
		
		if((operator instanceof TemporalBoundedOperator)){
			System.err.println("Wrong assignment of bounded operator to MTL formula");
			System.exit(-1);
		}
		
		if(leftFormula instanceof LtlFormula){
			System.err.println("Wrong assignment of left LTL formula to MTL formula");
			System.exit(-1);
		}
		
		if(rightFormula instanceof LtlFormula){
			System.err.println("Wrong assignment of right LTL formula to MTL formula");
			System.exit(-1);
		}
	}

	
	@Override
	public String toString() {
		return  super.toString() ;
	}

	public boolean isTerminal() {
		if(operator == null)
			if(((StateFormula) rightFormula).getStateFormula().equals("#True") || ((StateFormula) rightFormula).getStateFormula().equals("#False"))
				return true;
		return false;
	}

	public MtlFormula instantiateInterval(StateVariable paramVar) {
		
		return new MtlFormula(
			(leftFormula != null?
					
					(leftFormula instanceof MtlFormula ? 
							((MtlFormula) leftFormula).instantiateInterval(paramVar): leftFormula)
					:null),
			(rightFormula != null?
					(rightFormula instanceof MtlFormula ? 
							((MtlFormula) rightFormula).instantiateInterval(paramVar): rightFormula)
					
					:null),
			
			(operator != null ? 
					(operator instanceof TemporalIntervalOperator ? 
							new TemporalIntervalOperator(((TemporalIntervalOperator) operator).getOpName(), ((TemporalIntervalOperator) operator).getInterval().instantiate(paramVar)): operator)
						: null)				
					
		);
		
	}


	@Override
	public boolean isLeaf() {
		// TODO Auto-generated method stub
		return false;
	}


	public boolean containsTemporalOperator() {
		if(operator == null)
			return false;
		else if(operator instanceof TemporalIntervalOperator ) {
			return true;
		}
		else {
			return (leftFormula!= null? ( (MtlFormula) leftFormula).containsTemporalOperator(): false) 
					|| ( (MtlFormula) rightFormula).containsTemporalOperator();
					
		}
	}
}
