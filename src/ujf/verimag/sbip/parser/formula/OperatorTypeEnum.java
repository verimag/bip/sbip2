package ujf.verimag.sbip.parser.formula;


public enum OperatorTypeEnum {
	
	BIN,	/** Binary operator **/
	UN;		/** Unary operator **/

}
