package ujf.verimag.sbip.parser.formula;


public enum TemporalOperatorEnum {
	
	N,	/** Next **/
	U,	/** Until **/
	R,	/** Release **/
	G,	/** Always **/
	F;	/** Eventually **/
}
