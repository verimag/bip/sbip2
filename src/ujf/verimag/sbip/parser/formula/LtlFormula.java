package ujf.verimag.sbip.parser.formula;

import ujf.verimag.sbip.bipInterface.trace.StateVariable;

public class LtlFormula extends Formula {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LtlFormula(Formula leftFormula, Formula rightFormula, Operator operator) {
		super(leftFormula, rightFormula, operator);
		if((operator instanceof TemporalIntervalOperator)){
			System.err.println("Wrong assignment of timed operator to LTL formula");
			System.exit(-1);
		}
		
		if(leftFormula instanceof MtlFormula){
			System.err.println("Wrong assignment of left MTL formula to LTL formula");
			System.exit(-1);
		}
		
		if(rightFormula instanceof MtlFormula){
			System.err.println("Wrong assignment of right MTL formula to LTL formula");
			System.exit(-1);
		}
		 
	}

	
	public LtlFormula(Formula leftFormula) {
		super(leftFormula, null, null);
		if((operator instanceof TemporalIntervalOperator)){
			System.err.println("Wrong assignment of timed operator to LTL formula");
			System.exit(-1);
		}
		
		if(leftFormula instanceof MtlFormula){
			System.err.println("Wrong assignment of left MTL formula to LTL formula");
			System.exit(-1);
		}
		
		if(rightFormula instanceof MtlFormula){
			System.err.println("Wrong assignment of right MTL formula to LTL formula");
			System.exit(-1);
		}
		 
	}
	
	
	
	@Override
	public String toString() {
		return  super.toString() ;
	}


	@Override
	public boolean isLeaf() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean isTerminal() {
		if(operator == null)
			if(((StateFormula) rightFormula).getStateFormula().equals("#True") || ((StateFormula) rightFormula).getStateFormula().equals("#False"))
				return true;
		return false;
	}


	public LtlFormula instantiateBound(StateVariable paramVar) {
		return this;
	}
}
