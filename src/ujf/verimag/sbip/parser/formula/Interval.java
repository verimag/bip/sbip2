package ujf.verimag.sbip.parser.formula;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.swing.JOptionPane;

import ujf.verimag.sbip.bipInterface.trace.StateVariable;

public class Interval implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//Extend to parametrizable
	Object lb;
	Object ub;
	int parametered = 0;
	
	public Interval() {
		super();
		this.ub = Double.MAX_VALUE;
		this.lb = 0;
		parametered = 0;
	}
	
	
	public Interval(Object lb, Object up, int parametered) {
//		if(lb>up){
//			System.err.println("Wrong definition of Interval : LeftBound > UpperBound.");
//			JOptionPane.showMessageDialog(null, "Wrong definition of Interval : LeftBound > UpperBound.", "Interval error", JOptionPane.ERROR_MESSAGE);
////			System.exit(-2);
//		}
//		System.out.println(lb+"---"+ up);
		this.lb = lb;
		this.ub = up;
		this.parametered = parametered;
	}

	public Interval(Interval interval) {
		this.lb = interval.getLeft();
		this.ub = interval.getRight();
		this.parametered = interval.parametered;
	}

	public Interval instantiate(StateVariable paramvar) {
		if(paramvar.getType() == "int") {
			switch(parametered) {
			case 0: 
				return new Interval(lb, ub, 0);
			case 1: 
				return new Interval(Double.parseDouble(paramvar.getValue()), ub, 0);
			case 2: 
				return new Interval(lb, Double.parseDouble(paramvar.getValue()), 0);
			default :  return new Interval(lb, ub, 0) ;
			}
		}
//		if(paramvar.getType() == "int") {
//			if(String.valueOf(lb).equals(paramvar.getVariable())) {
//					
//					lowb = Double.parseDouble(paramvar.getValue());
//					upb = Double.parseDouble(String.valueOf(ub));
//				
//			}
//			else
//				if( String.valueOf( ub).equals(paramvar.getVariable())) {
//						
//						lowb = Double.parseDouble(String.valueOf(lb));
//						upb = Double.parseDouble(paramvar.getValue());
//					
//				}
//				else {
//
////					System.out.println(this);
//					return new Interval( Double.parseDouble((String)lb), Double.parseDouble((String)ub));
//					
//				}
//			}
		return null;
		
//		return new Interval(lowb, upb);
		
	}

	public double getLb() {
//		if(lb instanceof String)
//			return Double.parseDouble((String) lb);
//			else 
//				if(lb instanceof Integer)
//				return Double.valueOf((int)lb);
//			else if(lb instanceof Double)
//				return (double) lb;
		try {
			return (double) lb;
			}
	catch(ClassCastException e) {
		JOptionPane.showMessageDialog(null,"Monitor error : could not find variable '"+lb+"' as a parameter!\n Maybe you meant to do a parametric exploration.", "Fatal Error", JOptionPane.ERROR_MESSAGE);
		return (double) lb;
	}
	}

	public void setLb(double lb) {
		this.lb = lb;
	}

	public double getUb() {
//		if(ub instanceof String)
//		return Double.parseDouble((String) ub);
//		else 
//			if(ub instanceof Integer)
//			return Double.valueOf((int)ub);
//		else if(ub instanceof Double)
//			return (double) ub;
		try {
			return (double) ub;
		}
		catch(ClassCastException e) {
			JOptionPane.showMessageDialog(null,"Monitor error : could not find variable '"+ub+"' as a parameter!\n Maybe you meant to do a parametric exploration.", "Fatal Error", JOptionPane.ERROR_MESSAGE);
			return (double) ub;
		}
		
//		else {
////			System.exit(-1);
//			return (-2);
//		}
	}

	public void setUb(double ub) {
		this.ub = ub;
	}
 
	public String toString() {
		if(this.ub instanceof Double)
		if((Double) this.ub == Double.MAX_VALUE)
			return "[" + getLeft() + ", +INFTY]";
		
		return "[" + getLeft() + ", " + getRight() + "]";
	}
	
	public Object getRight() {
//		if((this.ub instanceof Double) )
//			return String.valueOf((double) this.ub);
//		else if((this.ub instanceof Integer))
//			return String.valueOf((int) this.ub);
//		else return (String) this.ub;
		
		return ub;
		
	}


	public Object getLeft() {
//		if((this.lb instanceof Double) )
//		{
//			System.out.println("Double"+lb);
//			return String.valueOf((double) this.lb);
//		}
//		else if((this.lb instanceof Integer))
//		{
//			System.out.println("Integer"+lb);
//			return String.valueOf((int) this.lb);
//		}
//		else {
//			System.out.println("other"+lb);
//			return (String) this.lb;
//		}
		
		return this.lb;
	}


	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();
	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
}
