package ujf.verimag.sbip.parser.formula;



public class BooleanOperator extends Operator{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	BooleanOperatorEnum opName;

	public BooleanOperator(BooleanOperatorEnum opName) {
		super();
		this.opName = opName;
	}

	public BooleanOperatorEnum getOpName() {
		return opName;
	}

	public void setOpName(BooleanOperatorEnum opName) {
		this.opName = opName;
	}

	@Override
	public String toString() {
		return opName.toString();
	}

	@Override
	public boolean isTimable() {
		return false;
	}
	
	public boolean isUnary() {
		switch (opName) {
			case AND: return false;
			case OR: return false;
			case NOT: return true;
			default: return true;
		}
		
	}

	
	public String getName() {
		switch (opName) {
		case AND: return "AND";
		case OR: return "OR";
		case NOT: return "NOT";
		default: return " ";
		}
	
	}
}
