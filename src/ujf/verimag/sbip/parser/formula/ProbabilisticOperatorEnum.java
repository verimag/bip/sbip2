package ujf.verimag.sbip.parser.formula;


public enum ProbabilisticOperatorEnum {
	
	PHT_GE,	/** P>= **/
	PHT_LE,	/** P<= **/
	PE;		/** P=? **/

	public String toString() {
		if(this.name().equalsIgnoreCase("PHT_GE"))
			return "P>=";
		if(this.name().equalsIgnoreCase("PHT_LE"))
			return "P<=";
		if(this.name().equalsIgnoreCase("PE"))
			return "P=?";
		return null;
	}
	
}
