package ujf.verimag.sbip.parser.formula;


public class TemporalBoundedOperator extends Operator {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	TemporalOperatorEnum opName;
	int bound;
	
	public TemporalBoundedOperator(TemporalOperatorEnum opName, int bound) {
		super();
		this.opName = opName;
		this.bound = bound;
	}

	public TemporalOperatorEnum getOpName() {
		return opName;
	}

	public void setOpName(TemporalOperatorEnum opName) {
		this.opName = opName;
	}

	public int getBound() {
		return bound;
	}

	public void setBound(int bound) {
		this.bound = bound;
	}

	@Override
	public String toString() {
		return opName + "{" + bound + "}";
	}

	@Override
	public boolean isTimable() {
		return false;
	}

	
	public boolean isUnary() {
		switch (opName) {
			case U: return false;
			case R: return false;
			default: return true;
		}
		
	}
	
	public String getName() {
		switch (opName) {
		case U: return "U";
		case R: return "R";
		case N: return "N";
		case F: return "F";
		case G: return "G";
		default: return " ";
		}
	}
	
	
}
