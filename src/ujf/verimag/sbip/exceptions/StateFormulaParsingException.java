package ujf.verimag.sbip.exceptions;


public class StateFormulaParsingException extends MonitoringException {

	/**
	 * Thrown when a monitoring a state of a BIP trace wrt
	 * a state LTL formula: It may be because the specified 
	 * state variables are not appearing in the trace, or ...
	 */
	private static final long serialVersionUID = 1L;

	public StateFormulaParsingException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StateFormulaParsingException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public StateFormulaParsingException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public StateFormulaParsingException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
