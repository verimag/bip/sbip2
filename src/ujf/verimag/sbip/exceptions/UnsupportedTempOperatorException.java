package ujf.verimag.sbip.exceptions;

public class UnsupportedTempOperatorException extends MonitoringException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnsupportedTempOperatorException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UnsupportedTempOperatorException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public UnsupportedTempOperatorException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public UnsupportedTempOperatorException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
}
