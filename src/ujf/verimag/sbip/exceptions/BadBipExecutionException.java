package ujf.verimag.sbip.exceptions;

public class BadBipExecutionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BadBipExecutionException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BadBipExecutionException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public BadBipExecutionException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public BadBipExecutionException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
	
	

}
