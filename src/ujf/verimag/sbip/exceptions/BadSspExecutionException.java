package ujf.verimag.sbip.exceptions;

public class BadSspExecutionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BadSspExecutionException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BadSspExecutionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public BadSspExecutionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public BadSspExecutionException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
	

}
