package ujf.verimag.sbip.exceptions;

public class MonitoringException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MonitoringException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MonitoringException(String arg0, Throwable arg1) {
		super("When Monitoring BIP trace with respect to the PB-LTL property:" + arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public MonitoringException(String arg0) {
		super("When Monitoring BIP trace with respect to the PB-LTL property:" + arg0);
		// TODO Auto-generated constructor stub
	}

	public MonitoringException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
}
