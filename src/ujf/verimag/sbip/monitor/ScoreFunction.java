package ujf.verimag.sbip.monitor;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JOptionPane;

import org.apache.commons.math3.genetics.CrossoverPolicy;

import ujf.verimag.sbip.bipInterface.trace.State;
import ujf.verimag.sbip.gui.model.Score_Formula;
import ujf.verimag.sbip.monitor.mtl.MtlMonitor;
import ujf.verimag.sbip.parser.formula.Formula;
import ujf.verimag.sbip.parser.formula.MtlFormula;
import ujf.verimag.sbip.parser.formula.StateFormula;

public class ScoreFunction implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private HashMap<Integer, Formula> levels;
	 private int levelNumber;
	 
	 public ScoreFunction() {
		 this.levels = new HashMap<Integer, Formula>();
		 this.levelNumber = 0;
	 }

	public ScoreFunction(HashMap<Integer, Formula> levels, int levelNumber) {
		this.levels = levels;
		this.levelNumber = levelNumber;
	}
	
	public boolean addNextLevel(int score, Formula f) {
		if(score == levelNumber + 1) {
			levels.put(score, f);
			levelNumber ++;
			return true;
		}
		else return false;
	}
	 
	public String save(String project) throws FileNotFoundException, IOException {
    	
    	String propertyName = JOptionPane.showInputDialog("Please type the name of the score function :");
		
		if(propertyName != null )
		if(!propertyName.equals(""))
		{
			File file= new File(project+propertyName+".sf");
//			if(!file.exists()){
				@SuppressWarnings("resource")
				ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));  
			    out.writeObject(this); 
				return file.getAbsolutePath();
		}
			
		else {
			JOptionPane.showMessageDialog(null, "Error : cannot create a file with empty name", "Error", JOptionPane.ERROR_MESSAGE);
		}
		return null;
	}

	public int getNbLevels() {
		return levelNumber;
	}
	
	public Formula getFormula(int score) {
		return levels.get(score);
	}

	public int evaluate(State state) {

		int score = 0;
		boolean crossLevel = true;
		try {
		if(levelNumber != 0)
		for(int i = 1; i<= levelNumber && crossLevel; i++) {
			MtlMonitor monitor = new MtlMonitor(null);
			MtlFormula f1 = monitor.rewrite((MtlFormula) levels.get(i), state, 0);
			MtlFormula f2 = monitor.simplify(f1);
			if(f2.isTerminal()) {
				if(((StateFormula) f2.getRightFormula()).getStateFormula().equals("#True")) {
					score = i;
				}
				else {
					crossLevel = false;
				}
			}
			else {
				crossLevel = false;
			}
		}
		}catch(NullPointerException e) {
			System.err.println("Unadequate score function!");
			crossLevel = false;
			return -1;
		}
		return score;
	}

	
	 
	


}
