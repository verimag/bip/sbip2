package ujf.verimag.sbip.monitor.mtl;

import ujf.verimag.sbip.parser.formula.MtlFormula;
import ujf.verimag.sbip.parser.formula.TemporalIntervalOperator;

public class Characterizer {
	
	public double computeSufficientTime(MtlFormula f){
		Filter filter = new Filter();
		MtlFormula ff = (MtlFormula) filter.replaceEventually(filter.replaceGlobally(f));
		
		if(ff.getOperator() != null)
		if(ff.getOperator().isTimable()){
			return ((TemporalIntervalOperator) ff.getOperator()).getInterval().getUb() + computeSufficientTime((MtlFormula) ff.getRightFormula());
				
		}
		
		return 0.0;
		
	}

}
