package ujf.verimag.sbip.monitor.mtl;

import ujf.verimag.sbip.parser.formula.Interval;
import ujf.verimag.sbip.parser.formula.MtlFormula;
import ujf.verimag.sbip.parser.formula.StateFormula;
import ujf.verimag.sbip.parser.formula.TemporalIntervalOperator;
import ujf.verimag.sbip.parser.formula.TemporalOperatorEnum;

public class Filter {

	public MtlFormula replaceGlobally(MtlFormula f){
		
		if(f.getOperator() != null){
			if(f.getOperator() instanceof TemporalIntervalOperator && ((TemporalIntervalOperator) f.getOperator()).getOpName() == TemporalOperatorEnum.G){ 
					return 
							new MtlFormula( 
									new MtlFormula(null,new StateFormula("#False"), null),
									replaceGlobally(  (MtlFormula) f.getRightFormula()),
									new TemporalIntervalOperator(TemporalOperatorEnum.R, new Interval(((TemporalIntervalOperator) f.getOperator()).getInterval()) )
									);
//						new  Formula( new Operator(MTL.Operator.Type.NOT) , 
//							new Formula (new Operator(MTL.Operator.Type.EVENTUALLY, f.operator.interval.a, f.operator.interval.b),
//									new Formula( new Operator(MTL.Operator.Type.NOT), replaceGlobally( (Formula) f.phi1)
//											)
//										)
//									);
				
			}	
			else{
				if(f.getOperator().isUnary()){
					return new MtlFormula(
							null, 
							replaceGlobally( (MtlFormula) f.getRightFormula()), 
							f.getOperator()
							);
				}
				else{
					return new MtlFormula(
							replaceGlobally( (MtlFormula) f.getLeftFormula()),
							replaceGlobally( (MtlFormula) f.getRightFormula()),
							f.getOperator()
							);
				}
			}
		}
		return f;
	}
	
	
	public MtlFormula replaceEventually(MtlFormula f){
		
		
		if(f.getOperator() != null){
			if(f.getOperator() instanceof TemporalIntervalOperator && ((TemporalIntervalOperator) f.getOperator()).getOpName() == TemporalOperatorEnum.F){ 
				return 
						new MtlFormula( 
								new MtlFormula(null,new StateFormula("#True"), null),
								replaceEventually( (MtlFormula) f.getRightFormula()),
								new TemporalIntervalOperator(TemporalOperatorEnum.U, new Interval(((TemporalIntervalOperator) f.getOperator()).getInterval()) )
								);
			}
			else{
				if(f.getOperator().isUnary()){
					return new MtlFormula(
							null, 
							replaceEventually( (MtlFormula) f.getRightFormula()), 
							f.getOperator()
							);
				}
				else{
					return new MtlFormula(
							replaceEventually( (MtlFormula) f.getLeftFormula()),
							replaceEventually( (MtlFormula) f.getRightFormula()),
							f.getOperator()
							);
				}
			}
		}
				
		return f;
	}
}
