package ujf.verimag.sbip.monitor.mtl;



import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import ujf.verimag.sbip.analysis.Simulator;
import ujf.verimag.sbip.bipInterface.bip.ThreadedTraceGenerator;
import ujf.verimag.sbip.bipInterface.trace.State;
import ujf.verimag.sbip.bipInterface.trace.StateVariable;
import ujf.verimag.sbip.bipInterface.trace.Trace;
import ujf.verimag.sbip.parser.bool.BoolExprParser;
import ujf.verimag.sbip.parser.bool.BoolOutput;
import ujf.verimag.sbip.parser.formula.BooleanOperator;
import ujf.verimag.sbip.parser.formula.BooleanOperatorEnum;
import ujf.verimag.sbip.parser.formula.Formula;
import ujf.verimag.sbip.parser.formula.Interval;
import ujf.verimag.sbip.parser.formula.MtlFormula;
import ujf.verimag.sbip.parser.formula.StateFormula;
import ujf.verimag.sbip.parser.formula.TemporalIntervalOperator;
import ujf.verimag.sbip.parser.formula.TemporalOperatorEnum;

public class MtlMonitor  extends ujf.verimag.sbip.monitor.Monitor implements Runnable{
	
	int traceLength = 0;
	ThreadedTraceGenerator gen ;
	MtlFormula formula;
	int verdict;
	private StateVariable parametricVariable = null;
	private Simulator simulator;
	
	public int getTraceLength(){
		return traceLength;
	}
	
	public int check(Formula form, Trace w){
		
	
		
			MtlFormula f = (MtlFormula) form;
			traceLength = w.getStates().size();
			Filter filter = new Filter();
			f = filter.replaceEventually(filter.replaceGlobally(f));
			if (debug) {
//				System.out.println("Checking ... "+f);
				log.add("Checking ... "+f);
			}
			int i = 0;
			
			while( i+1 < w.getStates().size() && !f.isTerminal()){
				State state = w.getState(i);
				state.addVariable(parametricVariable);
				MtlFormula f1 = rewrite(f, state,  Interval.round(w.getState(i+1).getClockValue() - w.getState(i).getClockValue(), 3));
				f = simplify(f1);
				if (debug) {
//					System.out.println("Step["+i+"]: ["+w.getState(i)+" , "+ Interval.round(w.getState(i+1).getClockValue() - w.getState(i).getClockValue(), 3)+ "] \n \t (rw)" + f1 + "\n \t (sm)"+f + "\n");
					log.add("Step["+i+"]: ["+w.getState(i)+" , "+ Interval.round(w.getState(i+1).getClockValue() - w.getState(i).getClockValue(), 3)+ "] \n \t (rw)" + f1 + "\n \t (sm)"+f + "\n");
				}
				i ++;
				
			}
			
	
			
			if(f.getOperator() == null){
				if(((StateFormula) f.getRightFormula() ).getStateFormula().equals("#True")) {
					if (debug) {
//						System.out.println("Property proven True!");
						log.add("Property proven True!");
						log.add("END");
					}
					
					return 1;
				}
				else if(((StateFormula) f.getRightFormula() ).getStateFormula().equals("#False")) {
					if (debug) {
//						System.out.println("Property proven False!");
						log.add("Property proven False!");
						log.add("END");
					}
					return 0;
				}
				else {
					if (debug)  {
//						System.out.println("Monitor not able to conclude!");
						log.add("Monitor not able to conclude!");
						log.add("END");
					}
					
					return 2;
				}
			}
			else {
				if (debug) {
					System.out.println("Monitor not able to conclude!");
					log.add("Monitor not able to conclude!");
					log.add("END");
				}
				
				return 2;
			}
		
		
		
	}
	
	public MtlFormula simplify(MtlFormula f){
		if(f == null) return null;
		
		if(f.getOperator() != null)
		if(f.getOperator() instanceof BooleanOperator){
			if(((BooleanOperator)f.getOperator()).getOpName() == BooleanOperatorEnum.AND){
	
				MtlFormula left = simplify( (MtlFormula) f.getLeftFormula());
				MtlFormula right = simplify( (MtlFormula) f.getRightFormula());
				if(left == null) return null;
				if(right == null) return null;
				
				
				if(left.getOperator() == null)
					if( ((StateFormula) left.getRightFormula()).getStateFormula().equals("#False") ){
						return new MtlFormula(null ,  new StateFormula("#False") , null);			
					}
				
				if(right.getOperator() == null)
					if(((StateFormula) right.getRightFormula()).getStateFormula().equals("#False")){
						return new MtlFormula(null ,  new StateFormula("#False") , null);			
					}
				
				if(right.getOperator() == null)
					if(((StateFormula) right.getRightFormula()).getStateFormula().equals("#True")  ){
						return left;			
					}
				
				if(left.getOperator() == null)
					if(((StateFormula) left.getRightFormula()).getStateFormula().equals("#True")   ){
						return right;				
					}
				
				return new MtlFormula( left, right, new BooleanOperator(BooleanOperatorEnum.AND) );
				
			}
			
			
			else if(((BooleanOperator)f.getOperator()).getOpName() == BooleanOperatorEnum.OR){
	
				MtlFormula left = simplify( (MtlFormula) f.getLeftFormula());
				MtlFormula right = simplify( (MtlFormula) f.getRightFormula());
				
				
				if(left.getOperator() == null)
					if(((StateFormula) left.getRightFormula()).getStateFormula().equals("#True")   ){
						return new MtlFormula(null ,  new StateFormula("#True") , null);					
					}
				
				if(right.getOperator() == null)
					if(((StateFormula) right.getRightFormula()).getStateFormula().equals("#True")   ){
						return new MtlFormula(null ,  new StateFormula("#True") , null);					
					}
				
				if(right.getOperator() == null)
					if(((StateFormula) right.getRightFormula()).getStateFormula().equals ("#False")  ){
						return left;			
					}
				if(left.getOperator() == null)
					if(((StateFormula) left.getRightFormula()).getStateFormula().equals ("#False")  ){
						return right;				
					}
				
				return new MtlFormula( left, right, new BooleanOperator(BooleanOperatorEnum.OR) );
				
			}		
		}
		
		return f;
	}
	
	public MtlFormula rewrite(MtlFormula formula, State state, double timeValue) {
			if(formula == null) return null;
			
			if(formula.getOperator() == null){
//				System.out.println("\nRewrite ap");
				
				BoolExprParser parser = new BoolExprParser();
				String s = parser.instantiate(((StateFormula) formula.getRightFormula()).getStateFormula(), state);
				if(s == null) {
					simulator.abort();
					return null;
				}
				else
				{
					BoolOutput eval = parser.parse(s); 
					if(eval.getValue()) return new MtlFormula(null,new StateFormula("#True"), null);
					else return new MtlFormula(null,new StateFormula("#False"), null);
				}
				
			}
			else{
				if(formula.getOperator() instanceof BooleanOperator){
					if(((BooleanOperator) formula.getOperator()).getOpName() == BooleanOperatorEnum.NOT){
						BoolExprParser parser = new BoolExprParser();
						String sformula = ((StateFormula) formula.getRightFormula().getRightFormula()).getStateFormula();
						String s = parser.instantiate(sformula , state);
						BoolOutput eval = parser.parse(s); 
						if(!eval.getValue()) return new MtlFormula(null,new StateFormula("#True"), null);
						else return new MtlFormula(null,new StateFormula("#False"), null);
	//					
					}
					else if(((BooleanOperator) formula.getOperator()).getOpName() == BooleanOperatorEnum.AND){
//						System.out.println("Rewrite And");
						return new MtlFormula(
								rewrite((MtlFormula) formula.getLeftFormula(), state, timeValue), 
								rewrite((MtlFormula) formula.getRightFormula(), state, timeValue ),
								new BooleanOperator(BooleanOperatorEnum.AND)
							);
					}
					
					else if(((BooleanOperator) formula.getOperator()).getOpName() == BooleanOperatorEnum.OR){
//						System.out.println("Rewrite Or");
						return new MtlFormula(
								rewrite(((MtlFormula) formula.getLeftFormula()), state, timeValue), 
								rewrite(((MtlFormula) formula.getRightFormula()), state, timeValue ),
								new BooleanOperator(BooleanOperatorEnum.OR)
							);
					}
				}
				else if(formula.getOperator() instanceof TemporalIntervalOperator)
				{
					if(((TemporalIntervalOperator) formula.getOperator()).getOpName() == TemporalOperatorEnum.N){
//						System.out.println("Rewrite Next");
						return (MtlFormula) (formula.getRightFormula());
					}
					else if(((TemporalIntervalOperator) formula.getOperator()).getOpName() == TemporalOperatorEnum.U){
//						System.out.println("Rewrite Until");
						Interval inter = ((TemporalIntervalOperator) formula.getOperator()).getInterval();
						if(inter.getLb() > 0 && timeValue <= inter.getUb()){
							
							return new MtlFormula( 
									rewrite((MtlFormula) formula.getLeftFormula(), state, timeValue),								
									new MtlFormula(
											formula.getLeftFormula(),
											formula.getRightFormula(),
											new TemporalIntervalOperator(
													TemporalOperatorEnum.U,
													new Interval(
															(double)Math.max(Math.max(inter.getLb() - timeValue, 0), 0), 
															(double) (inter.getUb() - timeValue),
															0
															)
													)
											
											),
									new BooleanOperator(BooleanOperatorEnum.AND)
									);
						}

						if( inter.getLb() == 0 && timeValue <= inter.getUb()){
							
							return new MtlFormula( 
									rewrite((MtlFormula) formula.getRightFormula(), state, timeValue),
									
									new MtlFormula(
											rewrite( (MtlFormula) formula.getLeftFormula() , state, timeValue),
											new MtlFormula(
													formula.getLeftFormula(),
													formula.getRightFormula(),
													new TemporalIntervalOperator(
															TemporalOperatorEnum.U,
															new Interval((double) 0,
																	(double) (inter.getUb() - timeValue),
																	0
																	)
															)
													)	,
											new BooleanOperator(BooleanOperatorEnum.AND)
																			
											)	,
									new BooleanOperator(BooleanOperatorEnum.OR)
									);
						}
						
						if(inter.getLb() == 0 && timeValue > inter.getUb()){
							return rewrite((MtlFormula) formula.getRightFormula(), state, timeValue);
						}
						
						if(inter.getLb() > 0 && timeValue > inter.getUb()){
							return new MtlFormula(null,new StateFormula("#False"), null);
						}
						
					}
					else if(((TemporalIntervalOperator) formula.getOperator()).getOpName() == TemporalOperatorEnum.R){
//						System.out.println("Rewrite Release");
						Interval inter = ((TemporalIntervalOperator) formula.getOperator()).getInterval();
						
						if(inter.getLb() > 0 && timeValue <= inter.getUb()){
							return new MtlFormula( 
									rewrite((MtlFormula) formula.getRightFormula(), state, timeValue),
									
									new MtlFormula(
											formula.getLeftFormula(),
											
											formula.getRightFormula(),
											
											new TemporalIntervalOperator(
													TemporalOperatorEnum.R,
													new Interval(
															(double) Math.max( Math.max(inter.getLb() - timeValue, 0), 0), 
															(double) (inter.getUb() - timeValue),
															0
															)
													)
											),
									new BooleanOperator(BooleanOperatorEnum.AND)
									);
						}

						if(inter.getLb() == 0 && timeValue <= inter.getUb()){
							return new MtlFormula( 
									rewrite((MtlFormula) formula.getRightFormula(), state, timeValue),
									
									new MtlFormula(
											rewrite( (MtlFormula) formula.getLeftFormula() , state, timeValue),
											
											new MtlFormula(
													formula.getLeftFormula(),
													
													formula.getRightFormula(),
													
													new TemporalIntervalOperator(
															TemporalOperatorEnum.R,
															new Interval(
																	(double) 0,
																	(double) (inter.getUb() - timeValue),
																	0
																	)
															)
													)	,
											new BooleanOperator(BooleanOperatorEnum.OR)
											),	
									new BooleanOperator(BooleanOperatorEnum.AND)
									);
						}
						
						if( timeValue > inter.getUb()){
							return rewrite((MtlFormula) formula.getRightFormula(), state, timeValue);
						}
						
						
					}
				}
				
			}
			
			return formula;
		
	}

	
		
	
	public MtlMonitor(Simulator simulator){

		this.simulator = simulator;
	}
	
	public MtlMonitor(ThreadedTraceGenerator gen, MtlFormula f, Simulator simulator){
		this.gen = gen;
		this.formula = f;
		this.parametricVariable = null;
		this.simulator = simulator;
	}
	
	public MtlMonitor(StateVariable paramVar, Simulator simulator) {
		this.parametricVariable = paramVar;
		this.simulator = simulator;
	}

	public MtlMonitor(ThreadedTraceGenerator generator, MtlFormula f, StateVariable paramVar, Simulator simulator) {
		this.gen = generator;
		this.formula = f;
		this.parametricVariable = paramVar;
		this.simulator = simulator;
//		System.out.println("Monitor formula : "+formula);
	}

	public void run(){
			Filter filter = new Filter();
//			System.out.println("Start monitor");
			formula = filter.replaceEventually(filter.replaceGlobally(formula));
//			System.out.println("Checking ... "+formula);
			boolean addedVariable = false;
			
			while(!gen.isRunning()){
				try {
					Thread.sleep(0);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			int i =0;
			
			while((formula != null) && (!formula.isTerminal() || gen.getStatesSize() > 1) ){
				State state =  gen.getModelState(0);
				if(state!=null && !addedVariable)
					{
					state.addVariable(parametricVariable);
					addedVariable = true;
					}
				State next_state =  gen.getModelState(1);
				if(state != null && next_state!=null){

					addedVariable = false;
					if(!formula.isTerminal()){
						MtlFormula f1 = rewrite(formula, state,  Interval.round(next_state.getClockValue() - state.getClockValue(), 3));
						formula = simplify(f1);
//						System.out.println("Step["+i+"]: ["+state+" , "+ Interval.round(next_state.getClockValue() - state.getClockValue(), 3)+ "] \n \t (rw)" + f1 + "\n \t (sm)"+formula +"\n" + next_state+"\n");
						i++;
//						System.out.println("\tStep["+i+"]");
						
//						gen.log(state);
						gen.consumeModelState();
						traceLength++;
						if(formula !=null)
						if(formula.isTerminal())
							gen.arreter();	
						
					}
					else{
						gen.arreter();						
					}
					
				}
				
				if(gen.getStatesSize() == 0){
					try {
						addedVariable = false;
						Thread.sleep(0);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}	
			
			conclude(this.formula);
			gen.arreter();
//			System.out.println("Ended Monitor");
	}
	
	
	private void conclude(Formula f) {
		if(f == null ) verdict = 2;
		else
		if(f.getOperator() == null ){
			if(((StateFormula) f.getRightFormula()).getStateFormula().equals("#True")) {
//				System.out.println("Property proven True!");
				verdict =  1;
			}
			else if(((StateFormula) f.getRightFormula()).getStateFormula().equals("#False")) {
//				System.out.println("Property proven False!");
				verdict = 0;
			}
			else {
//				System.out.println("Monitor not able to conclude!");
				verdict = 2;
			}
		}
		else {
//			System.out.println("Monitor not able to conclude!");
			verdict = 2;
		}
		
	}

	public int getVerdict() {
		
		return verdict;
	}



	
}