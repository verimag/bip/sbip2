package ujf.verimag.sbip.monitor;

import ujf.verimag.sbip.exceptions.*;
import ujf.verimag.sbip.parser.formula.*;

import java.util.ArrayList;

import ujf.verimag.sbip.bipInterface.trace.*;

public abstract class Monitor {
	
	int traceLength = 0;
	Formula formula;
	int verdict;
	protected boolean debug = false;
	
	public ArrayList<String> log  = new ArrayList<String>();
	
	abstract public int check(Formula f, Trace w);
	
	private double EvaluateStateExpression(StateFormula sf, State st) throws StateFormulaParsingException{
		org.nfunk.jep.JEP exprParser = new org.nfunk.jep.JEP();
		exprParser.addStandardFunctions();	/** ajout des fonctions standard au parseur d'expression (abs(),...) **/
		for(int i=0; i<st.getStateVariables().size(); i++)
		{
			/** Ajouter les variables d'etat et leur valeurs au parseur d'expression **/
			/** On ne supporte que les types premitifs : bool, int, float et double **/
			/** les meme type que support la version actuelle de BIP **/
			if(st.getStateVariables().get(i).getType().equals("bool")) {
				exprParser.addVariable(st.getStateVariables().get(i).getVariable(), new Boolean(Boolean.parseBoolean(st.getStateVariables().get(i).getValue())));
				//System.out.println("bool "+st.getStates().get(i).getVariable()+"="+st.getStates().get(i).getValue());
			} else if(st.getStateVariables().get(i).getType().equals("int")) {
				exprParser.addVariable(st.getStateVariables().get(i).getVariable(), new Integer(Integer.parseInt(st.getStateVariables().get(i).getValue())));
				//System.out.println("int "+st.getStates().get(i).getVariable()+"="+st.getStates().get(i).getValue());
			} else if(st.getStateVariables().get(i).getType().equals("float")) {
				exprParser.addVariable(st.getStateVariables().get(i).getVariable(), new Float(Float.parseFloat(st.getStateVariables().get(i).getValue())));
				//System.out.println("float "+st.getStates().get(i).getVariable()+"="+st.getStates().get(i).getValue());
			} else if(st.getStateVariables().get(i).getType().equals("double")) {
				exprParser.addVariable(st.getStateVariables().get(i).getVariable(), new Double(Double.parseDouble(st.getStateVariables().get(i).getValue())));
				//System.out.println("double "+st.getStates().get(i).getVariable()+"="+st.getStates().get(i).getValue());
			} else {
				throw new UnsupportedOperationException("\nUnsupported type " + st.getStateVariables().get(i).getType() + " in variable " + st.getStateVariables().get(i).getVariable());
			}
		}
		
		exprParser.parseExpression(sf.getStateFormula());
		
		double result = exprParser.getValue();
		//System.out.println("######## Un Etat de la trace #######");
		//System.out.println("######## master.tm = " + expr_parser.getVarValue("master.tm").toString());
		//System.out.println("######## slave.ts = " + expr_parser.getVarValue("slave.ts").toString());
		//System.out.println("######## Resultat de verification pour un etat de la trace : "+resultat);
		
		if(exprParser.hasError())
		{
			// add costume exceptions and remove exit statements
			throw new StateFormulaParsingException("\nWhen evaluating state formula:\n" + exprParser.getErrorInfo());
			//System.out.print("Error when Evaluating State Formula with respect to System Traces : ");
			//System.out.println(expr_parser.getErrorInfo());
			//System.exit(-6);
		}
		
		return result;
		
	}
	
	public void enableDebug() {
		debug = true;
	}
}
