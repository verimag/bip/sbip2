package ujf.verimag.sbip.monitor.ltl;

import ujf.verimag.sbip.parser.formula.*;


public class Filter {

	public Formula replaceGlobally(LtlFormula f){
		
		if(f.getOperator() != null){
			if(f.getOperator() instanceof TemporalBoundedOperator && ((TemporalBoundedOperator) f.getOperator()).getOpName() == TemporalOperatorEnum.G){ 
					return new LtlFormula( 
									new LtlFormula(null,new StateFormula("#False"), null),
									replaceGlobally(  (LtlFormula) f.getRightFormula()),
									new TemporalBoundedOperator(TemporalOperatorEnum.R,((TemporalBoundedOperator)f.getOperator()).getBound()));
				
			}	
			else{
				if(f.getOperator().isUnary()){
					return new LtlFormula(null, replaceGlobally( (LtlFormula) f.getRightFormula()), f.getOperator());
				}
				else{
					return new LtlFormula(
							replaceGlobally( (LtlFormula) f.getLeftFormula()),
							replaceGlobally( (LtlFormula) f.getRightFormula()),
							f.getOperator()
							);
				}
			}
		}
		return f;
	}
	
	
	public Formula replaceEventually(LtlFormula f){
		
		
		if(f.getOperator() != null){
			if(f.getOperator() instanceof TemporalBoundedOperator && ((TemporalBoundedOperator) f.getOperator()).getOpName() == TemporalOperatorEnum.F){ 
				return 
						new LtlFormula( 
								new LtlFormula(null,new StateFormula("#True"), null),
								replaceEventually( (LtlFormula) f.getRightFormula()),
								new TemporalBoundedOperator(TemporalOperatorEnum.U, ((TemporalBoundedOperator) f.getOperator()).getBound()));
			}
			else{
				if(f.getOperator().isUnary()){
					return new LtlFormula(null, replaceEventually( (LtlFormula) f.getRightFormula()), f.getOperator());
				}
				else{
					return new LtlFormula(
							replaceEventually( (LtlFormula) f.getLeftFormula()),
							replaceEventually( (LtlFormula) f.getRightFormula()),
							f.getOperator()
							);
				}
			}
		}
				
		return f;
	}
}