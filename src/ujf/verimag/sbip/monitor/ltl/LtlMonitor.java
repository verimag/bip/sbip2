package ujf.verimag.sbip.monitor.ltl;

import ujf.verimag.sbip.analysis.Simulator;
import ujf.verimag.sbip.bipInterface.bip.*;
import ujf.verimag.sbip.bipInterface.trace.*;
import ujf.verimag.sbip.parser.formula.*;
import ujf.verimag.sbip.parser.bool.*;


public class LtlMonitor extends ujf.verimag.sbip.monitor.Monitor implements Runnable {
	
	LtlFormula formula;
	ThreadedTraceGenerator gen ;
	int verdict;
	int traceLength = 0;
	private StateVariable parametricVariable = null;
	
	
	public LtlMonitor(){}
	
	public LtlMonitor(LtlFormula f) {
		formula = f;
		this.gen = null;
		this.parametricVariable = null;
	}
	
	public LtlMonitor(ThreadedTraceGenerator gen, LtlFormula f){
		this.gen = gen;
		this.formula = f;
		this.parametricVariable = null;
	}
	
	public LtlMonitor(StateVariable paramVar) {
		this.parametricVariable = paramVar;
	}

	public LtlMonitor(ThreadedTraceGenerator generator, LtlFormula f, StateVariable paramVar) {
		this.gen = generator;
		this.formula = f;
		this.parametricVariable = paramVar;
//		System.out.println("Monitor formula : "+formula);
	}
	
	/** Changed from LtlFormula to Formula to extends the Monitor superType. 
	 * Implied to add isTerminal at the level of the abstract class Formula **/
	public int check(Formula f, Trace w){	 
		traceLength = w.getStates().size();
		Filter filter = new Filter();
		f = filter.replaceEventually((LtlFormula)filter.replaceGlobally((LtlFormula)f));
		if (debug) {
//			System.out.println("Checking ... "+f);
			log.add("Checking ... "+f);
		}
		
		int i = 0;
		//double currentTime = 0;
		
		while( i < w.getStates().size() && !f.isTerminal()){ /** changed from i+1 to i **/
			State state = w.getState(i);
			state.addVariable(parametricVariable);
			
			LtlFormula f1 = rewrite((LtlFormula)f, state,  1);
			if (debug) { 
//				System.out.println("i=" + i + ", rewrite result: " + f1.toString());
				log.add("i=" + i + ", rewrite result: " + f1.toString());
			}
			
			f = simplify(f1);
			if (debug) { 
//				System.out.println("i=" + i + ", simplify result: " + f.toString());
				log.add("i=" + i + ", simplify result: " + f.toString());
			}
			i++;	
		}

		if(f.getOperator() == null){
			if(((StateFormula) f.getRightFormula() ).getStateFormula().equals("#True")) {
				if (debug) {
//				System.out.println("Property proven True!");
					log.add("Property proven True!");
					log.add("END");
				}
				return 1;
			}
			else if(((StateFormula) f.getRightFormula() ).getStateFormula().equals("#False")) {
				if (debug) {
//				System.out.println("Property proven False!");
					log.add("Property proven False!");
					log.add("END");
				}
				
				return 0;
			}
			else {
				if (debug) {			
//				System.out.println("Monitor not able to conclude!");
					log.add("Monitor not able to conclude!");
					log.add("END");
				}
				return 2;
			}
		}
		else {
			if (debug) {
//			System.out.println("Monitor not able to conclude!");
				log.add("Monitor not able to conclude!");
				log.add("END");
			}
			return 2;
		}
	}
	
	/** Simplifies AND and OR boolean formulas **/
	public LtlFormula simplify(LtlFormula f){
		if(f.getOperator() != null)
			/** case of a formula with a boolean operator **/
			if(f.getOperator() instanceof BooleanOperator){
				
				/** AND **/
				if(((BooleanOperator)f.getOperator()).getOpName() == BooleanOperatorEnum.AND){
					LtlFormula left = simplify( (LtlFormula) f.getLeftFormula());
					LtlFormula right = simplify( (LtlFormula) f.getRightFormula());

					if(left.getOperator() == null)
						if( ((StateFormula) left.getRightFormula()).getStateFormula().equals("#False")){
							return new LtlFormula(null ,  new StateFormula("#False") , null);			
						}

					if(right.getOperator() == null)
						if(((StateFormula) right.getRightFormula()).getStateFormula().equals("#False")){
							return new LtlFormula(null ,  new StateFormula("#False") , null);			
						}

					if(right.getOperator() == null)
						if(((StateFormula) right.getRightFormula()).getStateFormula().equals("#True")){
							return left;			
						}

					if(left.getOperator() == null)
						if(((StateFormula) left.getRightFormula()).getStateFormula().equals("#True")){
							return right;				
						}

					return new LtlFormula( left, right, new BooleanOperator(BooleanOperatorEnum.AND));
				}
				
				/** OR **/
				else if(((BooleanOperator)f.getOperator()).getOpName() == BooleanOperatorEnum.OR){
					LtlFormula left = simplify( (LtlFormula) f.getLeftFormula());
					LtlFormula right = simplify( (LtlFormula) f.getRightFormula());


					if(left.getOperator() == null)
						if(((StateFormula) left.getRightFormula()).getStateFormula().equals("#True")   ){
							return new LtlFormula(null, new StateFormula("#True"), null);					
						}

					if(right.getOperator() == null)
						if(((StateFormula) right.getRightFormula()).getStateFormula().equals("#True")   ){
							return new LtlFormula(null,  new StateFormula("#True"), null);					
						}

					if(right.getOperator() == null)
						if(((StateFormula) right.getRightFormula()).getStateFormula().equals ("#False")  ){
							return left;			
						}
					if(left.getOperator() == null)
						if(((StateFormula) left.getRightFormula()).getStateFormula().equals ("#False")  ){
							return right;				
						}

					return new LtlFormula(left, right, new BooleanOperator(BooleanOperatorEnum.OR) );
				}		
			}
		return f;
	}
	
	
	public LtlFormula rewrite(LtlFormula formula, State state, int bound) {
			//if(formula == null) return null;
			
			/** case of a state formula (LTL formula with null op and a state formula as right formula) **/
			if(formula.getOperator() == null){
//				System.out.println("\nRewrite ap: " + formula.toString());
				BoolExprParser parser = new BoolExprParser();
				String s = parser.instantiate(((StateFormula) formula.getRightFormula()).getStateFormula(), state);
				//System.out.println("Instantiated : "+ s);
				BoolOutput eval = parser.parse(s); 
//				System.out.println("Eval :" + eval.getExpression());
//				System.out.println("Eval :" + eval.getValue());
				if(eval.getValue()) return new LtlFormula(null,new StateFormula("#True"), null);
				else return new LtlFormula(null,new StateFormula("#False"), null);
				
			}
			else{
				/** case of a Formula with a boolean operator **/
				if(formula.getOperator() instanceof BooleanOperator){
					
					/** case of a NOT **/
					if(((BooleanOperator) formula.getOperator()).getOpName() == BooleanOperatorEnum.NOT){
						BoolExprParser parser = new BoolExprParser();
						String sformula = ((StateFormula) formula.getRightFormula().getRightFormula()).getStateFormula();
						String s = parser.instantiate(sformula , state);
						BoolOutput eval = parser.parse(s); 
						if(!eval.getValue()) return new LtlFormula(null,new StateFormula("#True"), null);
						else return new LtlFormula(null,new StateFormula("#False"), null);
					}
					
					/** case of an AND **/
					else if(((BooleanOperator) formula.getOperator()).getOpName() == BooleanOperatorEnum.AND){
						//System.out.println("Rewrite And");
						return new LtlFormula(
								rewrite((LtlFormula) formula.getLeftFormula(), state, bound), 
								rewrite((LtlFormula) formula.getRightFormula(), state, bound ),
								new BooleanOperator(BooleanOperatorEnum.AND)
							);
					}
					/** case of an OR **/
					else if(((BooleanOperator) formula.getOperator()).getOpName() == BooleanOperatorEnum.OR){
						//System.out.println("Rewrite Or");
						return new LtlFormula(
								rewrite(((LtlFormula) formula.getLeftFormula()), state, bound), 
								rewrite(((LtlFormula) formula.getRightFormula()), state, bound ),
								new BooleanOperator(BooleanOperatorEnum.OR)
							);
					}
				}
				
				/** case of a Formula with a Temporal operator **/
				else if(formula.getOperator() instanceof TemporalBoundedOperator)
				{
					/** case of an Next **/
					if(((TemporalBoundedOperator) formula.getOperator()).getOpName() == TemporalOperatorEnum.N){
						//System.out.println("Rewrite Next");
						return (LtlFormula) (formula.getRightFormula());
					}
					/** case of an Until **/
					else if(((TemporalBoundedOperator) formula.getOperator()).getOpName() == TemporalOperatorEnum.U){
//						System.out.println("Rewrite Until");
						int trace_bound = ((TemporalBoundedOperator) formula.getOperator()).getBound();
						
						/*if(bound <= trace_bound){	// to verify
							return new LtlFormula( 
													rewrite((LtlFormula) formula.getLeftFormula(), state, bound),								
													new LtlFormula(
																	formula.getLeftFormula(),
																	formula.getRightFormula(),
																	new TemporalBoundedOperator(TemporalOperatorEnum.U,trace_bound - bound // to verify
																			)
													),
													new BooleanOperator(BooleanOperatorEnum.AND));
						}*/

						if(bound <= trace_bound){ /** bound is the index of the state in the trace **/
//							System.out.println(bound + " <= " + trace_bound);
							return new LtlFormula(
									rewrite((LtlFormula) formula.getRightFormula(), state, bound),
									new LtlFormula(
											rewrite((LtlFormula) formula.getLeftFormula(), state, bound),
											new LtlFormula(
													formula.getLeftFormula(), 
													formula.getRightFormula(),
													new TemporalBoundedOperator(TemporalOperatorEnum.U, trace_bound - bound)),	/** we consume one state each time **/
											new BooleanOperator(BooleanOperatorEnum.AND)),
									new BooleanOperator(BooleanOperatorEnum.OR));
						}
						
						/*if(bound > trace_bound){ // to verify
							return rewrite((LtlFormula) formula.getRightFormula(), state, bound);
						}*/
						
						if(bound > trace_bound){
//							System.out.println(bound + " > " + trace_bound);
							return new LtlFormula(null,new StateFormula("#False"), null);
						}
						
					}
					/** case of an Release **/
					else if(((TemporalBoundedOperator) formula.getOperator()).getOpName() == TemporalOperatorEnum.R){
//						System.out.println("Rewrite Release");
						int trace_bound = ((TemporalBoundedOperator) formula.getOperator()).getBound();
						
						/*if(bound <= trace_bound){// to verify
							return new LtlFormula( 
									rewrite((LtlFormula) formula.getRightFormula(), state, bound),
									
									new LtlFormula(
											formula.getLeftFormula(),
											formula.getRightFormula(),											
											new TemporalBoundedOperator(
													TemporalOperatorEnum.R,
													((TemporalBoundedOperator)formula.getOperator()).getBound()
													)
											),
									new BooleanOperator(BooleanOperatorEnum.AND)
									);
						}*/

						if(bound <= trace_bound){
//							System.out.println(bound + " <= " + trace_bound);
							return new LtlFormula( 
									rewrite((LtlFormula) formula.getRightFormula(), state, bound),
									
									new LtlFormula(
											rewrite( (LtlFormula) formula.getLeftFormula() , state, bound),											
											new LtlFormula(
													formula.getLeftFormula(),
													formula.getRightFormula(),
													new TemporalBoundedOperator(TemporalOperatorEnum.R, trace_bound - bound)),
											new BooleanOperator(BooleanOperatorEnum.OR)),	
									new BooleanOperator(BooleanOperatorEnum.AND));
						}
						
						if( bound > trace_bound){
//							System.out.println(bound + " > " + trace_bound);
							return rewrite((LtlFormula) formula.getRightFormula(), state, bound);
						}
					}
				}
			}
			return (LtlFormula)formula;
	}

	public void run(){
			Filter filter = new Filter();
			//System.out.println(f);
			formula = (LtlFormula)filter.replaceEventually((LtlFormula)filter.replaceGlobally(formula));
//			System.out.println("Checking ... "+formula);
			int i = 0;
			boolean addedVariable = false;
			
			
			while(!formula.isTerminal() || gen.getStatesSize() > 1){
				State state =  gen.getModelState(0);
				if(state!=null && !addedVariable) {
					state.addVariable(parametricVariable);
					addedVariable = true;
				}
				State next_state =  gen.getModelState(1);
				if(state != null && next_state!=null){
					addedVariable = false;
					System.out.println(state);
					if(!formula.isTerminal()){
						LtlFormula f1 = rewrite(formula, state, 1);
						formula = simplify(f1);
//						System.out.println("Step["+i+"]: ["+state+" , "+ Interval.round(next_state.getClockValue() - state.getClockValue(), 3)+ "] \n \t (rw)" + f1 + "\n \t (sm)"+formula +"\n" + next_state+"\n");
//						System.out.println("\tStep["+i+"]" + f1);
						i ++;
						//gen.log(state);
						gen.consumeModelState();
						traceLength++;
						if(formula.isTerminal())
							gen.arreter();	
						
					}
					else{
						gen.arreter();						
					}
				}
				
				if(gen.getStatesSize() == 0){
					try {
						//System.out.println("\twaiting.");
						addedVariable = false;
						Thread.sleep(0);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}	
			
			conclude(this.formula);
			gen.arreter();
//			System.out.println("Ended Monitor");
	}
		
	private void conclude(Formula f) {
		if(f.getOperator() == null ){
			if(((StateFormula) f.getRightFormula()).getStateFormula().equals("#True")) {
//				System.out.println("Property proven True!");
				verdict =  1;
			}
			else if(((StateFormula) f.getRightFormula()).getStateFormula().equals("#False")) {
//				System.out.println("Property proven False!");
				verdict = 0;
			}
			else {
//				System.out.println("Monitor not able to conclude!");
				verdict = 2;
			}
		}
		else {
//			System.out.println("Monitor not able to conclude!");
			verdict = 2;
		}
		
	}
	
	public int getTraceLength(){
		return traceLength;
	}

	public int getVerdict() {
		return verdict;
	}
}
