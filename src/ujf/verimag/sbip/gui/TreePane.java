package ujf.verimag.sbip.gui;

import java.awt.Container;
import java.io.File;

import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;

public class TreePane extends JTabbedPane {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	FileBrowser fileBrowser;
	Container c;
	UserInterface GUI; 
	
	public TreePane(UserInterface userInterface){
		  super();
		  GUI = userInterface;
		  setBorder(new EmptyBorder(5,0,5,0));
		  fileBrowser = new FileBrowser(GUI);
		  c = fileBrowser.getGui();
		  addTab("Project Explorer", c);
		
	}

	public void create(String s) {
		fileBrowser.create(s );
		
	}

	public void reprint(){
		fileBrowser.reprint();
	}

	public void removeProject(String selectedProject) {
		fileBrowser.removeProject(selectedProject);
		
	}

	public void removeFile(File file) {
		fileBrowser.removeFile(file);
		
	}

	public void closeProject(String selectedProject) {
		fileBrowser.closeProject(selectedProject);
	}

	public void insertNodeInTree(String node) {
		fileBrowser.insertNodeInTree(node);
	}

	public File getSelectedFile() {
		return fileBrowser.getSelectedFile();
	}

//	public void reloadTreeModel() {
//		fileBrowser.reloadTreeModel();
//	}

	


}
