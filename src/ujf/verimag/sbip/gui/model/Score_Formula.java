package ujf.verimag.sbip.gui.model;

import javax.swing.JButton;

import ujf.verimag.sbip.parser.formula.Formula;

public class Score_Formula {

	private int score;
	private Formula formula;
//	private JButton delete;
	
	public Score_Formula(int score, Formula formula) {//, JButton delete) {
		super();
		this.score = score;
		this.formula = formula;
//		this.delete = delete;
	}
	
	
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public Formula getFormula() {
		return formula;
	}
	public void setFormula(Formula formula) {
		this.formula = formula;
	}
//	public JButton getDelete() {
//		return delete;
//	}
//	public void setDelete(JButton delete) {
//		this.delete = delete;
//	}
	
	

	
	
	
	
}
