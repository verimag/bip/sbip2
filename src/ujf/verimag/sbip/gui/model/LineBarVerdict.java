package ujf.verimag.sbip.gui.model;

import java.awt.BasicStroke;
import java.awt.Stroke;


import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.SymbolAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.DefaultCategoryDataset;



public class LineBarVerdict  {
	
	ChartPanel chartPanel = null;
	
	public LineBarVerdict(SmcTableModel smc, boolean isPestim){
		
		if(isPestim)
		{
			JFreeChart lineChart = ChartFactory.createLineChart(
			         "Param value Vs. SMC verdict",
			         "Parameter value","SMC Verdict",
			         createDataset(smc, isPestim),
			         PlotOrientation.VERTICAL,
			         true,true,false);
			         
			 final LineAndShapeRenderer renderer = (LineAndShapeRenderer) lineChart.getCategoryPlot().getRenderer();
			 
			  Stroke stroke = new BasicStroke(
			            10f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL);
			        renderer.setBaseOutlineStroke(stroke); 
					chartPanel = new ChartPanel( lineChart );
//					chartPanel.setPreferredSize(new Dimension(200, 200));
				      
//			      this.add(chartPanel);
		}
		else{
			System.out.println("building barchart");
			JFreeChart barChart = ChartFactory.createBarChart(
					"Param value Vs. SMC verdict",           
					"Parameter value","SMC Verdict",          
			         createDataset(smc, isPestim),          
			         PlotOrientation.VERTICAL,           
			         true, true, false);

			ValueAxis valueAxis =(ValueAxis) barChart.getCategoryPlot().getRangeAxis();
			valueAxis.setAutoRange(false);
//			valueAxis.setTickLabelsVisible(false);
//			valueAxis.setTickMarksVisible(false);
			
			
			
			NumberAxis rangeAxis = (NumberAxis) barChart.getCategoryPlot().getRangeAxis();
			rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
			BarRenderer renderer = (BarRenderer) barChart.getCategoryPlot().getRenderer();
			renderer.setMaximumBarWidth(0.1);
			
			SymbolAxis sa = new SymbolAxis("SMC Verdict",
				    new String[]{"False","True"});
				barChart.getCategoryPlot().setRangeAxis(sa);
				
				renderer.setBase(-0.5);

			     
			 chartPanel = new ChartPanel( barChart );
//				chartPanel.setPreferredSize(new Dimension(300, 250));
//		      this.add(chartPanel);
		}
		
		 
	}
	
	public ChartPanel getChartPanel(){
		return this.chartPanel;
	}
	
	private DefaultCategoryDataset createDataset(SmcTableModel smc , boolean isPestim) {
	      DefaultCategoryDataset dataset = new DefaultCategoryDataset( );
//	      dataset.addValue( 15 , "schools" , "1970" );
	      
	      for (int i = 0; i < smc.getRowCount(); i++) {
	    	  if(isPestim)
	    		  dataset.addValue((Number) smc.getRow(i).getVerdict(), "SMC verdict", smc.getRow(i).getParamValue());
	    	  else{
	    		  System.out.println("building here : "+smc.getRow(i).getVerdict());
	    		  int n = ((boolean) smc.getRow(i).getVerdict() ? 1 : 0);
	    		  
	    		  dataset.addValue(n, "SMC verdict", smc.getRow(i).getParamValue());
	    	  }
	      }
	      return dataset;
	   }

}
