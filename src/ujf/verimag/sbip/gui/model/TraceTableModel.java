package ujf.verimag.sbip.gui.model;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import ujf.verimag.sbip.analysis.TraceVerdict;
import ujf.verimag.sbip.parser.formula.Interval;

public class TraceTableModel extends AbstractTableModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String[] entetes = { "Trace Name", "Consumed symbols", "URL", "Process time", "Evaluation" };	
	private ArrayList<TraceVerdict> data;
	
	public TraceTableModel(){
		data = new ArrayList<TraceVerdict>();
	}
	
	public TraceTableModel(ArrayList<TraceVerdict> data2) {
		super();
		setData(data2);
	}

	public void setData(ArrayList<TraceVerdict> traceData){
		data = traceData;
	}
	
	public String getColumnName(int columnIndex) {
		return entetes[columnIndex];
	} 
	
	public int getColumnCount() {
		return 5;
	}

	@Override
	public int getRowCount() {		
		return data.size();
	}

	@Override
	public Object getValueAt(int arg0, int arg1) {
		switch (arg1) {
			case 0:
				return data.get(arg0).getName();
				
			case 1:
				return data.get(arg0).getSize();
			
			case 2:
				return data.get(arg0).getURL();
				
			case 3:
				return convertTime(data.get(arg0).getTime())+ " ms";
	
			case 4:
				if(data.get(arg0).getEvaluation() == 0) return "False";
				else 
					if(data.get(arg0).getEvaluation() == 1) return "True";
					else 
						return "Not able to conclude";

			default:
				return data.get(arg0).getTrace();
		}
		
	}
	
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
			case 0:
				return String.class;
				
			case 1:
				return Integer.class;
	
			case 2:
				return String.class;
				
			case 3:
				return String.class;
	
			case 4:
				return String.class;
	
	
			default:
				return TraceVerdict.class;
		}
	}
	
	public double getPercentageTracesEvaluatedTo(Boolean b){
		int cpt = 0;
		for (int i = 0; i < data.size(); i++) {
			if(data.get(i).getEvaluation() == 1) cpt ++;
		}
		
		Double d = Interval.round((cpt + 0.0)/ (data.size() + 0.0) * 100, 1);
		return d;
	}

	public TraceVerdict getRow(int row) {
		return data.get(row);
	}
	
	private String convertTime(long time){
		int milliseconds = (int) (time  % 1000);
		int seconds = (int) (time  / 1000) % 60 ;
		int minutes = (int) ((time  / (1000*60)) % 60);
		int hours = (int) (time / (1000*60*60)) ;
		String dateFormatted  = String.format("%02d:%02d:%02d:%03d",hours,minutes,seconds,milliseconds);
		return dateFormatted ;
	}

}
