package ujf.verimag.sbip.gui.model;

import java.util.Comparator;

/**
 * Comparator of individuals based on their score (BIC or F1_score)
 * @author mediouni
 *
 */
public class DataCompare implements Comparator<Score_Formula> {

	// Sort in an increasing order
    public int compare(Score_Formula o1, Score_Formula o2) {
    	if(o1.getScore() > o2.getScore()) return 1;
    	else if(o1.getScore() == o2.getScore()) return 0;
    	else return -1;
    }
}
