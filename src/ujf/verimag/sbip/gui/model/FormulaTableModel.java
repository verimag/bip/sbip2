package ujf.verimag.sbip.gui.model;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.table.AbstractTableModel;


public class FormulaTableModel extends AbstractTableModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String[] entetes = { "Score Value", "State Formula"};
	private ArrayList<Score_Formula> data;
	
	public FormulaTableModel(){
		data = new ArrayList<Score_Formula>();
	}
	
	public FormulaTableModel(ArrayList<Score_Formula> data2) {
		super();
		setData(data2);
	}

	public void setData(ArrayList<Score_Formula> traceData){
		data = traceData;
	}
	
	public String getColumnName(int columnIndex) {
		return entetes[columnIndex];
	} 
	
	public int getColumnCount() {
		return 2;
	}

	@Override
	public int getRowCount() {		
		return data.size();
	}

	@Override
	public Object getValueAt(int arg0, int arg1) {
		switch (arg1) {
			case 0:
				return data.get(arg0).getScore();
				
			case 1:
				return data.get(arg0).getFormula();
			
//			case 2:
//				return data.get(arg0).getDelete();
				

			default:
				return data.get(arg0).getScore();
		}
		
	}
	
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
			case 0:
				return Integer.class;
				
			case 1:
				return String.class;
	
//			case 2:
//				return JButton.class;
			
			default:
				return Integer.class;
		}
	}
	
	
	public Score_Formula getRow(int row) {
		return data.get(row);
	}
	


}
