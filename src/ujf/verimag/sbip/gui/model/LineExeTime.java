package ujf.verimag.sbip.gui.model;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Stroke;


import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.DefaultCategoryDataset;



public class LineExeTime  {
	
	ChartPanel chartPanel = null;
	
	public LineExeTime(SmcTableModel smc){
		
		
			JFreeChart lineChart = ChartFactory.createLineChart(
			         "Param value Vs. Process time",
			         "Parameter value","Process time",
			         createDataset(smc),
			         PlotOrientation.VERTICAL,
			         true,true,false);
			 final LineAndShapeRenderer renderer = (LineAndShapeRenderer) lineChart.getCategoryPlot().getRenderer();
			 renderer.setSeriesPaint(0, Color.BLUE);
			 
			  Stroke stroke = new BasicStroke(
			            10f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL);
			        renderer.setBaseOutlineStroke(stroke); 
					chartPanel = new ChartPanel( lineChart );
			
		 
	}
	
	public ChartPanel getChartPanel(){
		return this.chartPanel;
	}
	
	private DefaultCategoryDataset createDataset(SmcTableModel smc ) {
	      DefaultCategoryDataset dataset = new DefaultCategoryDataset( );

	      
	      for (int i = 0; i < smc.getRowCount(); i++) {
	    	  dataset.addValue((Number) smc.getRow(i).getProcessTime(), "Process time", smc.getRow(i).getParamValue());
	      }
	      return dataset;
	   }

}
