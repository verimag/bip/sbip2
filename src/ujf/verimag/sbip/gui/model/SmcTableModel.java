package ujf.verimag.sbip.gui.model;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import ujf.verimag.sbip.analysis.SmcVerdict;

public class SmcTableModel extends AbstractTableModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String[] entetes = { "Param Value", "Trace number", "% True traces", "Process time", "Verdict" };	
	private ArrayList<SmcVerdict> data;
	
	public SmcTableModel(){
		data = new ArrayList<SmcVerdict>();
	}
	
	public SmcTableModel(ArrayList<SmcVerdict> data2) {
		super();
		setData(data2);
	}

	public void setData(ArrayList<SmcVerdict> traceData){
		data = traceData;
	}
	
	public String getColumnName(int columnIndex) {
		return entetes[columnIndex];
	} 
	
	public int getColumnCount() {
		return 5;
	}

	@Override
	public int getRowCount() {		
		return data.size();
	}

	@Override
	public Object getValueAt(int arg0, int arg1) {
		switch (arg1) {
			case 0:
				return data.get(arg0).getParamValue();
				
			case 1:
				return data.get(arg0).getNbTraces();
			
			case 2:
				return data.get(arg0).getTrue_percentage();
				
			case 3:
				return convertTime(data.get(arg0).getProcessTime()) + " ms";
	
			case 4:
				return data.get(arg0).getVerdict();

			default:
				return data.get(arg0).getParamValue();
		}
		
	}
	
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
			case 0:
				return String.class;
				
			case 1:
				return Integer.class;
	
			case 2:
				return Double.class;
				
			case 3:
				return String.class;
	
			case 4:
				return Object.class;
	
	
			default:
				return String.class;
		}
	}
	
	

	public SmcVerdict getRow(int row) {
		return data.get(row);
	}

	private String convertTime(long time){
		int milliseconds = (int) (time  % 1000);
		int seconds = (int) (time  / 1000) % 60 ;
		int minutes = (int) ((time  / (1000*60)) % 60);
		int hours = (int) (time / (1000*60*60)) ;
		String dateFormatted  = String.format("%02d:%02d:%02d:%03d",hours,minutes,seconds,milliseconds);
		return dateFormatted ;
	}
}
