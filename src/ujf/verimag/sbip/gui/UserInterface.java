package ujf.verimag.sbip.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.JViewport;

import ujf.verimag.sbip.gui.tabs.PanelEditor;
import ujf.verimag.sbip.gui.tabs.PanelParametricSMC;
import ujf.verimag.sbip.gui.tabs.PanelRareEvent;
import ujf.verimag.sbip.gui.tabs.PanelSMC;
import ujf.verimag.sbip.gui.tabs.PanelSimulation;
import ujf.verimag.sbip.parser.formula.LtlFormula;
import ujf.verimag.sbip.parser.formula.MtlFormula;

public class UserInterface extends JFrame implements WindowListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TabbedPane tabbedPane;
	private TreePane treePanel;
	private JLabel currentProject;
	private JMenuBar menuBar;
	private boolean autoresize = false;
	private int textScale = 100;
	private int initialTextSize;
	private ComponentListener resizeListener;
	private JToolBar toolBar;
	private static String version = "2.2.5";

	
	public TabbedPane getTabbedPane() {
		return tabbedPane;
	}
	 
		
 	public UserInterface() throws IOException{
		
	
	       
		super("SBIP - SMC Engine");
		System.out.println("Building GUI ...");
//		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setMinimumSize(new Dimension(650, 400));
		setExtendedState(JFrame.MAXIMIZED_BOTH); 
		setLocationRelativeTo(null);
		setLayout(new BorderLayout());
		
		initializeMenu();
		
		initializeTabbedPane(this);
		
		initializeTreePanel();	
		
		initializeToolBar();
		
		JPanel rightPanel = new JPanel();
        rightPanel.add(tabbedPane);
        rightPanel.setLayout(new GridLayout(1,1));
        
        JPanel leftPanel = new JPanel();
        leftPanel.add(treePanel);
        leftPanel.setLayout(new GridLayout(1,1));
        
        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        splitPane.setLeftComponent(leftPanel);
        splitPane.setRightComponent(rightPanel);
        splitPane.setDividerLocation(200);
        splitPane.setContinuousLayout(true);

        this.add(splitPane, BorderLayout.CENTER);
        
        JPanel footer = new JPanel(new FlowLayout(FlowLayout.CENTER));
        currentProject = new JLabel("Current project : ( NONE )");
        footer.setBorder(BorderFactory.createLineBorder(Color.GRAY) );
        footer.add(currentProject);
        this.add(footer, BorderLayout.SOUTH);
        this.addWindowListener(this);
        
        
        resizeListener = new ComponentListener() {
			
			
			public void componentShown(ComponentEvent e) {
				
			}
			
			public void componentResized(ComponentEvent e) {
						
				resizeText(((JFrame) e.getSource()).getWidth() / textScale );
				
				
			}
			
			public void componentMoved(ComponentEvent e) {
				
			}
			
			public void componentHidden(ComponentEvent e) {
				
			}
		};
		
		
		
		pack();
		setVisible(true);
		initialTextSize = getFont().getSize();
		
	}


	
public static String getVersion() {
		return version;
	}


private void initializeToolBar() {
	 toolBar = new ToolBar(this);
	 this.add(toolBar, BorderLayout.NORTH);
		
}


//	private void setInitialEnabled() {
//		deleteFolderButton.setEnabled(false);
//		emptyFolderButton.setEnabled(false);
//		viewFileButton.setEnabled(false);
//		openFileButton.setEnabled(false);
//		newFileButton.setEnabled(false);
//		deleteFileButton.setEnabled(false);
//		saveButton.setEnabled(false);
//		previousbutton.setEnabled(false);
//		nextbutton.setEnabled(false);
//		compilebutton.setEnabled(false);
//		simbutton.setEnabled(false);
//		runbutton.setEnabled(false);
//		graphbutton.setEnabled(false);
//		
//	}
//	
//	private void setEnabledFolder() {
//		deleteFolderButton.setEnabled(true);
//		emptyFolderButton.setEnabled(true);
//		newFileButton.setEnabled(true);
//		previousbutton.setEnabled(true);
//		nextbutton.setEnabled(true);		
//	}

	

	protected void resizeText(int size) {
				
		
		resizeMenu(menuBar, size);
		resizeComponents(
				this.getContentPane(),  
				size
				);	
		
		treePanel.reprint();
		this.getContentPane().revalidate();
		
		
	}
	
	private void resizeMenu(JMenuBar menuBar, int size) {
		for(int i = 0; i < menuBar.getMenuCount(); i++){
			menuBar.getMenu(i).setFont(
					new Font("SansSerif", Font.PLAIN, size)
					);
			for (int j = 0; j < menuBar.getMenu(i).getMenuComponentCount(); j++) {
				menuBar.getMenu(i).getMenuComponent(j).setFont(
						new Font("SansSerif", Font.PLAIN, size)
						);
			}
		}
	}

	private void resizeComponents(Container contentPane, int size) {
		contentPane.setFont(
				new Font("SansSerif", Font.PLAIN, size)
		);
		for(int i = 0; i < contentPane.getComponentCount(); i++){
//			System.out.println( contentPane.getComponent(i));
			if(contentPane.getComponent(i) instanceof Container){
				resizeComponents(((Container) contentPane.getComponent(i)), size);
				
				
			}
			else{
				contentPane.getComponent(i).setFont(
						new Font("SansSerif", Font.PLAIN, size)
				);
			}
			
			if(contentPane.getComponent(i) instanceof JComponent){
				if(((JComponent) contentPane.getComponent(i)).getBorder() instanceof javax.swing.border.TitledBorder)
				((javax.swing.border.TitledBorder) ((JComponent) contentPane.getComponent(i)).getBorder()).
			        setTitleFont(
			        		new Font("SansSerif", Font.BOLD, size)
							);
		        contentPane.getComponent(i).repaint();
			}
			
			contentPane.getComponent(i).revalidate();
			
			
		}
	}

	private void initializeTreePanel() {
		treePanel = new TreePane(this);
		
	}

	private void initializeTabbedPane(UserInterface userInterface) throws IOException {
		System.out.print("\tInit tabbed pane ... ");
		
		tabbedPane = new TabbedPane(userInterface);		
//		this.add(tabbedPane, BorderLayout.CENTER);
				
		System.out.println("Checked!");
	}

//	private void initializeControlPanel() {
//		System.out.print("\tInit control pane ... ");
//		System.out.println("Checked!");
//		
//	}

	private void initializeMenu() {
		System.out.print("\tInit menu pane ... ");
		menuBar = new JMenuBar();

		JMenu fileMenu = new JMenu("File");
		fileMenu.setMnemonic(KeyEvent.VK_F);
		menuBar.add(fileMenu);
//		JMenuItem newMenuItem = new JMenuItem("New", KeyEvent.VK_N);
//		fileMenu.add(newMenuItem);
		
		
		 
		JMenu runMenu = new JMenu("Run");
		runMenu.setMnemonic(KeyEvent.VK_R);
		menuBar.add(runMenu);
//		JMenuItem executeMenuItem = new JMenuItem("Execute...", KeyEvent.VK_E);
//		runMenu.add(executeMenuItem);
		
		JMenu winMenu = new JMenu("Window");
		winMenu.setMnemonic(KeyEvent.VK_W);
		menuBar.add(winMenu);
//		JMenuItem winMenuItem = new JMenuItem("Enable resizing text automatically", KeyEvent.VK_S);
////		winMenu.add(winMenuItem);
//		winMenuItem.addActionListener(new ActionListener() {
//			
//			public void actionPerformed(ActionEvent e) {
//				activateDeactivateAutoResize();
//				
//			}
//		});
		
		
		JMenu helpMenu = new JMenu("Help");
		helpMenu.setMnemonic(KeyEvent.VK_H);
		menuBar.add(helpMenu);
//		JMenuItem helpMenuItem = new JMenuItem("Starting with SMC solver...", KeyEvent.VK_J);
//		helpMenu.add(helpMenuItem);
		
		
		

		    this.setJMenuBar(menuBar);
		System.out.println("Checked!");
		
	}

	protected void activateDeactivateAutoResize() {
		if(autoresize){
//			((JMenuItem) e.getSource()).setText("Enable resizing text automatically");
			autoresize = false;
			this.removeComponentListener(resizeListener);
			resizeText(initialTextSize);
			
		}
		else{
//			((JMenuItem) e.getSource()).setText("Disable resizing text automatically");
			autoresize = true;
			this.addComponentListener(resizeListener);
			resizeText(this.getWidth()/textScale);
		}
	}

//	public void resetTree() {
//
//		System.out.println("Rebuild tree");
//		treePanel.reprint();
////		this.pack();
////		treePanel = new TreePane(this);
//
//	}

	public void setCurrentProject(String string) {
		
//			System.out.println("updating : "+string+".");
			this.currentProject.setText("Current project : "+string);
			this.tabbedPane.enablePanels();
			tabbedPane.setSelectedProject(string);
//			setEnabledFolder();
		
	}

	public void setFormula(MtlFormula form, String project) {
		tabbedPane.setFormula(form, project);
	}

	public void setModel(String s, String project) {
		tabbedPane.setModel(s, project);
		
	}

	public void createProjectInTree(String s) {
		treePanel.create(s);
		
	}

//	public void cleanSmcError() {
//		tabbedPane.cleanSmcError();
//		
//	}

	public void refreshTextSize() {
		if(autoresize){
//			System.out.println("Refresh text()");
			resizeText(this.getWidth() / textScale);
		}
				
	}
	
	public boolean confirmChoice(String s){
		int i =  JOptionPane.showConfirmDialog(null, s, "Confirmation", JOptionPane.YES_NO_OPTION);
		if (i == JOptionPane.YES_OPTION) return true;
		return false;
	}

	
	public void insertNodeInTree(String node) {
		treePanel.insertNodeInTree(node);
		
	}

	protected void launchPanelEditor(File file) {
		boolean isOpen = false;
		for (int i = 0; i < tabbedPane.getTabCount(); i++) {
			if(tabbedPane.getComponentAt(i) instanceof PanelEditor){
				if(((PanelEditor) tabbedPane.getComponentAt(i)).getFile().getAbsolutePath().equals(file.getAbsolutePath())  ) 
				{
					tabbedPane.setSelectedIndex(i);
					isOpen = true;
				}
			}
		}
		
		if(!isOpen){
			JPanel panEditor = new PanelEditor(tabbedPane, file );			
//			tabbedPane.addTab("     File editor    ", panEditor);
			tabbedPane.addCloseablePanel(panEditor,"   "+file.getName()+"  ", true);
			tabbedPane.setSelectedIndex(tabbedPane.getTabCount()-1);
			tabbedPane.resizeText();
		}
	}

	protected void launchPanelParamSMC() {
		boolean isOpen = false;
		for (int i = 0; i < tabbedPane.getTabCount(); i++) {
			if(tabbedPane.getComponentAt(i) instanceof JScrollPane){
				JViewport vp = (JViewport)((JScrollPane) tabbedPane.getComponentAt(i)).getComponent(0);
//				System.out.println(vp);
				for (int j = 0; j < vp.getComponentCount(); j++) {
//					System.out.println("\t"+vp.getComponent(j));
					if(vp.getComponent(j) instanceof PanelParametricSMC)
					{
						tabbedPane.setSelectedIndex(i);
						isOpen = true;
					}
				}
			}
		}
		
		if(!isOpen){
			PanelParametricSMC panSMC = new PanelParametricSMC(tabbedPane);
//			tabbedPane.addTab("     Parametric SMC setting     ", new JScrollPane(panSMC));
			tabbedPane.addCloseablePanel(new JScrollPane(panSMC), "     Exploration setting     ", false);
			tabbedPane.resizeText();
			tabbedPane.setSelectedIndex(tabbedPane.getTabCount()-1);
		}
		
	}
	
	protected void launchPanelSMC() {
		boolean isOpen = false;
		for (int i = 0; i < tabbedPane.getTabCount(); i++) {
			if(tabbedPane.getComponentAt(i) instanceof JScrollPane){
				JViewport vp = (JViewport)((JScrollPane) tabbedPane.getComponentAt(i)).getComponent(0);
//				System.out.println(vp);
				for (int j = 0; j < vp.getComponentCount(); j++) {
//					System.out.println("\t"+vp.getComponent(j));
					if(vp.getComponent(j) instanceof PanelSMC)
					{
						tabbedPane.setSelectedIndex(i);
						isOpen = true;
					}
				}
			}
		}
		
		if(!isOpen){
			PanelSMC panSMC = new PanelSMC(tabbedPane);
//			tabbedPane.addTab("     SMC setting     ", new JScrollPane(panSMC));
			tabbedPane.addCloseablePanel(new JScrollPane(panSMC), "     SMC setting     " , false);
			tabbedPane.resizeText();
			tabbedPane.setSelectedIndex(tabbedPane.getTabCount()-1);
		}
		
	}
	

	protected void launchPanelSimulation() {
		boolean isOpen = false;
		for (int i = 0; i < tabbedPane.getTabCount(); i++) {
			if(tabbedPane.getComponentAt(i) instanceof PanelSimulation)
				{

				((PanelSimulation) tabbedPane.getComponentAt(i)).setCurrentSystem();
				
					tabbedPane.setSelectedIndex(i);
					isOpen = true;
				}
			
		}
		
		if(!isOpen){
			tabbedPane.addCloseablePanel( new PanelSimulation(tabbedPane), "     System Simulation     " , false);
			tabbedPane.setSelectedIndex(tabbedPane.getTabCount()-1);
			tabbedPane.resizeText();
		}
		
	}
	
	public void windowActivated(WindowEvent e) {
		
	}

	public void windowClosed(WindowEvent e) {
		
	}

	public void windowClosing(WindowEvent e) {
		boolean b = confirmChoice("Are you sure you want to close the main window?");
		if(b) {
			System.exit(0);
		}
		
	}

	public void windowDeactivated(WindowEvent e) {
		
	}

	public void windowDeiconified(WindowEvent e) {
		
	}

	public void windowIconified(WindowEvent e) {
		
	}

	public void windowOpened(WindowEvent e) {
		
	}

	
	


	public TreePane getTreePanel() {
		return treePanel;
	}


	public boolean isAutoresize() {
		return autoresize;
	}


	public void resetCurrentProject() {
		currentProject.setText("Current project : ( NONE )");
		
	}


	public void setFormula(LtlFormula form, String project) {
		tabbedPane.setFormula(form, project);
	}


	protected void launchPanelRareEvent()	{
		boolean isOpen = false;
		for (int i = 0; i < tabbedPane.getTabCount(); i++) {
			if(tabbedPane.getComponentAt(i) instanceof JScrollPane){
				JViewport vp = (JViewport)((JScrollPane) tabbedPane.getComponentAt(i)).getComponent(0);
//				System.out.println(vp);
				for (int j = 0; j < vp.getComponentCount(); j++) {
//					System.out.println("\t"+vp.getComponent(j));
					if(vp.getComponent(j) instanceof PanelRareEvent)
					{
						tabbedPane.setSelectedIndex(i);
						isOpen = true;
					}
				}
			}
		}
		
		if(!isOpen){
			PanelRareEvent panRareEvent;
			try {
				panRareEvent = new PanelRareEvent(tabbedPane);
				JScrollPane jsp = new JScrollPane(panRareEvent);
				tabbedPane.addCloseablePanel(jsp, "     Importance splitting setting     " , false);
				tabbedPane.resizeText();
				tabbedPane.setSelectedIndex(tabbedPane.getTabCount()-1);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			tabbedPane.addTab("     SMC setting     ", new JScrollPane(panSMC));
			
		}
		
	}
}
