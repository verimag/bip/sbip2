package ujf.verimag.sbip.gui;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Container;
import java.awt.Component;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.tree.*;

import ujf.verimag.sbip.gui.dialogs.TraceViewer;
import ujf.verimag.sbip.parser.formula.LtlFormula;
import ujf.verimag.sbip.parser.formula.MtlFormula;

import javax.swing.filechooser.FileSystemView;

import java.util.Arrays;
import java.io.*;


class FileBrowser {
	
	private FileSystemView fileSystemView;
    private File currentFile;
 	private JPanel gui;
 	private JTree tree;
    private DefaultTreeModel treeModel;    
    UserInterface GUI;
	protected DefaultMutableTreeNode node;
	boolean clicked = false;
	
    public FileBrowser(UserInterface gUI2) {
    	this.GUI = gUI2;
	}

    public Container getGui() {
    	
    	
        if (gui==null) {
            gui = new JPanel(new BorderLayout(3,3));
            gui.setBorder(new EmptyBorder(5,0,5,0));        

            fileSystemView = FileSystemView.getFileSystemView();
            Desktop.getDesktop();
           
            DefaultMutableTreeNode root = new DefaultMutableTreeNode();
            root.setUserObject(new File("./Workspace"));
            treeModel = new DefaultTreeModel(root);

            TreeSelectionListener treeSelectionListener = new TreeSelectionListener() {
               
            	
				public void valueChanged(TreeSelectionEvent tse){
                	 
                     node =
                        (DefaultMutableTreeNode)tse.getPath().getLastPathComponent();
                    
                    File file = (File) node.getUserObject();
                    currentFile = (File) node.getUserObject();
//                    System.out.println(file.getName());
                    if (file.isDirectory()) 
                    {
                        File[] files = fileSystemView.getFiles(file, false);
                        Arrays.sort(files);
                        //!!
//                        if (node.isLeaf()) 
                        {
                            for (File child : files) {
//                                if (child.isDirectory()) 
                                {
//                                	System.out.println(child.getAbsolutePath());
                                	DefaultMutableTreeNode child_node = new DefaultMutableTreeNode(child);
                                	boolean add = true;
                                	for(int i = 0; i< treeModel.getChildCount(node); i++)
                                	{
                                		if( treeModel.getChild(node,i).toString().equals( child.getAbsolutePath()))
                                		{
//                                			System.out.println( "Found");
                                			add = false;
                                		}
//                                		else
//                                			System.out.println("Not found: "+child.getName());
                                	}
                                	
                                	if(add)
                                	 {
//                                		System.out.println("adding : "+ child_node.toString());
                                		node.add(child_node);
                                	 }
                                }
                            }
                        }
                    }

                    if(((File)node.getUserObject()).isFile())
                    {
                    	System.out.println("selected : "+((File)node.getUserObject()).getName());
                    	String s = ((File)node.getUserObject()).getName();
                    	if(s.endsWith(".mtl") || s.endsWith(".ltl") || s.endsWith(".sf")){
//                    			FileInputStream in;
//								try {
//									in = new FileInputStream(((File)node.getUserObject()));
//									   ObjectInputStream ois = new ObjectInputStream(in);
//		                    		   MtlFormula form = (MtlFormula) ois.readObject();
		                    		   File parent = new File(((File)node.getUserObject()).getParent());
		                    		   File project = new File(parent.getParent());
//		                    		   System.out.println("\t in "+ project.getName());
//		                    		   GUI.setFormula(form, project.getName());
		                    		   GUI.setCurrentProject(project.getName());
//								} 
//								catch (FileNotFoundException e) {
//									e.printStackTrace();
//								} 
//									catch (IOException e) {
//									e.printStackTrace();
//								} 
//								catch (ClassNotFoundException e) {
//									e.printStackTrace();
//								}
                    		  
                    	}
                    	else if(s.endsWith(".t")){
                   		 File parent = new File(((File)node.getUserObject()).getParent());
             		     File project = new File(parent.getParent());
//             		     System.out.println("\t in "+ project.getName());
             		     GUI.setCurrentProject(project.getName());
//             		    TraceViewer tv = new TraceViewer(((File)node.getUserObject()));
                    	
                    	}
                    	else if(s.endsWith(".bip") || s.endsWith(".txt")){
                    		File parent = new File(((File)node.getUserObject()).getParent());
                  		   File project = new File(parent.getParent());
//                  		   System.out.println("\t in "+ project.getName());
                  		   GUI.setCurrentProject(project.getName());
//                    		GUI.launchPanelEditor((File)node.getUserObject()) ;
                    	}
                    	else if( s.endsWith(".hpp") || s.endsWith(".cpp") || s.endsWith(".h") || s.endsWith(".c")){
                    		File parent_ext = new File(((File)node.getUserObject()).getParent());
                    		File parent = new File(parent_ext.getParent());
                  		   File project = new File(parent.getParent());
//                  		   System.out.println("\t in "+ project.getName());
                  		   GUI.setCurrentProject(project.getName());
//                    		GUI.launchPanelEditor((File)node.getUserObject()) ;
                    	}
                    	else{
                    		String[] parts = s.split("\\.");
                    		if(parts.length == 1){
                    			File parent = new File(((File)node.getUserObject()).getParent());
	                    		File project = new File(parent.getParent());
//	                    		System.out.println("\t in "+ project.getName());
	                    		GUI.setCurrentProject(project.getName());
//                    			GUI.setModel(((File)node.getUserObject()).getAbsolutePath(), project.getName());
                    		}
                    	}
                    	
                    	
//                    	GUI.cleanSmcError();
//                    	GUI.enableFileButtons();
//                    	
                    }
                    if(((File)node.getUserObject()).isDirectory() && !((File)node.getUserObject()).getName().equals("Properties") && !((File)node.getUserObject()).getName().equals("Outputs") && !((File)node.getUserObject()).getName().equals("Models") && !((File)node.getUserObject()).getName().equals("ext-cpp"))
                    	{
                    	GUI.setCurrentProject(((File)node.getUserObject()).getName());
//                    	System.out.println("selected project: "+((File)node.getUserObject()).getName()+".");
                    	
                    	}
                    
                    if(((File)node.getUserObject()).isDirectory() && (((File)node.getUserObject()).getName().equals("Properties") || ((File)node.getUserObject()).getName().equals("Outputs") || ((File)node.getUserObject()).getName().equals("Models")))
                        {
                    	File parent = new File(((File)node.getUserObject()).getAbsolutePath());
              		   File project = new File(parent.getParent());
//              		   System.out.println("\t in "+ project.getName());
              		   GUI.setCurrentProject(project.getName());
                    }
                    
                    if(((File)node.getUserObject()).isDirectory() && ((File)node.getUserObject()).getName().equals("ext-cpp"))
                    {
                	File parent = new File(((File)node.getUserObject()).getParent());
          		   File project = new File(parent.getParent());
//          		   System.out.println("\t in "+ project.getName());
          		   GUI.setCurrentProject(project.getName());
                }
                
                
                }

				
            };

            File[] roots = fileSystemView.getFiles(new File("./Workspace"),false);
            Arrays.sort(roots);
            for (File fileSystemRoot : roots) {
                DefaultMutableTreeNode node = new DefaultMutableTreeNode(fileSystemRoot);
                root.add( node );
//                File[] files = fileSystemView.getFiles(fileSystemRoot, true);
//                Arrays.sort(files);
                
                
                File folderOutputs = new File(fileSystemRoot.getAbsolutePath()+"/Outputs");
                if(!folderOutputs.exists()) {
                	System.out.println("creating : "+folderOutputs.getAbsolutePath());
                	folderOutputs.mkdir();                            
                }              

                File folderModels = new File(fileSystemRoot.getAbsolutePath()+"/Models");
                if(!folderModels.exists()) {
                	System.out.println("creating : "+folderModels.getAbsolutePath());
                	folderModels.mkdir();                            
                }
                
                File folderProperties = new File(fileSystemRoot.getAbsolutePath()+"/Properties");
                if(!folderProperties.exists()) {
                	System.out.println("creating : "+folderProperties.getAbsolutePath());
                	folderProperties.mkdir();                            
                }
                 
            }

            tree = new JTree(treeModel);
            tree.setRootVisible(false);
            tree.addTreeSelectionListener(treeSelectionListener);
            
            tree.addMouseMotionListener(new MouseMotionAdapter() {
				
				@Override
				public void mouseMoved(MouseEvent arg0) {
					clicked = false;
				}
				
			});
            tree.addMouseListener(new MouseAdapter() {
				
				
				
				public void mouseClicked(MouseEvent ev) {
					if(node!= null)
					if(clicked) {
						File file = (File) node.getUserObject();
						if(file.isFile())
	                    {
//	                    	System.out.println("selected : "+((File)node.getUserObject()).getName());
	                    	String s = file.getName();
	                    	if(s.endsWith(".mtl")){
                    			FileInputStream in;
								try {
									in = new FileInputStream(file);
									 @SuppressWarnings("resource")
									ObjectInputStream ois = new ObjectInputStream(in);
		                    		   MtlFormula form = (MtlFormula) ois.readObject();
		                    		   File parent = new File(file.getParent());
		                    		   File project = new File(parent.getParent());
		                    		   GUI.setFormula(form, project.getName());
								} catch (FileNotFoundException e) {
									e.printStackTrace();
								} catch (IOException e) {
									e.printStackTrace();
								} catch (ClassNotFoundException e) {
									e.printStackTrace();
								}
                    		  
	                    	}
	                    	else if(s.endsWith(".ltl")){
                    			FileInputStream in;
								try {
									in = new FileInputStream(file);
									 @SuppressWarnings("resource")
									ObjectInputStream ois = new ObjectInputStream(in);
		                    		   LtlFormula form = (LtlFormula) ois.readObject();
//		                    		   System.out.println("found formula : "+ form);
		                    		   File parent = new File(file.getParent());
		                    		   File project = new File(parent.getParent());
		                    		   GUI.setFormula(form, project.getName());
								} catch (FileNotFoundException e) {
									e.printStackTrace();
								} catch (IOException e) {
									e.printStackTrace();
								} catch (ClassNotFoundException e) {
									e.printStackTrace();
								}
                    		  
                    	}
	                    	else if(s.endsWith(".t")){
	                   		 
	             		    @SuppressWarnings("unused")
							TraceViewer tv = new TraceViewer(file);
	                    	
	                    	}
	                    	else if(s.endsWith(".bip") || s.endsWith(".hpp") || s.endsWith(".cpp") || s.endsWith(".h") || s.endsWith(".c") || s.endsWith(".txt")){
	                    		;
	                    		GUI.launchPanelEditor(file) ;
	                    	}
	                    	else if(s.endsWith(".sf")){
	                    		// do nothing
	                    	}
	                    	else{
	                    		String[] parts = s.split("\\.");
	                    		if(parts.length == 1){
	                    			File parent = new File(file.getParent());
		                    		File project = new File(parent.getParent());
	                    			GUI.setModel(((File)node.getUserObject()).getAbsolutePath(), project.getName());
	                    		}
	                    	}
	                    	
	                    }
						clicked = false;
					}
					else {
						clicked = true;
					}
					
				}
			});
            tree.setCellRenderer(new FileTreeCellRenderer());
            tree.expandRow(0);
            tree.getSelectionModel().setSelectionMode
            (TreeSelectionModel.SINGLE_TREE_SELECTION);
           
            JScrollPane treeScroll = new JScrollPane(tree);
            tree.setVisibleRowCount(30);
            gui.add(treeScroll, BorderLayout.CENTER);

          
        }
        return gui;
    }

 
    public void create(String s){ 
    	
//    	System.out.println("create project at : "+s);
    	DefaultMutableTreeNode project = new DefaultMutableTreeNode(new File(s));
//    	System.out.println("Root : "+treeModel.getRoot().toString());
    	treeModel.insertNodeInto( project, (MutableTreeNode) treeModel.getRoot(), treeModel.getChildCount(treeModel.getRoot()));
    	treeModel.insertNodeInto( new DefaultMutableTreeNode(new File(s+"/Properties")), project, 0);
    	treeModel.insertNodeInto( new DefaultMutableTreeNode(new File(s+"/Outputs")), project, 0);
    	treeModel.insertNodeInto( new DefaultMutableTreeNode(new File(s+"/Models")), project, 0);
    	
    	if(treeModel.getChildCount(treeModel.getRoot()) == 1) treeModel.reload();
    

    }

    public void reprint(){
    	TreeNode node = (TreeNode) treeModel.getRoot();
    	reprint(node);
    }

    public void reprint(TreeNode node){
    	for (int i = 0; i < node.getChildCount(); i++) {
			treeModel.nodeChanged(node.getChildAt(i));
			if(node.getChildAt(i).getChildCount() != 0){
				reprint(node.getChildAt(i));
			}
			else{
				File f = new File(node.getChildAt(i).toString());
		    	if(node!= (TreeNode) treeModel.getRoot())
				if(!f.exists()){
//					System.out.println("Updating not found file "+f.getAbsolutePath());
					treeModel.removeNodeFromParent((MutableTreeNode) node.getChildAt(i));
				}
			}
		}
    }
    
	public void removeProject(String selectedProject) {
		DefaultMutableTreeNode projectChild = null;
		for (int i = 0; i < ((MutableTreeNode) treeModel.getRoot()).getChildCount(); i++) {
			if(((MutableTreeNode) treeModel.getRoot()).getChildAt(i).toString().equals(selectedProject)){
//				System.out.println("found child node : "+  ((MutableTreeNode) treeModel.getRoot()).getChildAt(i).toString());
				projectChild = (DefaultMutableTreeNode) ((MutableTreeNode) treeModel.getRoot()).getChildAt(i);
			}
		}
		if(projectChild != null)
			treeModel.removeNodeFromParent(projectChild);
		
	
		
	}   
	

	public void closeProject(String selectedP) {
		File project = new File("./Workspace/"+selectedP);
		
		MutableTreeNode projectChild = null;
		for (int i = 0; i < ((MutableTreeNode) treeModel.getRoot()).getChildCount(); i++) {
			if(((MutableTreeNode) treeModel.getRoot()).getChildAt(i).toString().equals(project.getAbsolutePath())){
//				System.out.println("found child node : "+  ((MutableTreeNode) treeModel.getRoot()).getChildAt(i).toString());
				projectChild = (MutableTreeNode) ((MutableTreeNode) treeModel.getRoot()).getChildAt(i);
			}
		}
		if(projectChild != null)
			for (int i = 0; i < projectChild.getChildCount(); i++) {
				treeModel.removeNodeFromParent((MutableTreeNode) projectChild.getChildAt(i));
			}
		
	}

	public void insertNodeInTree(String node2) {
		File f = new File(node2);
		DefaultMutableTreeNode node = new DefaultMutableTreeNode(f);
		if(node2.endsWith(".cpp") || node2.endsWith(".hpp") || node2.endsWith(".c") || node2.endsWith(".h") )
				f = f.getParentFile();
		MutableTreeNode projectChild = null;
		for (int i = 0; i < ((MutableTreeNode) treeModel.getRoot()).getChildCount(); i++) {
			if(((MutableTreeNode) treeModel.getRoot()).getChildAt(i).toString().equals(f.getParentFile().getParent())){
//				System.out.println("found child node : "+  ((MutableTreeNode) treeModel.getRoot()).getChildAt(i).toString());
				projectChild = (MutableTreeNode) ((MutableTreeNode) treeModel.getRoot()).getChildAt(i);
				
				MutableTreeNode models = null;
				if(projectChild != null)
				for (int j = 0; j < projectChild.getChildCount(); j++) {
					if(projectChild.getChildAt(j).toString().equals(f.getParent())){
//						System.out.println("\t found child node : "+  projectChild.getChildAt(j).toString());
						models = (MutableTreeNode) projectChild.getChildAt(j);

				    	
						
						MutableTreeNode node3 = null;
						if(models != null){
							for (int k = 0; k < models.getChildCount(); k++) {
								
								if(models.getChildAt(k).toString().equals(f.getAbsolutePath())){
//									System.out.println("\t\t found child node : "+  models.getChildAt(k).toString());
									node3 = (MutableTreeNode) models.getChildAt(k);					
									
									
								}
							}
							
							
							
							if(node3 == null)
								treeModel.insertNodeInto( node, models, 0);
							else {
								if(node2.endsWith(".cpp") || node2.endsWith(".hpp")  || node2.endsWith(".c") || node2.endsWith(".h") )
									treeModel.insertNodeInto( node, node3, 0);
							}
						}
						
						
					}
				}
				
				
				
			}
		}
		
		
    	
	}

	public File getSelectedFile() {
		return currentFile;
	} 
 

	public void removeFile(File file1) {
		
		File file = file1;
		if(file1.getName().endsWith(".cpp") || file1.getName().endsWith(".hpp") || file1.getName().endsWith(".c") || file1.getName().endsWith(".h") )
			file = file.getParentFile();
//		System.out.println("\tRemove "+file.getAbsolutePath());
		MutableTreeNode projectChild = null;
		for (int i = 0; i < ((MutableTreeNode) treeModel.getRoot()).getChildCount(); i++) {
			if(((MutableTreeNode) treeModel.getRoot()).getChildAt(i).toString().equals(file.getParentFile().getParentFile().getAbsolutePath())){
//				System.out.println("found child node : "+  ((MutableTreeNode) treeModel.getRoot()).getChildAt(i).toString());
				projectChild = (MutableTreeNode) ((MutableTreeNode) treeModel.getRoot()).getChildAt(i);
				
				MutableTreeNode output = null;
				if(projectChild != null)
				for (int j = 0; j < projectChild.getChildCount(); j++) {
					if(projectChild.getChildAt(j).toString().equals(file.getParentFile().getAbsolutePath())){
//						System.out.println("\t found child node : "+  projectChild.getChildAt(j).toString());
						output = (MutableTreeNode) projectChild.getChildAt(j);
						
						MutableTreeNode traceNode = null;
						if(output != null)
						for (int k = output.getChildCount()-1; k >= 0; k--) {
							
							if(output.getChildAt(k).toString().equals(file.getAbsolutePath())){
//								System.out.println("\t\t found child node : "+  output.getChildAt(k).toString());
								traceNode = (MutableTreeNode) output.getChildAt(k);
								
								if(traceNode != null)
								{
									if(file1.getName().endsWith(".cpp") || file1.getName().endsWith(".hpp") || file1.getName().endsWith(".c") || file1.getName().endsWith(".h") )
									{
										// in case of deleting things from ext-cpp
										MutableTreeNode cppNode = null;
										for (int l = traceNode.getChildCount()-1; l >= 0; l--) {
											if(traceNode.getChildAt(l).toString().equals(file1.getAbsolutePath())) {
												cppNode = (MutableTreeNode) traceNode.getChildAt(l);
												if(cppNode != null)
													treeModel.removeNodeFromParent(cppNode);
											}
										}
										if(traceNode.getChildCount() == 0) {
											treeModel.removeNodeFromParent(traceNode);
											(new File( traceNode.toString())).delete();
										}
										else
											treeModel.nodeChanged(traceNode);
									}
									else
										treeModel.removeNodeFromParent(traceNode);									
								}
								treeModel.nodeChanged(output);
							}
						}
					}
				}
				
				
				
			}
		}
		
	}

}


class FileTreeCellRenderer extends DefaultTreeCellRenderer {

    private static final long serialVersionUID = 1L;

	private FileSystemView fileSystemView;

    private JLabel label;

    FileTreeCellRenderer() {
        label = new JLabel();
        label.setOpaque(true);
        fileSystemView = FileSystemView.getFileSystemView();
    }

    public Component getTreeCellRendererComponent(
        JTree tree,
        Object value,
        boolean selected,
        boolean expanded,
        boolean leaf,
        int row,
        boolean hasFocus) {

        DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
        File file = (File)node.getUserObject();
        
        if(file!= null)
        if(file.isDirectory())
        	label.setIcon(new ImageIcon("./Icons8/icons8-Open-20.png"));
        else if(file.getName().endsWith(".mtl") || file.getName().endsWith(".ltl"))
        	label.setIcon(new ImageIcon("./Icons8/icons8-Philosophy Filled.png"));
        else if(file.getName().endsWith(".sf"))
        	label.setIcon(new ImageIcon("./Icons8/icons8-frequency-f-15.png"));
        else if(file.getName().endsWith(".bip"))
        	label.setIcon(new ImageIcon("./Icons8/icons8-Genealogy-15.png"));
        else if(file.getName().endsWith(".t"))
        	label.setIcon(new ImageIcon("./Icons8/icons8-Omega-1.png"));
        else if(file.getName().endsWith(".cpp") || file.getName().endsWith(".c"))
        	label.setIcon(new ImageIcon("./Icons8/cpp.png")); 
        else if(file.getName().endsWith(".hpp") || file.getName().endsWith(".h"))
        	label.setIcon(new ImageIcon("./Icons8/hpp.png"));
        else if(file.getName().endsWith(".txt"))
        	label.setIcon(new ImageIcon("./Icons8/txt.png"));
        else if(file.canExecute())
             	label.setIcon(new ImageIcon("./Icons8/icons8-Console-1.png"));
        else
        	label.setIcon(fileSystemView.getSystemIcon(file));
        
        label.setText(fileSystemView.getSystemDisplayName(file));

        if (selected) {
            label.setBackground(backgroundSelectionColor);
            label.setForeground(textSelectionColor);
        } else {
            label.setBackground(backgroundNonSelectionColor);
            label.setForeground(textNonSelectionColor);
        }
      
        return label;
    }
}