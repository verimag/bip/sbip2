package ujf.verimag.sbip.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import ujf.verimag.sbip.gui.tabs.PanelParametricSMC;
import ujf.verimag.sbip.gui.tabs.PanelRareEvent;
import ujf.verimag.sbip.gui.tabs.PanelSMC;

public class ProgressionViewer extends JFrame implements WindowListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextArea text ;
	private JProgressBar jp = null;
	private JScrollPane jsp;
	private JComponent panProg;
	private ProgressionViewer thisFrame = this;
	private boolean isRunning;
	private boolean close;
	private JPanel parentPanel = null;
	
public ProgressionViewer(PanelRareEvent pan, int nbLevels){
		
		
		isRunning = true;
		parentPanel = pan;
		
		jp = new JProgressBar(0, nbLevels);
		jp.setValue(0);
		jp.setStringPainted(true);
		jp.setPreferredSize(new Dimension(500, 30));
		panProg = new JPanel();
		panProg.setBorder( new TitledBorder("Progression :"));
		panProg.add(jp);
		
		text = new JTextArea();
		text.setText("Initializing progression viewer ...\nStarting Importance Splitting Algorithms ...\n");
		text.setLineWrap(true);
		text.setEditable(false);
		
		JPanel panText = new JPanel();
		panText.setBorder( new TitledBorder("Simulation log : "));
		jsp = new JScrollPane(text);
		jsp.setPreferredSize(new Dimension(500, 300));

		panText.add(jsp);
		
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.setBorder(new EmptyBorder(10, 20, 10, 20));
		panel.add(panText, BorderLayout.CENTER);
		panel.setDoubleBuffered(true);
		panel.add(panProg, BorderLayout.SOUTH);
		
		this.setSize(520, 450);
		this.add(panel, BorderLayout.CENTER);
		this.setTitle("Rare events execution");
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setResizable(false);
		this.pack();
		this.setVisible(true);
		
		this.addWindowListener(this);
		
		
	}
	
	public ProgressionViewer(PanelSMC pan){
		
		isRunning = true;
		parentPanel = pan;
			
		text = new JTextArea();
		text.setText("Initializing progression viewer ...\nStarting Statistical Model Checking ...\n");
		text.setLineWrap(true);
		text.setEditable(false);
		jsp = new JScrollPane(text);
		jsp.setPreferredSize(new Dimension(500, 300));
		
		JPanel panText = new JPanel();
		panText.setBorder( new TitledBorder("Simulation log : "));
		panText.add(jsp);
		
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.setBorder(new EmptyBorder(10, 20, 10, 20));
		panel.add(panText, BorderLayout.CENTER);
		panel.setDoubleBuffered(true);
		
		this.setSize(520, 450);
		this.add(panel);
		this.setTitle("SMC execution");
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.pack();
		this.setVisible(true);
		
		this.addWindowListener(this);
		
		
	}
	
	public ProgressionViewer(PanelParametricSMC pan, int nbSims) {

		isRunning = true;
		parentPanel = pan;
		
		jp = new JProgressBar(0, nbSims);
		jp.setValue(0);
		jp.setStringPainted(true);
		jp.setPreferredSize(new Dimension(500, 30));
		panProg = new JPanel();
		panProg.setBorder( new TitledBorder("Progression :"));
		panProg.add(jp);
		
		text = new JTextArea();
		text.setText("Initializing progression viewer ...\nStarting Statistical Model Checking ...\n");
		text.setLineWrap(true);
		
		JPanel panText = new JPanel();
		panText.setBorder( new TitledBorder("Simulation log : "));
		jsp = new JScrollPane(text);
		jsp.setPreferredSize(new Dimension(500, 300));

		panText.add(jsp);
		
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.setBorder(new EmptyBorder(10, 20, 10, 20));
		panel.add(panText, BorderLayout.CENTER);
		panel.setDoubleBuffered(true);
		panel.add(panProg, BorderLayout.SOUTH);
		
		this.setSize(520, 450);
		this.add(panel, BorderLayout.CENTER);
		this.setTitle("SMC execution");
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setResizable(false);
		this.pack();
		this.setVisible(true);
		
		this.addWindowListener(this);
	}

	public void updateLog(String s){
//		text.setText(text.getText()+"\n"+s);
		text.append(s+"\n");
		jsp.getVerticalScrollBar().setValue(jsp.getVerticalScrollBar().getMaximum());
		
	}

	public void incrementProgressBar(){
		if(jp != null) jp.setValue(jp.getValue()+1);
	}

	public void stopSimulation(){
		this.isRunning = false;
	}

	public void activateProgressBar(int nbSims){
		if(jp == null) {
			updateLog("Estimated number of traces : "+nbSims);
			jp = new JProgressBar(0, nbSims);
			jp.setValue(0);
			jp.setStringPainted(true);
			jp.setPreferredSize(new Dimension(500, 30));
			panProg = new JPanel();
			panProg.setBorder( new TitledBorder("Progression :"));
			panProg.add(jp);
			JPanel pan = new JPanel();
			pan.setBorder(new EmptyBorder(10, 20, 10, 20));
			pan.add(panProg);
			this.getContentPane().add(pan, BorderLayout.SOUTH);
			this.pack();
		}
		
		
	}
	
	
	/// Window listener part
	
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void windowClosing(WindowEvent e) {
		if(isRunning){
			int i = JOptionPane.showConfirmDialog(null, "Are you sure you want to abort simulation?", "Confirmation", JOptionPane.YES_NO_OPTION);
			if(i == JOptionPane.YES_OPTION){
				close = true;
				thisFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			}
			else{
				close = false;
				thisFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
			}
		}
		else{
			thisFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		}
		
		
	}
	
	@Override
	public void windowClosed(WindowEvent e) {
		if(close)
			if(parentPanel instanceof PanelSMC)
				((PanelSMC) parentPanel).abortExecution();
			else if (parentPanel instanceof PanelParametricSMC)
				((PanelParametricSMC) parentPanel).abortExecution();
			else if (parentPanel instanceof PanelRareEvent)
				((PanelRareEvent) parentPanel).abortExecution();
	}
	
	@Override
	public void windowActivated(WindowEvent e) {
		
		
	}
	
	

}
