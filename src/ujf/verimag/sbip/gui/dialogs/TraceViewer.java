package ujf.verimag.sbip.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.SwingWorker;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.Highlighter;
import javax.swing.text.JTextComponent;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.Highlighter.HighlightPainter;

import org.antlr.v4.runtime.misc.ParseCancellationException;

import ujf.verimag.sbip.analysis.TraceVerdict;
import ujf.verimag.sbip.bipInterface.bip.Utilities;
import ujf.verimag.sbip.bipInterface.trace.Trace;
import ujf.verimag.sbip.gui.TabbedPane;
import ujf.verimag.sbip.monitor.Monitor;
import ujf.verimag.sbip.monitor.ltl.LtlMonitor;
import ujf.verimag.sbip.monitor.mtl.MtlMonitor;
import ujf.verimag.sbip.parser.formula.Formula;
import ujf.verimag.sbip.parser.formula.MtlFormula;
import ujf.verimag.sbip.parser.ltl.LtlExprParser;
import ujf.verimag.sbip.parser.mtl.MtlExprParser;

public class TraceViewer extends JFrame {

	
	private static final long serialVersionUID = 1L;
	TraceVerdict trace;
	String trace_content = " ";
	Trace parsedTrace = null;
	

	JPanel panButtons = new JPanel();
	JTabbedPane centralPane = new JTabbedPane() ;
	JPanel panError = new JPanel();
	JLabel error = new JLabel("  ");
	
	public TraceViewer(TraceVerdict t , boolean save_traces) {
		this.trace = t;
		
		
		if(save_traces )
		{
			System.out.println(trace.getURL());
    		BufferedReader br;
			try {
				br = new BufferedReader(new FileReader(trace.getURL()));
				StringBuilder sb = new StringBuilder();
				for (String line; (line = br.readLine()) != null;) {
						sb.append((line)+"\n");
        	       }
        	    trace_content = sb.toString();
        	
			
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		else
		{
			trace_content = ((Trace) trace.getTrace()).toString();
		}

		buildFrame(trace.getName(), !save_traces);
		
		
		
	}
	
	

	public TraceViewer(File file) {
		System.out.println(file.getAbsolutePath());
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(file.getAbsolutePath()));
			StringBuilder sb = new StringBuilder();
			for (String line; (line = br.readLine()) != null;) {
					sb.append((line)+"\n");
    	       }
    	    trace_content = sb.toString();
    	
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		buildFrame(file.getName(), ! trace_content.contains("[BIP ENGINE]: "));
	}
	
	
	
	

	private void buildFrame(String traceName, boolean isParsedTrace) {
		JFrame frame = this;
		String title = (isParsedTrace? (String) ("Parsed trace : "+  traceName): (String) ("Raw trace : "+ traceName));
		frame.setTitle(title); 
		frame.setPreferredSize(new Dimension(600, 650));
		frame.getContentPane().setLayout(new BorderLayout());
		
		// set the content 
		panButtons.setBorder(new EmptyBorder(5,0,5,0));
		final JButton butParse = new JButton("Parse raw trace");
		butParse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Utilities util2 = new Utilities();
				error.setText("Parsing trace...");
				System.out.println();
				parsedTrace = util2.extractTrace(trace_content);
				centralPane.add(new JScrollPane(new PanelTrace(parsedTrace.toString())), "Parsed trace");
				error.setText("Trace parsing completed!");
				centralPane.setSelectedIndex(centralPane.countComponents()-1);
				butParse.setEnabled(false);
			}
		});
		final JButton butCheck = new JButton("Monitor trace");
		butCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				error.setText(" ");
				centralPane.add( new panMonitor(), "Monitoring tab");
				centralPane.setSelectedIndex(centralPane.countComponents()-1);
				butCheck.setEnabled(false);
			}
		});
		
		if(!isParsedTrace) panButtons.add(butParse);
		else {
			if(trace!=null)
			parsedTrace = trace.getTrace();
		}
		
		
		panButtons.add(butCheck);		
		centralPane.add(new JScrollPane(new PanelTrace(trace_content)), "Trace");
		
		
		panError.setBorder(new EmptyBorder(5,0,5,0));
		panError.add(error);
		
		centralPane.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				error.setText(" ");
			}
		});

		frame.getContentPane().add(panButtons, BorderLayout.NORTH);	
		frame.getContentPane().add(centralPane, BorderLayout.CENTER);
		frame.getContentPane().add(panError, BorderLayout.SOUTH);
		
		// activate frame
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.pack();
	}
//////////////////////////////////////////////////////
	///////////////////////////////////////
		////////////////////////////////
	
	
	
private Highlighter.HighlightPainter myHighlightPainter = (HighlightPainter) new MyHighlightPainter(Color.yellow);
	
	public void highlight(JTextComponent textComp, String pattern) {
        // First remove all old highlights
        removeHighlights(textComp);

        try {
            Highlighter hilite = textComp.getHighlighter();
            Document doc = textComp.getDocument();
            String text = doc.getText(0, doc.getLength());

            int pos = 0;
            // Search for pattern
            while ((pos = text.indexOf(pattern, pos)) >= 0) {
                // Create highlighter using private painter and apply around pattern
                hilite.addHighlight(pos, pos + pattern.length(), myHighlightPainter);
                pos += pattern.length();
            }

        } catch (BadLocationException e) {
        }
    }

    // Removes only our private highlights
    public void removeHighlights(JTextComponent textComp) {
        Highlighter hilite = textComp.getHighlighter();
        Highlighter.Highlight[] hilites = hilite.getHighlights();

        for (int i = 0; i < hilites.length; i++) {
            if (hilites[i].getPainter() instanceof MyHighlightPainter) {
                hilite.removeHighlight(hilites[i]);
            }
        }
    }
	
	 private int findLastNonWordChar (String text, int index) {
	        while (--index >= 0) {
	            if (String.valueOf(text.charAt(index)).matches("\\W")) {
	                break;
	            }
	        }
	        return index;
	    }

	    private int findFirstNonWordChar (String text, int index) {
	        while (index < text.length()) {
	            if (String.valueOf(text.charAt(index)).matches("\\W")) {
	                break;
	            }
	            index++;
	        }
	        return index;
	    }
	    
	    class MyHighlightPainter
        extends DefaultHighlighter.DefaultHighlightPainter {

    public MyHighlightPainter(Color color) {
        super(color);
//    		super(new Color(57,105,138));
    }
}

private class PanelTrace extends JTextPane{

	
	    
	public PanelTrace(String content) {
		
		 final SimpleAttributeSet attrPurple = new SimpleAttributeSet();
         StyleConstants.setForeground(attrPurple, new Color(168, 0, 118));
         StyleConstants.setBold(attrPurple, true);

         final SimpleAttributeSet attrRed = new SimpleAttributeSet();
         StyleConstants.setForeground(attrRed, Color.RED);
         StyleConstants.setBold(attrRed, true);
         
         final SimpleAttributeSet attrBlue = new SimpleAttributeSet();
         StyleConstants.setForeground(attrBlue, Color.BLUE);
         StyleConstants.setBold(attrBlue, true);
         
         final SimpleAttributeSet attrGreen = new SimpleAttributeSet();
         StyleConstants.setForeground(attrGreen, new Color(0, 168, 50));
         StyleConstants.setBold(attrGreen, true);

         final SimpleAttributeSet attrBlack = new SimpleAttributeSet();
         StyleConstants.setForeground(attrBlack, Color.black);
         StyleConstants.setBold(attrBlack, false);

         final SimpleAttributeSet attrLightGray = new SimpleAttributeSet();
         StyleConstants.setForeground(attrLightGray, Color.lightGray);
         StyleConstants.setBold(attrLightGray, false);
 
         DefaultStyledDocument doc = new DefaultStyledDocument() {
        	 
             /**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public void insertString (int offset, String str, AttributeSet a) throws BadLocationException {
                 super.insertString(offset, str, a);
                 
                 String text = getText(0, getLength());
               
                 int before = findLastNonWordChar(text, offset);
                 if (before < 0) before = 0;
                 int after = findFirstNonWordChar(text, offset + str.length());
                 int wordL = before;
                 int wordR = before;

                 while (wordR <= after) {
                     if (wordR == after || String.valueOf(text.charAt(wordR)).matches("\\W")) {
                    	 
                    	 if (text.substring(wordL, wordR).matches("(\\W)*(choose|var|@GlobalTime)"))
                             setCharacterAttributes(wordL, wordR - wordL, attrRed, false);
                    	 else if (text.substring(wordL, wordR).matches("(\\W)*(#True|#False|int|bool|float|component|string|clock|unit|priority|initial|place|from|to|do|provided|on|define|down|up|export|extern|const|lazy|delayable|eager)"))
                             setCharacterAttributes(wordL, wordR - wordL, attrBlue, false);
                    	 else if (text.substring(wordL, wordR).matches("(\\W)*(BIP|ENGINE|STOCHASTIC)"))
                             setCharacterAttributes(wordL, wordR - wordL, attrPurple, false);
                         else 
                             setCharacterAttributes(wordL, wordR - wordL, attrBlack, false);
                         wordL = wordR;
                     }
                     
                     
                     wordR++;
                 }
                 int position = text.indexOf("State", 0);
                 while(position != -1){
                	 setCharacterAttributes(position, text.indexOf("\n", position)- position, attrGreen, false);
                	 position = text.indexOf("State", position+1);
                 }
                              
             }

             public void remove (int offs, int len) throws BadLocationException {
                 super.remove(offs, len);

                 String text = getText(0, getLength());
                 int before = findLastNonWordChar(text, offs);
                 if (before < 0) before = 0;
                 int after = findFirstNonWordChar(text, offs);

                 if (text.substring(before, after).matches("(\\W)//(.)*\n"))
                     setCharacterAttributes(before, after - before, attrLightGray, false); 
                 else if (text.substring(before, after).matches("(\\W)*(choose|var)"))
                     setCharacterAttributes(before, after - before, attrRed, false);
                 else if (text.substring(before, after).matches("(\\W)*(int|bool|double|string|component|clock|unit|priority|initial|place|from|to|do|provided|on|define|down|up|export|extern|const|lazy|delayable|eager)"))
                     setCharacterAttributes(before, after - before, attrBlue, false);
                 else if (text.substring(before, after).matches("(\\W)*(BIP|ENGINE|STOCHASTIC)"))
                     setCharacterAttributes(before, after - before, attrPurple, false);
                 else
                     setCharacterAttributes(before, after - before, attrBlack, false);
                 
    
                 
                 
             }
         };
        setDocument(doc);
		setEditable(false);
		setText(content);
		final JTextComponent thisComponent = this;
		addCaretListener(new CaretListener() {
			
			public void caretUpdate(CaretEvent e) {
				
				if(getSelectedText() != null && getSelectedText().trim().length()>0)
					highlight(thisComponent, getSelectedText());
				else
					removeHighlights(thisComponent);
			}
		});
	}
	 

}





SwingWorker<?, ?> worker = null;
public JComboBox<String> formulaType;
public JTextArea txtformula;
private class panMonitor extends JPanel{
	private JTextArea text;
	private JScrollPane jsp;
	
	public void addText(String line) {
		 text.append(line+"\n");
		jsp.getVerticalScrollBar().setValue(jsp.getVerticalScrollBar().getMaximum());
    	
	}
	
	
	public panMonitor( ) {

  
		
		setBorder(new EmptyBorder(5,0,5,0));
		JPanel panFormula = new JPanel();
		TitledBorder title1 = BorderFactory.createTitledBorder("Property");
		title1.setTitleJustification(TitledBorder.CENTER);
		panFormula.setBorder(title1);
		panFormula.setLayout(new BoxLayout(panFormula, BoxLayout.X_AXIS));
		
		
		final String[] type = new String[2];  
		type[0] = "MTL";
		type[1] = "LTL";		
		formulaType = new JComboBox<String>(type);	
		txtformula = new JTextArea();
		txtformula.setText(" ");
//		txtformula.setText("F[0.0, 200] (sender.rec==1.0)");
		
		
		panFormula.add(new JLabel("   Type: "));
		panFormula.add(formulaType);	
		panFormula.add(new JLabel("   Path formula: "));
		panFormula.add(txtformula);
		txtformula.setLineWrap(true);
		txtformula.setWrapStyleWord(true);
		
		
		
		JPanel panSim = new JPanel();
		TitledBorder title2 = BorderFactory.createTitledBorder("Monitoring log");
		title2.setTitleJustification(TitledBorder.CENTER);
		panSim.setBorder(title2);
		panSim.setLayout(new BoxLayout(panSim, BoxLayout.X_AXIS));
		text = new JTextArea();
		text.setEditable(false);
		text.setLineWrap(true);
		text.addCaretListener(new CaretListener() {
			
			public void caretUpdate(CaretEvent e) {
				
				if(text.getSelectedText() != null && text.getSelectedText().trim().length()>0)
					highlight(text, text.getSelectedText());
				else
					removeHighlights(text);
			}
		});
		
		jsp = new JScrollPane(text);
		panSim.setMaximumSize( 
			     new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE) );
		panSim.add(jsp);
		
		
		JPanel panRun = new JPanel();
		TitledBorder title3 = BorderFactory.createTitledBorder("Actions");
		title3.setTitleJustification(TitledBorder.CENTER);
		panRun.setBorder(title3);
		final JButton butRun = new JButton("Start monitoring");
		butRun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				error.setForeground(Color.BLACK);
				error.setText(" ");
				if(butRun.getText().equals("Start monitoring")) {
					butRun.setText("Stop monitoring");

					
					
					worker = new SwingWorker<Object, Object>() {
				          protected Object doInBackground() throws Exception {
//				        	    System.out.println("trace: "+ parsedTrace);
				        	    if(parsedTrace == null) {
				        	    	Utilities util = new Utilities();
				        			parsedTrace = util.extractTrace(trace_content);
				        	    }
				        	    
				        	    if(parsedTrace != null) {
				        	    	Formula phi = null;
				        	    	Monitor monitor = null;
				        	    	
				        	    	if(formulaType.getSelectedIndex() == 0) {
//				        	    		System.out.println("Parsing MTL");
				        	   	    	MtlExprParser parser = new MtlExprParser();
				        	   	    	try {
				        	   	    		phi = parser.parse(txtformula.getText());
				        	    		}
				        	    		catch (ParseCancellationException e2) {
											error.setForeground(Color.RED);
											error.setText("Parsing failed : the MTL formula is incorrect!");	
											butRun.setText("Start monitoring");
											return null;
											
										}
//						        	  	System.out.println("Formula () : "+ phi);
						        	  	monitor = new MtlMonitor(null);
						        	  	monitor.enableDebug();
				        	    	}
				        	    	else {
//				        	    		System.out.println("Parsing LTL");
				        	   	    	LtlExprParser parser = new LtlExprParser();
				        	   	    	String txtphi = "P=?["+txtformula.getText()+"]";
						        	  	try {
						        	  		phi = parser.parse(txtphi);
						        	  	} catch (ParseCancellationException e2) {
											error.setForeground(Color.RED);
											error.setText("Parsing failed : the LTL formula is incorrect!");	
											butRun.setText("Start monitoring");
											return null;
											
										}
//						        	  	System.out.println("Formula () : "+ phi);
						        	  	monitor = new LtlMonitor();
						        	  	monitor.enableDebug();
				        	    	}
					        	  	
					        	  	
					        	  	if(phi!= null && monitor != null) {
					        	  		monitor.check(phi, parsedTrace);
						        	  	while(monitor.log.size()==0) {
						        	  		System.out.println("waiting ...");
						        	  	};
						        	  	
						        	  	int i = 0;
						        	  	while(!monitor.log.get(i).equals("END")) {
						        	  		addText(monitor.log.get(i));
						        	  		if(i+1 < monitor.log.size()) i++;
						        	  	}
					        	  	}else {
					        	    	addText("Error in instantiation");
					        	    	addText("Nothing to be done.");
					        	    }
					        	  	
				        	    }
				        	    else {
				        	    	addText("Error : Trace is empty!");
				        	    	addText("Nothing to be done.");
				        	    }
				        	  	
								return null;
				          }	
				          
				          protected void done() {
				        	  
				          }
				    };
				    worker.execute();
				}
				else if(butRun.getText().equals("Stop monitoring")) {
					butRun.setText("Clear log");
					stopWorker();
				}
				else if(butRun.getText().equals("Clear log")) {
					text.setText("");
					butRun.setText("Start monitoring");					
				}
				
			}
		});
		panRun.add(butRun);
		

		this.setLayout(new BorderLayout());
		this.add(panFormula, BorderLayout.NORTH);
		this.add(panSim, BorderLayout.CENTER);
		this.add(panRun, BorderLayout.SOUTH);
	}
	
}


public void stopWorker() {
//  JOptionPane.showMessageDialog(null, "Kill worker");	
	if (worker != null) 
//	if(!worker.isDone()){
		worker.cancel(true);
		worker.isCancelled();
//	}
	
}




public static void main(String[] args) {
	TraceViewer t = new TraceViewer(
				new File("/home/mediouni/workspace/sbip2/./Workspace/Bluetooth/Outputs/trace_1000.t")
				);

	t.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
}

}


