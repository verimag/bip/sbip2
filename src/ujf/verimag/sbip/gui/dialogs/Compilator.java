package ujf.verimag.sbip.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.channels.FileChannel;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import ujf.verimag.sbip.gui.TabbedPane;

public class Compilator extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextArea text ;
	private JScrollPane jsp;
	private int selectedEngine;
	private String packageName;
	@SuppressWarnings("unused")
	private String compoundName;
	private String projectModel;
	private TabbedPane tabbedpane;
	
	public Compilator(int sel){
		this.selectedEngine = sel;
	}
	
	public Compilator(TabbedPane tabbedpane , String projectM, int selectedIndex, String packageName, String compoundName) {
		
		this.tabbedpane = tabbedpane;
		this.projectModel = projectM;
		this.selectedEngine = selectedIndex;
		this.packageName = packageName;
		this.compoundName = compoundName;
		
		text = new JTextArea();
		text.setLineWrap(true);
		text.setEditable(false);
		jsp = new JScrollPane(text);
		jsp.setPreferredSize(new Dimension(600, 450));
		
		JPanel panText = new JPanel();
		panText.setBorder( new TitledBorder("Compilation log : "));
		panText.add(jsp);
		
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.setBorder(new EmptyBorder(10, 20, 10, 20));
		panel.add(panText, BorderLayout.CENTER);
		panel.setDoubleBuffered(true);
		
		this.setSize(700, 550);
		this.add(panel);
		this.setTitle("BIP compilation");
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.pack();
		this.setVisible(true);
		
			SwingWorker<?, ?> worker = new SwingWorker<Object, Object>() {
		          public Object doInBackground() throws Exception {
		        	  try {
		        			
		        	  runCompilationFromScript();
		        	 
		          } catch (IOException e) {

		  			e.printStackTrace();
		  		}
		        	  return null;
		          }
			 };
			 worker.execute();
		
		
		
	}
	
	
	
	
	

	private void runCompilationFromScript() throws IOException {
		getCompilationScript();
//		final StringBuilder builder = new StringBuilder();
		
//		ProcessBuilder cmdBuild = new ProcessBuilder();

		

		
		String cmd = "./"+getScriptName()+" -c "+packageName;//+" "+compoundName;
		final Process p = Runtime.getRuntime().exec(cmd, null, new File(projectModel));
//		cmdBuild.redirectErrorStream(true);
//		cmdBuild.command(cmd);
//		cmdBuild.directory(new File(projectModel));
//		Process p = cmdBuild.start();
		

//		p = Runtime.getRuntime().exec(new String[]{"bash","-c",cmd});
		

		       
		
					try (final BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
							final BufferedReader berror = new BufferedReader(new InputStreamReader(p.getErrorStream()))) {

				        String line = "";
				        String line2 = "";
				        while ((line = b.readLine()) != null || (line2 = berror.readLine()) != null) {
				        	
				  
				        	
				        	
			//	        	builder.append(line+ "\n");
			//	        	System.out.println(line);

				        	if(line!="" && line!= null) updateLog(line);
				        	if(line2!="" && line2!= null) updateLog(line2);
				        }
				        
//				       String  raw = builder.toString();
						
				        
				    } catch (IOException e1) {
		    			System.out.println("Stream closed");
					}
					
		
			
		
		try
		{
			p.waitFor();
		}		
		catch(InterruptedException e){
			System.out.println("Aborted trace generator!");
		}
    	
		System.out.println("Process exit value : "+p.exitValue());
		removeCompilationScript();
		
		 File system = new File(projectModel+"/system");
   		System.out.println(system.getAbsolutePath()+" :: "+system.exists());
   		if(system.exists()){
   			this.updateLog("\nThe compilation terminated with success!");
   			tabbedpane.insertNodeInTree(system.getAbsolutePath());
   			this.updateLog("\n");
   			
   		}
	}
	
	public void updateLog(String s){
//		text.setText(text.getText()+"\n"+s);
		text.append("\n"+s);
		jsp.getVerticalScrollBar().setValue(jsp.getVerticalScrollBar().getMaximum());
	}

	private void removeCompilationScript() {
//		if(selectedEngine == 2)
		{
			File new_script = new File(projectModel+"/"+getScriptName());
			new_script.delete();
		}
		
	}

	private void getCompilationScript() {
//		if(selectedEngine == 2)
		{
			File script = new File("./scripts/"+getScriptName());
			File new_script = new File(projectModel+"/"+getScriptName());
			
			try {
				copyFileUsingChannel(script, new_script);
				new_script.setExecutable(true);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@SuppressWarnings("resource")
	private void copyFileUsingChannel(File source, File dest) throws IOException {
	    FileChannel sourceChannel = null;
	    FileChannel destChannel = null;
	    try {
	        sourceChannel = new FileInputStream(source).getChannel();
	        destChannel = new FileOutputStream(dest).getChannel();
	        destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
	       }finally{
	           sourceChannel.close();
	           destChannel.close();
	   }
	}
	
	public String getScriptName(){
		if(selectedEngine == 1)
			return "compile_run.sh";
//		if(selectedEngine == 1)
//			return "compile_run_bip2.sh";
		
		if(selectedEngine == 0)
			return "compile_run_bip.sh";
		return "script.sh";
	}

}
