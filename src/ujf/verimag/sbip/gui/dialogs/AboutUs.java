package ujf.verimag.sbip.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Calendar;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import ujf.verimag.sbip.gui.UserInterface;

public class AboutUs extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AboutUs(){
		super("About SBIP - SMC Engine");
		setSize(new Dimension(550, 280));
		setLayout(new BorderLayout());
		setResizable(false);
		setLocationRelativeTo(null);
		((JComponent) this.getContentPane()).setBorder(new EmptyBorder(10,10,10,10));
		Image image;
		JLabel icone = new JLabel("SBIP");
        
        try {
			image = ImageIO.read(new File("logo.png"));
			image = image.getScaledInstance(75, 75, 20);
           icone = new JLabel(new ImageIcon(image ));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        JPanel panIcone = new JPanel();
        panIcone.add(icone);
        
        JPanel panText = new JPanel();
        JEditorPane texte = new JEditorPane();
        texte.setContentType("text/html");
        texte.setEditable(false);
        texte.setOpaque(false);
        texte.setBorder(BorderFactory.createEmptyBorder());
        texte.setBackground(new Color(0,0,0,0));
        texte.setText        		
        		("<html>" + "<h3>SBIP - SMC Solver version "+UserInterface.getVersion()+", "+monthToString(Calendar.getInstance().get(Calendar.MONTH))+" "+ Calendar.getInstance().get(Calendar.YEAR)+ "<br />" + 
        		"CeCILL-B FREE SOFTWARE LICENSE<hr> </h3>" + 
        		"<i>Verimag, University Grenoble-Alpes, France</i> <br />" + 
        		"©Copyright 2016-"+Calendar.getInstance().get(Calendar.YEAR)+". All rights reserved." + "<br />" +  "<br />" + 
        		"SBIP framework is an environment for BIP modeling and Sta-<br />" + 
        		"tistical Model Checking. Additional information about us can<br />" + 
        		"be found <a href=\"http://www.verimag.fr\">here</a>  .<br /><br />" + 
        		"This version is maintained by Braham Lotfi Mediouni. Please <br />" +
        		"contact us at the following <a href=\"mailto:braham-lotfi.mediouni@univ-grenoble-alpes.fr\">address</a> for any information or for <br /> " +
        		"reporting a bug."+
        		
        		
        		
        		
        		"</html>");

        
        texte.addHyperlinkListener(new HyperlinkListener() {
        	public void hyperlinkUpdate(HyperlinkEvent hle) {
        		if (HyperlinkEvent.EventType.ACTIVATED.equals(hle.getEventType())) {
        			Desktop desktop = Desktop.getDesktop();
        			try {
						desktop.browse(new URI(hle.getURL().toString()));
					} catch (IOException e) {
						e.printStackTrace();
					} catch (URISyntaxException e) {
						e.printStackTrace();
					}
        		}
        		}
        		});
        		
        panText.add(texte);
	
          

        this.add(panText, BorderLayout.CENTER);
        this.add(panIcone, BorderLayout.WEST);
        
		setAlwaysOnTop(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);
		
	}

	private String monthToString(int i) {
		switch(i) {
		case 0:
			return "January";
		case 1:
			return "February";
		case 2:
			return "March";
		case 3:
			return "April";
		case 4:
			return "May";
		case 5:
			return "June";
		case 6:
			return "July";
		case 7:
			return "August";
		case 8:
			return "September";
		case 9:
			return "October";
		case 10:
			return "November";
		case 11:
			return "December";
		default:
			return "";
		}
	}
	


}
