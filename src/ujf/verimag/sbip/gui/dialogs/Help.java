package ujf.verimag.sbip.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeSelectionModel;

public class Help extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private  JEditorPane  text = new JEditorPane();
	private JTree tree ;
	
	@SuppressWarnings("deprecation")
	public Help() {
		super("Help me");
		setSize(new Dimension(900, 600));
		setLayout(new BorderLayout());
		setLocationRelativeTo(null);
		((JComponent) this.getContentPane()).setBorder(new EmptyBorder(10,10,10,10));
				
		text.setContentType("text/html");
		text.setEditable(false);
		text.addHyperlinkListener(new HyperlinkListener() {
        	public void hyperlinkUpdate(HyperlinkEvent hle) {
        		if (HyperlinkEvent.EventType.ACTIVATED.equals(hle.getEventType())) {
        			Desktop desktop = Desktop.getDesktop();
        			try {
						desktop.browse(new URI(hle.getURL().toString()));
					} catch (IOException e) {
						e.printStackTrace();
					} catch (URISyntaxException e) {
						e.printStackTrace();
					}
        		}
        		}
        		});
		
		// JTREE
		 DefaultMutableTreeNode root = new DefaultMutableTreeNode("Helper");
	        //create the child nodes
	        DefaultMutableTreeNode start  = new DefaultMutableTreeNode("How to start");
	        DefaultMutableTreeNode gui = new DefaultMutableTreeNode("Graphical interface");	        
	        DefaultMutableTreeNode model = new DefaultMutableTreeNode("Model edition");
	        DefaultMutableTreeNode property = new DefaultMutableTreeNode("Property edition");
	        DefaultMutableTreeNode smc = new DefaultMutableTreeNode("System analysis");
	        
	        
	        
	        //add the child nodes to the root node
	        root.add(start);
	        root.add(gui);
	        root.add(model);
	        root.add(property);
	        root.add(smc);
//	        root.add(output);
	         
	        //create the tree by passing in the root node
	        tree = new JTree(root);
	        try {
	        	File file = new File("help/start.html");
				text.setPage(file.toURL());
			} catch (IOException e) {
				e.printStackTrace();
			} 

            tree.setRootVisible(false);
            tree.setBorder(new EmptyBorder(10,0,0,0));
            tree.getSelectionModel().setSelectionMode
            (TreeSelectionModel.SINGLE_TREE_SELECTION); 
            tree.setCellRenderer(new FileTreeCellRenderer());
            
	        tree.addTreeSelectionListener(new TreeSelectionListener() {
				
				public void valueChanged(TreeSelectionEvent tse) {
					DefaultMutableTreeNode node = (DefaultMutableTreeNode)tse.getPath().getLastPathComponent();
//					System.out.println(node.getUserObject());

					switch((String) node.getUserObject()) {
					case "How to start" : 
						try {
							File file = new File("help/start.html");
							text.setPage(file.toURL());
						} catch (IOException e) {
							e.printStackTrace();
						} 
						break;
						
					case "Graphical interface" : 
						try {
							File file = new File("help/gui.html");
							text.setPage(file.toURL());
						} catch (IOException e) {
							e.printStackTrace();
						} 
						break;
					case "Model edition" : 
						try {
							File file = new File("help/model.html");
							text.setPage(file.toURL());
						} catch (IOException e) {
							e.printStackTrace();
						} 
						break;
					case "Property edition" : 
						try {
							File file = new File("help/property.html");
							text.setPage(file.toURL());
						} catch (IOException e) {
							e.printStackTrace();
						} 
						break;
					case "System analysis" : 
						try {
							File file = new File("help/analysis.html");
							text.setPage(file.toURL());
						} catch (IOException e) {
							e.printStackTrace();
						} 
						break;

					case "Output interpretation" : 
						try {
							File file = new File("help/output.html");
							text.setPage(file.toURL());
						} catch (IOException e) {
							e.printStackTrace();
						} 
						break;
						
					default : 
						try {
							File file = new File("help/start.html");
							text.setPage(file.toURL());
						} catch (IOException e) {
							e.printStackTrace();
						} 
					}
				}
			});
	        
	   	        
	        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
	        splitPane.setLeftComponent(new JScrollPane(tree));
	        splitPane.setRightComponent(new JScrollPane(text));
	        splitPane.setDividerLocation(200);
	        splitPane.setContinuousLayout(true);
	        splitPane.setBorder(new EmptyBorder(10,10,10,10));
	        add(splitPane);
		
		setAlwaysOnTop(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);
	}

//	public static void main(String[] args) {
//		Help h = new Help();
//	}
	

	
	
	

	
class FileTreeCellRenderer extends DefaultTreeCellRenderer {

    private static final long serialVersionUID = 1L;
    private JLabel label;

    FileTreeCellRenderer() {
        label = new JLabel();
        label.setOpaque(true);
    }

    public Component getTreeCellRendererComponent(
        JTree tree,
        Object value,
        boolean selected,
        boolean expanded,
        boolean leaf,
        int row,
        boolean hasFocus) {

        DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
    
        
        
        	label.setIcon(new ImageIcon("./Icons8/icons8-Help-20.png"));
        	label.setText((String) node.getUserObject());

        if (selected) {
            label.setBackground(backgroundSelectionColor);
            label.setForeground(textSelectionColor);
        } else {
            label.setBackground(backgroundNonSelectionColor);
            label.setForeground(textNonSelectionColor);
        }

        return label;
    }
}

}
