package ujf.verimag.sbip.gui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;

import ujf.verimag.sbip.gui.dialogs.AboutUs;
import ujf.verimag.sbip.gui.dialogs.Compilator;
import ujf.verimag.sbip.gui.dialogs.Help;
import ujf.verimag.sbip.gui.dialogs.TraceViewer;
import ujf.verimag.sbip.gui.tabs.PanelEditor;

public class ToolBar extends JToolBar{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private  JButton newFolderButton = new JButton(new ImageIcon("./Icons8/icons8-Add Folder.png"));
	private  JButton deleteFolderButton = new JButton(new ImageIcon("./Icons8/icons8-Delete Folder.png"));
	private  JButton emptyFolderButton = new JButton(new ImageIcon("./Icons8/icons8-Trash Can.png"));
	private  JButton viewFileButton = new JButton(new ImageIcon("./Icons8/icons8-View-30.png"));
	private  JButton openFileButton = new JButton(new ImageIcon("./Icons8/icons8-Edit File.png"));
	private  JButton newFileButton = new JButton(new ImageIcon("./Icons8/icons8-Add File.png"));
	private  JButton deleteFileButton = new JButton(new ImageIcon("./Icons8/icons8-Delete File_2.png"));
	private  JButton saveButton = new JButton(new ImageIcon("./Icons8/icons8-Save.png"));
	private  JButton previousbutton = new JButton(new ImageIcon("./Icons8/icons8-Previous.png"));
	private  JButton nextbutton = new JButton(new ImageIcon("./Icons8/icons8-Next.png"));
	private  JButton compilebutton = new JButton(new ImageIcon("./Icons8/icons8-Compile.png"));
	private  JButton simbutton = new JButton(new ImageIcon("./Icons8/icons8-Circled Play_2.png"));
	private  JButton runbutton = new JButton(new ImageIcon("./Icons8/icons8-Run Command.png"));
	private  JButton graphbutton = new JButton(new ImageIcon("./Icons8/icons8-Positive Dynamic.png"));
	private  JButton rareEventbutton = new JButton(new ImageIcon("./Icons8/icons8-active-directory.png"));
	private  JButton fitbutton = new JButton(new ImageIcon("./Icons8/icons8-Fit to Width.png"));
	private  JButton configbutton = new JButton(new ImageIcon("./Icons8/icons8-Settings.png"));
	private  JButton helpbutton = new JButton(new ImageIcon("./Icons8/icons8-Help.png"));
	private  JButton infobutton = new JButton(new ImageIcon("./Icons8/icons8-Info.png"));	
	
	
	
	private UserInterface gui;
	private TabbedPane tabbedPane;
	private TreePane treePanel;
	
	public ToolBar(UserInterface gui) {
		super("Toolbar", JToolBar.HORIZONTAL);
		setFloatable(false);
		this.gui = gui;
		this.tabbedPane = gui.getTabbedPane();
		this.treePanel = gui.getTreePanel();

		initializeToolBar();
		
	}
	
	
	private void initializeToolBar() {
		
		
		 
		 newFolderButton.setToolTipText("Create new project");
		 newFolderButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				String s = JOptionPane.showInputDialog(null, "Enter the name of the new project : ", "Project creation", JOptionPane.QUESTION_MESSAGE);
				if(s!= null)
				if(!s.equals("")){
					File f = new File("./Workspace/"+s);
					if(f.exists()){
						JOptionPane.showMessageDialog(null, "Project creation failed : The project '"+s +"' already exists!");
					}
					else{
						f.mkdir();
						File props = new File("./Workspace/"+s+"/Properties");
						props.mkdir();
						File models = new File("./Workspace/"+s+"/Models");
						models.mkdir();
						File outputs = new File("./Workspace/"+s+"/Outputs");
						outputs.mkdir();
						tabbedPane.createProjectInTree(f.getAbsolutePath());
						JOptionPane.showMessageDialog(null, "The project '"+s +"' has been successfully created");
					}
				}
				else{
					JOptionPane.showMessageDialog(null, "Project creation failed : the project name is an empty string!");
				}
				
			}
		});
		 this.add(newFolderButton);
		 
		 deleteFolderButton.setToolTipText("Delete project");
		 deleteFolderButton.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent arg0) {
					if(!tabbedPane.getSelectedProject().equals("")) {
						if(gui.confirmChoice("You are about to delete the project '"+tabbedPane.getSelectedProject()+"'.\nThis operation is definitive and cannot be reverted.\n Are you sure you want to delete the project '"+tabbedPane.getSelectedProject()+"'?")){
							File folder = new File("./Workspace/"+tabbedPane.getSelectedProject());
							treePanel.removeProject(folder.getAbsolutePath());
							delete(folder);
							JOptionPane.showMessageDialog(null, "The project '"+tabbedPane.getSelectedProject()+"' has been deleted.", "Project deletion", JOptionPane.INFORMATION_MESSAGE);
							
							tabbedPane.resetAll();
							gui.resetCurrentProject();
							
						}
						
					}
					else 

						JOptionPane.showMessageDialog(null, "Operation failed : please select a project first!");
					
					
				}

				private void delete(File folder) {
					if(folder.isDirectory()){
						File[] traces = folder.listFiles(new FilenameFilter() {
							
							public boolean accept(File dir, String name) {
								return true;
							}
						});
						for (int i = 0; i < traces.length; i++) {
//							System.out.println("Deleting "+traces[i].getAbsolutePath());
							delete(traces[i]);							
						}					
					}	
					
					folder.delete();				
				}
			});
		 this.add(deleteFolderButton);
		 
//		 JButton openFolderButton = new JButton(new ImageIcon("Icons8/icons8-Open.png"));
//		 openFolderButton.setToolTipText("Open project");
//		 this.add(openFolderButton);
//		 
//		 JButton closeFolderButton = new JButton(new ImageIcon("Icons8/icons8-Extensions Folder.png"));
//		 closeFolderButton.setToolTipText("Close project");
//		 closeFolderButton.addActionListener(new ActionListener() {
//			
//			public void actionPerformed(ActionEvent e) {
//				treePanel.closeProject(tabbedPane.getSelectedProject());
//			}
//		});
//		 this.add(closeFolderButton);
		 
		 emptyFolderButton.setToolTipText("Empty Outputs folder");
		 emptyFolderButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				if(!tabbedPane.getSelectedProject().equals("")){
					File folder = new File("./Workspace/"+tabbedPane.getSelectedProject()+"/Outputs");
//					System.out.println("emptying "+folder.getAbsolutePath());
					if(folder.isDirectory()){
						File[] traces = folder.listFiles(new FilenameFilter() {
							
							public boolean accept(File dir, String name) {
								return true;
							}
						});
						if(traces.length == 0){
							JOptionPane.showMessageDialog(null, "The 'Outputs' directory is already empty : nothing to be done");
						}
						else if(gui.confirmChoice("Are you sure you want to delete execution traces for project '"+ tabbedPane.getSelectedProject() +"'?"))
						{
							for (int i = 0; i < traces.length; i++) {
//								System.out.println("Deleting "+traces[i].getAbsolutePath());
								traces[i].delete();		
								treePanel.removeFile(traces[i]);
//								treePanel.removeTrace(traces[i].getAbsolutePath(), folder.getParentFile().getAbsolutePath());
							}
							JOptionPane.showMessageDialog(null, "The 'Outputs' directory has been emptied : "+traces.length+" traces deleted.");
							
						}
						
					}				
				}
				else 
					JOptionPane.showMessageDialog(null, "Operation failed : please select a project first!");
				
				
			}
		});
		 this.add(emptyFolderButton);
		 
		 this.addSeparator(); 

		 viewFileButton.setToolTipText("View trace file");
		 this.add(viewFileButton);
		 viewFileButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				if(!tabbedPane.getSelectedProject().equals("")) {
					File file = treePanel.getSelectedFile();
					if(!file.isDirectory()){
						if(file.getAbsolutePath().endsWith(".t")){
							@SuppressWarnings("unused")
							TraceViewer tv = new TraceViewer(file);                   		 
						}
						else{
							JOptionPane.showMessageDialog(null, "This operation is only applicable to trace files with extension '.t'!");
							
						}
						
					}
					else{
						JOptionPane.showMessageDialog(null, "This operation is not applicable to directories: select a trace file!");
						
					}
				}
				else 
					JOptionPane.showMessageDialog(null, "Operation failed : please select a project first!");
				
			}
		});
		 
		 openFileButton.setToolTipText("Edit file");
		 this.add(openFileButton);
		 openFileButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				if(!tabbedPane.getSelectedProject().equals("")) {
					
					File file = treePanel.getSelectedFile();
					if(!file.isDirectory()){
						if(file.getAbsolutePath().endsWith(".bip") || file.getAbsolutePath().endsWith(".hpp") || file.getAbsolutePath().endsWith(".cpp") || file.getAbsolutePath().endsWith(".txt")){
							gui.launchPanelEditor(file);
						}
						else{
							JOptionPane.showMessageDialog(null, "This operation is only applicable to '.bip', '.cpp', '.hpp' and .txt' files!");
							
						}
						
					}
					else{
						JOptionPane.showMessageDialog(null, "This operation is not applicable to directories: select a model file!");
						
					}
				}
				
				else 
					JOptionPane.showMessageDialog(null, "Operation failed : please select a project first!");
				
			}
		});
		 
		 newFileButton.setToolTipText("Create new file");
		 this.add(newFileButton);
		 newFileButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {

				if(!tabbedPane.getSelectedProject().equals("")){
					String[] extensions = new String[4];
					extensions[0] = "BIP model '.bip'";
					extensions[1] = "C++ source code file '.cpp'";
					extensions[2] = "C++ header file'.hpp'";
					extensions[3] = "Distribution file '.txt'";
					@SuppressWarnings("rawtypes")
					JComboBox comboExt = new JComboBox<>(extensions);
					JTextField fileName = new JTextField("");
					
					Object[] obj = new Object[4];
					obj[0] = new JLabel("File name : ");
					obj[1] = fileName;
					obj[2] = new JLabel("File extension : ");
					obj[3] = comboExt;
					int sel = JOptionPane.showConfirmDialog(null, obj , "File creation", JOptionPane.PLAIN_MESSAGE);
					if(sel == JOptionPane.OK_OPTION){
						String s= fileName.getText();
						switch(comboExt.getSelectedIndex()){
						case 0 : s+=".bip";
							break;
						case 1 : s+=".cpp";
							break;
						case 2 : s+=".hpp";
							break;
						case 3 : s+=".txt";
							break;
						default : s+=".bip";
						}
						if(!fileName.getText().equals("")){
								String prefix = "/";
								if((comboExt.getSelectedIndex() == 2 || comboExt.getSelectedIndex() == 1)) {
									prefix = "/ext-cpp/";
									File f = new File("./Workspace/"+tabbedPane.getSelectedProject()+"/Models"+prefix);
									if(!f.exists()){
										f.mkdir();
										tabbedPane.insertNodeInTree(f.getAbsolutePath());
										
									}
								}
								File f = new File("./Workspace/"+tabbedPane.getSelectedProject()+"/Models"+prefix+s);
								if(f.exists()){
									JOptionPane.showMessageDialog(null, "Model creation failed : The file '"+s +"' already exists!");
								}
								else{
									try {
//										System.out.println(f.getAbsolutePath());
										f.createNewFile();
										tabbedPane.insertNodeInTree(f.getAbsolutePath());
										JOptionPane.showMessageDialog(null, "The file '"+s +"' has been successfully created");
										
									} catch (IOException e1) {
										e1.printStackTrace();
									}
									}
							
							
						}
						else{
							JOptionPane.showMessageDialog(null, "File creation failed : the file name is an empty string!");
						}
					}
				}
				else{
					JOptionPane.showMessageDialog(null, "Operation failed : please select a project first!");
					
				}
			}
		});
		 
		 deleteFileButton.setToolTipText("Delete file");
		 deleteFileButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				if(!tabbedPane.getSelectedProject().equals("")){
					
					
					File file = treePanel.getSelectedFile();
					if(!file.isDirectory()){
						if(gui.confirmChoice("You are about to delete the file '"+file.getName()+"'.\nThis operation is definitive and cannot be reverted.\n Are you sure you want to delete this file?")){
							if(file.delete()){
								treePanel.removeFile(file);
								JOptionPane.showMessageDialog(null, "The file '"+file.getName()+"' has been deleted successfully!");
							}						
						}
					}
					else{
						JOptionPane.showMessageDialog(null, "This operation is not applicable to directories: select a file!");
						
					}
				
				}
				else{
					JOptionPane.showMessageDialog(null, "Operation failed : please select a project first!");
					
				}
			}
		});
		 this.add(deleteFileButton);
		 
//		 this.addSeparator(); 

		 saveButton.setToolTipText("Save");
		 this.add(saveButton);
		 saveButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				if(!tabbedPane.getSelectedProject().equals("")){
					
				Component pan = tabbedPane.getSelectedComponent();
				if(pan instanceof PanelEditor) {
//					System.out.println("PanelEditor");
					PanelEditor panEditor = (PanelEditor) pan;
					panEditor.saveChanges();
				}
				else
					JOptionPane.showMessageDialog(null, "This operation is only applicable to edited files: nothing to be done!");
				}
				else{
					JOptionPane.showMessageDialog(null, "Operation failed : please select a project first!");
					
				}
			}
		});
		 
//		 JButton saveAsButton = new JButton(new ImageIcon("./Icons8/icons8-Save as.png"));
//		 saveAsButton.setToolTipText("Save as ...");
//		 this.add(saveAsButton);
		 
		 this.addSeparator(); 

//		 JButton cutbutton = new JButton(new ImageIcon("./Icons8/icons8-Scissors.png"));
//		 cutbutton.setToolTipText("Cut");
//		 this.add(cutbutton);
//		 
//		 JButton copybutton = new JButton(new ImageIcon("./Icons8/icons8-Copy.png"));
//		 copybutton.setToolTipText("Copy");
//		 this.add(copybutton);
//		 
//		 JButton pastebutton = new JButton(new ImageIcon("./Icons8/icons8-Paste_2.png"));
//		 pastebutton.setToolTipText("Paste");
//		 this.add(pastebutton);
//		 this.addSeparator(); 
		 
		 previousbutton.setToolTipText("Previous tab");
		 previousbutton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int index = tabbedPane.getSelectedIndex() ;
				if( index != 0)
					if( tabbedPane.isEnabledAt(index -1))
					tabbedPane.setSelectedIndex(index-1);
				
			}
		});
		 this.add(previousbutton);
		 
		 nextbutton.setToolTipText("Next tab");
		 nextbutton.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					int index = tabbedPane.getSelectedIndex() ;
					if( index != tabbedPane.getTabCount() - 1)
						if( tabbedPane.isEnabledAt(index +1))
						tabbedPane.setSelectedIndex(index+1);
					
				}
			});
		 this.add(nextbutton);
		 this.addSeparator(); 
		 
		 compilebutton.setToolTipText("Compile BIP model");
		 this.add(compilebutton);
		 compilebutton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(!tabbedPane.getSelectedProject().equals("")){
					
					Component pan = tabbedPane.getSelectedComponent();
					if(pan instanceof PanelEditor) {
	//					System.out.println("PanelEditor");
						PanelEditor panEditor = (PanelEditor) pan;
						if(panEditor.isEditingBIP() ) panEditor.initiateCompile();
						else 
							JOptionPane.showMessageDialog(null, "This operation is only applicable to edited BIP models: nothing to be done!");
						
					}
					else 
						JOptionPane.showMessageDialog(null, "This operation is only applicable to edited BIP models: nothing to be done!");
				}else{
					JOptionPane.showMessageDialog(null, "Operation failed : please select a project first!");
					
				}

				
			}
		});
		 
		 simbutton.setToolTipText("Simulate system");
		 this.add(simbutton);
		 simbutton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				if(tabbedPane.hasSelectedModel() == 0) {
					
					gui.launchPanelSimulation();
////					tabbedPane.addTab("     System Simulation     ", new PanelSimulation(tabbedPane));
//					tabbedPane.addCloseablePanel( new PanelSimulation(tabbedPane), "     System Simulation     " , false);
//					tabbedPane.setSelectedIndex(tabbedPane.getTabCount()-1);
//					tabbedPane.resizeText();
				}
				else {
					JOptionPane.showMessageDialog(null, "Operation failed : please select a project first!");
				}
				
			}
		});
		 
		 runbutton.setToolTipText("Start SMC simulation");
		 this.add(runbutton);
		 runbutton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				if(!tabbedPane.getSelectedProject().equals("")){
					
				if(tabbedPane.hasSelectedModel() == 0){
					if(tabbedPane.hasSelectedFormula() == 0){
						
						gui.launchPanelSMC();
					}
						else{
							if(tabbedPane.hasSelectedFormula() == -1)
								JOptionPane.showMessageDialog(null, "Cannot start SMC simulation : select a formula first!");
							
							else
								JOptionPane.showMessageDialog(null, "Cannot start SMC simulation : the formula has been selected in a different project!");
							
						}
					}
					else{
						if(tabbedPane.hasSelectedModel() == -1)
							JOptionPane.showMessageDialog(null, "Cannot start SMC simulation : select a model first!");
						
						else
							JOptionPane.showMessageDialog(null, "Cannot start SMC simulation : the model has been selected in a different project!");
						
					}
				
				}else{
			JOptionPane.showMessageDialog(null, "Operation failed : please select a project first!");
			
		}
			}
		});
		 
		 graphbutton.setToolTipText("Start parametric exploration");
		 graphbutton.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent arg0) {
					if(!tabbedPane.getSelectedProject().equals("")){
						
					if(tabbedPane.hasSelectedModel() == 0){
						if(tabbedPane.hasSelectedFormula() == 0){
							
							gui.launchPanelParamSMC();
			 			}
						else{
							if(tabbedPane.hasSelectedFormula() == -1)
								JOptionPane.showMessageDialog(null, "Cannot start parametric SMC simulation : select a formula first!");
							
							else
								JOptionPane.showMessageDialog(null, "Cannot start parametric SMC simulation : the formula has been selected in a different project!");
							
						}
					}
					else{
						if(tabbedPane.hasSelectedModel() == -1)
							JOptionPane.showMessageDialog(null, "Cannot start parametric SMC simulation : select a model first!");
						
						else
							JOptionPane.showMessageDialog(null, "Cannot start parametric SMC simulation : the model has been selected in a different project!");
						
					}
					
					}else
						JOptionPane.showMessageDialog(null, "Operation failed : please select a project first!");
				}
			});
		 this.add(graphbutton);
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 

		 rareEventbutton.setToolTipText("Run rare events analysis");
		 rareEventbutton.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent arg0) {
					if(!tabbedPane.getSelectedProject().equals("")){
						
					if(tabbedPane.hasSelectedModel() == 0){
							
							gui.launchPanelRareEvent();
			 			
					}
					else{
						if(tabbedPane.hasSelectedModel() == -1)
							JOptionPane.showMessageDialog(null, "Cannot start rare events analysis : select a model first!");
						
					}
					
					}else
						JOptionPane.showMessageDialog(null, "Operation failed : please select a project first!");
				}
			});
		 this.add(rareEventbutton);
		 this.addSeparator(); 
		 
		 
		 

		 fitbutton.setToolTipText("Adjust text size to width");
		 fitbutton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				if(gui.isAutoresize())
					((JButton) e.getSource()).setToolTipText("Adjust text size to width");
				else 
					((JButton) e.getSource()).setToolTipText("Disable adjust text size to width");
				gui.activateDeactivateAutoResize();
			}
		});
		 this.add(fitbutton);
		 
		 configbutton.setToolTipText("Configure BIP compilation scripts");
		 configbutton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				String[] bipType = new String[2];
				bipType[1] = "Stochastic RT-BIP"; bipType[0] = "Untimed BIP"; //bipType[1] = "RT-BIP"; 
				JComboBox<String> comboBIP = new JComboBox<String>(bipType);
				comboBIP.setSelectedIndex(1);
				
				
				Object[] objects = new Object[2];
				objects[0] = new JLabel("Select the BIP engine you want to configure the script for : ");
				objects[1] = comboBIP;
				int select = JOptionPane.showConfirmDialog(null, objects, "Bip engine selection", JOptionPane.PLAIN_MESSAGE);
				if(select == JOptionPane.OK_OPTION){
					Compilator cmp = new Compilator(comboBIP.getSelectedIndex());
					gui.launchPanelEditor(new File("./scripts/"+cmp.getScriptName()));
				}
			}
		});
		 this.add(configbutton);
		 
		 
		 
		 
		 this.addSeparator(); 
		 
		 helpbutton.setToolTipText("Help me");
		 this.add(helpbutton);
		 helpbutton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("unused")
				Help h = new Help();
			}
		});
		 
		 infobutton.setToolTipText("About us");
		 this.add(infobutton);
		 infobutton.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) {

					@SuppressWarnings("unused")
					AboutUs au = new AboutUs();
				}
			});
		 
//		 contactbutton.setToolTipText("Contact us");
//		 this.add(contactbutton);
//		 contactbutton.addActionListener(new ActionListener() {
//				
//				public void actionPerformed(ActionEvent e) {
//					JOptionPane.showMessageDialog(null, "Sorry! This operation is not implemented yet!");
//					
//				}
//			});
		 
		 this.addSeparator(); 
//		 setInitialEnabled() ;
		 
	}
}
