package ujf.verimag.sbip.gui;



import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import ujf.verimag.sbip.gui.tabs.PanelFormula;
import ujf.verimag.sbip.gui.tabs.PanelModel;
import ujf.verimag.sbip.gui.tabs.PanelSimulation;
import ujf.verimag.sbip.gui.tabs.PanelStart;
import ujf.verimag.sbip.parser.formula.LtlFormula;
import ujf.verimag.sbip.parser.formula.MtlFormula;

public class TabbedPane extends JTabbedPane{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private PanelModel panModel ;
	
	private String selectedProject = "";
	UserInterface GUI;
	

	private PanelFormula panFormula;

	private PanelStart panInit;

//	private PanelSMC panSMC;
	
	public void enablePanels(){
		if(selectedProject.equals("")){
			for (int i = 0; i < getTabCount(); i++) {
				setEnabledAt(i, true);
				
			}
		}
	}
	
	public void disablePanels(){
		if(selectedProject.equals("")){
			for (int i = 1; i < getTabCount(); i++) {
				setEnabledAt(i,false);
				
			}
			setSelectedIndex(0);
		}
	}
	
	public void setSelectedProject(String s){
		selectedProject = s;
	}

		public TabbedPane(UserInterface userInterface) throws IOException{
			super();
			this.GUI = userInterface;
			setBorder(new EmptyBorder(5,0,5,0));

			panInit = new PanelStart(this);
			addTab("     Start here     ", new JScrollPane( panInit));
			
			panModel = new PanelModel(this);
			addTab("     Model selection     ", new JScrollPane(panModel));
			
			panFormula = new PanelFormula(this);
			addTab("     Formula specification     ", new JScrollPane(panFormula));
			
			
//			PanelSMC panSMC = new PanelSMC(this);
//			addTab("     SMC setting     ", new JScrollPane(panSMC));
			

			disablePanels();
			
			
			
			
			
//			setSelectedIndex(4);
			
		}
		




		public void setFormula(MtlFormula form, String project) {
			panFormula.setFormula(form);
			panFormula.setProject(project);
			if(isEnabledAt(2)) setSelectedIndex(2);
//			enableComboEngine(false);
//			enableLimitParam(true);
			
		}

		public void setModel(String s, String project) {
			panModel.setSelectedModel(s);
			panModel.setProject(project);
			if(isEnabledAt(1)) setSelectedIndex(1);
			
			
		}

//		public void resetTree() {
//			GUI.resetTree();
//			
//		}
		
		public String getSelectedProject() {
			return selectedProject;
		}

		public void createProjectInTree(String absolutePath) {
			GUI.createProjectInTree(absolutePath);
			
		}
		
		public String getSelectedModel() {
			return panModel.getSelectedModel();
		}

		public String getFormula() {
			return panFormula.getFormula();
		}

		public int hasSelectedModel() {			
			if(panModel.getSelectedModel().equals("")) return -1;
			if(!panModel.getProject().equals(selectedProject)) return -2;
			return 0;
		}
		
		public int hasSelectedFormula() {

			if(panFormula.getFormula().equals("")) return -1;
			if(!panFormula.getProject().equals(selectedProject)) return -2;
			return 0;
		}

//		public void enableComboEngine(boolean b) {
//			panSMC.enableComboEngine(b);
//			
//		}
//
//		public void enableLimitParam(boolean b) {
//			panSMC.enableLimitParam(b);
//			
//		}

		public MtlFormula getMtlFormula() {
			return panFormula.getMtlFormula();
		}

//		public Dimension getTabSize() {
//			return panSMC.getPreferredSize();
//		}
//
//		public void cleanSmcError() {
//			panSMC.cleanError();
//			
//		}

		public void resizeText() {
//			System.out.println("ResizeText");
			GUI.refreshTextSize();
			
		}

		public Dimension getTabSize() {
			return panInit.getPreferredSize();
		}

		public void resetAll() {
			selectedProject = "";
			panFormula.reset();
			panModel.reset();
			disablePanels();
			
		}

		public void insertNodeInTree(String node) {
			GUI.insertNodeInTree(node);
			
		}

		public void addCloseablePanel(final JComponent pan, String title, final boolean confirmClose)
		{
			final JButton l;
			final JLabel b;
			
			boolean closeable = true;
			l= new JButton(title); 
			l.setContentAreaFilled(false);
			l.setBorderPainted(false);
			l.setMargin(new Insets(0, 0, 0, 0));
			l.setBounds(0, 0, 0, 0);
//			l.setFocusable(false);
			l.setOpaque(false);
			l.setBorder(null);
			 l.setFocusPainted(false);
			
			 
			//not sure exactly if this even does anything...
			l.setVerticalAlignment(SwingConstants.TOP);
			l.setAlignmentX(TOP_ALIGNMENT);
			
//			System.out.println(f.exists());
			b= new JLabel(new ImageIcon("./Icons8/icons8-Cancel-15.png"));
			b.setCursor(new Cursor(Cursor.HAND_CURSOR));
			
			JPanel panTitre = new JPanel(new BorderLayout());
			
			panTitre.setOpaque(false);
			panTitre.add(l, BorderLayout.CENTER);
			panTitre.add(b, BorderLayout.EAST);
			
			this.addTab(title, pan);
//			System.out.println(this.getTabComponentAt(this.getTabCount()-1));
			panTitre.add(b, BorderLayout.EAST);
			this.setTabComponentAt(this.getTabCount()-1, panTitre );
			
			 l.addActionListener(new ActionListener() {
					
					public void actionPerformed(ActionEvent e) {
						if(isEnabledAt(tabIndex(pan) ))
							setSelectedComponent(pan);
					}
				});
			 
			 if (closeable)
			        {
				 		final TabbedPane thisTab = this;
			            b.setToolTipText("Click to close");

			            b.setOpaque(false);
			            b.setBackground(Color.gray);

			            b.addMouseListener(new MouseAdapter()
			            {
			                @Override
			                public void mouseExited(MouseEvent e)
			                {
//			                    b.setBorder(BorderFactory.createRaisedBevelBorder());
			                    b.setOpaque(false);
			                }

			                @Override
			                public void mouseEntered(MouseEvent e)
			                {
//			                    b.setBorder(BorderFactory.createLoweredBevelBorder());
//			                	b.setOpaque(true);
			                	
			                }

			                @Override
			                public void mouseReleased(MouseEvent e)
			                {
			                    b.setOpaque(false);
			                    b.repaint();
			                    if(isEnabledAt(tabIndex(pan) ))
			                    if (b.contains(e.getPoint()))
			                    {
//			                        b.setBorder(BorderFactory.createLoweredBevelBorder());
			                        
			                        if(confirmClose) {
			                        	int choice = JOptionPane.showConfirmDialog(null, 
			                        			"Are you sure you want to close this tab?"
			                        			, "Confirm close", JOptionPane.YES_NO_OPTION);
			                        	
			                        	if(choice == JOptionPane.YES_OPTION)
						                {
			                        		thisTab.remove( pan);
						                }
			                        	
			                        }
			                        else  {
			                        	if(pan instanceof PanelSimulation)
			                        		((PanelSimulation) pan).stopWorker();
			                        	thisTab.remove( pan);
			                        }
			                         
			                    }
//			                    else
//			                        b.setBorder(BorderFactory.createRaisedBevelBorder());

			                }


							@Override
			                public void mousePressed(MouseEvent e)
			                {
			                    b.setOpaque(true);
			                    b.repaint();
			                }
			            });

//			            b.setBorder(BorderFactory.createRaisedBevelBorder());
			           
			        }


		
			    
		}

		protected int tabIndex(JComponent pan) {
			
			for (int i = 0; i < this.getComponentCount(); i++) {
				if(this.getComponentAt(i).equals(pan)) return i;
			}
			
			return 0;
		}

//		public void setFormula(LtlFormula form, String project) {
//			panFormula.setFormula(form);
//			panFormula.setProject(project);
//			if(isEnabledAt(2)) setSelectedIndex(2);
//			
//		}

		public void setFormula(LtlFormula form, String project) {
			// TODO Auto-generated method stub
//			System.out.println("Setting it to : " + form);
			panFormula.setFormula(form);
			panFormula.setProject(project);
			if(isEnabledAt(2)) setSelectedIndex(2);
		}

		public LtlFormula getLtlFormula() {
			return panFormula.getLtlFormula();
		}
		

		public String getFormulaType() {
			return panFormula.getFormulaType();
		}
		
}
