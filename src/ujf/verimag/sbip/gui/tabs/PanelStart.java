package ujf.verimag.sbip.gui.tabs;

import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import ujf.verimag.sbip.gui.TabbedPane;

public class PanelStart extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TabbedPane tabbedpane;
	
	
	public PanelStart(TabbedPane tp){
		this.tabbedpane = tp;
		getPanStart();
	}
	

    public void getPanStart(){
    	setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
    	setBorder(new EmptyBorder(20,20,20,20));
    	
    	JPanel panel = new JPanel();
    	TitledBorder title = BorderFactory.createTitledBorder("Start here");
		title.setTitleJustification(TitledBorder.CENTER);
		panel.setBorder(title);
    	panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
    	JTextArea txtAdvice = new JTextArea();
		txtAdvice.setEditable(false);
		txtAdvice.setText("\n You are using the Stochastic Real-Time BIP tool for Statistical Model Checking (SMC)."
				+ "\n\n This tool allows you to create SMC projects where you can validate properties on SRT-BIP models."
				+ "\n The properties are two kinds:"
				+ "\n\n\t - Bounded LTL properties"
				+ "\n\n\t - Time-bounded MTL properties"
				+ "\n\n To begin, you must create a new project. After this, you can import the system model(s) under study. "
				+ "\n\n The specification of the formulae is made in the third tab of the main panel."
				+ "\n\n Once you select a model and a property, you can initiate the SMC process."
				+ "\n\n\n Note that you can select an existing project by clicking on it in the Package Explorer (on the left).\n");
		txtAdvice.setBorder(BorderFactory.createLoweredBevelBorder());
		txtAdvice.setMargin(new Insets(20,20,20,20));
//		txtAdvice.setBackground(Color.WHITE);

		/// transparence
		txtAdvice.setOpaque(false);
		txtAdvice.setBorder(BorderFactory.createEmptyBorder());
		txtAdvice.setBackground(new Color(0,0,0,0));
		
		JPanel panAdvice = new JPanel();
//		panAdvice.setLayout(new BoxLayout(panAdvice, BoxLayout.X_AXIS));
		panAdvice.setBorder(new EmptyBorder(5,5,5,5));
		panAdvice.add(txtAdvice);
		panel.add(panAdvice);
		
		
		JButton create = new JButton("Create a new project");
		create.setBounds(0,0, 300, 50);
		JPanel panBut = new JPanel();
		panBut .add(create);
		panel.add(panBut);
		
		create.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String s = JOptionPane.showInputDialog(null, "Enter the name of the new project : ", "Project creation", JOptionPane.QUESTION_MESSAGE);
				if(s!= null)
				if(!s.equals("")){
					File f = new File("./Workspace/"+s);
					if(f.exists()){
						JOptionPane.showMessageDialog(null, "Project creation failed : The project '"+s +"' already exists!");
					}
					else{
						f.mkdir();
						File props = new File("./Workspace/"+s+"/Properties");
						props.mkdir();
						File models = new File("./Workspace/"+s+"/Models");
						models.mkdir();
						File outputs = new File("./Workspace/"+s+"/Outputs");
						outputs.mkdir();
						tabbedpane.createProjectInTree(f.getAbsolutePath());
						JOptionPane.showMessageDialog(null, "The project '"+s +"' has been successfully created");
					}
				}
				else{
					JOptionPane.showMessageDialog(null, "Project creation failed : the project name is an empty string!");
				}
				
			}
		});
		
		add(panel);
    }
}
