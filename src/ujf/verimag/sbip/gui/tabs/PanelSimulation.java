package ujf.verimag.sbip.gui.tabs;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingWorker;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import ujf.verimag.sbip.gui.TabbedPane;

public class PanelSimulation extends JPanel  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TabbedPane tabbedpane;
	private JTextArea text;
	private JScrollPane jsp;
	private JLabel currentSystem = new JLabel();
	SwingWorker<?, ?> worker = null;
	
	public void stopWorker() {
//        JOptionPane.showMessageDialog(null, "Kill worker");	
		if (worker != null) 
		if(!worker.isDone()){
			worker.cancel(true);
			worker.isCancelled();
			JOptionPane.showMessageDialog(null, "The simulation has been aborted!", "Closed simulation panel", JOptionPane.OK_OPTION);	
			
		}
		
	}

	public PanelSimulation(TabbedPane tabbedPane) {
		this.tabbedpane = tabbedPane;
		getPanSimulation();
	}

	private void getPanSimulation() {
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setBorder(new EmptyBorder(20,20,20,20));
//		setPreferredSize(new Dimension((int)tabbedpane.getTabSize().getWidth(),(int)tabbedpane.getTabSize().getHeight()));
		setCurrentSystem();
		add(currentSystem);
		add(new JLabel("    "));
		
		JPanel panelConfig = new JPanel();
		TitledBorder title = BorderFactory.createTitledBorder("Parameters");
		title.setTitleJustification(TitledBorder.CENTER);
		panelConfig.setBorder(title);
		panelConfig.setLayout(new BoxLayout(panelConfig, BoxLayout.X_AXIS));
		
		SpinnerModel model1 = new SpinnerNumberModel(1000, 1, Integer.MAX_VALUE, 1);
	    final JSpinner slimit = new JSpinner(model1);
	    slimit.setEnabled(false);
		
		final JCheckBox climit = new  JCheckBox("--limit");
		climit.setSelected(false);
		climit.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				slimit.setEnabled(((JCheckBox) e.getSource()).isSelected());
			}
		});
		
		final JCheckBox clogvar = new  JCheckBox("--log-variables");
		clogvar.setSelected(false);
		final JTextField params = new JTextField("");
		
		panelConfig.add(climit);
		panelConfig.add(new JLabel("    "));
		panelConfig.add(slimit);
		panelConfig.add(new JLabel("    "));
		panelConfig.add(clogvar);
		panelConfig.add(new JLabel("    "));
		panelConfig.add(new JLabel("Additional parameters : "));
		panelConfig.add(params);
		panelConfig.setMaximumSize( 
			     new Dimension(Integer.MAX_VALUE, panelConfig.getPreferredSize().height) );
//		panelConfig.setPreferredSize(new Dimension((int)tabbedpane.getTabSize().getWidth(), (int)(tabbedpane.getTabSize().getHeight()*1/10)));

		
		JPanel panSim = new JPanel();
		TitledBorder title2 = BorderFactory.createTitledBorder("Simulation");
		title2.setTitleJustification(TitledBorder.CENTER);
		panSim.setBorder(title2);
		panSim.setLayout(new BoxLayout(panSim, BoxLayout.X_AXIS));
		text = new JTextArea();
		text.setEditable(false);
		text.setLineWrap(true);
		
		jsp = new JScrollPane(text);
		panSim.setMaximumSize( 
			     new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE) );
//		panSim.setPreferredSize(new Dimension((int)tabbedpane.getTabSize().getWidth(), (int)tabbedpane.getTabSize().getHeight()));
//		jsp.setPreferredSize(new Dimension((int)tabbedpane.getTabSize().getWidth(), (int)tabbedpane.getTabSize().getHeight()));
		panSim.add(jsp);
		
		JPanel panButtons = new JPanel();
		panButtons.setLayout(new FlowLayout());
		final JButton butRun = new JButton("Start simulation");
		butRun.setBounds(0, 0, 300, 50);
		butRun.setEnabled(true);
//		final JButton closeTab = new JButton("Close tab");
//		closeTab.setBounds(0, 0, 300, 50);
//		closeTab.setEnabled(true);
//		closeTab.addActionListener(new ActionListener() {
//			
//			public void actionPerformed(ActionEvent arg0) {
//				tabbedpane.removeTabAt(tabbedpane.getSelectedIndex());
//				
//			}
//		});
		
		butRun.addActionListener(new ActionListener() {
			
			String mode = "Start";
			Process p;
			String line;
			
			public void actionPerformed(ActionEvent arg0) {
				if(mode.equals("Start")) {
					butRun.setText("Stop simulation");
					mode = "Stop";
//					closeTab.setEnabled(false);
					
					
					worker = new SwingWorker<Object, Object>() {
				          protected Object doInBackground() throws Exception {									
				        	  long seed = Math.round(Math.random()*(Integer.MAX_VALUE-1)+1);
								File modelFile = new File(tabbedpane.getSelectedModel());
								String cmd = "./"+modelFile.getName()  +" --seed "+ seed  + (clogvar.isSelected()? " --log-variables" : "") 
										+ (climit.isSelected()? " --limit "+ (int) slimit.getValue(): "") +" " +params.getText();
								
								
//								Process p = Runtime.getRuntime().exec();
								
								try {
									p = Runtime.getRuntime().exec(new String[]{"bash","-c",cmd}, null, modelFile.getParentFile());
								} catch (IOException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
								
//								Process p = Runtime.getRuntime().exec(new String[]{"bash","-c",cmd}, null, new File(URL).getParentFile());
								try (final BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
							        
							        while ((line = b.readLine()) != null) {
//							        	System.out.println(line);
							        	addText(line);
							        	
							        }
							        
									
							        
							    } catch (IOException e1) {
									// TODO Auto-generated catch block
//									e1.printStackTrace();
							    	System.out.println("Stream closed");
								}
								try
								{
									p.waitFor();
									addText("\nProcess exited with code : "+p.exitValue());
//									if(p.exitValue() != 0) 
									{
										try (final BufferedReader b = new BufferedReader(new InputStreamReader(p.getErrorStream()))) {
									        
									        while ((line = b.readLine()) != null) {
//									        	System.out.println(line);
									        	addText(line);
									        	
									        }
									        
											
									        
									    } catch (IOException e1) {
											// TODO Auto-generated catch block
//											e1.printStackTrace();
									    	System.out.println("Stream closed");
										}
									}
								}		
								catch(InterruptedException e){
									System.out.println("Aborted trace generator!");
								}
								return null;
				          }	
				          
				          protected void done() {
				        	  
				          }
				    };
				    worker.execute();
					
					
				}
				else if(mode.equals("Stop")) {
					butRun.setText("Reset trace");
					mode = "Reset";
					p.destroy();
//					closeTab.setEnabled(true);
				}
				else if(mode.equals("Reset")) {
					butRun.setText("Start simulation");
					mode = "Start";
					text.setText("");
				}
			}
		});
		
//		panButtons.add(closeTab);
		panButtons.add(butRun);
		

		this.add(panelConfig);
		this.add(panSim);
		this.add(panButtons);
		
		
		
		
		
	}
	
	public void setCurrentSystem() {
//		System.out.println(tabbedpane.getSelectedModel());
		currentSystem.setText("Simulating system : "+tabbedpane.getSelectedModel());
		
		
	}

	public void addText(String line) {
		 text.append(line+"\n");
 		jsp.getVerticalScrollBar().setValue(jsp.getVerticalScrollBar().getMaximum());
     	
	}

	



}
