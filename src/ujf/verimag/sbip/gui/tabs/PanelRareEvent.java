package ujf.verimag.sbip.gui.tabs;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.CancellationException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileSystemView;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import org.antlr.v4.runtime.misc.ParseCancellationException;

import ujf.verimag.sbip.analysis.ImportanceSplitter;
import ujf.verimag.sbip.bipInterface.trace.State;
import ujf.verimag.sbip.bipInterface.trace.StateVariable;
import ujf.verimag.sbip.gui.TabbedPane;
import ujf.verimag.sbip.gui.dialogs.ProgressionViewer;
import ujf.verimag.sbip.gui.model.DataCompare;
import ujf.verimag.sbip.gui.model.FormulaTableModel;
import ujf.verimag.sbip.gui.model.Score_Formula;
import ujf.verimag.sbip.monitor.ScoreFunction;
import ujf.verimag.sbip.parser.formula.LtlFormula;
import ujf.verimag.sbip.parser.formula.MtlFormula;
import ujf.verimag.sbip.parser.ltl.LtlExprParser;
import ujf.verimag.sbip.parser.mtl.MtlExprParser;

public class PanelRareEvent extends JPanel {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TabbedPane tabbedpane;
	private ArrayList<Score_Formula> data = new ArrayList<Score_Formula>();
	private FormulaTableModel tableModel;
	private JTable 	table;
	private SwingWorker worker;

	public PanelRareEvent(TabbedPane tp) throws IOException{
		this.tabbedpane = tp;
		getPanRE();
		
	}
	
	private void getPanRE(){
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setBorder(new EmptyBorder(20,20,20,20));
		setPreferredSize(tabbedpane.getTabSize());
		
		JPanel panScoreFunction = new JPanel();
		panScoreFunction.setLayout(new BoxLayout(panScoreFunction, BoxLayout.Y_AXIS));
		TitledBorder title = BorderFactory.createTitledBorder(" Score Function ");
		title.setTitleJustification(TitledBorder.CENTER);
		panScoreFunction.setBorder(title);


		
		JPanel panControl = new JPanel();
		panControl.setBorder(new EmptyBorder(5,50,5,50));
		panControl.setLayout(new BoxLayout(panControl, BoxLayout.X_AXIS));


		JButton b_save = new JButton("Save function");
		JButton b_reset = new JButton("Reset function");
		JButton b_load = new JButton("Load function");
		
		panControl.add(b_save);
		panControl.add(Box.createRigidArea(new Dimension(10, 0)));
		panControl.add(b_reset);
		panControl.add(Box.createRigidArea(new Dimension(10, 0)));
		panControl.add(b_load);
		
		
		
		JPanel panTable = new JPanel();
		panTable.setBorder(new EmptyBorder(5,50,5,50));
		panTable.setLayout(new BoxLayout(panTable, BoxLayout.Y_AXIS));
		tableModel = new FormulaTableModel(data);
		table = new JTable(tableModel)
		{
			private static final long serialVersionUID = 1L;

			   public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
		    	   
		           Component component = super.prepareRenderer(renderer, row, column);
		           int rendererWidth = component.getPreferredSize().width;
		           TableColumn tableColumn = getColumnModel().getColumn(column);
		           tableColumn.setPreferredWidth(Math.max(rendererWidth + getIntercellSpacing().width, tableColumn.getPreferredWidth()));
		           ((DefaultTableCellRenderer) renderer).setHorizontalAlignment( JLabel.CENTER );
		           return component;
		        }
		    };
		DefaultTableCellRenderer renderer = (DefaultTableCellRenderer)
	            table.getTableHeader().getDefaultRenderer();
	        renderer.setHorizontalAlignment(JLabel.CENTER);
		table.setShowVerticalLines(true);
		JScrollPane scrollpane = new JScrollPane(table);
		panTable.add(scrollpane);

		
		
		JPanel panAddFormula = new JPanel();
		panAddFormula.setBorder(new EmptyBorder(5,50,5,50));
		panAddFormula.setLayout(new BoxLayout(panAddFormula, BoxLayout.X_AXIS));
		SpinnerModel model1 = new SpinnerNumberModel(1, 1, Integer.MAX_VALUE, 1);
		final JSpinner sScore = new JSpinner(model1);
		final JTextArea txtformula = new JTextArea("");
			txtformula.setLineWrap(true);
		txtformula.setWrapStyleWord(true);
		JScrollPane jsp = new JScrollPane(txtformula);

		
		JButton add =  new JButton("Add new level");
		add.setToolTipText("Add new level");
		
		panAddFormula.add(new JLabel("Score : "));
		panAddFormula.add(Box.createRigidArea(new Dimension(10, 0)));
		panAddFormula.add(sScore);
		panAddFormula.add(Box.createRigidArea(new Dimension(10, 0)));
		panAddFormula.add(new JLabel(" State Formula : "));
		panAddFormula.add(Box.createRigidArea(new Dimension(10, 0)));
		panAddFormula.add(jsp);
		panAddFormula.add(Box.createRigidArea(new Dimension(10, 0)));
		panAddFormula.add(add);		
		
		panScoreFunction.add(panControl);
		panScoreFunction .add(panTable);
		panScoreFunction.add(panAddFormula);

		/// Panel params		
		JPanel panParams = new JPanel();
		panParams.setLayout(new BoxLayout(panParams, BoxLayout.Y_AXIS));
//		panParams.setLayout(new GridLayout(3, 1));
		TitledBorder title2 = BorderFactory.createTitledBorder(" Rare Events Parameters ");
		title2.setTitleJustification(TitledBorder.CENTER);
		panParams.setBorder(title2);
		
		JPanel panNbTraces = new JPanel();
		panNbTraces.setBorder(new EmptyBorder(5,50,5,50));
		panNbTraces.setLayout(new BoxLayout(panNbTraces, BoxLayout.X_AXIS));
		SpinnerModel model2 = new SpinnerNumberModel(1000, 1, Integer.MAX_VALUE, 1);
		final JSpinner nb_traces = new JSpinner(model2);
		panNbTraces.add(new JLabel("Number of traces : "));
		panNbTraces.add(nb_traces);
		
		JPanel panAlpha = new JPanel();
		panAlpha.setBorder(new EmptyBorder(5,50,5,50));
		panAlpha.setLayout(new BoxLayout(panAlpha, BoxLayout.X_AXIS));
		SpinnerModel model3 = new SpinnerNumberModel(0.1, 0, 1, 0.1);
	    final JSpinner Alpha = new JSpinner(model3);
	    panAlpha.add(new JLabel("Alpha : "));
	    panAlpha.add(Alpha);
	    
		JPanel panStopCond = new JPanel();
		panStopCond.setBorder(new EmptyBorder(5,50,5,50));
		panStopCond.setLayout(new BoxLayout(panStopCond, BoxLayout.X_AXIS));
		SpinnerModel model4 = new SpinnerNumberModel(10, 1, Integer.MAX_VALUE, 1);
		final JSpinner stopCond = new JSpinner(model4);
		panStopCond.add(new JLabel("Stop condition : "));
		panStopCond.add(stopCond);
		
	    
		panParams.add(panNbTraces);
		panParams.add(panAlpha);
		panParams.add(panStopCond);

		
		
		
		
		// Panel run
		JPanel panRun = new JPanel();
		TitledBorder title3 = BorderFactory.createTitledBorder(" Analysis ");
		title3.setTitleJustification(TitledBorder.CENTER);
		panRun.setBorder(title3);		
		JButton run = new JButton("Run simulation");
		panRun.add(run);
		

		
		add(panScoreFunction);
		add(new JLabel(" "));
		add(panParams);
		add(new JLabel(" "));
		add(panRun);
		
		
		
		
		///  Actions
		b_load.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent ev) {
				String parentPath = "./Workspace/"+tabbedpane.getSelectedProject()+"/Properties";
				File[] files = FileSystemView.getFileSystemView().getFiles(new File(parentPath), false);
				ArrayList<File> scoreFiles = new ArrayList<File>();
				for ( File f : files) {
					if(f.getName().endsWith(".sf")) {
						scoreFiles.add(f);
//						System.out.println("adding file : "+f);
					}
				}
				File[] sfiles = new File[scoreFiles.size()];
				String[] snames = new String[scoreFiles.size()];
				sfiles = (File[]) scoreFiles.toArray(sfiles);
				for (int i = 0; i< sfiles.length; i++) {
					snames[i] = sfiles[i].getName();
				}
				JList<String> sfList = new JList<String>( snames);
				
				
				
				JScrollPane scrollp = new JScrollPane(sfList);
				scrollp.setPreferredSize(new Dimension(400, 100));
				Object[] o = new Object[2];
				o[0] = new JLabel("Select the score function file : ");
				o[1] = scrollp;
				JOptionPane.showMessageDialog(null, o, "Score function selection", JOptionPane.PLAIN_MESSAGE);
				
//				System.out.println(sfList.getSelectedValue());
				if(sfList.getSelectedValue() != null) {
//					String s = sfList.getSelectedValue().getAbsolutePath();
					String s = sfiles[sfList.getSelectedIndex()].getAbsolutePath();
					if(s.endsWith(".sf")){
						File file = new File(s);
	        			FileInputStream in;
						try {
							in = new FileInputStream(file);
							 @SuppressWarnings("resource")
							ObjectInputStream ois = new ObjectInputStream(in);
	                		ScoreFunction sf = (ScoreFunction) ois.readObject();
	                		data.clear();
	                		for(int i = 1; i<= sf.getNbLevels(); i++) {
	                			data.add(new Score_Formula(i, sf.getFormula(i)));
//	                			System.out.println("Adding : "+i+"=>"+sf.getFormula(i));
	                		}
//	                		System.out.println(data.size());
	                		
	                		table.revalidate();
	                		table.selectAll();
	                		table.clearSelection();
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						} catch (ClassNotFoundException e) {
							e.printStackTrace();
						}
	        		  
	            	}
					else {
						
					}
				}
				
			}
		});
		
		b_save.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				ScoreFunction sf = new ScoreFunction();
				boolean correctFunction = true;
				int i;
				for (i = 0; i < data.size() && correctFunction; i++) {
					correctFunction = sf.addNextLevel(data.get(i).getScore(), data.get(i).getFormula());
				}
				if(!correctFunction) {
					JOptionPane.showMessageDialog(null,  "Incorrect score function : score value "+ (i) +" was skipped!", "Error", JOptionPane.ERROR_MESSAGE);			
				}
				else 
				if(!data.isEmpty()){
					try {

						String parentPath = "./Workspace/"+tabbedpane.getSelectedProject()+"/Properties/";
						String s = sf.save(parentPath);

						
//						String s = sf.save("./Properties/");
						if(s!= null) {
							tabbedpane.insertNodeInTree(s);
							JOptionPane.showMessageDialog(null, "Score function has been successfully saved!", " Saving operation", JOptionPane.PLAIN_MESSAGE);
						}
						
						
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					
				}
				else
					JOptionPane.showMessageDialog(null,  "Empty score function : nothing to be done!", "Error", JOptionPane.ERROR_MESSAGE);			
				
			}
		});
		
		b_reset.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				int i = JOptionPane.showConfirmDialog(null,  "Are you sure you want to reset the score function?", "Score function reset", JOptionPane.YES_NO_OPTION );
				if(i == JOptionPane.YES_OPTION) {
					data.clear();
					table.revalidate();
				}
			}
		});
		
		add.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent arg0) {
				System.out.println(sScore.getValue()+" ("+txtformula.getText()+ ")");
				
				if(isScoreExists((int) sScore.getValue())) {
					JOptionPane.showMessageDialog(null,  "Error : score value "+ sScore.getValue() +" is already assigned!", "Error", JOptionPane.ERROR_MESSAGE);
				}
				else if(!txtformula.getText().isEmpty()) {
					String s = txtformula.getText();
					

						MtlExprParser parser = new MtlExprParser();
						try {
							MtlFormula f =parser.parse(s);
							if(!f.containsTemporalOperator()) {
								data.add(new Score_Formula(((Integer) sScore.getValue()) , f));
								sortData();
								table.revalidate();
							}
							else {
								JOptionPane.showMessageDialog(null,  "Formula error : this is not a state formula!", "Error", JOptionPane.ERROR_MESSAGE);							
							}
							
						} catch (ParseCancellationException e2) {
							JOptionPane.showMessageDialog(null,  "Parsing failed : the formula is incorrect!", "Error", JOptionPane.ERROR_MESSAGE);
						}						

				}
				else {
					JOptionPane.showMessageDialog(null,  "Error : empty formula!", "Error", JOptionPane.ERROR_MESSAGE);
					
				}
			}
		});
		
		table.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseMoved(java.awt.event.MouseEvent evt){
		    	
		    }
		    
		    public void mouseClicked(java.awt.event.MouseEvent evt) {
		    	if(SwingUtilities.isRightMouseButton(evt)) {
		    		int row = table.rowAtPoint(evt.getPoint());
			        int col = table.columnAtPoint(evt.getPoint());
			        if (row >= 0 && col >= 0) {
			        	if(table.isRowSelected(row)) {
			        		System.out.println("Clicked : "+row);	
							JPopupMenu popup = createPopup();
							popup.show(table , (int) evt.getX(), (int) evt.getY());
			        	}
			        	
			        }
		    	}
		        
		    }

			
		});
		
		final PanelRareEvent thisPanel = this;
		run.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				final ScoreFunction score = new ScoreFunction();
				boolean correctFunction = true;
				int i;
				for (i = 0; i < data.size() && correctFunction; i++) {
					correctFunction = score.addNextLevel(data.get(i).getScore(), data.get(i).getFormula());
				}
				if(!correctFunction) {
					JOptionPane.showMessageDialog(null,  "Incorrect score function : score value "+ (i) +" was skipped!", "Error", JOptionPane.ERROR_MESSAGE);			
				}
				else 
				if(!data.isEmpty()){
					final String url = tabbedpane.getSelectedModel();
//					System.out.println("URL : "+url);
					double alpha = (double) Alpha.getValue();
					int N = (int) nb_traces.getValue();
					int stop_condition = (int) stopCond.getValue();
					final ProgressionViewer pviewer = new ProgressionViewer(thisPanel, score.getNbLevels());
					final ImportanceSplitter algo = new ImportanceSplitter(N, alpha, stop_condition, pviewer);
	        		
					pviewer.updateLog("\nRunning importance splitting with params ("+alpha+", "+N+", "+stop_condition+") :");
					
					
					worker = new SwingWorker() {
				          protected Object doInBackground() throws Exception {
				        	  
				        	long start_time = System.currentTimeMillis();
							double d = algo.runFixedLevelAlgorithm(url, score);
				        	long runtime = (System.currentTimeMillis() - start_time);
				        		// Run the test and print results out
				        	
				        		pviewer.stopSimulation();
				        		pviewer.updateLog("\n\nProbability estimation result : " +  d);
					        	pviewer.updateLog("\nConfidence interval : "+algo.getConfidenceInterval());
					        	pviewer.updateLog("\nExecution time : "+ convertTime(runtime) +" ms" );	
				        		updateTabbedPane();
				  			return null;
				          }				
				          
				          

						protected void done() {
				        	   try {
				        		   algo.abort();
//				        		   System.out.println("!!Done!!");
//				        		   thisPan.error.setText(" ");
				        	   } catch (CancellationException e) {
				        	       // Do your task after cancellation
				        	   }
				          }
				    };
				    worker.execute();
					
									
				}
				else {
					JOptionPane.showMessageDialog(null,  "Empty score function : nothing to be done!", "Error", JOptionPane.ERROR_MESSAGE);			
				}
			}
		});
	}
	
	public boolean isScoreExists(int score) {
		for (int i = 0; i < data.size(); i++) {
			if(data.get(i).getScore() == score)
				return true;
		}
		return false;
	}


	

	public JPopupMenu createPopup() {
		JPopupMenu popup = new JPopupMenu();
	    JMenuItem item;
	    popup.add(item = new JMenuItem("Delete level"));
	    item.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
//				System.out.println("Delete");
				int[] rows = table.getSelectedRows();
				for (int i = rows.length - 1; i >=0; i -- ) {
					data.remove(rows[i]);
				}
				table.revalidate();
				table.clearSelection();
			}
		});
	    popup.add(item = new JMenuItem("Modify score value"));
	    item.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
//				System.out.println("Modify");
				if(table.getSelectedRowCount() == 1) {
					SpinnerModel model = new SpinnerNumberModel(1, 1, Integer.MAX_VALUE, 1);
					final JSpinner sScore = new JSpinner(model);
					Object[] o = new Object[2];
					o[0] = "Enter the new score value : ";
					o[1] = sScore;
					int i = JOptionPane.showConfirmDialog(null, o, "Score modification", JOptionPane.OK_CANCEL_OPTION);
					if(i == JOptionPane.OK_OPTION) {
						int score = (int) sScore.getValue();
						if(!isScoreExists(score)) {
							data.get(table.getSelectedRow()).setScore(score);
							sortData();
							table.setRowSelectionInterval(0, data.size()-1);
							table.revalidate();
							table.clearSelection();
						}
						else {
							JOptionPane.showMessageDialog(null,  "Modification failed : score value "+ sScore.getValue() +" is already assigned!", "Error", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
				else {
					JOptionPane.showMessageDialog(null, "Modification error : only one level score value can be modified at a time", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
	    
	    popup.add(item = new JMenuItem("Copy formula to clipboard"));
	    item.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(table.getSelectedRowCount() == 1) {
					
				 Toolkit defaultToolkit = Toolkit.getDefaultToolkit();
				 Clipboard clipboard = defaultToolkit.getSystemClipboard();
				 clipboard.setContents(new StringSelection(data.get(table.getSelectedRow()).getFormula().toString()), null);
		        
				}else {
					JOptionPane.showMessageDialog(null, "Copy failed : only one formula can be copied at a time", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
	    

//	    popup.setLabel("Justification");
	    return popup;
	}
	
	


	public void sortData() {
		Collections.sort(data,new DataCompare());
	}

	public void abortExecution(){
		if(worker != null)
			if(!worker.isDone()) worker.cancel(true);
	}
	
	private void updateTabbedPane() {
		// add result tab
		
	}
	
	private String convertTime(long time){
		int milliseconds = (int) (time  % 1000);
		int seconds = (int) (time  / 1000) % 60 ;
		int minutes = (int) ((time  / (1000*60)) % 60);
		int hours = (int) (time / (1000*60*60)) ;
		String dateFormatted  = String.format("%02d:%02d:%02d:%03d",hours,minutes,seconds,milliseconds);
		return dateFormatted ;
	}
	
	
//	public static void main(String[] args) {
//		System.out.println("Test panel rare events");
//		try {
//			PanelRareEvent panRE = new PanelRareEvent(null);
//			JFrame f = new JFrame();
//			f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//			f.setExtendedState(JFrame.MAXIMIZED_BOTH); 
//			f.setLocationRelativeTo(null);
//			f.setContentPane(panRE);
//			f.pack();
//			f.setVisible(true);
//			
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//	}

}
