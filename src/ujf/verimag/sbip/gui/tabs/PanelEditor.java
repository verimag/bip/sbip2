package ujf.verimag.sbip.gui.tabs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.Highlighter;
import javax.swing.text.Highlighter.HighlightPainter;
import javax.swing.text.JTextComponent;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.undo.UndoManager;

import ujf.verimag.sbip.gui.TabbedPane;
import ujf.verimag.sbip.gui.dialogs.Compilator;
import ujf.verimag.sbip.gui.model.LineNumber;


public class PanelEditor extends JPanel implements KeyListener{

	/**
	 * 
	 */
	private Highlighter.HighlightPainter myHighlightPainter = (HighlightPainter) new MyHighlightPainter(Color.yellow);
	private String researched;
	private static final long serialVersionUID = 1L;
	private TabbedPane tabbedpane;
	private File file;
	private JTextPane txtEditor;
	static JFrame findFrame ;
	protected UndoManager undoManager;
	private SuggestionPanel suggestion;
	private boolean enableSuggestion = false;


	boolean undoing = false;
	DefaultStyledDocument doc;
	
	
	public PanelEditor(TabbedPane tp, File file){
		this.tabbedpane = tp;
		this.file = file;
		getPanEditor();
		
	}
	
	public boolean isEditingBIP(){
		return (file.getAbsolutePath().endsWith(".bip"));
	}
	
	 private int findLastNonWordChar (String text, int index) {
	        while (--index >= 0) {
	            if (String.valueOf(text.charAt(index)).matches("\\W")) {
	                break;
	            }
	        }
	        return index;
	    }

	    private int findFirstNonWordChar (String text, int index) {
	        while (index < text.length()) {
	            if (String.valueOf(text.charAt(index)).matches("\\W")) {
	                break;
	            }
	            index++;
	        }
	        return index;
	    }

    public void getPanEditor(){
    	setLayout( new BorderLayout());
    	setBorder(new EmptyBorder(5,0,5,0));
    	
//    	 final StyleContext cont = StyleContext.getDefaultStyleContext();
//         final AttributeSet attrRed = cont.addAttribute(cont.getEmptySet(), StyleConstants.Foreground, Color.RED);
//         final AttributeSet attrBlue = cont.addAttribute(cont.getEmptySet(), StyleConstants.Foreground, Color.BLUE);
//         final AttributeSet attrLightGray = cont.addAttribute(cont.getEmptySet(), StyleConstants.Foreground, Color.LIGHT_GRAY);
//         final AttributeSet attrPurple= cont.addAttribute(cont.getEmptySet(), StyleConstants.Foreground, new Color(168, 0, 118) );
//         final AttributeSet attrGreen= cont.addAttribute(cont.getEmptySet(), StyleConstants.Foreground, new Color(0, 168, 50) );
//         final AttributeSet attrBlack = cont.addAttribute(cont.getEmptySet(), StyleConstants.Foreground, Color.BLACK);
         
         final SimpleAttributeSet attrPurple = new SimpleAttributeSet();
         StyleConstants.setForeground(attrPurple, new Color(168, 0, 118));
         StyleConstants.setBold(attrPurple, true);

         final SimpleAttributeSet attrRed = new SimpleAttributeSet();
         StyleConstants.setForeground(attrRed, Color.RED);
         StyleConstants.setBold(attrRed, true);
         
         final SimpleAttributeSet attrBlue = new SimpleAttributeSet();
         StyleConstants.setForeground(attrBlue, Color.BLUE);
         StyleConstants.setBold(attrBlue, true);
         
         final SimpleAttributeSet attrGreen = new SimpleAttributeSet();
         StyleConstants.setForeground(attrGreen, new Color(0, 168, 50));
         StyleConstants.setBold(attrGreen, true);

         final SimpleAttributeSet attrBlack = new SimpleAttributeSet();
         StyleConstants.setForeground(attrBlack, Color.black);
         StyleConstants.setBold(attrBlack, false);

         final SimpleAttributeSet attrLightGray = new SimpleAttributeSet();
         StyleConstants.setForeground(attrLightGray, Color.lightGray);
         StyleConstants.setBold(attrLightGray, false);
         
         doc = new DefaultStyledDocument() {
        	 
             /**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public void insertString (int offset, String str, AttributeSet a) throws BadLocationException {
                 super.insertString(offset, str, a);
                 
                 String text = getText(0, getLength());
               
                 int before = findLastNonWordChar(text, offset);
                 if (before < 0) before = 0;
                 int after = findFirstNonWordChar(text, offset + str.length());
                 int wordL = before;
                 int wordR = before;

                 while (wordR <= after) {
                     if (wordR == after || String.valueOf(text.charAt(wordR)).matches("\\W")) {
                    	 
                    	 if (text.substring(wordL, wordR).matches("(\\W)*(function|type|connector|package|atom|compound|port|data|end)"))
                             setCharacterAttributes(wordL, wordR - wordL, attrRed, false);
                    	 else if (text.substring(wordL, wordR).matches("(\\W)*(int|bool|double|component|string|clock|unit|priority|initial|place|from|to|do|provided|on|define|down|up|export|extern|const|lazy|delayable|eager)"))
                             setCharacterAttributes(wordL, wordR - wordL, attrBlue, false);
                    	 else if (text.substring(wordL, wordR).matches("(\\W)*(if|then|else|fi)"))
                             setCharacterAttributes(wordL, wordR - wordL, attrPurple, false);
                         else 
                             setCharacterAttributes(wordL, wordR - wordL, attrBlack, false);
                         wordL = wordR;
                     }
                     
                     
                     wordR++;
                 }
                 int position = text.indexOf("@", 0);
                 while(position != -1){
                	 setCharacterAttributes(position, text.indexOf("\n", position)- position, attrGreen, false);
                	 position = text.indexOf("@", position+1);
                 }
                 
                  position = text.indexOf("//", 0);
                 while(position != -1){
                	 setCharacterAttributes(position, text.indexOf("\n", position) - position, attrLightGray, false);
                	 position = text.indexOf("//", position+1);
                 }
//                  
             }

             public void remove (int offs, int len) throws BadLocationException {
                 super.remove(offs, len);

                 String text = getText(0, getLength());
                 int before = findLastNonWordChar(text, offs);
                 if (before < 0) before = 0;
                 int after = findFirstNonWordChar(text, offs);

                 if (text.substring(before, after).matches("(\\W)//(.)*\n"))
                     setCharacterAttributes(before, after - before, attrLightGray, false); 
                 else if (text.substring(before, after).matches("(\\W)*(function|type|connector|package|atom|compound|port|data|end)"))
                     setCharacterAttributes(before, after - before, attrRed, false);
                 else if (text.substring(before, after).matches("(\\W)*(int|bool|double|string|component|clock|unit|priority|initial|place|from|to|do|provided|on|define|down|up|export|extern|const|lazy|delayable|eager)"))
                     setCharacterAttributes(before, after - before, attrBlue, false);
                 else if (text.substring(before, after).matches("(\\W)*(if|then|else|fi)"))
                     setCharacterAttributes(before, after - before, attrPurple, false);
                 else
                     setCharacterAttributes(before, after - before, attrBlack, false);
                 
                 
                 int position = text.indexOf("@", 0);
                 while(position != -1){
                	 setCharacterAttributes(position, text.indexOf("\n", position)- position, attrGreen, false);
                	 position = text.indexOf("@", position+1);
                 }
                 
                 position = text.indexOf("//", 0);
                 while(position != -1){
                	 setCharacterAttributes(position, text.indexOf("\n", position)- position, attrLightGray, false);
                	 position = text.indexOf("//", position+1);
                 }
//                 
                 
                 
             }
         };

    	String file_content = readFile();
    	txtEditor = new JTextPane(doc);
		txtEditor.setText(file_content);
		
    	undoManager= new UndoManager();
    	doc.addUndoableEditListener(new UndoableEditListener() {
			int textSize = doc.getLength();
			public void undoableEditHappened(UndoableEditEvent e) {
				if(textSize != doc.getLength() && doc.getLength()!=0)
				 {
					
						undoManager.addEdit(e.getEdit());
						textSize = doc.getLength();
					
				 }
			}
		});
    	txtEditor.addCaretListener(new CaretListener() {
			
			public void caretUpdate(CaretEvent e) {
				
				if(txtEditor.getSelectedText() != null && txtEditor.getSelectedText().trim().length()>0)
					highlight(txtEditor, txtEditor.getSelectedText());
				else
					removeHighlights(txtEditor);
			}
		});
	
		txtEditor.setBorder(BorderFactory.createLoweredBevelBorder());
		txtEditor.setMargin(new Insets(20,20,20,20));
		txtEditor.setBackground(Color.WHITE);
		JScrollPane scrollPane = new JScrollPane(txtEditor);
		LineNumber lineNumber = new LineNumber( txtEditor );
		scrollPane.setRowHeaderView( lineNumber );
		
//		JPanel panTitle = new JPanel();
//		panTitle.setBorder(new EmptyBorder(0,0,0,0));
//		panTitle.add(new JLabel("Editing file : "+file.getName()));
//		add(panTitle, BorderLayout.NORTH);
		add(scrollPane, BorderLayout.CENTER);
		
		
//		JButton save = new JButton("Save changes");
//		JButton compile = new JButton("Compile model");		
//		JButton close = new JButton("Close panel");
//		JPanel panBut = new JPanel();
//		panBut .add(close);
//		panBut .add(save);
//		if(isEditingBIP())panBut .add(compile);
//		add(panBut, BorderLayout.SOUTH);
		
//		save.addActionListener(new ActionListener() {
//			
//			public void actionPerformed(ActionEvent e) {
//				saveChanges();
//			}
//		});
		txtEditor.addKeyListener(this);
		txtEditor.requestFocusInWindow();
//		close.addActionListener(new ActionListener() {
//			
//			public void actionPerformed(ActionEvent arg0) {
//				
//				int choice = JOptionPane.showConfirmDialog(null,  "You are about to close the file editor panel. \nThis operation will cancel unsaved changes in the file. \nAre you sure you want to close the panel?","Close confirmation", JOptionPane.YES_NO_OPTION);
//				if(choice == JOptionPane.YES_OPTION) {
//					int index = tabbedpane.getSelectedIndex();
//					int new_index = index - 1;
//					while(new_index != 0 && !tabbedpane.isEnabledAt(new_index)) 
//						new_index -- ;
//					
//					tabbedpane.setSelectedIndex(new_index);
//					tabbedpane.removeTabAt(index);
//				}
//				
//			}
//		});
		
//		compile.addActionListener(new ActionListener() {
//			
//			public void actionPerformed(ActionEvent e) {
//				
//				initiateCompile();
//			}
//				
//		});
    }
    
	@SuppressWarnings("unused")
	public void saveChanges() {
    	PrintWriter out = null;
		try 
		{
			String s = file.getAbsolutePath();
			File tmp_file = new File(s+"-tmp");
			out = new PrintWriter(tmp_file.getAbsolutePath());
			out.print(txtEditor.getText());
			out.close();
			
			 File oldFile = new File(s);
		      oldFile.delete();

		      // And rename tmp file's name to old file name
		      File newFile = new File(tmp_file.getAbsolutePath());
		      newFile.renameTo(oldFile);
		      JOptionPane.showMessageDialog(null, "The modifications have been saved in file '"+file.getName()+"' ", "Saving file", JOptionPane.PLAIN_MESSAGE);
				
		} 
			catch (FileNotFoundException e1) {
			if(out!= null) out.close();
		}
	}

	public void initiateCompile() {
    	String[] bipType = new String[2];
		bipType[1] = "Stochastic RT-BIP"; bipType[0] = "Untimed BIP"; //bipType[1] = "RT-BIP"; 
		JComboBox<String> comboBIP = new JComboBox<String>(bipType);
		comboBIP.setSelectedIndex(1);
		JTextField packageName = new JTextField(file.getName().substring(0, file.getName().lastIndexOf(".")));
		JTextField compoundName = new JTextField("\"Compound()\"");
		
		Object[] objects = new Object[6];
		objects[0] = new JLabel("Select the BIP engine you want to compile with : ");
		objects[1] = comboBIP;
		objects[2] = new JLabel("Enter package name : ");
		objects[3] = packageName;
		objects[4] = new JLabel("Enter compound name :");
		objects[5] = compoundName;
		
		int select = JOptionPane.showConfirmDialog(null, objects, "Bip engine selection", JOptionPane.PLAIN_MESSAGE);
		if(select == JOptionPane.OK_OPTION){
//			System.out.println("Compile with "+comboBIP.getSelectedItem());
			@SuppressWarnings("unused")
			Compilator compil = new Compilator(tabbedpane, file.getParent(), comboBIP.getSelectedIndex(),packageName.getText(),compoundName.getText() );
			
		}
	}
	

	public void highlight(JTextComponent textComp, String pattern) {
        // First remove all old highlights
        removeHighlights(textComp);

        try {
            Highlighter hilite = textComp.getHighlighter();
            Document doc = textComp.getDocument();
            String text = doc.getText(0, doc.getLength());

            int pos = 0;
            // Search for pattern
            while ((pos = text.indexOf(pattern, pos)) >= 0) {
                // Create highlighter using private painter and apply around pattern
                hilite.addHighlight(pos, pos + pattern.length(), myHighlightPainter);
                pos += pattern.length();
            }

        } catch (BadLocationException e) {
        }
    }

    // Removes only our private highlights
    public void removeHighlights(JTextComponent textComp) {
        Highlighter hilite = textComp.getHighlighter();
        Highlighter.Highlight[] hilites = hilite.getHighlights();

        for (int i = 0; i < hilites.length; i++) {
            if (hilites[i].getPainter() instanceof MyHighlightPainter) {
                hilite.removeHighlight(hilites[i]);
            }
        }
    }
    
   

    // A private subclass of the default highlight painter
    class MyHighlightPainter
            extends DefaultHighlighter.DefaultHighlightPainter {

        public MyHighlightPainter(Color color) {
            super(color);
//        		super(new Color(57,105,138));
        }
    }
    
    public String readFile()		  
    {
    	byte[] encoded;
    	try {
    		encoded = Files.readAllBytes(file.toPath());

    		return new String(encoded, Charset.defaultCharset());
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
    	return "";
    }


    public File getFile() {
    	return this.file;
    }

    
    public void keyPressed(KeyEvent e) {
//    	System.out.println(e.getModifiers());

    	if ((e.getKeyCode() == KeyEvent.VK_F) && ((e.getModifiers() & KeyEvent.CTRL_MASK) != 0)) {
    		
    		
    		JPanel panel = new JPanel(new GridLayout(3, 1));
    		
    		researched = "";
    		
    		final JButton but_replace = new JButton("Replace all");
			but_replace.setEnabled(false);
    		final JTextField find = new JTextField(txtEditor.getSelectedText());
    		if(txtEditor.getSelectedText() != null ){
    			highlight(txtEditor,  txtEditor.getSelectedText() );
    			but_replace.setEnabled(true);
    		}
    		final JTextField replace = new JTextField();
    		
    		
    		find.addCaretListener(new CaretListener() {
				
				public void caretUpdate(CaretEvent e) {
					{
						removeHighlights(txtEditor);
						researched = find.getText();
						if(!researched.equals("")) highlight(txtEditor, researched);
						
						if(find.getText().equals("")) {
							but_replace.setEnabled(false);
						}
						else 
							but_replace.setEnabled(true);
						
					}
					
				}
			});
    		
    		
    		JPanel panel1 = new JPanel();
    		panel1.setBorder(new EmptyBorder(5,5,5,5));
    		panel1.setLayout(new BoxLayout(panel1, BoxLayout.X_AXIS));
    		
    		JPanel panel2 = new JPanel();
    		panel2.setBorder(new EmptyBorder(5,5,5,5));
    		panel2.setLayout(new BoxLayout(panel2, BoxLayout.X_AXIS));
    		
    		panel1.add(new JLabel("      Find : "));
    		panel1.add(find);
    		panel2.add(new JLabel("Replace : "));
    		panel2.add(replace);
    		
    		
    		
    		but_replace.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) {
					replaceText(txtEditor, find.getText(), replace.getText());
				}
			});
    		
    		panel.setPreferredSize(new Dimension(250, 200));
    		panel.setBorder(new EmptyBorder(15,15,15,15));
    		panel.add(panel1);
    		panel.add(panel2);
    		panel.add(but_replace);

    		findFrame= new JFrame("Find/Replace");
    		findFrame.setLocationRelativeTo(null);
    		findFrame.add(panel);
    		findFrame.setSize(500, 300);
    		findFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    		findFrame.pack();
    		findFrame.setVisible(true);
    		findFrame.setAlwaysOnTop(true);
    		findFrame.setResizable(false);
    		findFrame.addWindowListener(new WindowListener() {
				
 
				public void windowOpened(WindowEvent e) {
					 
					
				}
				
 
				public void windowIconified(WindowEvent e) {
					 
					
				}
				
 
				public void windowDeiconified(WindowEvent e) {
					 
					
				}
				
 
				public void windowDeactivated(WindowEvent e) {
					 
					
				}
				
 
				public void windowClosing(WindowEvent e) {
					removeHighlights(txtEditor);
					
				}
				
 
				public void windowClosed(WindowEvent e) {
					 
					
				}
				
 
				public void windowActivated(WindowEvent e) {
					 
					
				}
			});
    		   		
    		
    		
    	}
    	else if ((e.getKeyCode() == KeyEvent.VK_C) && ((e.getModifiers() & KeyEvent.CTRL_MASK) != 0)&& ((e.getModifiers() & KeyEvent.SHIFT_MASK) != 0)) {
    		
//    		System.out.println(txtEditor.getSelectionStart()+" -- "+txtEditor.getSelectionEnd());
    		commentLines(txtEditor.getSelectionStart(),txtEditor.getSelectionEnd( ));
    	}
    	
    	else if ((e.getKeyCode() == KeyEvent.VK_Z) && ((e.getModifiers() & KeyEvent.CTRL_MASK) != 0)) {
    		try {
    		if(undoManager.canUndo())
    			// COMMENTED THIS LINE 
//    		if(!undoManager.getUndoPresentationName().equals("Annuler")) 
    			undoManager.undo();
//    		System.out.println(undoManager.getUndoPresentationName());
    		}
    		catch(Exception exception) {
    			System.out.println(exception.getMessage());
    		}
    	}
    	
    	else if ((e.getKeyCode() == KeyEvent.VK_Y) && ((e.getModifiers() & KeyEvent.CTRL_MASK) != 0)) {
    		try {
	    		if(undoManager.canRedo())
	//    		if(!undoManager.getUndoPresentationName().equals("Annuler")) 
	    			undoManager.redo();
//	    		System.out.println(undoManager.getRedoPresentationName());
	    	}
			catch(Exception exception) {
				System.out.println(exception.getMessage());
			}
    	}
    	

	}


	private void commentLines(int selectionStart, int selectionEnd) {
		 final StyleContext cont = StyleContext.getDefaultStyleContext();	        
		final AttributeSet attrBlack = cont.addAttribute(cont.getEmptySet(), StyleConstants.Foreground, Color.black);
        
		int currentP = 0;
		int lastP = 0;
//		 Document doc = txtEditor.getDocument();
        String s="";
		try {
			s = doc.getText(0, doc.getLength());
		} catch (BadLocationException e) {
			
			e.printStackTrace();
		}
		String newS = "";
		String[] lines = s.split("\n");
		boolean foundLine = false;
		for (int i = 0; i < lines.length; i++) {
			currentP = lastP +lines[i].length();
//			if(currentP < selectionStart){
//				newS+=lines[i]+"\n";
//			}
//			else{
			foundLine = (intersect(selectionStart, selectionEnd, lastP, currentP));
					
				
				if(foundLine){
//					System.out.println(true);
					if(lines[i].matches("(\\s|\\t)*//(.)*"))
					{
//						System.out.println("remove comment");
						try {
							/// Problème trouvé ici
							newS = "";
							if(lines[i].indexOf("//")>0) newS = lines[i].substring(0,lines[i].indexOf("//"));	
							newS += lines[i].substring(lines[i].indexOf("//")+2, lines[i].length());				
							
							doc.remove(lastP, lines[i].length());
							doc.insertString(lastP, newS, attrBlack);
							currentP-= (lines[i].length() - newS.length());
							selectionEnd-= (lines[i].length() - newS.length());
							
						} catch (BadLocationException e) {
							
							e.printStackTrace();
						}
					}

					else{
//						System.out.println(lines[i]);
//						System.out.println("Comment");
//						newS += "//"+lines[i]+"\n";
						try {
//							if(lastP == -1) lastP = 0;
//							System.out.println("["+selectionStart+","+selectionEnd+"]--["+lastP+","+currentP+"]");
							
							doc.insertString(lastP, "//", attrBlack);
							currentP += 2;
							selectionEnd += 2;
						} catch (BadLocationException e) {
							e.printStackTrace();
						}
					}
				}
//				else{
////					newS+=lines[i]+"\n";
//				}
//			}
			lastP =  currentP+1;
		}
		
//		txtEditor.revalidate();
	//		txtEditor.setText(newS);
		removeHighlights(txtEditor);
	}

	
	private void untabulateLines(int selectionStart, int selectionEnd) {
		 final StyleContext cont = StyleContext.getDefaultStyleContext();	        
		final AttributeSet attrBlack = cont.addAttribute(cont.getEmptySet(), StyleConstants.Foreground, Color.black);
      
		int currentP = 0;
		int lastP = 0;
      String s="";
		try {
			s = doc.getText(0, doc.getLength());
		} catch (BadLocationException e) {
			
			e.printStackTrace();
		}
		String newS = "";
		String[] lines = s.split("\n");
		boolean foundLine = false;
		for (int i = 0; i < lines.length; i++) {
			currentP = lastP +lines[i].length();
			foundLine = (intersect(selectionStart, selectionEnd, lastP, currentP));
					
				
				if(foundLine){
					if(lines[i].matches("(\\t)(.)*"))
					{
						try {
							newS = lines[i].substring(1, lines[i].length());				
							
							doc.remove(lastP, lines[i].length());
							doc.insertString(lastP, newS, attrBlack);
							currentP-= (lines[i].length() - newS.length());
							selectionEnd-= (lines[i].length() - newS.length());
							
						} catch (BadLocationException e) {
							
							e.printStackTrace();
						}
					}
				}

			lastP =  currentP+1;
		}
		
		removeHighlights(txtEditor);
	}
	
	
	private void tabulateLines(int selectionStart, int selectionEnd) {
		 final StyleContext cont = StyleContext.getDefaultStyleContext();	        
		final AttributeSet attrBlack = cont.addAttribute(cont.getEmptySet(), StyleConstants.Foreground, Color.black);
       
		int currentP = 0;
		int lastP = 0;
       String s="";
		try {
			s = doc.getText(0, doc.getLength());
		} catch (BadLocationException e) {
			
			e.printStackTrace();
		}
		String[] lines = s.split("\n");
		boolean foundLine = false;
		for (int i = 0; i < lines.length; i++) {
			currentP = lastP +lines[i].length();
			foundLine = (intersect(selectionStart, selectionEnd, lastP, currentP));
					
				
				if(foundLine){
					try {
						doc.insertString(lastP, "\t", attrBlack);
						currentP += 1;
						selectionEnd += 1;
					} catch (BadLocationException e) {
						e.printStackTrace();
					}
				}

			lastP =  currentP+1;
		}
		
		removeHighlights(txtEditor);
	}
	
	private boolean intersect(int selectionStart, int selectionEnd, int lastP, int currentP) {
//		System.out.println("["+selectionStart+","+selectionEnd+"]--["+lastP+","+currentP+"]");
		if(selectionStart>selectionEnd) return false;
		if(selectionStart <=currentP && selectionStart >= lastP) return true;
		if(lastP<= selectionEnd && lastP > selectionStart && currentP<= selectionEnd && currentP >= selectionStart) return true;
		if(selectionEnd <=currentP && selectionEnd >=lastP) return true;
		return false;
	}

	protected void replaceText(JTextPane txtEditor2, String text, String text2) {
		try {
			final StyleContext cont = StyleContext.getDefaultStyleContext();	        
			final AttributeSet attrBlack = cont.addAttribute(cont.getEmptySet(), StyleConstants.Foreground, Color.black);
		    
			doc.getText(0, doc.getLength());
			int i =0;
			i = doc.getText(0, doc.getLength()).indexOf(text,0);
			while(i!=-1){
				doc.remove(i, text.length());
				doc.insertString(i, text2, attrBlack);
				i = doc.getText(0, doc.getLength()).indexOf(text,i+text2.length());
				
			}
			
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		
	}



	public void keyReleased(KeyEvent e) {
		if(enableSuggestion){
			if (e.getKeyCode() == KeyEvent.VK_DOWN && suggestion != null) {
	            suggestion.moveDown();
	        } else if (e.getKeyCode() == KeyEvent.VK_UP && suggestion != null) {
	            suggestion.moveUp();
	        } else if (Character.isLetterOrDigit(e.getKeyChar())) {
	        	hideSuggestion();
	            enableSuggestion = false;
	        } else if (Character.isWhitespace(e.getKeyChar())) {
	            hideSuggestion();
	            enableSuggestion = false;
	        }
		}
		else if((e.getKeyCode() == KeyEvent.VK_SPACE) && ((e.getModifiers() & KeyEvent.CTRL_MASK) != 0)){
			enableSuggestion = true;
    		showSuggestionLater();
		}
		
		else if((e.getKeyCode() == KeyEvent.VK_T) && ((e.getModifiers() & KeyEvent.CTRL_MASK) != 0) && ((e.getModifiers() & KeyEvent.SHIFT_MASK) != 0)){
//			System.out.println("Ctrl+Shift+T");
			untabulateLines(txtEditor.getSelectionStart(),txtEditor.getSelectionEnd( ));
		}
		else if((e.getKeyCode() == KeyEvent.VK_T) && ((e.getModifiers() & KeyEvent.CTRL_MASK) != 0) && ((e.getModifiers() & KeyEvent.SHIFT_MASK) == 0)){
//			System.out.println("Ctrl+T");
			tabulateLines(txtEditor.getSelectionStart(),txtEditor.getSelectionEnd( ));
		}
		
	}
	
	
	


	public void keyTyped(KeyEvent e) {
		if(enableSuggestion)
		if (e.getKeyChar() == KeyEvent.VK_ENTER) {
            if (suggestion != null) {
                if (suggestion.insertSelection()) {
                    e.consume();
                    final int position = txtEditor.getCaretPosition();
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            try {
                            	txtEditor.getDocument().remove(position - 1, 1);
                            } catch (BadLocationException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
            hideSuggestion();
            enableSuggestion = false;
        }
		
	}
	
/////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////// 
/////////////////////  Auto complete part ///////////////////// 
/////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////// 

	
    public class SuggestionPanel {
        @SuppressWarnings("rawtypes")
		private JList list;
        private JPopupMenu popupMenu;
        private String subWord;
        private final int insertionPosition;

        public SuggestionPanel(JTextPane textpane, int position, String subWord, Point location) {
            this.insertionPosition = position;
            this.subWord = subWord;
            popupMenu = new JPopupMenu();
            popupMenu.removeAll();
            popupMenu.setOpaque(false);
            popupMenu.setBorder(null);
            list = createSuggestionList(position, subWord);
            if(list != null)
            {
            	popupMenu.add(list, BorderLayout.CENTER);
                popupMenu.show(textpane, location.x, textpane.getBaseline(1, 0) + location.y);
            }
            else  suggestion.hide();
            
        }

        public void hide() {
            popupMenu.setVisible(false);
            if (suggestion == this) {
                suggestion = null;
            }
        }

        @SuppressWarnings({ "rawtypes", "unused" })
		private JList createSuggestionList(int position, String subWord) {
            Object[] data = new  Object[0];
//            for (int i = 0; i < data.length; i++) {
//                data[i] = subWord + i;
//            }
            
            String pack = "package"; 
            String con = "connector";
            String atom = "atom";
            String compound = "compound";
            String component = "component";
            String port = "port";
            String clock = "clock";

            String stoch = "@stochastic";
            String cpp = "@cpp";

            String If = "if";

            
            if(If.startsWith(subWord)){
            	data = new Object[2];
            	data[0] = "Insert if statement";
            	data[1] = "Insert if/else statement";
        	}
            if( stoch.startsWith(subWord)){
            	data = new Object[1];
            	data[0] = "Insert stochastic annotation";
            }
            else if( cpp.startsWith(subWord)){
            	data = new Object[1];
            	data[0] = "Insert cpp annotation";
            }
            else if( clock.startsWith(subWord)){
            	data = new Object[1];
            	data[0] = "Create clock declaration";
            }
            else if( pack.startsWith(subWord)){
            	data = new Object[1];
            	data[0] = "Create package";
            }
            else
            if(subWord.equals("on")){
            	data = new Object[1];
            	data[0] = "Create a new transition";
            }


            else if(port.startsWith(subWord)){
            	data = new Object[2];
            	data[0] = "Create a port type";
            	data[1] = "Instantiate a port";
        	}
            else if(atom.startsWith(subWord)){
            	data = new Object[1];
            	data[0] = "Create atom type";
        	} 
            
            
            else if(subWord.startsWith("co")){
//            	if(subWord.startsWith("con")){
            	data = new Object[4];
        		int i = 0;
            		if(con.startsWith(subWord)){
                    	data[0] = "Create connector type";
                    	data[1] = "Instantiate connector";
                    	i = 2;
                	}
//            	}
//            	else if(subWord.startsWith("compo")){
            		
            		if(compound.startsWith(subWord)){
                    	data[i] = "Create compound type";
                    	i++;
                	}
            		if(component.startsWith(subWord)){
                    	data[i] = "Instantiate a component";
                	}	
                	
//            	}
            	            	
            }
            
            
            
            if(data != null){
            	@SuppressWarnings("unchecked")
				JList list = new JList(data);
                list.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
                list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                list.setSelectedIndex(0);
                list.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (e.getClickCount() == 2) {
                            insertSelection();
                        }
                    }
                });
                return list;
            }
            else{
            	this.hide();
            }
            return null;
            
        }

        public boolean insertSelection() {
            if (list.getSelectedValue() != null) {
                try {
                    String selectedSuggestion = getAutoComplete( ((String) list.getSelectedValue()));
                    selectedSuggestion = selectedSuggestion.substring(subWord.length());
                    doc.insertString(insertionPosition, selectedSuggestion, null);
                    return true;
                } catch (BadLocationException e1) {
                    e1.printStackTrace();
                }
                hideSuggestion();
            }
            return false;
        }

        private String getAutoComplete(String suggestion) {
//        	System.out.println(suggestion);
        	
        	switch(suggestion){
        		case "Insert if statement" : return patternIf();
        		case "Insert if/else statement" : return patternIfElse();
        		case "Create clock declaration": return patternClock();
	        	case "Create package": return patternPackage(); 
	        	case "Create a new transition": return patternTransition();
	        	case "Create a port type":return patternPortType();
	        	case "Instantiate a port": return patternPortDec();
	        	case "Create atom type": return patternAtom();
	        	case "Create connector type": return patternConnectorType();
	        	case "Instantiate connector": return patternConnectorInst();
	        	case "Create compound type":return patternCompound(); 
	        	case "Instantiate a component": return patternComponent();
	        	case "Insert stochastic annotation": return patternStoch();
	        	case "Insert cpp annotation": return patternCpp();
	        	default :	return "";
        	}
		}

        private String patternIf() {
			String pattern = "if (condition) then statement; fi";
			return pattern;
		}
        
        private String patternIfElse() {
			String pattern = "if (condition) then statement; else statement; fi";
			return pattern;
		}

		private String patternComponent() {
			String pattern = "component component_t Component1(ID)";
			return pattern;
		}

		public void moveUp() {
            int index = Math.min(list.getSelectedIndex() - 1, 0);
            selectIndex(index);
        }

        public void moveDown() {
            int index = Math.min(list.getSelectedIndex() + 1, list.getModel().getSize() - 1);
            selectIndex(index);
        }

        private void selectIndex(int index) {
            final int position = txtEditor.getCaretPosition();
            list.setSelectedIndex(index);
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                	txtEditor.setCaretPosition(position);
                };
            });
        }
    }
	  	
    	
	    protected void showSuggestionLater() {
	        SwingUtilities.invokeLater(new Runnable() {
	            @Override
	            public void run() {
	                showSuggestion();
	            }

	        });
	    }

	    
	    public String patternClock() {
			String pattern = "clock x unit nanosecond";
			return pattern;
		}
	    
	    public String patternCpp() {
			String pattern = "@cpp(src=\"bib.c\", include=\"bib.h\")";
			return pattern;
		}
	    
	    public String patternStoch() {
			String pattern = "@stochastic(dist=\"normal\",clk=x,param=\"6,2\")";
			return pattern;
		}


		public String patternConnectorInst() {
	    	String pattern = "connector connector_type connect( cmp1.p, cmp2.p)";
					
			
			return pattern;
		}


		public String patternConnectorType() {
	    	String pattern = "connector type connector_type( port_type p1, port_type p2)"
					+"\n\t\tdefine p1 p2"
					+ "\n\t\ton p1 p2 down { p1.var = p2.var; }"				
					+"\n\tend";
			
			return pattern;
		}


		public String patternPackage() {
			String pattern = "package package_name\n"
					+"\n\t// Constant data definitions "
					+"\n\tconst data int CONST = 0"
					+"\n\n\t// Port types definitions"
					+"\n\n\t// Connector types definitions"
					+"\n\n\t// Atom types definitions"
					+"\n\n\t// Compound types definitions"
					+"\n\t" + patternCompound()
					+"\nend";
			
			return pattern;
					
		}

		private String patternCompound() {
			String pattern = "compound type Compound()"
					+"\n\n\t\t// Variable declarations"
					+"\n\n\t\t// Component instantiations"
					+"\n\n\t\t// Connector instantiations"
					+"\n\n\t\t// Priority instantiations"
					+"\n\tend";
			return pattern;
		}
		
		private String patternAtom() {
	    	String pattern = "atom type Atom()"
					+"\n\t\t// Variable declarations"
					+"\n\t\tdata int var = 0"
					+"\n\n\t\t// Port declarations"
					+"\n\t\t"+ patternPortDec()
					+"\n\n\t\t// Clock declarations"
					+"\n\t\t"+patternClock()
					+"\n\n\t\t// Locations declaration"
					+"\n\t\tplace s0"
					+"\n\n\t\t// Initial location declarations"
					+"\n\t\tinitial to s0 do { var = 0; x = 0; }"
					+"\n\n\t\t// transitions"
					+"\n\n\t\t// invariants"
					+"\n\n\t\t// Priorities"
					+"\n\tend";
			return pattern;
		}
		
		private String patternPortDec() {
	    	String pattern = "port port_t port_name ( var )";
			return pattern;
		}

		private String patternPortType() {
	    	String pattern = "port type port_t ( int var )";
			return pattern;
		}

		private String patternTransition() {
			String pattern = "on port_name"
					+"\n\t\t     from location_1 to location_2"
					+"\n\t\t     provided ( guard )"
					+"\n\t\t     delayable"
					+"\n\t\t     do { x = 0; }";
			return pattern;
		}

		protected void showSuggestion() {
	        hideSuggestion();
	        final int position = txtEditor.getCaretPosition();
	        Point location;
	        try {
	            location = txtEditor.modelToView(position).getLocation();
	        } catch (BadLocationException e2) {
	            e2.printStackTrace();
	            return;
	        }
	        String text = txtEditor.getText();
	        int start = Math.max(0, position - 1);
	        while (start > 0) {
	            if (!Character.isWhitespace(text.charAt(start))) {
	                start--;
	            } else {
	                start++;
	                break;
	            }
	        }
	        if (start > position) {
	            return;
	        }
	        final String subWord = text.substring(start, position);
	        if (subWord.length() < 2) {
	            return;
	        }
	        suggestion = new SuggestionPanel(txtEditor, position, subWord, location);
	        SwingUtilities.invokeLater(new Runnable() {
	            @Override
	            public void run() {
	            	txtEditor.requestFocusInWindow();
	            }
	        });
	    }

	    private void hideSuggestion() {
	        if (suggestion != null) {
	            suggestion.hide();
	        }
	    }

    
}
