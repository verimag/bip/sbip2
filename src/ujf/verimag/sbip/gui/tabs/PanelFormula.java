package ujf.verimag.sbip.gui.tabs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import org.antlr.v4.runtime.misc.ParseCancellationException;

import ujf.verimag.sbip.gui.TabbedPane;
import ujf.verimag.sbip.parser.formula.Formula;
import ujf.verimag.sbip.parser.formula.LtlFormula;
import ujf.verimag.sbip.parser.formula.MtlFormula;
import ujf.verimag.sbip.parser.ltl.LtlExprParser;
import ujf.verimag.sbip.parser.mtl.MtlExprParser;

public class PanelFormula extends JPanel {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TabbedPane tabbedpane;
	private JComboBox<String> formulaType;
	private JPanel panAdvice;
	private JTextArea txtformula;
	private JPanel panCenter;
	private JTextArea parsedFormula;
	private JPanel panCheck;
	
	private Formula formula;
	protected String strformulaType;
	private String inProject = "";
	private JLabel error;
	
	public String getFormula(){
		if(formula != null)
			return formula.toString();
		else return "";
	}
	
	public String getProject(){
		return inProject;
	}

	public PanelFormula(TabbedPane tp) throws IOException{
		this.tabbedpane = tp;
		getPanFormula();
		
	}
	
	private void getPanFormula(){
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		setBorder(new EmptyBorder(20,20,20,20));
		
		
		final String[] type = new String[3];  
		type[0] = " ";
		type[1] = "MTL";
		type[2] = "LTL";
		
		formulaType = new JComboBox<String>(type);
		JPanel panChoose = new JPanel();
		panChoose.setLayout(new FlowLayout(FlowLayout.LEFT));
		panChoose.add(new JLabel("Select the property type : "));
		panChoose.add(formulaType);
		panChoose.setBorder(new EmptyBorder(5,5,5,5));
		add(panChoose);
		
		final JPanel panMid = new JPanel();
		TitledBorder title = BorderFactory.createTitledBorder("Write formula");
		title.setTitleJustification(TitledBorder.CENTER);
		panMid.setBorder(title);
		panMid.setLayout(new BoxLayout(panMid, BoxLayout.PAGE_AXIS));
		
		final JTextArea txtAdvice = new JTextArea();
		txtAdvice.setEditable(false);
		

		txtAdvice.setBackground(new Color(0,0,0,0));
		txtAdvice.setBorder(BorderFactory.createLoweredBevelBorder());
		txtAdvice.setMargin(new Insets(120,120,120,120));
//		txtAdvice.setBackground(Color.lightGray);
		
		/// Transparence
//		txtAdvice.setOpaque(false);
		
		panAdvice = new JPanel();
		panAdvice.setLayout(new BoxLayout(panAdvice, BoxLayout.X_AXIS));
		panAdvice.setBorder(new EmptyBorder(5,5,5,5));
		panAdvice.add(txtAdvice);
		panMid.add(panAdvice);
		
		
		
		txtformula = new JTextArea();
//		txtformula.setPreferredSize(new Dimension((int)tabbedpane.getTabSize().getWidth()/5, (int)tabbedpane.getTabSize().getHeight()*1/3));

		JButton buttonCheck = new JButton("Check Formula");
		buttonCheck.setBounds(0, 0, 200, 50);	
		JButton buttonSave = new JButton("Save Formula");
		buttonSave.setBounds(0, 0, 200, 50);
		JPanel panButton = new JPanel();
		panButton.add(buttonCheck);
		panButton.add(buttonSave);
		JPanel panFormula = new JPanel();
		panFormula.setLayout(new BoxLayout(panFormula, BoxLayout.X_AXIS));
		panFormula.setBorder(new EmptyBorder(5,5,5,5));
		panFormula.add(new JLabel("Type your formula : "));
		panFormula.add(txtformula);
		panCenter = new JPanel(new BorderLayout());
		panCenter.add(panFormula,BorderLayout.CENTER);
		panCenter.add(panButton, BorderLayout.SOUTH);
		panMid.add(panCenter);
		add(panMid);

		add(new JLabel(" "));
		
		
		final JPanel panBot = new JPanel();
		TitledBorder title2 = BorderFactory.createTitledBorder("Check formula");
		title2.setTitleJustification(TitledBorder.CENTER);
		panBot.setBorder(title2);
		panBot.setLayout(new BoxLayout(panBot, BoxLayout.PAGE_AXIS));
		
		parsedFormula = new JTextArea("( NONE )");
		parsedFormula.setLineWrap(true);
		parsedFormula.setWrapStyleWord(true);
		txtformula.setLineWrap(true);
		txtformula.setWrapStyleWord(true);

		
		
		parsedFormula.setEditable(false);
		panCheck = new JPanel();
		panCheck.setLayout(new BoxLayout(panCheck, BoxLayout.X_AXIS));
		panCheck.setBorder(new EmptyBorder(5,5,5,5));
		panCheck.add(new JLabel("Parsed formula : "));
		panCheck.add(parsedFormula);
		panBot.add(panCheck);
		
		error = new JLabel(" ");
		final JPanel panError = new JPanel();
		panError.setBorder(new EmptyBorder(5,5,10,5));
		panError.add(error);
		panBot.add(panError);
		add(panBot);
		
		panMid.setVisible(false);
		panBot.setVisible(false);
		
		formulaType.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				if(tabbedpane.getSelectedProject().equals("")){
					error.setForeground(Color.RED);
					error.setText("Error : please select a project!");
					formulaType.setSelectedItem(type[0]);
				}
				else{
					error.setText(" ");
					
					int i = formulaType.getSelectedIndex();
					if( i == 0){
						strformulaType = " ";
						panMid.setVisible(false);
						panBot.setVisible(false);
//						tabbedpane.enableComboEngine(true);
//						tabbedpane.enableLimitParam(true);
					}
					else if( i == 1){
						strformulaType = "MTL";
						txtAdvice.setText("Here we detail the syntax of an MTL formula:"
								+ "\n\n\t - List of temporal operators : Until (U), Release (R), Globally (G), Eventually (F)  and Next (N)"
								+ "\n\n\t - List of boolean operators : And (&&), Or (||) and Not (!)"
								+ "\n\n\t - All temporal operators (except the Next operator) are followed by an interval"
								+ "\n\n\t - Intervals are defined as follows : \"[lower_bound, upper_bound]\""
								+ "\n\n\t - State formulae are boolean expressions over data variables"
								+ "\n\n\t - Arithmetic operations can be used in the boolean expressions"
								+ "\n\n\t - Available mathematical functions :  exponential (exp) and absolute value (abs)"
								+ "\n\n\t - For more clarity, we advise to use parentheses when writing properties"
								+ "\n\n\t - Example : G[0,30] ((a || b) U[2,5] c)");

//						parsedFormula.setText("( NONE )");
//						formula = null;
						panMid.setVisible(true);
						panBot.setVisible(true);
//						tabbedpane.enableComboEngine(false);
//						tabbedpane.enableLimitParam(true);
					}
					else if( i == 2){
						strformulaType = "LTL";
						txtAdvice.setText("Here we detail the syntax of an LTL formula:"
								+ "\n\n\t - List of temporal operators : Until (U), Release (R), Globally (G), Eventually (F)  and Next (N)"
								+ "\n\n\t - List of boolean operators : And (&&), Or (||) and Not (!)"
								+ "\n\n\t - All temporal operators (except the Next operator) are followed by an bound"
								+ "\n\n\t - Bounds are defined as follows : \"{Integer_value}\""
								+ "\n\n\t - State formulae are boolean expressions over data variables"
								+ "\n\n\t - Arithmetic operations can be used in the boolean expressions"
								+ "\n\n\t - Available mathematical functions : absolute value (abs)"
								+ "\n\n\t - For more clarity, we advise to use parentheses when writing properties"
								+ "\n\n\t - Example : G{4} a || (abs(x+5)<3) ");

//						parsedFormula.setText("( NONE )");
//						formula = null;
						panMid.setVisible(true);
						panBot.setVisible(true);
						
//						tabbedpane.enableComboEngine(true);
//						tabbedpane.enableLimitParam(false);
					}
					
					
				}
			}
		});
		
buttonCheck.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {

				String s = txtformula.getText();

				if(!s.equals("")){
					if(strformulaType.equals("MTL") ){
						MtlExprParser parser = new MtlExprParser();
						try {
							MtlFormula f =parser.parse(s);
							formula = f;
							error.setForeground(Color.BLUE);
							error.setText("MTL formula has been parsed successfully!");
							
							parsedFormula.setText(f.toString());
							inProject = tabbedpane.getSelectedProject();
						
						} catch (ParseCancellationException e2) {
							error.setForeground(Color.RED);
							error.setText("Parsing failed : the MTL formula is incorrect!");
							parsedFormula.setText("( NONE )");
						
						}
					}
					else {
						if(strformulaType.equals("LTL") ){
							LtlExprParser parser = new LtlExprParser();
							try {
								s = "P=?["+s+"]";
								LtlFormula f =parser.parse(s);
								formula = f;
								error.setForeground(Color.BLUE);
								error.setText("LTL formula has been parsed successfully!");
								
								parsedFormula.setText(f.toString());
								inProject = tabbedpane.getSelectedProject();
							
							} catch (ParseCancellationException e2) {
								error.setForeground(Color.RED);
								error.setText("Parsing failed : the LTL formula is incorrect!");
								parsedFormula.setText("( NONE )");
							
							}
						}
					}
					
				
					
					
				}
				
			}
		});


	buttonSave.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				String s = txtformula.getText();
				if(!s.equals("")){
					
					if(strformulaType.equals("MTL") ){
						MtlExprParser parser = new MtlExprParser();
						try {
							MtlFormula f =parser.parse(s);
							formula = f;
							
							parsedFormula.setText(f.toString());
							
							String propertyName = JOptionPane.showInputDialog("Please type the name of the property :");
							
							if(propertyName != null )
							if(!propertyName.equals(""))
							{
								File file= new File("./Workspace/"+tabbedpane.getSelectedProject()+"/Properties/"+propertyName+".mtl");
//								if(!file.exists()){
									@SuppressWarnings("resource")
									ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));  
								    out.writeObject(f); 
								    
								    error.setForeground(Color.BLUE);
									error.setText("MTL formula \""+propertyName+"\" has been saved in the current project '"+tabbedpane.getSelectedProject()+"'!");
									
		
									inProject = tabbedpane.getSelectedProject();
									tabbedpane.insertNodeInTree(file.getAbsolutePath());
								}
								
							else {
								JOptionPane.showMessageDialog(null, "Error : cannot create a file with empty name", "Error", JOptionPane.ERROR_MESSAGE);
							}
//							else {
//								JOptionPane.showMessageDialog(null, "Saving failed : MTL formula '"+propertyName+"' already exists!", "Error", JOptionPane.ERROR_MESSAGE);
//							}
//							    tabbedpane.resetTree();
//							}
//							else{
//								error.setForeground(Color.RED);
//								error.setText("Saving failed : MTL formula \""+propertyName+"\" already exists! ");
//							}
							
						
						} catch (ParseCancellationException e2) {
							error.setForeground(Color.RED);
							error.setText("Parsing failed : the MTL formula is incorrect!");
							parsedFormula.setText("( NONE )");
						
						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					else {
						if(strformulaType.equals("LTL") ){
							LtlExprParser parser = new LtlExprParser();
							try {
								s = "P=?["+s+"]";
								LtlFormula f =parser.parse(s);
								formula = f;
								
								parsedFormula.setText(f.toString());
								
								String propertyName = JOptionPane.showInputDialog("Please type the name of the property :");
								
								if(propertyName != null )
								if(!propertyName.equals(""))
								{
									File file= new File("./Workspace/"+tabbedpane.getSelectedProject()+"/Properties/"+propertyName+".ltl");
//									if(!file.exists()){
										@SuppressWarnings("resource")
										ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));  
									    out.writeObject(f); 
									    
									    error.setForeground(Color.BLUE);
										error.setText("LTL formula \""+propertyName+"\" has been saved in the current project '"+tabbedpane.getSelectedProject()+"'!");
										
			
										inProject = tabbedpane.getSelectedProject();
										tabbedpane.insertNodeInTree(file.getAbsolutePath());
									}
									
								else {
									JOptionPane.showMessageDialog(null, "Error : cannot create a file with empty name", "Error", JOptionPane.ERROR_MESSAGE);
								}
//								else {
//									JOptionPane.showMessageDialog(null, "Saving failed : MTL formula '"+propertyName+"' already exists!", "Error", JOptionPane.ERROR_MESSAGE);
//								}
//								    tabbedpane.resetTree();
//								}
//								else{
//									error.setForeground(Color.RED);
//									error.setText("Saving failed : MTL formula \""+propertyName+"\" already exists! ");
//								}
								
							
							} catch (ParseCancellationException e2) {
								error.setForeground(Color.RED);
								error.setText("Parsing failed : the LTL formula is incorrect!");
								parsedFormula.setText("( NONE )");
							
							} catch (FileNotFoundException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
						
					}
					
					
					
				}
				
			}
		});
				
	}

	public void setFormula(MtlFormula form) {
		strformulaType = "MTL";
		formula = form;
		parsedFormula.setText(formula.toString());
//		txtformula.setText(formula.toString());
		formulaType.setSelectedIndex(1);
		inProject = tabbedpane.getSelectedProject();

		panAdvice.setVisible(true);
		panCenter.setVisible(true);
		panCheck.setVisible(true);
		
	}
	

	public void setFormula(LtlFormula form) {
//		System.out.println("Doing this");
		strformulaType = "LTL";
		formula = form;
		parsedFormula.setText(formula.toString());
//		txtformula.setText(formula.toString());
		formulaType.setSelectedIndex(2);
		inProject = tabbedpane.getSelectedProject();

		panAdvice.setVisible(true);
		panCenter.setVisible(true);
		panCheck.setVisible(true);
		
	}

	public void setProject(String project) {
		inProject = project;
		
	}

	public String getFormulaType() {
		return this.strformulaType;
	}

	public MtlFormula getMtlFormula() {
		return (MtlFormula) this.formula;
	}

	public void reset() {

		inProject = "";
		strformulaType = "";
		parsedFormula.setText("( NONE )");
		txtformula.setText("");
		error.setText(" ");
		formula = null;
	}

	public LtlFormula getLtlFormula() {
		return (LtlFormula) this.formula;
	}

	

}
