package ujf.verimag.sbip.gui.tabs;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import ujf.verimag.sbip.analysis.Simulator;
import ujf.verimag.sbip.gui.TabbedPane;
import ujf.verimag.sbip.gui.model.LineBarVerdict;
import ujf.verimag.sbip.gui.model.LineExeTime;
import ujf.verimag.sbip.gui.model.SmcTableModel;

public class PanelParamSmcResult extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Simulator simulator;
	private TabbedPane tabbedpane;

	public PanelParamSmcResult(TabbedPane tp , Simulator simulator, int init_v, int final_v, int step){
		this.tabbedpane = tp;
		this.simulator = simulator;
		getPanSim(init_v, final_v, step);
	}
	
	
	public void freeMemory() {
		tabbedpane.removeTabAt(tabbedpane.getSelectedIndex());
		for (int i = 0; i < tabbedpane.getTabCount(); i++) {
			tabbedpane.setEnabledAt(i, true);
		}
		tabbedpane.setSelectedIndex(tabbedpane.getTabCount()-1);
		
		
		simulator.clearData();
			
	}
		
	private void getPanSim(int init_v, int final_v, int step) {
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setBorder(new EmptyBorder(20,20,20,20));
		
		JPanel panSim = new JPanel();
		panSim.setLayout(new BoxLayout(panSim, BoxLayout.Y_AXIS));
		TitledBorder title = BorderFactory.createTitledBorder("Simulation");
		title.setTitleJustification(TitledBorder.CENTER);
		panSim.setBorder(title);
		
		JPanel panResume = new JPanel();
		panResume.setLayout(new BoxLayout(panResume, BoxLayout.Y_AXIS));
		panResume.add(new JLabel("SMC type : "+simulator.getSmc_type()));
		panResume.add(new JLabel("SMC parameters : Alpha ("+simulator.getAlpha()+") "
				+(simulator.getSmc_type().equals("Hypothesis testing") ?"- Beta ("+simulator.getBeta()+") " : " ")
				+ "- Delta ("+simulator.getDelta()+")")
				);
		panResume.add(new JLabel("Simulation command : "
				+tabbedpane.getSelectedModel()
				+ (simulator.getSimType().equals("trace")? " --limit " + simulator.getLimit():"")
				+(simulator.isLog_vars()?" --log-variables":"")));
		panResume.add(new JLabel("Evaluated property : P"
				+(simulator.getSmc_type().equals("Probability estimation")? "=?{ " : simulator.getRelation().toString()+simulator.getTeta()+"{ " )
				+tabbedpane.getFormula()+" }"));
		
		panResume.add(new JLabel(" "));
		panResume.add(new JLabel("Parametric execution on variable : "
				+ (simulator.getParamVar().getType()+" " +simulator.getParamVar().getVariable())
				+ (simulator.getParamVar().getType().equals("int")? " in interval ["+init_v+","+final_v+"] with step = "+step:"")
				));
//		panResume.add(new JLabel("Required number of traces : "+simulator.getTraces_number()));
		panResume.add(new JLabel("Overall Execution time : "+simulator.getOverallExecutiontime()+" ms"));
		panResume.add(new JLabel(" "));
		
		
		final SmcTableModel model = new SmcTableModel(simulator.getSmcData());
		final JTable 	table = new JTable(model){
		    /**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			   public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
		    	   
		           Component component = super.prepareRenderer(renderer, row, column);
		           int rendererWidth = component.getPreferredSize().width;
		           TableColumn tableColumn = getColumnModel().getColumn(column);
		           tableColumn.setPreferredWidth(Math.max(rendererWidth + getIntercellSpacing().width, tableColumn.getPreferredWidth()));
		           ((DefaultTableCellRenderer) renderer).setHorizontalAlignment( JLabel.CENTER );
		           return component;
		        }
		    };
		DefaultTableCellRenderer renderer = (DefaultTableCellRenderer)
	            table.getTableHeader().getDefaultRenderer();
	        renderer.setHorizontalAlignment(JLabel.CENTER);
		table.setAutoCreateRowSorter(true);
		table.setShowVerticalLines(true);
		JScrollPane scrollpane = new JScrollPane(table);
		JPanel panTable = new JPanel();
		panTable.setLayout(new BoxLayout(panTable, BoxLayout.X_AXIS));
		panTable.add(scrollpane);
		
		panSim.add(panResume);
		panSim.add(panTable);
		panSim.setPreferredSize(new Dimension((int)tabbedpane.getTabSize().getWidth(), (int)tabbedpane.getTabSize().getHeight()*2/3));
//		panSim.add(new JLabel("Proportion of traces evaluated to \"true\" : "+ model.getPercentageTracesEvaluatedTo(true)+"%"));
		
		
		JPanel panCharts = new JPanel();
		panCharts.setLayout(new BoxLayout(panCharts, BoxLayout.X_AXIS));
		TitledBorder title6 = BorderFactory.createTitledBorder("Visualisation");
		title6.setTitleJustification(TitledBorder.CENTER);
		panCharts.setBorder(title6);
		
		LineBarVerdict linebar1 = new LineBarVerdict(model, simulator.getSmc_type().equals("Probability estimation"));
		LineExeTime linebar2 = new LineExeTime(model);
		panCharts.add(linebar1.getChartPanel());
		panCharts.add(linebar2.getChartPanel());
		panCharts.setPreferredSize(new Dimension((int)tabbedpane.getTabSize().getWidth()*85/100, (int)tabbedpane.getTabSize().getHeight()*2/3));
		
		
		
		
		
		JPanel panControl = new JPanel();
		panControl.setLayout(new FlowLayout());
		TitledBorder title2 = BorderFactory.createTitledBorder("Control");
		title2.setTitleJustification(TitledBorder.CENTER);
		panControl.setBorder(title2);
		
		JButton closeBut = new JButton("Close simulation");
		closeBut.setBounds(0, 0, 300, 50);
		panControl.add(closeBut);
		closeBut.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				
				freeMemory();		
				
			}
		
			
		});
		
		
//		table.addMouseListener(new java.awt.event.MouseAdapter() {
//		    boolean clicked = false;
//		    public void mouseMoved(java.awt.event.MouseEvent evt){
//		    	clicked = false;
//		    }
//		    
//		    public void mouseClicked(java.awt.event.MouseEvent evt) {
//		        int row = table.rowAtPoint(evt.getPoint());
//		        int col = table.columnAtPoint(evt.getPoint());
//		        if (row >= 0 && col >= 0) {
//		        	if(clicked)
//		            {
//		        		
//		        	    TraceViewer tv = new TraceViewer(model.getRow(row), simulator.isSave_traces());        		
//						clicked = false;
//		            }			        	
//		        	else{
//		        		clicked = true;
//		        	}
//		        }
//		    }
//		});
		
		
		add(panSim);
		add(panCharts);
		add(panControl);
		
	}


}
