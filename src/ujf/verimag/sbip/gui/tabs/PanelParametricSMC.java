package ujf.verimag.sbip.gui.tabs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.CancellationException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingWorker;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import ujf.verimag.sbip.analysis.LtlSimulator;
import ujf.verimag.sbip.analysis.MtlSimulator;
import ujf.verimag.sbip.analysis.OrderRelation;
import ujf.verimag.sbip.analysis.Simulator;
import ujf.verimag.sbip.bipInterface.trace.StateVariable;
import ujf.verimag.sbip.gui.TabbedPane;
import ujf.verimag.sbip.gui.dialogs.ProgressionViewer;
import ujf.verimag.sbip.parser.formula.Interval;

public class PanelParametricSMC extends JPanel {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TabbedPane tabbedpane;
	private Simulator simulator;
	private JComboBox<String> comboSMC;
	private JComboBox<String> comboBIP;
	private JSpinner slimit;
	private JLabel error;
	private JComboBox<String> comboSIM;
	private JCheckBox logTrace;
	private JCheckBox saveTrace;
	private ProgressionViewer pviewer;
	private SwingWorker<?, ?> worker = null;
	
	
	public PanelParametricSMC(TabbedPane tp){
		this.tabbedpane = tp;
		getPanParamSMC();
	}
	
	
	@SuppressWarnings("rawtypes")
	private void getPanParamSMC() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setBorder(new EmptyBorder(20,20,20,20));
		
		JPanel panSim = new JPanel();
		panSim.setLayout(new BoxLayout(panSim, BoxLayout.Y_AXIS));
		TitledBorder title = BorderFactory.createTitledBorder("Simulation");
		title.setTitleJustification(TitledBorder.CENTER);
		panSim.setBorder(title);
		
	
		logTrace = new JCheckBox("Automatically log data variables in the traces (--log-variables)");
		logTrace.setBorder(new EmptyBorder(5,50,5,50));
		logTrace.setSelected(true);
//		logTrace.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				log_vars = logTrace.isSelected();
//				
//			}
//		});
		
		saveTrace = new JCheckBox("Save evaluated traces" );
		saveTrace.setBorder(new EmptyBorder(5,50,5,50));
		saveTrace.setSelected(false);
//		saveTrace.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				save_traces = saveTrace.isSelected();
//				
//			}
//		});
		
		
		JPanel panComboBip = new JPanel();
		panComboBip.setLayout(new BoxLayout(panComboBip, BoxLayout.X_AXIS));
		panComboBip.setBorder(new EmptyBorder(5,50,5,50));
		String[] bipType = new String[2];
		bipType[1] = "Stochastic RT-BIP"; bipType[0] = "Untimed BIP"; //bipType[1] = "RT-BIP"; 
		comboBIP = new JComboBox<String>(bipType);
		comboBIP.setSelectedIndex(1);
//		panComboBip.add(new JLabel("Select BIP engine : "));
//		panComboBip.add(comboBIP);
		
		
		JPanel panComboSIM = new JPanel();
		panComboSIM.setLayout(new BoxLayout(panComboSIM, BoxLayout.X_AXIS));
		panComboSIM.setBorder(new EmptyBorder(5,50,5,50));
		String[] SIMType = new String[2];
		SIMType[0] = "Generate all the trace"; SIMType[1] = "Generate symbol per symbol"; 
		comboSIM = new JComboBox<String>(SIMType);
		comboSIM.setSelectedIndex(0);
		panComboSIM.add(new JLabel("Select simulation mode : "));
		panComboSIM.add(comboSIM);
		
		
		JPanel panLimit = new JPanel();
		panLimit.setLayout(new BoxLayout(panLimit, BoxLayout.X_AXIS));
		SpinnerModel model1 = new SpinnerNumberModel(1000, 1, Integer.MAX_VALUE, 1);
	    slimit = new JSpinner(model1);
//	    slimit.addChangeListener(new ChangeListener() {
//			public void stateChanged(ChangeEvent e) {
//				limit = (int) ( slimit.getValue());					
//			}
//		});
		panLimit.add(new JLabel("Select the length of the traces (--limit) : "));
		panLimit.add(slimit);
		panLimit.setBorder(new EmptyBorder(5,50,5,50));
		
		
		
		panSim.add(logTrace);
//		panSim.add(saveTrace);
		panSim.add(panComboBip);
		panSim.add(panComboSIM);
		panSim.add(panLimit);
		
		
		
		
		JPanel panCombo = new JPanel();
		panCombo.setLayout(new BoxLayout(panCombo, BoxLayout.X_AXIS));
		panCombo.setBorder(new EmptyBorder(20,0,20,0));
		String[] smcType = new String[3];
		smcType[0] = ""; smcType[1] = "Hypothesis testing"; smcType[2] = "Probability estimation";
		comboSMC = new JComboBox<String>(smcType);
		panCombo.add(new JLabel("Select the SMC type : "));
		panCombo.add(comboSMC);
		
		
		JPanel panSMC = new JPanel();
		panSMC.setLayout(new BoxLayout(panSMC, BoxLayout.Y_AXIS));
		TitledBorder title2 = BorderFactory.createTitledBorder("Statistical Model Checking");
		title2.setTitleJustification(TitledBorder.CENTER);
		panSMC.setBorder(title2);
		
		JPanel panAlpha = new JPanel();
		panAlpha.setLayout(new BoxLayout(panAlpha, BoxLayout.X_AXIS));
		SpinnerModel model2 = new SpinnerNumberModel(0.1, 0, 1, 0.1);
	    final JSpinner Alpha = new JSpinner(model2);
	    Alpha.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				adaptEditor(Alpha);	
				Alpha.requestFocus();
			}
		});
	    Alpha.setEnabled(false);
		panAlpha.add(new JLabel("Alpha : "));
		panAlpha.add(Alpha);
		panAlpha.setBorder(new EmptyBorder(5,50,5,50));
		
		JPanel panBeta = new JPanel();
		panBeta.setLayout(new BoxLayout(panBeta, BoxLayout.X_AXIS));
		SpinnerModel model3 = new SpinnerNumberModel(0.1, 0, 1, 0.1);
	    final JSpinner Beta = new JSpinner(model3);
	    Beta.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				adaptEditor(Beta);	
				Beta.requestFocus();
			}
		});
	    Beta.setEnabled(false);
		panBeta.add(new JLabel("Beta  : "));
		panBeta.add(Beta);
		panBeta.setBorder(new EmptyBorder(5,50,5,50));
		
		JPanel panDelta = new JPanel();
		panDelta.setLayout(new BoxLayout(panDelta, BoxLayout.X_AXIS));
		SpinnerModel model4 = new SpinnerNumberModel(0.1, 0, 1, 0.1);
	    final JSpinner Delta = new JSpinner(model4);
	    Delta.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				adaptEditor(Delta);	
				Delta.requestFocus();
			}
		});
	    Delta.setEnabled(false);
		panDelta.add(new JLabel("Delta : "));
		panDelta.add(Delta);
		panDelta.setBorder(new EmptyBorder(5,50,5,50));
		
		JPanel panTeta = new JPanel();
		panTeta.setBorder(new EmptyBorder(5,50,5,50));		
		panTeta.setLayout(new BoxLayout(panTeta, BoxLayout.X_AXIS));
		String[] relationOrder = new String[2];
		relationOrder[0] = ">=";
		relationOrder[1] = "<=";
		final JComboBox<String> comboRelation = new JComboBox<String>(relationOrder);
		SpinnerModel model8 = new SpinnerNumberModel(0.8, 0, 1, 0.1);
	    final JSpinner teta = new JSpinner(model8);
	    teta.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				adaptEditor(teta);	
				teta.requestFocus();
			}
		});
//	    final JLabel property = new JLabel("P[==0.5]{ Phi }");
	    
	    panTeta.add(new JLabel("Relation order : "));
	    panTeta.add(comboRelation);
	    panTeta.add(new JLabel(" Teta : "));
	    panTeta.add(teta);
	    teta.setEnabled(false);
		comboRelation.setEnabled(false);
		
//	    panTeta.add(property);
		
		panSMC.add(panAlpha);
		panSMC.add(panBeta);
		panSMC.add(panDelta);
		panSMC.add(panTeta);
		
		JPanel panParamVariable = new JPanel();
		panParamVariable.setBorder(new EmptyBorder(5,50,5,50));				
		panParamVariable.setLayout(new BoxLayout(panParamVariable, BoxLayout.X_AXIS));
		String[] varType = new String[2];
		varType[0] = "int";
		varType[1] = "bool";
		final JTextField varName = new JTextField();
		varName.setText("x");
//		varName.setColumns(10);
		@SuppressWarnings("unchecked")
		final JComboBox jVarType = new JComboBox(varType);
//		SpinnerModel model5 = new SpinnerNumberModel(0, Integer.MIN_VALUE, Integer.MAX_VALUE, 1);
		SpinnerModel model5 = new SpinnerNumberModel(0, Integer.MIN_VALUE, Integer.MAX_VALUE, 1);
		 final JSpinner initiaValue = new JSpinner(model5);
		 
	    SpinnerModel model6 = new SpinnerNumberModel(20, Integer.MIN_VALUE, Integer.MAX_VALUE, 1);
	    final JSpinner finalValue = new JSpinner(model6);
	    SpinnerModel model7 = new SpinnerNumberModel(5, Integer.MIN_VALUE, Integer.MAX_VALUE, 1);
	    final JSpinner step = new JSpinner(model7);
	    jVarType.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				int i = ((JComboBox) e.getSource()).getSelectedIndex();
				if(i == 1){
					initiaValue.setEnabled(false);
					finalValue.setEnabled(false);
					step.setEnabled(false);
				}
				else{
					initiaValue.setEnabled(true);
					finalValue.setEnabled(true);
					step.setEnabled(true);
				}
				
			}
		});
	    
	    
	    
	    panParamVariable.add(new JLabel("Var name : "));
	    panParamVariable.add(varName);
	    panParamVariable.add(new JLabel(" Type : "));
	    panParamVariable.add(jVarType);
	    panParamVariable.add(new JLabel(" Initial : "));
	    panParamVariable.add(initiaValue);
	    panParamVariable.add(new JLabel(" Final : "));
	    panParamVariable.add(finalValue);
	    panParamVariable.add(new JLabel(" Step : "));
	    panParamVariable.add(step);

	    

		TitledBorder title3 = BorderFactory.createTitledBorder("Parametric Variable");
		title3.setTitleJustification(TitledBorder.CENTER);
		JPanel panVar = new JPanel();
		panVar.setLayout(new BoxLayout(panVar, BoxLayout.Y_AXIS));	
//		panVar.setMaximumSize(new Dimension(3000, 500));
		panVar.setBorder(title3);
		panVar.add(panParamVariable);

		panVar.setMaximumSize(new Dimension(Integer.MAX_VALUE, panVar.getPreferredSize().height));
		
		JPanel panButtons = new JPanel();
		panButtons.setLayout(new FlowLayout());
		final JButton butRun = new JButton("Start simulation");
		butRun.setBounds(0, 0, 300, 50);
		butRun.setEnabled(false);
		final JButton closeTab = new JButton("Close tab");
		closeTab.setBounds(0, 0, 300, 50);
		closeTab.setEnabled(true);
//		panButtons.add(closeTab);
		panButtons.add(butRun);
		
		error = new JLabel("  ");
		JPanel panError = new JPanel(new FlowLayout());
		panError.setBorder(new EmptyBorder(5,5,20,5));
		panError.add(error);
		
		
		add(panSim);
		add(panCombo);
		add(panSMC);
		add(new JLabel(" "));
		add(panVar);
		add(panButtons);
		add(panError);
		
		comboSMC.addActionListener(new ActionListener() {				
			

			public void actionPerformed(ActionEvent arg0) {					
				int i = comboSMC.getSelectedIndex();
				if(i == 0){
//					smc_type = "";
					Alpha.setEnabled(false);
					Beta.setEnabled(false);
					Delta.setEnabled(false);
					teta.setEnabled(false);
//					comboRelation.setEnabled(false);
					butRun.setEnabled(false);
				}
				else
					if(i == 1){
//						smc_type = "Hypothesis testing";
						Alpha.setEnabled(true);
						Beta.setEnabled(true);
						Delta.setEnabled(true);
						teta.setEnabled(true);
//						comboRelation.setEnabled(true);
						butRun.setEnabled(true);
					}
					else
						if(i == 2){
//							smc_type = "Probability estimation";
							Alpha.setEnabled(true);
							Beta.setEnabled(false);
							Delta.setEnabled(true);
							teta.setEnabled(false);
//							comboRelation.setEnabled(false);
							butRun.setEnabled(true);
						}
			}
		});
			
		comboSIM.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				int i = comboSIM.getSelectedIndex();
				if(i == 0){
//					sim_type = "trace";
//					saveTrace.setEnabled(true);
					slimit.setEnabled(true);
				}
				else
				{
//					sim_type = "symbol";
//					saveTrace.setSelected(true);
//					saveTrace.setEnabled(false);
					slimit.setEnabled(false);
//					save_traces = true;
				}
			}
				
		});
		
		final PanelParametricSMC thisPan = this;		
		
		butRun.addActionListener(new ActionListener() {
			
			
			public void actionPerformed(ActionEvent e) {
				
					
				error.setForeground(Color.BLUE);						
				error.setText(" Simulation in progress ...");
				
				if(tabbedpane.hasSelectedModel() == 0){
					if(tabbedpane.hasSelectedFormula() == 0){
						
						double a = Interval.round( (double) Alpha.getValue() , 15);
						double b = Interval.round( (double) Beta.getValue() , 15);
						double d = Interval.round( (double) Delta.getValue() , 15);
						double diff = Interval.round( 1-b , 15);
						double t = Interval.round( (double) teta.getValue() , 15);
						double p0 = t + d;
						double p1 = t - d;
						
						if( (p0 > 1 || p0 < 0 || p1 > 1 || p1 < 0) && comboSMC.getSelectedIndex()== 1)
						{
							error.setForeground(Color.RED);
							error.setText("Simulation failed because of wrong parameter choice : Delta and Teta");
							JOptionPane.showConfirmDialog(null, "Error : Confidence interval ("+p1+","+p0+") bounds are incorrects. \n Values should not be >1 OR <0." , "Error",  JOptionPane.CANCEL_OPTION);
						}
						else	{
								
						if(a == diff){
							error.setForeground(Color.RED);
							error.setText("Simulation failed because of wrong parameter choice : ( Alpha = 1-Beta )");
							JOptionPane.showMessageDialog(null, "Simulation failed because of wrong parameter choice : ( Alpha = 1-Beta )", "Simulation failure", JOptionPane.ERROR_MESSAGE);
						}
						else{
							if(d == 0){
								error.setForeground(Color.RED);
								error.setText("Simulation failed because of wrong parameter choice : ( Delta = 0 )");
								JOptionPane.showMessageDialog(null, "Simulation failed because of wrong parameter choice : ( Delta = 0 )", "Simulation failure", JOptionPane.ERROR_MESSAGE);
							}
							else{
								
								
								pviewer = new ProgressionViewer(thisPan , getNbSims());
								StateVariable paramVar = new StateVariable(varName.getText(), 
										String.valueOf( initiaValue.getValue()), 
										(String) jVarType.getSelectedItem());
								pviewer.updateLog("Simulating with param variable : "+paramVar+"\n");
								
								if(tabbedpane.getFormulaType().equals("MTL")) {
								simulator = new MtlSimulator( 
										tabbedpane.getSelectedProject(),
										tabbedpane.getSelectedModel(),
										tabbedpane.getMtlFormula(),
										logTrace.isSelected(), 
										saveTrace.isSelected(), 
										comboBIP.getSelectedIndex(),
										comboSIM.getSelectedIndex(),
										(int) slimit.getValue(),
										comboSMC.getSelectedIndex(),
										Interval.round( (double) Alpha.getValue() , 15),
										Interval.round( (double) Beta.getValue() , 15),
										Interval.round( (double) Delta.getValue() ,15), 
										Interval.round( (double) teta.getValue() , 15), 
										OrderRelation.fromString((String) comboRelation.getSelectedItem()),
										pviewer, paramVar,
										tabbedpane
									);		
								
								}
								else 
									if(tabbedpane.getFormulaType().equals("LTL")) {
										simulator = new LtlSimulator( 
												tabbedpane.getSelectedProject(),
												tabbedpane.getSelectedModel(),
												tabbedpane.getLtlFormula(),
												logTrace.isSelected(), 
												saveTrace.isSelected(), 
												comboBIP.getSelectedIndex(),
												comboSIM.getSelectedIndex(),
												(int) slimit.getValue(),
												comboSMC.getSelectedIndex(),
												Interval.round( (double) Alpha.getValue() , 15),
												Interval.round( (double) Beta.getValue() , 15),
												Interval.round( (double) Delta.getValue() ,15), 
												Interval.round( (double) teta.getValue() , 15), 
												OrderRelation.fromString((String) comboRelation.getSelectedItem()),
												pviewer, paramVar,
												tabbedpane
											);	
									}
								
								
								   	worker = new SwingWorker() {
								          protected Object doInBackground() throws Exception {
								        	  if(jVarType.getSelectedIndex() == 0){
								        		  int i = (int) initiaValue.getValue();
								        		  while(i < (int) finalValue.getValue()){
								        			  simulator.runSimulation(String.valueOf(i));
								        			  i += (int) step.getValue();
								        			  pviewer.incrementProgressBar();
								        		  }
								        		  i = (int) finalValue.getValue(); 
								        		  simulator.runSimulation(String.valueOf(i));	
							        			  pviewer.incrementProgressBar();
								        	  }
								        	  else{
								        		  simulator.runSimulation("false");
							        			  pviewer.incrementProgressBar();
								        		  simulator.runSimulation("true");	
							        			  pviewer.incrementProgressBar();					        		  
								        	  }
								        	  
								        	  pviewer.stopSimulation();
//								  			  System.out.println("Done parametric");						
												updateTabbedPane((int)initiaValue.getValue(), (int)finalValue.getValue(), (int)step.getValue());
												return null;
								          }				
								          
								          protected void done() {
								        	   try {
								        		   simulator.abort();
//								        		   System.out.println("!!Done!!");
								        		   thisPan.error.setText(" ");
								        	   } catch (CancellationException e) {
								        	       // Do your task after cancellation
								        	   }
								          }
								    };
								    worker.execute();
							}	   					
						}
							
						}
					}
					else{
						error.setForeground(Color.RED);
						if(tabbedpane.hasSelectedFormula() == -1)
						{
							error.setText("Simulation failed : no formula has been selected!");
							JOptionPane.showMessageDialog(null, "Simulation failed : no formula has been selected!", "Simulation failure", JOptionPane.ERROR_MESSAGE);	
						}
						else{
							error.setText("Simulation failed : the formula has been selected in a different project!");

							JOptionPane.showMessageDialog(null, "Simulation failed : the formula has been selected in a different project!", "Simulation failure", JOptionPane.ERROR_MESSAGE);	
						}
					}
				}
				else{
					error.setForeground(Color.RED);
					if(tabbedpane.hasSelectedModel() == -1){
						error.setText("Simulation failed : no model has been selected!");
					JOptionPane.showMessageDialog(null, "Simulation failed : no model has been selected!", "Simulation failure", JOptionPane.ERROR_MESSAGE);	
				}
					else{
						error.setText("Simulation failed : the model has been selected in a different project!");
						JOptionPane.showMessageDialog(null, "Simulation failed : the model has been selected in a different project!", "Simulation failure", JOptionPane.ERROR_MESSAGE);	
					}
				}
			
				
			}

			private int getNbSims() {
				if(jVarType.getSelectedIndex() == 1) return 2;
				else{
					int nb = ((((int)finalValue.getValue()) - ((int) initiaValue.getValue()))/  ((int) step.getValue())) + 1;
					if((((int)finalValue.getValue()) - ((int) initiaValue.getValue())) %  ((int) step.getValue()) > 0) nb ++;
					return nb;
				}
			}
		});
		
		closeTab.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				tabbedpane.removeTabAt(tabbedpane.getSelectedIndex());
			}
		});
		
		
		
	}


	protected void adaptEditor(JSpinner Alpha) {
		String zeroString = "0.";
		String s = Double.toString(Interval.round((double)Alpha.getValue()+0.1, 15));
		if((double)Alpha.getValue() != 1)
		
			s =		s.substring(2, s.length());					
			
			for (int i = 0; i < s.length(); i++) zeroString+="0";
//			System.out.println(s +"=>"+zeroString);
			Alpha.setEditor(new JSpinner.NumberEditor(Alpha, zeroString));
		
	
	}


	public void updateTabbedPane(int init_v, int final_v, int step) {
		
		for (int i = 0; i < tabbedpane.getTabCount(); i++) {
			tabbedpane.setEnabledAt(i, false);
		}
		JComponent panSim = new PanelParamSmcResult(tabbedpane, simulator, init_v, final_v, step);
//		panSim.setPreferredSize(this.getPreferredSize());
		tabbedpane.addTab("     Parametric SMC results     ", new JScrollPane(panSim));					
		tabbedpane.setSelectedIndex(tabbedpane.getTabCount()-1);
		tabbedpane.resizeText();
		error.setText(" ");
	}
	
	public void enableComboEngine(boolean b) {
		comboBIP.setEnabled(b);
		if(!b) comboBIP.setSelectedIndex(1);
	}

	public void enableLimitParam(boolean b) {
		slimit.setEnabled(b);
	}

	public void cleanError() {
		error.setText(" ");		
	}

	public void abortExecution(){
		if(worker != null)
			if(!worker.isDone()) worker.cancel(true);
	}

	
	
}

