package ujf.verimag.sbip.gui.tabs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileSystemView;
import javax.swing.filechooser.FileView;

import ujf.verimag.sbip.gui.TabbedPane;

public class PanelModel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	TabbedPane tabbedpane ;
	private JTextField selectedM;
	
	private String selectedModel = "";
	private String inProject = "";
	private JLabel labelError;
	
	

	public PanelModel(TabbedPane tp) throws IOException{
		this.tabbedpane = tp;
		getPanModel();
	}
	
	@SuppressWarnings("resource")
	private void copyFileUsingChannel(File source, File dest) throws IOException {
	    FileChannel sourceChannel = null;
	    FileChannel destChannel = null;
	    try {
	        sourceChannel = new FileInputStream(source).getChannel();
	        destChannel = new FileOutputStream(dest).getChannel();
	        destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
	       }finally{
	           sourceChannel.close();
	           destChannel.close();
	   }
	}
	
	public String getSelectedModel(){
		return selectedModel;
	}
	
	public String getProject(){
		return inProject;
	}
	
	public void setSelectedModel(String s){
		selectedModel = s;
		selectedM.setText(s);
		inProject = tabbedpane.getSelectedProject();
	}
	
	private void getPanModel() throws IOException 
	{
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setBorder(new EmptyBorder(20,20,20,20));
		
//		final JFileChooser chooser = new JFileChooser();	
		
		JPanel selectedP = new JPanel();
		selectedP.setLayout(new BoxLayout(selectedP, BoxLayout.X_AXIS));
		selectedP.setBorder(new EmptyBorder(5,5,10,10));
		
		selectedP.add(new JLabel ("Selected Model : "));
		
		
		selectedM = new JTextField();
		selectedM.setEditable(false);
		if(selectedModel.equals("")) selectedM.setText("( NONE )");
		else selectedM.setText(selectedModel);
		
		
		
		selectedP.add(selectedM);
		JPanel errorP = new JPanel();
		errorP.setBorder(new EmptyBorder(10,10,20,10));
		labelError = new JLabel(" ");
		errorP.add(labelError);
		

		Boolean old = UIManager.getBoolean("FileChooser.readOnly");  
		  UIManager.put("FileChooser.readOnly", Boolean.TRUE);  
		  final JFileChooser chooser = new JFileChooser("."); 
		  chooser.setFileView(new CustomFileViewer() );
		  
		  UIManager.put("FileChooser.readOnly", old);
		chooser.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				if(tabbedpane.getSelectedProject().equals("") ) {
					labelError.setForeground(Color.RED);
					labelError.setText("Importation failed : select/create a project first!");
				}
				else
				if(e.getActionCommand() == JFileChooser.APPROVE_SELECTION)
				{
					File f = chooser.getSelectedFile();
					System.out.println(f.getName());
					String s = f.getName();
					String[] split = s.split("\\.");
					if(split.length == 1 || s.endsWith(".bip") || s.endsWith(".cpp") || s.endsWith(".hpp") || s.endsWith(".c") || s.endsWith(".h") || s.endsWith(".txt")){
						String prefix = "/";
						if(s.endsWith(".cpp") || s.endsWith(".hpp") || s.endsWith(".c") || s.endsWith(".h")) {
							prefix = "/ext-cpp/";
							File f2 = new File("./Workspace/"+tabbedpane.getSelectedProject()+"/Models"+prefix);
							if(!f2.exists()){
								f2.mkdir();
								tabbedpane.insertNodeInTree(f2.getAbsolutePath());
								
							}
						}
						
						
						File localCopy = new File("./Workspace/"+ tabbedpane.getSelectedProject() +"/Models"+prefix+f.getName());
						System.out.println(localCopy.getAbsolutePath());
						try {
							if(localCopy.exists()){
								labelError.setForeground(Color.RED);
								labelError.setText("Importation failed : File \""+f.getName()+"\" already exists in the current project!");
							}
							else{
								localCopy.createNewFile();
								if(split.length == 1){
									localCopy.setExecutable(true);
								}
									copyFileUsingChannel(f, localCopy);
									selectedModel = localCopy.getAbsolutePath();
									selectedM.setText(selectedModel);
									
								inProject = tabbedpane.getSelectedProject();
								tabbedpane.insertNodeInTree(localCopy.getAbsolutePath());
								labelError.setForeground(Color.BLUE);
								labelError.setText("File \""+f.getName()+"\" has been successfully imported!");
							}
							
							
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					}
					else{
						labelError.setForeground(Color.RED);
						labelError.setText("Importation failed : the file should be a model file!");
					}
					
				}
			
				
				
			}
		});
		
		JPanel panMid = new JPanel();
		TitledBorder title = BorderFactory.createTitledBorder("Import a model");
		title.setTitleJustification(TitledBorder.CENTER);
		panMid.setBorder(title);
		panMid.setLayout(new BoxLayout(panMid, BoxLayout.PAGE_AXIS));
		panMid.add(chooser);
		
		JPanel panBot = new JPanel();
		TitledBorder title2 = BorderFactory.createTitledBorder("Selected model");
		title2.setTitleJustification(TitledBorder.CENTER);
		panBot.setBorder(title2);
		panBot.setLayout(new BoxLayout(panBot, BoxLayout.PAGE_AXIS));
		panBot.add(selectedP);
		panBot.add(errorP);
		panBot.setMaximumSize(new Dimension(Integer.MAX_VALUE, panBot.getPreferredSize().height));
		
		
		
		
        this.add(panMid);
        this.add(new JLabel(" "));
        this.add(panBot);
		
        
	}

	public void setProject(String project) {
		inProject = project;
		
	}

	public void reset() {
		inProject = "";
		selectedModel = "";
		selectedM.setText("( NONE )");
		labelError.setText(" ");
	}
	
	
	private class CustomFileViewer extends FileView{
		
		public Icon getIcon(File file)
		   {
			 if(file!= null)
			        if(file.isDirectory())
			        	return (new ImageIcon("./Icons8/icons8-Open-20.png"));
			        else if(file.getName().endsWith(".mtl") || file.getName().endsWith(".ltl"))
			        	return (new ImageIcon("./Icons8/icons8-Philosophy Filled.png"));
			        else if(file.getName().endsWith(".bip"))
			        	return (new ImageIcon("./Icons8/icons8-Genealogy-15.png"));
			        else if(file.getName().endsWith(".t"))
			        	return (new ImageIcon("./Icons8/icons8-Omega-1.png"));
			        else if(file.getName().endsWith(".cpp") || file.getName().endsWith(".c"))
			        	return (new ImageIcon("./Icons8/cpp.png")); 
			        else if(file.getName().endsWith(".hpp") || file.getName().endsWith(".h"))
			        	return (new ImageIcon("./Icons8/hpp.png"));
			        else if(file.getName().endsWith(".txt"))
			        	return (new ImageIcon("./Icons8/txt.png"));
			        else if(!file.getName().contains(".") && file.canExecute())
			        	return (new ImageIcon("./Icons8/icons8-Console-1.png"));
			        else
			        	return (FileSystemView.getFileSystemView().getSystemIcon(file));
			 
			 return null;
		   }
		
	}
}


