package ujf.verimag.sbip.test;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import ujf.verimag.sbip.parser.ltl.BoundedLtlLexer;
import ujf.verimag.sbip.parser.ltl.BoundedLtlParser;
import ujf.verimag.sbip.parser.formula.*;

public class TestLtlGrammarWithEmbeddedCode {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String testInput="";
		
		/** Testing unary_bounded_op **/
		testInput = "U{10}";
		testInput = "R{100}";
		
		/** Testing binary_bounded_op **/
		testInput = "F{5800}";
		testInput = "G{500}";
		testInput = "N{200}";
		
		/** Testing terminal_state_formula**/
		testInput = "x";									// an ID
		testInput = "!a";									// NOT ID
		testInput = "true";									// a truth value
		testInput = "false";								// a truth value
		testInput = "x > y";								// rel_expr without parenthesis
		testInput = "(a+2)>8";								// rel_expr with parenthesis
		testInput = "a >= b";

		/** Testing Boolean expressions **/
//		testInput = "(a >= b) && c";						// conjunction of rel_expr and ID
//		testInput = "(a >= b) && !c";						// conjunction of rel_expr and NOT ID
//		testInput = "(a >= b) && (c == 5)";					// conjunction of rel_exprs
//		testInput = "(a >= b) && true";						// conjunction of rel_expr and a TRUTH_VAL
//		testInput = "(a >= b) && ((x != y) || (z < 5))"; 	// nested bool_atoms with good parentheses
//		testInput = "(a >= b) && ((x != y) || (z < 5) || w)"; 	// nested bool_atoms with good parenthesis
//		testInput = "(a >= b) && ((x != y) || (z < 5) || (w))"; 	// nested bool_atoms with good parenthesis
//		testInput = "(a >= b) && ((x != y) || (z < 5)) && w"; 	// nested bool_atoms with good parenthesis
//		
		/** Test cases for pbltl_formula rule**/
//		testInput = "P>=0.8[N{10} x]";		//HT_OP with valid path_formula
//		testInput = "P<=0.052[N{5} x]";	//HT_OP with valid path_formula*/
//		testInput = "P>=0.25[G{1000}(abs((Master.tm-Slave.ts))<=50)]";
//		testInput = "P=?[y U{52}x]";		// state_formula binary_bounded_op state_formula
//		testInput = "P=?[G{5} x]";	//PESTIM_OP with valid path_formula
//		
		/**	test cases for embedding path formulae **/		
//		testInput = "P=?[y||z U{52}x]";				// this a bad case : without parentheses, the || op is seen as the root of the tree
//		testInput = "P=?[(y||z) U{52}x]";
//		testInput = "P=?[(y||z) U{52} (G{100} a)]";
////		testInput = "P=?[(y)||z U{52} (G{100} a)]";	// this a bad case : without parentheses, the || op is seen as the root of the tree
////		testInput = "P=?[a]";
////		testInput = "P=?[y || (z U{52} (G{100} a))]";
////		testInput = "P>=0.5[(a || N{100} c) U{52} b]";
////		testInput = "P=?[( G{4}(a) || (abs(x+5)<3) ) R{4} (b && c)]";
////		testInput = "P=?[( G{4}(a) || (abs(x+5*(8/2))<3) ) R{4} (b && c)]";
////		testInput = "P=?[G{4}(a) || (abs((x+5))<3)]";
//		testInput = "P=?[( G{4}((a-v)>50) || (abs(x+5*(8/2))<3) ) R{4} (b && c)]";
//		testInput = "P=?[G{4} a || (abs(x+5*(8/2))<3) R{4} (b && c)]";	// this a bad case : without parentheses, the || op is seen as the root of the tree
//		testInput = "P=?[G{4} a || b]";	// this a bad case : without parentheses, the || op is seen as the root of the tree
		
		/** Testing prob formula with bltl as a terminal state formula **/
		testInput = "P>=0.5[(a+2)>8]";
		
		/** Testing prob formula with bltl as a terminal state formula + parentheses **/
		testInput = "P>=0.5[((a+2)>8)]";
		
		/** Testing prob formula with bltl as a conjunction of terminal state formulas**/
		testInput = "P>=0.5[((a+2)>8) && b<3]";
		
		/** Testing prob formula with bltl as a composition of conjunctions and disjunction of terminal state formulas**/
		testInput = "P>=0.5[((a+2)>8) && b<3 || c==70]";
		
		/** Testing prob formula with bltl as a composition of conjunctions and disjunction (with parentheses) of terminal state formulas**/
		testInput = "P>=0.5[((a+2)>8) && (b<3 || c==70)]";
		
		/** Testing prob formula with bltl as a conjunction of terminal state formulas**/
		testInput = "P>=0.5[((a+2)>8) && b<3 && c == 70 && d >= 25]";
		
		/** Testing prob formula with bltl as a disjunction of terminal state formulas**/
		testInput = "P>=0.5[((a+2)>8) || b<3 || c == 70 || d >= 25]";
		
		/** Testing prob formula with bltl as a composition of unary temporal op and disjunction and conjunctions **/
		testInput = "P>=0.5[G{4}(a+2)>8 || b<3 && d >= 25]"; // "P>=0.5[G{4}((a+2)>8 || (b<3 && d >= 25))]"
		testInput = "P>=0.5[G{4}(a+2)>8 && b<3 || d >= 25]"; //	"P>=0.5[G{4}(((a+2)>8 && b<3) || d >= 25)]" 
		testInput = "P>=0.5[G{4}((a+2)>8 || b<3) && d >= 25]"; // "P>=0.5[G{4}(((a+2)>8 || b<3) && d >= 25)]"
		testInput = "P>=0.5[(G{4}(a+2)>8 || b<3) && d >= 25]"; //"P>=0.5[(G{4}((a+2)>8 || b<3)) && d >= 25]"
		testInput = "P>=0.5[(G{4}(a+2)>8) || b<3 && d >= 25]"; //"P>=0.5[(G{4}(a+2)>8) || (b<3 && d >= 25)]"
		testInput = "P>=0.5[b<3 || G{4}(a+2)>8 && d >= 25]"; //"P>=0.5[(b<3) || (G{4}(a+2)>8 && d >= 25)]"
		testInput = "P>=0.5[b<3 || (G{4}(a+2)>8) && d >= 25]"; //"P>=0.5[(b<3) || ((G{4}(a+2)>8) && (d >= 25))]"
		testInput = "P>=0.5[G{4}(a+2)>8]";
		
		/** Testing prob formula with bltl as a composition of unary/binary temporal ops and disjunction and conjunctions **/
		testInput = "P>=0.5[b<3 U{500} d>=25]";
		testInput = "P>=0.5[b<3 U{500} G{5} d>=25]"; // "P>=0.5[(b<3) U{500} (G{5} d>=25)]"
		testInput = "P>=0.5[b<3 U{500} G{5} d>=25 || a==3]"; //"P>=0.5[(b<3) U{500} (G{5} (d>=25 || a==3))]"
		testInput = "P>=0.5[a==3 || b<3 U{500} G{5} d>=25]"; //"P>=0.5[(a==3) || (b<3 U{500} (G{5} d>=25))]"
		testInput = "P>=0.5[(a==3 || b<3) U{500} G{5} d>=25]"; //"P>=0.5[(a==3 || b<3) U{500} (G{5} d>=25))]"
		
		testInput = "P=?[a>5 && b==3]";
		
		ANTLRInputStream input = new ANTLRInputStream(testInput);
		
		// create a lexer that feeds off of input CharStream
		BoundedLtlLexer lexer = new BoundedLtlLexer(input);
		
		// create a buffer of tokens pulled from the lexer
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		
		// create a parser that feeds off the tokens buffer
		BoundedLtlParser parser = new BoundedLtlParser(tokens);
		
		// disable the parse tree construction
		parser.setBuildParseTree(false);
		//parser.pbltl_formula();
		
		/** Testing binary_bounded_op **/
//		TemporalBoundedOperator tbo_b = (TemporalBoundedOperator)parser.binary_bounded_op().tbo; // begin parsing at given rule		
//		System.out.println(tbo_b.getName() + " " + tbo_b.getBound());
		
		
		/** Testing binary_bounded_op **/
//		TemporalBoundedOperator tbo_u = (TemporalBoundedOperator)parser.unary_bounded_op().tbo; // begin parsing at given rule		
//		System.out.println(tbo_u.getName() + " " + tbo_u.getBound());
		
		/** Testing terminal_state_formula**/
//		Formula sf = (LtlFormula)parser.terminal().f; // begin parsing at given rule		
//		if(sf.getLeftFormula() != null)
//			System.out.println("left = " + sf.getLeftFormula());
//		else
//			System.out.println("left = null");
//		
//		if(sf.getOperator() != null)
//			System.out.println("operator = " + sf.getOperator());
//		else
//			System.out.println("operator = null");
//		
//		if(sf.getRightFormula() != null)
//			System.out.println("right = " + sf.getRightFormula());
//		else
//			System.out.println("right = null");
		
		/** Testing prob formula **/
		ProbabilisticFormula pf = parser.pbltl().pf; // begin parsing at given rule
		System.out.println(pf.toString());
	}
}