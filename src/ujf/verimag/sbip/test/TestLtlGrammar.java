package ujf.verimag.sbip.test;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

import ujf.verimag.sbip.parser.ltl.*;

public class TestLtlGrammar {

	public static void main(String[] args)  throws Exception{
		
		String testInput=""; 
		
		/** Test cases for arith_expr rule **/
		/*
		// Rejected
		testInput = "";			// empty string
		testInput = "*8";		// missing a first operand
		testInput = "a-";		// missing a second operand
		testInput = "(x";		// missing a closing parenthesis
		testInput = "x)";		// missing an opening parenthesis
		testInput = "f(w)";		// an expression with a bad function name
		testInput = "abs()";	// an expression with a function without arguments
		testInput = "abs((z+))";// a function with a bad argument: missing second operand
		testInput = "abs(y";	// function missing outer most right parenthesis
		testInput = "abs((x-y)";// function missing inner right parenthesis
		testInput = "abs(x-y))";// function missing inner left parenthesis
		
		// Accepted
		testInput = "5";				// an ID
		testInput = "(5)";				// an (ID)
		testInput = "x";				// an INT
		testInput = "(x)";				// an (INT)
		testInput = "a/b";				// element ARITH_OP = {+,-,/,*,%} element
		testInput = "(3 + y)"; 			// (element ARITH_OP = {+,-,/,*,%} element)
		testInput = "a + b - c";		// element ARITH_OP = {+,-,/,*,%} element ARITH_OP = {+,-,/,*,%} element
		testInput = "a + (b-c)";		// element ARITH_OP = {+,-,/,*,%} (element ARITH_OP element)
		testInput = "(a + b) - c";		// (element ARITH_OP element) ARITH_OP = {+,-,/,*,%} element
		testInput = "(a + b) - c / d";	// (element ARITH_OP element) ARITH_OP = {+,-,/,*,%} element ARITH_OP element
		testInput = "(a + b) - (c / d)";// (element ARITH_OP element) ARITH_OP = {+,-,/,*,%} (element ARITH_OP element)
		testInput = "(a + (3 - y) / 5)";// nested arith_expr
		testInput = "abs(x)";			// function with an element
		testInput = "abs((x-y))";		// function with an arith_expr
		testInput = "abs((a + (b/3)))";	// function with an element ARITH_OP arith_expr
		testInput = "abs((a + b / 3))";	// function with an element ARITH_OP element ARITH_OP element
		*/
		
		/** Test cases for rel_expr rule **/
		/*
		//rejected
		testInput = "";				// empty string
		testInput = "(";			// a left parenthesis
		testInput = "x";			// a single element
		testInput = "a-2";			// a single arith_expr
		testInput = "abs((x-y))";	// a single arith_expr (with function)
		testInput = "A > sabs((x-y))";//element REL_OP arith_expr (bad function name)
		
		//accepted
		testInput = "a > b";		// element REL_OP element
		testInput = "a != c";		// element REL_OP element
		testInput = "w == x";		// element REL_OP element
		testInput = "(a+2) < 5";	// arith_expr REL_OP element
		testInput = "(w/5) <= x+y"; // arith_expr REL_OP arith_expr
		testInput = "x%3 >= (a-c)";	// arith_expr REL_OP arith_expr
		testInput = "abs((x-y)) > 50";//arith_expr (function) REL_OP element
		testInput = "A > abs((x-y))";//element REL_OP arith_expr (function)
		*/
		
		/** Test cases for bool_expr rule**/
		/*
		//rejected 
		testInput = "";			// an empty string
		testInput = "50";		// an INT
		testInput = "x > y";	// a rel_expr without parentheses
		testInput = "(a+2)>8";	// missing outer most parentheses
		testInput = "(a >= b) && (x != y || z < 5)"; 		// nested bool_atoms with bad parentheses
		testInput = "(a >= b) && ((x != y || (z < 5))"; 	// nested bool_atoms with bad parentheses
		testInput = "(a >= b) && ((x != y) || (z < 5)"; 	// nested bool_atoms with bad parentheses
		testInput = "(a >= b) && ((x != y) || z < 5)"; 		// nested bool_atoms with bad parentheses
		testInput = "(a >= b) != ((x != y)"; 				// rel_exprs with bad BOOL_OP
		
		//accepted
		testInput = "x";									// an ID
		testInput = "(y)";									// an (ID)
		testInput = "!a";									// NOT ID
		testInput = "(!b)";									// (NOT ID)
		testInput = "true";									// a truth value
		testInput = "false";								// a truth value
		testInput = "(true)";								// (a truth value)
		testInput = "(x > y)";								// rel_expr with parenthesis
		testInput = "((a+2)>8)";							// rel_expr with parenthesis
		testInput = "(a >= b) && c";						// conjunction of rel_expr and ID
		testInput = "(a >= b) && !c";						// conjunction of rel_expr and NOT ID
		testInput = "(a >= b) && (c == 5)";					// conjunction of rel_exprs
		testInput = "(a >= b) && true";						// conjunction of rel_expr and a TRUTH_VAL
		testInput = "(a >= b) && ((x != y) || (z < 5))"; 	// nested bool_atoms with good parentheses
		testInput = "(a >= b) && ((x != y) || (z < 5) || w)"; 	// nested bool_atoms with good parenthesis
		testInput = "(a >= b) && ((x != y) || (z < 5) || (w))"; 	// nested bool_atoms with good parenthesis
		testInput = "(a >= b) && ((x != y) || (z < 5)) && w"; 	// nested bool_atoms with good parenthesis
		*/
		
		/** Test cases for state_formula rule**/
		/*
		//rejected 
		testInput = "";			// an empty string
		testInput = "50";		// an INT
		testInput = "x > y";	// a rel_expr without parentheses
		testInput = "(a+2)>8";	// missing outer most parentheses
		testInput = "(a >= b) && (x != y || z < 5)"; 		// nested bool_atoms with bad parentheses
		testInput = "(a >= b) && ((x != y || (z < 5))"; 	// nested bool_atoms with bad parentheses
		testInput = "(a >= b) && ((x != y) || (z < 5)"; 	// nested bool_atoms with bad parentheses
		testInput = "(a >= b) && ((x != y) || z < 5)"; 		// nested bool_atoms with bad parentheses
		testInput = "((a >= b) != (x != y))"; 				// rel_exprs with bad BOOL_OP

		//accepted
		testInput = "true";										// a truth value
		testInput = "false";									// a truth value
		testInput = "x";										// an ID
		testInput = "!w";										// NOT ID
		testInput = "(y)";										// an (ID)
		testInput = "(!b)";										// (NOT ID)
		testInput = "(true)";									// (a truth value)
		testInput = "(x > y)";									// rel_expr with parenthesis
		testInput = "((a+2)>8)";								// rel_expr with parenthesis
		testInput = "(a >= b) && c";							// conjunction of rel_expr and ID
		testInput = "(a >= b) && !c";							// conjunction of rel_expr and NOT ID
		testInput = "(a >= b) && (c == 5)";						// conjunction of rel_exprs
		testInput = "(a >= b) && true";							// conjunction of rel_expr and a TRUTH_VAL
		testInput = "(a >= b) && ((x != y) || (z < 5))"; 		// nested bool_atoms with good parentheses
		testInput = "(a >= b) && ((x != y) || (z < 5) || w)"; 	// nested bool_atoms with good parenthesis
		testInput = "(a >= b) && ((x != y) || (z < 5) || (w))"; // nested bool_atoms with good parenthesis
		testInput = "(a >= b) && ((x != y) || (z < 5)) && w"; 	// nested bool_atoms with good parenthesis	
		*/
		
		/** Test cases for path_formula rule**/
		/*
		//rejected 
		testInput = "";											// an empty string
		testInput = "50";										// an INT
		testInput = "true";										// a truth value
		testInput = "false";									// a truth value
		testInput = "(true)";									// (a truth value)
		testInput = "x";										// an ID
		testInput = "!w";										// NOT ID
		testInput = "(y)";										// an (ID)
		testInput = "(!b)";										// (NOT ID)
		testInput = "((a+2)>8)";								// a state formula
		testInput = "(a >= b) && c";							// a state formula		
		
		testInput = "x N w";									// using NEXT_OP as a binary_bounded_op
		testInput = "N{5}w";									// bounded NEXT_OP
		testInput = "N 5";										// NEXT_OP invalid state_formula
		testInput = "N (a && (x-!=3))";							// NEXT_OP invalid state_formula
		testInput = "N (a && (x-1 != 3 || w))";					// NEXT_OP invalid state_formula
		testInput = "N a>15";									// NEXT_OP invalid state_formula
		
		testInput = "G(x)";										// missing the bound of binary_bounded_op
		testInput = "G8 (x)";									// bas syntax of the bound of binary_bounded_op
		testInput = "a G{10} x";								// using unary_bounded_op as a binary_bounded_op
		testInput = "G{1000} 5";								// bounded_unary_op invalid state_formula
		testInput = "F{50} (a && (x-!=3))";						// bounded_unary_op invalid state_formula
		testInput = "F{350} (a && (x-1 != 3 || w))";			// bounded_unary_op invalid state_formula
		testInput = "G{2} a>15";								// bounded_unary_op invalid state_formula
		
		testInput = "U{1000}(x)";								// starting with a binary_bounded_op
		testInput = "y U x";									// missing the bound of binary_bounded_op
		testInput = "y U{52 x";									// bad syntax of the bound of binary_bounded_op
		testInput = "y U52} x";									// bad syntax of the bound of binary_bounded_op
		testInput = "y U(52) x";								// bad syntax of the bound of binary_bounded_op
		testInput = "y U[52] x";								// bad syntax of the bound of binary_bounded_op
		testInput = "y U{52} x<5";								// state_formula binary_bounded_op invalid state_formula (parentheses)
		testInput = "(y-2!=w) U{52} (x<a || b)";				// state_formula binary_bounded_op invalid state_formula (parentheses)
		testInput = "(y==2) && (x!=7 || b >= 9) U{52} (x<a)";	// invalid state_formula (parentheses) binary_bounded_op state_formula
		
		//accepted
		testInput = "N x";										// NEXT_OP state_formula
		testInput = "N(a>15)";									// NEXT_OP state_formula
		testInput = "N !y";										// NEXT_OP state_formula
		testInput = "N (a && (x<3))";							// NEXT_OP state_formula
		testInput = "N (a && (x-1 != 3))";						// NEXT_OP state_formula
		testInput = "N (a && ((x-1 != 3) || w))";				// NEXT_OP state_formula
		
		testInput = "G{30} x";									// bounded_unary_op state_formula
		testInput = "F{52}(a>15)";								// bounded_unary_op state_formula
		testInput = "G{840} !y";								// bounded_unary_op state_formula
		testInput = "G{600} (a && (x<3))";						// bounded_unary_op state_formula
		testInput = "F{58} (a && (x-1 != 3))";					// bounded_unary_op state_formula
		testInput = "F{254} (a && ((x-1 != 3) || w))";			// bounded_unary_op state_formula
		testInput = "F{254} (a && ((x-1 != 3) || w) && (y==5))";// bounded_unary_op state_formula
		
		testInput = "y U{52} x";								// state_formula binary_bounded_op state_formula
		testInput = "(y==3) U{52} x";							// state_formula binary_bounded_op state_formula
		testInput = "y U{52} (x<5)";							// state_formula binary_bounded_op state_formula
		testInput = "(y==w) U{52} (x<5)";						// state_formula binary_bounded_op state_formula
		testInput = "(y-2==w) U{52} (x<a)";						// state_formula binary_bounded_op state_formula
		testInput = "(y-2!=w) U{52} ((x<a) || b)";				// state_formula binary_bounded_op state_formula
		testInput = "(y==2) && (x!=7) U{52} ((x<a) || b)";		// state_formula binary_bounded_op state_formula
		testInput = "(y==2) && ((x!=7) || (b >= 9)) U{52} (x<a)";// state_formula binary_bounded_op state_formula
		*/
		
		/** Test cases for pbltl_formula rule**/
		//rejected
		testInput = "";				// empty string
		testInput = "P>[N x]";		// incomplete HT_OP
		testInput = "P>=[N y]";		// incomplete HT_OP
		testInput = "P>=0[N x]";	// bad PROBA
		testInput = "P>=1[N x]";	// bad PROBA
		testInput = "P>=0.[N x]";	// bad PROBA
		testInput = "P<=0.5";		// missing path formula
		testInput = "P<=0.5[]";		// missing path formula
		testInput = "P<=0.5 N x";	// missing '[' ']' around the path formula
		testInput = "P<=0.5 [N x";	// missing closing ']' around the path formula
		testInput = "P>=0.02 N x]";	// missing opening '[' around the path formula
		testInput = "P=[N w]";		// incomplete PESTIM_OP
		testInput = "P=![N w]";		// bad PESTIM_OP
		testInput = "P=?";			// missing path_formula
		testInput = "P=?[]";		// missing path_formula
		testInput = "P=? G{52} x";	// missing '[' ']' around the path_formula
		testInput = "P=? [G{52} x";	// missing closing ']' around the path_formula
		testInput = "P=? G{52} x]";	// missing opening '[' around the path_formula
		testInput = "P=?[yU{52}x]";	// state_formula binary_bounded_op state_formula

		//accepted
		testInput = "P>=0.8[N{50} x]";		//HT_OP with valid path_formula
		testInput = "P<=0.052[N{254} x]";	//HT_OP with valid path_formula*/
		testInput = "P>=0.25[G{1000}(abs((Master.tm-Slave.ts))<=50)]";
		testInput = "P=?[y U{52}x]";		// state_formula binary_bounded_op state_formula
		testInput = "P=?[G{5} x]";	//PESTIM_OP with valid path_formula

		
		/**	test cases for embedding path formulae **/
		// accepted
		testInput = "P=?[y || z U{52} G{100} a]";
		testInput = "P=?[y||z U{52}x]";
		testInput = "P=?[y||z U{52} (G{100} a)]";
		testInput = "P=?[(y)||z U{52} (G{100} a)]";
		testInput = "P=?[a]";
		testInput = "P=?[y || (z U{52} (G{100} a))]";
		testInput = "P>=0.5[(a || N{10} c) U{52} b]";
		testInput = "P=?[( G{4}(a) || (abs(x+5)<3) ) R{4} (b && c)]";
		testInput = "P=?[( G{4}(a) || (abs(x+5*(8/2))<3) ) R{4} (b && c)]";
		testInput = "P=?[G{4} a || (abs(x+5)<3)]";
		testInput = "P<=0.19[G{4}a||b]";
		testInput = "P<=0.19[a&&b||c]";
		testInput = "P<=0.19[a||b&&c]";
		testInput = "P<=0.19[a U{5}b||c]";
		
		
		
//		testInput = "(a)";				// il y a un soucis de parenthesage
//		testInput = "a";
//		testInput = "a || b || c";
//		testInput = "a || b && c";
//		testInput = "a && b || c";
//		testInput = "a U{5}b||c";
//		testInput = "a || b U{5} c";
//		testInput = "G{3} a || b";
//		testInput = "a || G{5} b || c";
//		testInput = "G{3} a || b && c";
//		testInput = "G{3} a || b || c";
//		testInput = "G{3} (a || b) || c";
//		testInput = "(G{3} a || b) || c";
//		testInput = "a U{4} b || f && c U{8} d";
//		testInput = "F{1000}G{3} a || b";
//		testInput = "(F{1000}G{3} a) U{58} d || b";
//		testInput = "F{1000}G{3} a U{58} d || b";
//		testInput = "(a||b)&&(c||d)U{85796378}e";
//		testInput = "G{3}a||F{589}b && (c||d) U{85796378}e";
//		testInput = "G{3}a||F{589}b && c||d U{85796378}e";
//		testInput = "G{3}a||F{589}b";
//		testInput = "G{3}a && F{589}b";
		
		
		// create a CharStream that reads from standard input
		ANTLRInputStream input = new ANTLRInputStream(testInput);
		
		// create a lexer that feeds off of input CharStream
		BoundedLtlLexer lexer = new BoundedLtlLexer(input);
		
		// create a buffer of tokens pulled from the lexer
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		
		// create a parser that feeds off the tokens buffer
		BoundedLtlParser parser = new BoundedLtlParser(tokens);
		
		ParseTree tree = parser.pbltl(); // begin parsing at pbltl rule
		
		System.out.println(tree.toStringTree(parser)); // print LISP-style tree

	}

}