package ujf.verimag.sbip.test;

import java.util.ArrayList;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import ujf.verimag.sbip.bipInterface.trace.State;
import ujf.verimag.sbip.bipInterface.trace.StateVariable;
import ujf.verimag.sbip.bipInterface.trace.Trace;
import ujf.verimag.sbip.monitor.ltl.LtlMonitor;
import ujf.verimag.sbip.parser.formula.LtlFormula;
import ujf.verimag.sbip.parser.formula.ProbabilisticFormula;
import ujf.verimag.sbip.parser.ltl.BoundedLtlLexer;
import ujf.verimag.sbip.parser.ltl.BoundedLtlParser;

public class TestLtlMonitor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		/** Building a test trace */
		ArrayList<StateVariable> sv0 = new ArrayList<>();
		sv0.add(new StateVariable("a", "10", "int"));
		sv0.add(new StateVariable("b", "20", "int"));
		State s0 = new State(sv0);
		
		ArrayList<StateVariable> sv1 = new ArrayList<>();
		sv1.add(new StateVariable("a", "100", "int"));
		sv1.add(new StateVariable("b", "25", "int"));
		State s1 = new State(sv1);
		
		ArrayList<StateVariable> sv2 = new ArrayList<>();
		sv2.add(new StateVariable("a", "105", "int"));
		sv2.add(new StateVariable("b", "2", "int"));
		//sv2.add(new StateVariable("b", "10", "int"));
		State s2 = new State(sv2);
		
		ArrayList<StateVariable> sv3 = new ArrayList<>();
		sv3.add(new StateVariable("a", "184", "int"));
		sv3.add(new StateVariable("b", "3", "int"));
		State s3 = new State(sv3);
		
		ArrayList<State> states = new ArrayList<>();
		states.add(s0);
		states.add(s1);
		states.add(s2);
		states.add(s3);
		
		Trace t = new Trace(states);
		//System.out.print(t.toString());
		
		String sf;
		/** Building a BLTL formula (atomic proposition) to monitor on the trace t **/
		sf = "P>=0.5[a>5]"; // true
		sf = "P>=0.5[a<=5]"; // false
		
		/** Building a BLTL formula (conjunction of atomic) to monitor on the trace t **/
		sf = "P>=0.5[a>5 && b<20]"; // false
		sf = "P>=0.5[a>3 && b==20]"; // true
		
		/** Building a BLTL formula (disjunction of atomic) to monitor on the trace t **/
		sf = "P>=0.5[a>5 || b<20]"; // true
		sf = "P>=0.5[a==5 || b<20]"; // false
		
		/** Building a BLTL formula (Until operator) to monitor on the trace t **/
		/** Remark : when the property doesn't hold till the specified bound of the operator 
		 * the algorithm needs an additional state to conclude it false. So the trace length 
		 * need to be op_bound + 1. See the case when b=10 in s2 trace contains or not s3 **/
		sf = "P>=0.5[a>5 U{3} b<=3]"; // true
		sf = "P>=0.5[a>5 U{3} b==1]"; // false
		
		/** Building a BLTL formula (Until operator) to monitor on the trace t **/
		sf = "P>=0.5[(a>5 && b>=10) U{3} b<=3]"; // true
		sf = "P>=0.5[(a>5 && b>=10) U{3} b<=1]"; // false
		
		/** Building a BLTL formula (Always operator) to monitor on the trace t **/
		sf = "P>=0.5[G{3} a>5]"; // true
		sf = "P>=0.5[G{3} b>=20]"; // false
		sf = "P>=0.5[G{3} (b>=20 || a>5)]"; // true
		sf = "P>=0.5[G{3} (b>=20 && a>5)]"; // false
		
		/** Building a BLTL formula (Eventually operator) to monitor on the trace t **/
		sf = "P>=0.5[F{3} b>158]"; // False
		sf = "P>=0.5[F{3} a-5>100]"; // true
		
		ANTLRInputStream input = new ANTLRInputStream(sf);
		// create a lexer that feeds off of input CharStream
		BoundedLtlLexer lexer = new BoundedLtlLexer(input);
		// create a buffer of tokens pulled from the lexer
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		// create a parser that feeds off the tokens buffer
		BoundedLtlParser parser = new BoundedLtlParser(tokens);
		// disable the parse tree construction
		parser.setBuildParseTree(false);
		
		ProbabilisticFormula pf = parser.pbltl().pf;
		//System.out.println(pf.toString());
		
		LtlFormula ltlf = (LtlFormula)pf.getFormula();
		//System.out.println(ltlf.toString());
		
		/** Instantiate and run the LTL Monitor**/
		LtlMonitor m = new LtlMonitor();
		m.check(ltlf, t);

	}

}
