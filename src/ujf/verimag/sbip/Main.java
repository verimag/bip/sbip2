package ujf.verimag.sbip;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;


import com.thehowtotutorial.splashscreen.JSplash;

import ujf.verimag.sbip.gui.UserInterface;
import ujf.verimag.sbip.gui.dialogs.Compilator;

public class Main {
	public static void main(String[] args) {
		try {
			
		    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		        if ("Nimbus".equals(info.getName())) {
		        	 UIManager.setLookAndFeel(info.getClassName());
						
		        	UIManager.put("nimbusBase", new Color(57,105,138));   
		            break;
		        }
//		        System.out.println(info.getClassName());
		    }
		} catch (Exception e) {
		    // If Nimbus is not available, fall back to cross-platform
		    try {
		        UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		    } catch (Exception ex) {
		        // not worth my time
		    } 
		}
		

		try {

			File f = new File("splash.png");
			 
			 int year = Calendar.getInstance().get(Calendar.YEAR);

			JSplash splash = new JSplash( f.toURL(), true, true, false, "©Copyright 2016-"+year+" by Univ. Grenoble-Alpes. All rights reserved.               Version "+UserInterface.getVersion(), null, Color.white, Color.black);

			splash.setLocationRelativeTo(null);
			splash.setAlwaysOnTop(true);
			splash.splashOn();
			
			
			initWorkspace();
			splash.setProgress(20, "Initializing workspace...");
			checkEngines();
			splash.setProgress(40, "Loading simulation engines...");
			checkScripts();
			splash.setProgress(60, "Loading compilation scripts...");	
			checkIcons();
			splash.setProgress(80, "Loading image icons...");
			Thread.sleep(500);	
			splash.setProgress(100, "Ready!");
			Thread.sleep(1000);				
			splash.splashOff();
			
			
			
			new UserInterface();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		}

	private static void checkScripts() {
		for (int i = 0; i < 2; i++) {
			Compilator c = new Compilator(i);
			File f = new File("./scripts/"+c.getScriptName());
			if(!f.exists())
				showErrorPane("script", f);
		}
		
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}				
		
		
	}

	private static void showErrorPane(String type, File location) {
		JOptionPane.showMessageDialog(null, "Could not find "+type+ " '"+ location.getName()+ "' at location :\n"+location.getParentFile().getAbsolutePath(), "Initialization error", JOptionPane.ERROR_MESSAGE);
		System.exit(-1);
	}

	private static void checkIcons() {
			File f = new File("./Icons8");
			if(!f.exists())
				showErrorPane("icons folder", f);
		
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		
	}
	
	private static void checkEngines() {
		File f = new File("./engines");
		if(!f.exists())
			showErrorPane("engines folder", f);
		else {
			File stoch = new File("./engines/SRT-BIP");
			if(!stoch.exists())
				showErrorPane("stochastic bip engine folder", stoch);
			File bip = new File("./engines/untimed-bip");
			if(!bip.exists())
				showErrorPane("untimed-bip engine folder", bip);
		}
	
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	
}

	private static void initWorkspace() {

			File workspace = new File("./Workspace");
			if(!workspace.exists()) workspace.mkdir();
			else {
				if(workspace.isFile()){
//					System.err.println("Could not initialize Workspace : "
//							+"\n\t- Not able to create directory." 
//							+"\n\t- Existing file with the same name 'Workspace'.");
					
					JOptionPane.showMessageDialog(null, "Could not initialize Workspace : "
							+"\n\t- Not able to create directory." 
							+"\n\t- Existing file with the same name 'Workspace'.", "Initialization error", JOptionPane.ERROR_MESSAGE);
					
					System.exit(-3);
				}
			}

			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	}
	
	
	
	
	
}
