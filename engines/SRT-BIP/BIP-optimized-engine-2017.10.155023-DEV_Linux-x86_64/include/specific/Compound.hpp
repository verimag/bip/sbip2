#ifndef _BIP_Engine_Compound_HPP_
#define _BIP_Engine_Compound_HPP_

// inherited classes
#include <CompoundItf.hpp>
#include "Component.hpp"
#include "CompoundExportPort.hpp"
#include "CompoundExportData.hpp"
#include "Connector.hpp"
#include "Priority.hpp"

class InteractionValue;
class AtomInternalPort;
class AtomExternalPort;
class BipError;
class CycleInPriorities;

class Compound : public virtual ComponentItf, public Component, public CompoundItf {
 public:
  // constructors
  Compound(const string &name);

  // destructor
  virtual ~Compound();

  // specific
  virtual BipError &execute(PortValue &portValue, const TimeValue &time);
  BipError &execute(InteractionValue &interactionValue, const TimeValue &time);
  virtual BipError &initialize();
  void initializeAllAtomExternalPorts();
  BipError &update();
  void resetAll();
  BipError &recursiveUpdate();
  const Interval &invariant() const { return mInvariant; }
  const Interval &resume() const { return mResume; }
  const Interval &wait() const { return mWait; }
  BipError &resume(const TimeValue &time);
  bool hasResume() const { return mHasResume; }
  void setTime(const TimeValue &time);
  virtual const TimeValue &time() const { return mTime; }

  const vector<Connector *> &nonExportedConnectors() const { return mNonExportedConnectors.value(); }
  const vector<AtomInternalPort *> &nonExportedInternalPorts() const { return mNonExportedInternalPorts.value(); }
  const vector<AtomExternalPort *> &externalPorts() const { return mExternalPorts.value(); }
  const vector<Connector *> &allNonExportedConnectors() const { return mAllNonExportedConnectors.value(); }
  const vector<AtomInternalPort *> &allNonExportedInternalPorts() const { return mAllNonExportedInternalPorts.value(); }
  const vector<AtomExternalPort *> &allExternalPorts() const { return mAllExternalPorts.value(); }

 protected:
  // protected setters for references
  void addComponent(Component &component) {
    mComponents[component.name()] = &component;
    component.setHolder(*this);
  }

  void addPort(CompoundExportPort &port) {
    mPorts[port.name()] = &port;
    port.setHolder(*this);
    Component::addPort(port);
  }

  void addPriority(Priority &priority) {
    mPriorities.push_back(&priority);
    priority.setHolder(*this);
  }

  void addConnector(Connector &connector) {
    mConnectors[connector.name()] = &connector;
    connector.setHolder(*this);
  }

  void addData(CompoundExportData &data) {
    mData[data.name()] = &data;
    data.setHolder(*this);
    Component::addData(data);
  }

  // specific
  void computeNonExportedConnectors(vector<Connector *> &nonExportedConnectors);
  void computeNonExportedInternalPorts(vector<AtomInternalPort *> &nonExportedInternalPorts);
  void computeExternalPorts(vector<AtomExternalPort *> &externalPorts);
  void computeAllNonExportedConnectors(vector<Connector *> &allNonExportedConnectors);
  void computeAllNonExportedInternalPorts(vector<AtomInternalPort *> &allNonExportedInternalPorts);
  void computeAllExternalPorts(vector<AtomExternalPort *> &allExternalPorts);
  void computeAllCyclesInPriorities(set<CycleInPriorities *> &cycles);

  void computeCycles() const;
  void allCyclesFrom(Priority &priority, set<CycleInPriorities> &cycles, vector<Priority *> &path);

  BipError &initializeAllAtoms();
  void initializeAllConnectorsPriorities();

  void recomputeInvariantResumeWait();
  void recomputeWaitAfterResume();

  const set<CycleInPriorities *> &allCyclesInPriorities() const { return mAllCyclesInPriorities.value(); }

  mutable Initializable<vector<Connector *>, Compound> mNonExportedConnectors;
  mutable Initializable<vector<AtomInternalPort *>, Compound> mNonExportedInternalPorts;
  mutable Initializable<vector<AtomExternalPort *>, Compound> mExternalPorts;
  mutable Initializable<vector<Connector *>, Compound> mAllNonExportedConnectors;
  mutable Initializable<vector<AtomInternalPort *>, Compound> mAllNonExportedInternalPorts;
  mutable Initializable<vector<AtomExternalPort *>, Compound> mAllExternalPorts;
  mutable Initializable<set<CycleInPriorities *>, Compound> mAllCyclesInPriorities;

  TimeValue mTime;
  Interval mInvariant;
  Interval mResume;
  bool mHasResume;
  Interval mWait;

  // specific rights for deploying the system
  friend Component* deploy(int argc, char** argv);
};

#endif // _BIP_Engine_Compound_HPP_
