#ifndef _BIP_Engine_AtomExportPort_HPP_
#define _BIP_Engine_AtomExportPort_HPP_

// inherited classes
#include <AtomExportPortItf.hpp>
#include "Port.hpp"
#include "PortValue.hpp"
#include <Resetable.hpp>

class BipError;
class TimeValue;

class AtomExportPort : public virtual PortItf, public virtual Port, public AtomExportPortItf {
 public:
  // constructors
  AtomExportPort(const string &name);
  AtomExportPort(const string &name, bool hasEarlyUpdate);

  // destructor
  virtual ~AtomExportPort();

  // operations
  virtual bool isReady() const { return false; }
  virtual void setReady() { }

  // specific
  virtual BipError &execute(PortValue &portValue, const TimeValue &time);
  virtual ResetableItf &reset() { return mReset; }
  virtual bool hasResumeFor(PortValue &value);

 protected:
  void addInternalPort(AtomInternalPort &internalPort);

  // specific rights for deploying the system
  friend Component* deploy(int argc, char** argv);

  // specific
  ResetableItf mReset;
};

#endif // _BIP_Engine_AtomExportPort_HPP_
