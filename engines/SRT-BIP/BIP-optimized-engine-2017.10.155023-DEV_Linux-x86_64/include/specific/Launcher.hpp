#ifndef _BIP_Engine_Launcher_HPP_
#define _BIP_Engine_Launcher_HPP_

// inherited classes
#include <LauncherItf.hpp>

class Engine;
class Scheduler;
class Logger;
class Compound;
class GlobalClock;
class ModelClock;

class Launcher : public LauncherItf {
 public:
  // constructors
  Launcher(int argc, char **argv, Component &root);

  // operations
  virtual int initialize();
  virtual int launch();

  // destructor
  virtual ~Launcher();

 protected:
  // specific
  void printHelp(const string &bipExecutableName);

  Engine *mEngine;
  Scheduler *mScheduler;
  Logger *mLogger;
  GlobalClock *mPlatformClock;
  ModelClock *mModelClock;
};

#endif // _BIP_Engine_Launcher_HPP_
