#ifndef _BIP_Engine_GlobalClock_HPP_
#define _BIP_Engine_GlobalClock_HPP_

class TimeValue;

/** \brief Provides the basic interface for implementing the clock used by
 * the scheduler.
 *
 * 
 */
class GlobalClock {
 public:
  GlobalClock();
  virtual ~GlobalClock();

  virtual TimeValue time() const = 0;
  virtual bool wait(const TimeValue &time) = 0;
  virtual int speed() const { return 1; }
};

#endif // _BIP_Engine_GlobalClock_HPP_
