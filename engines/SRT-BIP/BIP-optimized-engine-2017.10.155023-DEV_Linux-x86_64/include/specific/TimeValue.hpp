#ifndef _BIP_Engine_TimeValue_HPP_
#define _BIP_Engine_TimeValue_HPP_

// inherited classes
#include <TimeValueItf.hpp>

#include <ostream>

class Interval;

class TimeValue : public TimeValueItf {
 public:
  // constructors
  TimeValue();
  TimeValue(int value, TimeUnit unit);
  TimeValue(double value, TimeUnit unit);
  TimeValue(long long value, TimeUnit unit);
  TimeValue(const TimeValue &value);

  // destructor
  virtual ~TimeValue();

  // operations
  virtual TimeValue &operator=(const TimeValue &value) { setTime(value.time()); return *this; }
  virtual bool operator<(const TimeValue &value) const { return time() < value.time(); }
  virtual bool operator>(const TimeValue &value) const { return time() > value.time(); }
  virtual bool operator<=(const TimeValue &value) const { return time() <= value.time(); }
  virtual bool operator>=(const TimeValue &value) const { return time() >= value.time(); }
  virtual bool operator==(const TimeValue &value) const { return time() == value.time(); }
  virtual bool operator!=(const TimeValue &value) const { return time() != value.time(); }
  virtual TimeValue &operator+=(const TimeValue &value);
  virtual TimeValue &operator-=(const TimeValue &value);

  TimeValue operator+(const TimeValue &value) const { TimeValue ret = *this; ret += value; return ret; }
  TimeValue operator-(const TimeValue &value) const { TimeValue ret = *this; ret -= value; return ret; }

  TimeValue operator-() const;
  TimeValue operator*(const long long &factor) const { TimeValue ret = *this; ret.mTime = ret.mTime * factor; return ret; }
  TimeValue operator*(int factor) const { TimeValue ret = *this; ret.mTime = ret.mTime * factor; return ret; }
  TimeValue operator*(const double &factor) const { TimeValue ret = *this; ret.mTime = (long long) (factor * ret.mTime); return ret; }
  long long operator/(const TimeValue &value) const { return time() / value.time(); }
  TimeValue operator/(int factor) const { TimeValue ret = *this; ret.mTime = ret.mTime / factor; return ret; }
  TimeValue operator/(const double &factor) const { TimeValue ret = *this; ret.mTime = ret.mTime / factor; return ret; }

  // constants
  static const TimeValue ZERO, MIN, MAX;

  friend std::ostream &operator<<(std::ostream &o, const TimeValue &time);
  friend TimeValue operator*(int factor, const TimeValue &time);
  friend TimeValue operator*(double factor, const TimeValue &time);

 protected:
  TimeValue(long long time) : TimeValueItf(), mTime(time) { }

  const long long &time() const { return mTime; }
  long long &time() { return mTime; }
  void setTime(long long time) { mTime = time; }

  long long mTime;
};

inline TimeValue &TimeValue::operator+=(const TimeValue &value) {
  assert(!(*this == MAX && value == MIN));
  assert(!(*this == MIN && value == MAX));
  
  if (*this != MAX && *this != MIN) {
    if (value == MAX) {
      *this = MAX;
    }
    else if (value == MIN) {
      *this = MIN;
    }
    else {
      time() += value.time();
    }
  }

  return *this;
}

inline TimeValue &TimeValue::operator-=(const TimeValue &value) {
  TimeValue negValue = -value;

  return *this += negValue;
}

inline TimeValue TimeValue::operator-() const {
  TimeValue ret;

  if (*this == MIN) {
    ret = MAX;
  }
  else if (*this == MAX) {
    ret = MIN;
  }
  else {
    ret = TimeValue(-time());
  }

  return ret;
}

inline TimeValue operator*(int factor, const TimeValue &time) {
  TimeValue ret = time;
  return time * factor;
}
inline TimeValue operator*(double factor, const TimeValue &time) {
  TimeValue ret = time;
  return time * factor;
}

#endif // _BIP_Engine_TimeValue_HPP_
