#ifndef _BIP_Engine_AtomExternalPort_HPP_
#define _BIP_Engine_AtomExternalPort_HPP_

// inherited classes
#include <AtomExternalPortItf.hpp>

#include <TimingConstraint.hpp>
#include <Resetable.hpp>

class AtomExternalPort : public AtomExternalPortItf {
 public:
  // constructors
  AtomExternalPort(const string &name, const EventConsumptionPolicy &policy);

  // destructor
  virtual ~AtomExternalPort();

  // operations
  virtual void notify();
  virtual TimeValue time();

  // specific
  bool hasExpectedEvent() const;
  bool hasOutdatedEvent() const;
  const TimingConstraint &timingConstraint() const { return mTimingConstraint; }
  void reset() const { mTimingConstraint.reset(); }

 protected:
  // specific
  void recomputeTimingConstraint(TimingConstraint &constraint) const;
  mutable Resetable<TimingConstraint, AtomExternalPort> mTimingConstraint;
};

#endif // _BIP_Engine_AtomExternalPort_HPP_
