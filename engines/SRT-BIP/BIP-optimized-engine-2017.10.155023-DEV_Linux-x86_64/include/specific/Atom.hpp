#ifndef _BIP_Engine_Atom_HPP_
#define _BIP_Engine_Atom_HPP_

// inherited classes
#include <AtomItf.hpp>
#include "AtomExportData.hpp"
#include "AtomExportPort.hpp"
#include "AtomInternalPort.hpp"
#include "Component.hpp"
#include "Compound.hpp"

class BipError;
class TimeValue;
class Interval;

class Atom : public virtual ComponentItf, public Component, public AtomItf {
 public:
  // constructors
  Atom(const string &name);
  Atom(const string &name, bool initialHasResume);

  // destructor
  virtual ~Atom();

  virtual BipError &execute(PortValue &portValue, const TimeValue &time) = 0;
  virtual BipError &execute(AtomExternalPort &external, const TimeValue &time) = 0;
  virtual string toString() const { return "-"; };

  // specific
  BipError &execute(AtomInternalPort &internalPort, const TimeValue &time);
  void reset();
  virtual const TimeValue &time() const { return holder().time(); }
  bool hasResume() const { return mHasResume; }
  void setHasResume(bool b) { mHasResume = b; }

  friend ostream& operator<<(ostream &o, const Atom &atom);
  friend ostream& operator<<(ostream &o, const Atom *atom);


  // references accessors
  void addInternalPort(AtomInternalPort &internalPort) {
    mInternalPorts[internalPort.name()] = &internalPort;
    internalPort.setHolder(*this);
  }

  void addExternalPort(AtomExternalPort &externalPort) {
    mExternalPorts[externalPort.name()] = &externalPort;
    externalPort.setHolder(*this);
  }

  void addPort(AtomExportPort &port) {
    mPorts[port.name()] = &port;
    port.setHolder(*this);
    Component::addPort(port);
  }

  void addData(AtomExportData &data) {
    mData[data.name()] = &data;
    data.setHolder(*this);
    Component::addData(data);
  }

 protected:
  // attributes
  bool mHasResume;
};

#endif // _BIP_Engine_Atom_HPP_
