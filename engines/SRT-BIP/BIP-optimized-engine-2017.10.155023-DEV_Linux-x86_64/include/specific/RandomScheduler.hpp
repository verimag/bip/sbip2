#ifndef _BIP_Engine_RandomScheduler_HPP_
#define _BIP_Engine_RandomScheduler_HPP_

#include "Scheduler.hpp"
#include "StringTree.hpp"
#include <TimeValue.hpp>

class Engine;
class Logger;
class BipError;
class Atom;
class Component;
class Connector;
class Compound;
class InteractionValue;
class ConnectorExportPort;
class CompoundExportPort;
class AtomInternalPort;
class AtomExternalPort;
class AtomExportPort;
class GlobalClock;
class TimingConstraint;

/** \brief Compute execution sequences in which interactions
 * are chosen randomly.
 */
class RandomScheduler : public Scheduler {
 public:
  RandomScheduler(Engine &engine, GlobalClock &platformClock, Logger &logger, bool debug = false, bool interactive = false, bool randomSeed = true,
                  unsigned int seed = 0, bool asap = false, bool firstEnabled = false, bool relaxed = false);
  virtual ~RandomScheduler();

  virtual BipError &initialize();
  virtual BipError &run();

  const GlobalClock &platformClock() const { return mPlatformClock; }

  bool debug() const { return mDebug; }
  bool interactive() const { return mInteractive; }
  bool randomSeed() const { return mRandomSeed; }
  unsigned int seed() const { return mSeed; }
  bool asap() const { return mAsap; }
  bool firstEnabled() const { return mFirstEnabled; }
  bool relaxed() const { return mRelaxed; }

  static void notify();
  static GlobalClock &staticPlatformClock() { assert(mStaticPlatformClock != NULL); return *mStaticPlatformClock; }


 protected:
  BipError &choose();
  BipError &chooseInteractive();
  bool choose(InteractionValue &interaction);
  bool choose(AtomInternalPort &internal);
  bool choose(AtomExternalPort &external);
  bool hasChosen() const;
  bool choose(const Interval &interval, const TimeValue &plannedTime, bool hasPriority);
  TimeValue plan(const Interval &interval) const;
  TimeValue schedulingCriterion(const Interval &interval, const TimeValue &planned) const;
  
  BipError &checkTimeSafety();
  BipError &checkTimeSafetyAndResume();

  bool deadlock() const;
  bool waitForNotificationsUntil(const TimeValue &time);
  static pid_t pid;
  static volatile unsigned int mNbReceivedNotifications;
  static void handleSignals(int signum);
  static GlobalClock *mStaticPlatformClock;

  void print();
  StringTree print(const ConnectorExportPort &port);
  StringTree print(const CompoundExportPort &port);
  StringTree print(const AtomInternalPort &port);
  StringTree print(const AtomExportPort &port);
  StringTree print(const InteractionValue &port);
  StringTree print(const Connector &connector);
  StringTree print(const Atom &atom);
  StringTree print(const Compound &compound);
  StringTree print(const Component &component);

  GlobalClock &platformClock() { return mPlatformClock; }

  GlobalClock &mPlatformClock;

  bool hasNotifications() const { return mNbTreatedNotifications != mNbReceivedNotifications; }
  void resetNotifications() { mNbTreatedNotifications = mNbReceivedNotifications; }

  Logger &logger() const { return mLogger; }
  bool hasChosenInteraction() const { return mChosenInteraction != NULL; }
  bool hasChosenInternal() const { return mChosenInternal != NULL; }
  bool hasChosenExternal() const { return mChosenExternal != NULL; }
  InteractionValue &chosenInteraction() const { return *mChosenInteraction; }
  AtomInternalPort &chosenInternal() const { return *mChosenInternal; }
  AtomExternalPort &chosenExternal() const { return *mChosenExternal; }
  const TimeValue &wakeUpTime() const { return mWakeUpTime; }

  Logger &mLogger;
  bool mDebug;
  bool mInteractive;
  bool mRandomSeed;
  unsigned int mSeed;

  unsigned int mNbTreatedNotifications;
  bool mAsap;
  bool mFirstEnabled;
  bool mRelaxed;

  InteractionValue *mChosenInteraction;
  AtomInternalPort *mChosenInternal;
  AtomExternalPort *mChosenExternal;
  TimeValue mWakeUpTime;
  TimeValue mMinCriterion;
  unsigned int mNbMinCriterion;
};

#endif // _BIP_Engine_RandomScheduler_HPP_
