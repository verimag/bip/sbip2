#ifndef _BIP_Engine_RealTimeClock_HPP_
#define _BIP_Engine_RealTimeClock_HPP_

#include <GlobalClock.hpp>
#include <TimeValue.hpp>

#include <time.h>
#include <signal.h>

/** \brief Provides the basic interface for implementing the clock used by
 * the scheduler.
 *
 * 
 */
class RealTimeClock : public GlobalClock {
 public:
  RealTimeClock();
  virtual ~RealTimeClock();

  void start() { mGetTimeOfDayAtStart = getTimeOfDay(); }
  virtual TimeValue time() const { return getTimeOfDay() - mGetTimeOfDayAtStart; }
  virtual bool wait(const TimeValue &time);

 protected:
  TimeValue getTimeOfDay() const;
  void armTimer(const TimeValue &delay);
  void disarmTimer();

  TimeValue mGetTimeOfDayAtStart;

  timer_t mTimer;
  itimerspec mTimerSpec;
  sigset_t maskSIGBIP;
};

#endif // _BIP_Engine_RealTimeClock_HPP_
