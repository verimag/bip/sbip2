#ifndef _BIP_Engine_Clock_HPP_
#define _BIP_Engine_Clock_HPP_

// inherited classes
#include <ClockItf.hpp>
#include <TimeValue.hpp>
#include <GlobalClock.hpp>

class Clock : public ClockItf {
 public:
  // constructors
  Clock();
  Clock(Atom &atom);
  Clock(const Clock &clock);

  // destructor
  virtual ~Clock();

  // operations
  virtual TimeValue time() const;
  virtual double speed() const { return mSpeed; }
  virtual void resetTo(const TimeValue &time);
  virtual void setSpeed(const double &speed);

  static void configureAllClocks(GlobalClock &modelClock);

 protected:
  GlobalClock &modelClock() const { return *mModelClock; }
  bool hasModelClock() const { return mModelClock != NULL; }
  void setModelClock(GlobalClock &modelClock) { mModelClock = &modelClock; }

  TimeValue mResetTime;
  TimeValue mResetTo;
  double mSpeed;

  static GlobalClock *mModelClock;
};

inline Clock::Clock(const Clock &clock) :
  mResetTime(clock.mResetTime),
  mResetTo(clock.mResetTo),
  mSpeed(clock.mSpeed) {
}

inline
TimeValue Clock::time() const {
  assert(hasModelClock());
  return ((modelClock().time() - mResetTime))*speed() + mResetTo;
}

inline
void Clock::resetTo(const TimeValue &time) {
  assert(hasModelClock());

  mResetTime = modelClock().time();
  mResetTo = time;
}

inline
void Clock::setSpeed(const double &speed) {
  assert(hasModelClock());

  resetTo(time());

  mSpeed = speed;
}


#endif // _BIP_Engine_Clock_HPP_
