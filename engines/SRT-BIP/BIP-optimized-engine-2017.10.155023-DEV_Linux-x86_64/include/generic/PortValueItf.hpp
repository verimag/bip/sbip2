#ifndef _BIP_Engine_PortValueItf_HPP_
#define _BIP_Engine_PortValueItf_HPP_

#include <bip-engineiface-config.hpp>
using namespace bipbasetypes;
using namespace biptypes;

// referenced classes
class Interval;

class PortValueItf {
 public:
  // destructor
  virtual ~PortValueItf();

  // operations
  virtual string toString() const = 0;

  // getters for references
  const Interval &interval() const { return *mInterval; }
  bool hasInterval() const { return mInterval != NULL; }

  // getters for attributes
  Urgency urgency() const { return mUrgency; }
  bool hasResume() const { return mHasResume; }

  // setters for references
  void setInterval(const Interval &interval) { mInterval = &interval; }
  void clearInterval() { mInterval = NULL; }

  // setters for attributes
  void setUrgency(Urgency urgency) { mUrgency = urgency; }
  void setHasResume(bool resume) { mHasResume = resume; }

 protected:
  // protected constructors
  PortValueItf();

  // attributes
  Urgency mUrgency;
  bool mHasResume;

  // references
  const Interval *mInterval;
};

#endif // _BIP_Engine_PortValueItf_HPP_
