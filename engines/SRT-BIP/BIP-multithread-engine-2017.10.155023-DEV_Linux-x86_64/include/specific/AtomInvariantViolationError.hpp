#ifndef _BIP_Engine_AtomInvariantViolationError_HPP_
#define _BIP_Engine_AtomInvariantViolationError_HPP_

// inherited classes
#include <AtomInvariantViolationErrorItf.hpp>
#include "BipError.hpp"

class AtomInvariantViolationError : public virtual BipErrorItf, public BipError, public AtomInvariantViolationErrorItf {
 public:
  // constructors
  AtomInvariantViolationError(const string &invariantName, Atom &atom);

  // destructor
  virtual ~AtomInvariantViolationError();
};

#endif // _BIP_Engine_AtomInvariantViolationError_HPP_
