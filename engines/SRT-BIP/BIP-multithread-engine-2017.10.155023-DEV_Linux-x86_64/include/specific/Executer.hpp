#ifndef _BIP_Engine_Executer_HPP_
#define _BIP_Engine_Executer_HPP_

#include "StringTree.hpp"
#include "ReadyQueue.hpp"
#include <ExecuteAtomJob.hpp>
#include "Resource.hpp"

class BipError;
class Atom;
class Component;
class Connector;
class Compound;
class Port;
class InteractionValue;
class ConnectorExportPort;
class CompoundExportPort;
class AtomInternalPort;
class AtomExportPort;
class Logger;
class Thread;
class Job;
class GlobalClock;

/** \brief Compute execution sequences in which interactions
 * are chosen randomly.
 */
class Executer {
 public:
  Executer(Compound &top, bool isRealTime, bool asap, bool first_enabled, bool relaxed, bool disable_time_safety, Logger &logger, unsigned int nbThreads, bool randomSeed = true, unsigned int seed = 0);
  virtual ~Executer();

  BipError &initialize();
  BipError &run();
  bool isRealTime() const { return mIsRealTime; }
  bool asap() const { return mAsap; }
  bool firstEnabled() const { return mFirstEnabled; }
  bool relaxed() const { return mRelaxed; }
  bool disabledTimeSafety() const { return mDisabledTimeSafety; }
  bool debug() const { return mDebug; }
  bool interactive() const { return mInteractive; }
  bool randomSeed() const { return mRandomSeed; }
  unsigned int seed() const { return mSeed; }
  unsigned int limit() const { return mLimit; }

  static ReadyQueue<Job> jobs;
  static ExecuteAtomJob::ResumeJoiner resumeSynchronizerForInitialize;

 protected:
  void initializeReserver(const Compound &compound);
  void initializeThreads();
  void startThreads();
  void stopThreads();
  void print();
  StringTree print(const ConnectorExportPort &port);
  StringTree print(const CompoundExportPort &port);
  StringTree print(const AtomInternalPort &port);
  StringTree print(const AtomExportPort &port);
  StringTree print(const InteractionValue &port);
  StringTree print(const Connector &connector);
  StringTree print(const Atom &atom);
  StringTree print(const Compound &compound);
  StringTree print(const Component &component);

  Logger &logger() { return mLogger; }

  /** \return Root component.
   */
  Compound& top() { return mTop; }

  Compound &mTop;

  bool mIsRealTime;
  bool mAsap;
  bool mFirstEnabled;
  bool mRelaxed;
  bool mDisabledTimeSafety;
  Logger &mLogger;
  bool mDebug;
  bool mInteractive;
  unsigned int mNbThreads;
  bool mRandomSeed;
  unsigned int mSeed;
  unsigned int mLimit;

  vector<Thread *> mThreads;
  Resource::Reserver mReserver;
};

#endif // _BIP_Engine_Executer_HPP_
