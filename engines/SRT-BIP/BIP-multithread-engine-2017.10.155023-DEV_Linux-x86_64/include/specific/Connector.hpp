#ifndef _BIP_Engine_Connector_HPP_
#define _BIP_Engine_Connector_HPP_

// inherited classes
#include <ConnectorItf.hpp>
#include "ConnectorExportPort.hpp"
#include "QuotedPortReference.hpp"

#include "Initializable.hpp"
#include "Resetable.hpp"
#include "ExecuteConnectorJob.hpp"
#include <ConnectorTimeSafetyValidator.hpp>

#include <TimeValue.hpp>
#include <Interval.hpp>

class InteractionValue;
class Priority;

class Connector : public ConnectorItf {
 public:
  // constructors
  Connector(const string &name, const bool &asyncResume);

  // destructor
  virtual ~Connector();

  // operations
  virtual const vector<Interaction *> &interactions() const { return mInteractions.value(); }

  // specific
  bool isTopLevel() const { return mIsTopLevel.value(); }
  Resetable<Interaction *, Connector> &allEnabledPorts() const { return mAllEnabledPorts; }
  Resetable<vector<InteractionValue *>, Connector> &enabledInteractions() const { return mEnabledInteractions; }
  Resetable<vector<InteractionValue *>, Connector> &maximalInteractions() const { return mMaximalInteractions; }
  Resetable<vector<InteractionValue *>, Connector> &locallyMaximalInteractions() const  { return mLocallyMaximalInteractions; }
  Initializable<vector<const Atom*>, Connector> &allAtoms() const { return mAllAtoms; }
  
  Resetable<TimeValue, Connector> &time() const { return mTime; }
  Resetable<Interval, Connector> &invariant() const { return mInvariant; }
      
  void release() const;
  void release(const vector<Interaction *> &interactions) const;
  void release(const vector<InteractionValue *> &interactions) const;

  Initializable<vector<Priority *>, Connector> &dominatingPriorities() const { return mDominatingPriorities; }
  Initializable<vector<Priority *>, Connector> &dominatedPriorities() const { return mDominatedPriorities; }
  
  string fullName() const;

  const vector<Priority *> &allDominatingPriorities() const { return mAllDominatingPriorities.value(); }
  const vector<const Connector *> &allHigherPriorityConnectors() const { return mAllHigherPriorityConnectors.value(); }

  void initialize();
  void initializeAfterPriorities();

  const ExecuteConnectorJob &executeJob() const { return mExecuteJob; }
  ExecuteConnectorJob &executeJob() { return mExecuteJob; }
  ConnectorTimeSafetyValidator &timeSafetyValidator() { return mTimeSafetyValidator; }

 protected:
  // update methods resetable objects
  void recomputeAllEnabledPorts(Interaction *&allEnabledPorts) const;
  void recomputeEnabledInteractions(vector<InteractionValue *> &enabledInteractions) const;
  void recomputeMaximalInteractions(vector<InteractionValue *> &maximalInteractions) const;
  void recomputeLocallyMaximalInteractions(vector<InteractionValue *> &locallyMaximalInteractions) const;
  void recomputeTime(TimeValue &time) const;
  void recomputeInvariant(Interval &invariant) const;

  // initialization methods for initializable objects
  void computeInteractions(vector<Interaction *> &interactions);
  void computeDominatingPriorities(vector<Priority *> &dominatingPriorities);
  void computeDominatedPriorities(vector<Priority *> &dominatedPriorities);
  void computeIsTopLevel(bool &isTopLevel);
  void computeAllDominatingPriorities(vector<Priority *> &allDominatingConnectors);
  void computeAllHigherPriorityConnectors(vector<const Connector *> &allHigherPriorityConnectors);
  void computeAllAtoms(vector<const Atom *> &allAtoms);
  vector<const Atom *> computeAllAtoms() const;
  vector<const Atom *> computeAllAtomsOf(const Port &port) const;

  // protected setters for references
  void addPort(QuotedPortReference &quotedPort);
  void setExportedPort(ConnectorExportPort &exportedPort);

  // specific
  void enumerateInteractions(vector<Interaction *> &allInteractions, vector<QuotedPortReference *> &partialInteraction, unsigned int nextPortIndex) const;
  bool enumerateInteractionValues(vector<InteractionValue *> &allInteractions, const Interaction &interaction, vector<PortValue *> &partialValues, unsigned int nextPortIndex, bool keepLocallyMaximalOnly) const;
  void enumerateLocallyMaximalInteractionValues(vector<InteractionValue *> &allInteractions, Interaction &interaction, unsigned int mandatoryIndex) const;
  const Interaction &allEnabledInteraction() const { return *(Interaction *) mAllEnabledPorts; }

  void computeIsTopLevel() const;

  bool mInitializedDependentConnectors;
  vector<Connector *> mDependentConnectors;

  bool mInitializedDependentPriorities;
  vector<Priority *> mDependentPriorities;

  mutable Resetable<Interaction *, Connector> mAllEnabledPorts;
  mutable Interaction *copyOfmAllEnabledPorts; // copy for thread-safety

  mutable Initializable<vector<Interaction *>, Connector> mInteractions;

  mutable Resetable<vector<InteractionValue *>, Connector> mEnabledInteractions;
  mutable Resetable<vector<InteractionValue *>, Connector> mMaximalInteractions;
  mutable Resetable<vector<InteractionValue *>, Connector> mLocallyMaximalInteractions;
  mutable Resetable<TimeValue, Connector> mTime;
  mutable Resetable<Interval, Connector> mInvariant;
  
  mutable Initializable<vector<Priority *>, Connector> mDominatingPriorities;
  mutable Initializable<vector<Priority *>, Connector> mDominatedPriorities;
  mutable Initializable<bool, Connector> mIsTopLevel;
  mutable Initializable<vector<Priority *>, Connector> mAllDominatingPriorities;
  mutable Initializable<vector<const Connector *>, Connector> mAllHigherPriorityConnectors;
  mutable Initializable<vector<const Atom *>, Connector> mAllAtoms;
  
  ExecuteConnectorJob mExecuteJob;
  ConnectorTimeSafetyValidator mTimeSafetyValidator;
};

#endif // _BIP_Engine_Connector_HPP_
