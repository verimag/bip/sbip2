#ifndef _BIP_Engine_ConnectorTimeSafetyValidator_HPP_
#define _BIP_Engine_ConnectorTimeSafetyValidator_HPP_

#include <bip-engineiface-config.hpp>
using namespace bipbasetypes;
using namespace biptypes;

#include <Resource.hpp>

class Logger;
class Connector;
class Atom;
class Port;

class ConnectorTimeSafetyValidator : public Resource::Validator {
 public:
  // constructors
  ConnectorTimeSafetyValidator(const Connector &connector);

  // destructor
  virtual ~ConnectorTimeSafetyValidator();

  // getters
  Logger &logger() { return *mLogger; }
  bool hasLogger() const { return mLogger != NULL; }
  const Connector &connector() const { return mConnector; }
  const vector<Atom *> allAtoms() const { return mAllAtoms; }
  const Interval &timeSafe() const { return mTimeSafe; }
  
  // setters
  void setLogger(Logger &logger) { mLogger = &logger; }
  void clearLogger() { mLogger = NULL; }  

  // operations
  virtual void reset(const vector<Resource *> &resources);
  virtual void free(Resource &resource);

  void initialize();
  
 protected:
  void setTimeSafe(const Interval &interval) { mTimeSafe = interval; }
  
  void validate();
  bool isAtomResource(const Resource &resource) const;
  bool isAtomResumeResource(const Resource &resource) const;
  bool isConnectorResource(const Resource &resource) const;
  Atom &atom(const Resource &resource) const;
  
  vector<Atom *> allAtoms();
  void allAtoms(vector<Atom *> &atoms, const Connector &connector);
  void allAtoms(vector<Atom *> &atoms, const Port &port);
  
  // all atoms involved
  vector<Atom *> mAllAtoms;
  vector<Resource *> mAllAtomResources;
  vector<Resource *> mAllAtomResumeResources;
  vector<Resource *> mAllConnectorResources;
  
  Logger *mLogger;
  const Connector &mConnector;
  atomic<bool> mError;
  atomic<unsigned int> mMissingResources;
  atomic<unsigned int> mResume;
  Interval mTimeSafe;
};

#endif // _BIP_Engine_ConnectorTimeSafetyValidator_HPP_
