#ifndef _BIP_Engine_AtomExportPort_HPP_
#define _BIP_Engine_AtomExportPort_HPP_

// inherited classes
#include <AtomExportPortItf.hpp>
#include "Port.hpp"
#include "PortValue.hpp"
#include "Job.hpp"
#include "Resetable.hpp"

class BipError;
class Job;
class TimeValue;

class AtomExportPort : public virtual PortItf, public virtual Port, public AtomExportPortItf {
 public:
  // constructors
  AtomExportPort(const string &name);
  AtomExportPort(const string &name, bool hasEarlyUpdate);

  // destructor
  virtual ~AtomExportPort();

  // operations
  virtual bool isReady() const { return mIsReady.load(); }
  virtual void setReady();

  // specific
  virtual BipError &execute(PortValue &portValue, const TimeValue &time);
  virtual ResetableItf &reset() { return mReset; }
  Resource &resource() { return mResource; }
  void unsetReady() { mIsReady.store(false); }

 protected:
  void addInternalPort(AtomInternalPort &internalPort);

  // specific rights for deploying the system
  friend Component* deploy(int argc, char** argv);

  // specific
  ResetableItf mReset;
  Resource mResource;
  atomic<bool> mIsReady;
};

inline void AtomExportPort::setReady() {
  bool oldIsReady = mIsReady.exchange(true);

  if (!oldIsReady) {
    if (isReset()) {
      reset().resetDependent();
    }

    if (hasEarlyUpdate()) {
      mResource.free();
    }
  }
}

#endif // _BIP_Engine_AtomExportPort_HPP_
