#ifndef _BIP_Engine_InteractionValue_HPP_
#define _BIP_Engine_InteractionValue_HPP_

// inherited classes
#include <InteractionValueItf.hpp>
#include "Interaction.hpp"
#include <Interval.hpp>
#include <TimingConstraint.hpp>
#include <Resetable.hpp>

class BipError;
class Connector;
class AtomExportPort;
class TimeValue;

class InteractionValue : public InteractionValueItf {
 public:
  // constructors
  InteractionValue();

  // destructor
  virtual ~InteractionValue();

  // specific
  const Connector &connector() const { return interaction().connector(); }
  const vector<Port *> &ports() const { return interaction().ports(); }
  BipError &execute(const TimeValue &time);
  
  bool isAllDefined() const;
  bool isEnabled() const;

  void applyDomination(TimingConstraint &constraint) const;
  void applyLocalDomination(TimingConstraint &constraint) const;
  void applyPrioritiesDomination(TimingConstraint &constraint) const;
  void applyDominationBy(const InteractionValue &interaction, TimingConstraint &constraint) const;

  void inheritDominatedUrgencies(TimingConstraint &constraint) const;
  void inheritLocalDominatedUrgencies(TimingConstraint &constraint) const;
  void inheritPrioritiesDominatedUrgencies(TimingConstraint &constraint) const;
  void inheritDominatedUrgencyOf(const InteractionValue &interaction, TimingConstraint &constraint) const;

  bool operator<=(const InteractionValue &interaction) const;
  bool operator==(const InteractionValue &interaction) const;
  bool operator!=(const InteractionValue &interaction) const { return ! (*this == interaction); }
  bool operator< (const InteractionValue &interaction) const { bool eq; bool ret = includedIn(interaction, eq); return ret && !eq; }
  bool operator> (const InteractionValue &interaction) const { return interaction < *this; }

  Interval interval() const { return timingConstraint().interval(); }
  const TimingConstraint &timingConstraint() const { return mTimingConstraint; }
  const TimingConstraint &timingConstraintAfterPriorities() const { return mTimingConstraintAfterPriorities; }
  void resetTimingConstraint() const { mTimingConstraint.reset(); }
  void resetTimingConstraintAfterPriorities() const { mTimingConstraintAfterPriorities.reset(); }

  friend ostream& operator<<(ostream &, const InteractionValue&);
  friend ostream& operator<<(ostream &, const InteractionValue*);

 protected:
  bool includedIn(const InteractionValue &interaction, bool &eq) const;

  // update methods resetable objects
  void recomputeTimingConstraint(TimingConstraint &constraint) const;
  void recomputeTimingConstraintAfterPriorities(TimingConstraint &constraint) const;

  mutable Resetable<TimingConstraint, InteractionValue> mTimingConstraint;
  mutable Resetable<TimingConstraint, InteractionValue> mTimingConstraintAfterPriorities;
};

#endif // _BIP_Engine_InteractionValue_HPP_
