#ifndef _BIP_Engine_ExecuteAtomJob_HPP_
#define _BIP_Engine_ExecuteAtomJob_HPP_

// inherited classes
#include <Job.hpp>
#include <Scheduler.hpp>
#include "Resource.hpp"

class BipError;
class Atom;
class PortValue;
class BipError;
class Logger;
class ExecuteConnectorJob;
class AtomInternalPort;
class AtomExternalPort;
class TimeValue;

class ExecuteAtomJob : public Job {
 public:
  // constructors
  ExecuteAtomJob(Atom &atom);

  // destructor
  virtual ~ExecuteAtomJob();

  // getters for references
  Atom &atom() const { return mAtom; }
  Logger &logger() { return *mLogger; }
  bool hasLogger() const { return mLogger != NULL; }
  bool asap() const { return mAsap; }
  bool firstEnabled() const { return mFirstEnabled; }
  bool relaxed() const { return mRelaxed; }
  bool disabledTimeSafety() const { return mDisabledTimeSafety; }
  const Resource::Writer &writer() const { return mWriter; }
  Resource::Writer &writer() { return mWriter; }

  bool hasChosen() const { return hasChosenPortValue() || hasChosenInternal() || hasChosenExternal(); }
  PortValue &chosenPortValue() { return *mChosenPortValue; }
  bool hasChosenPortValue() const { return mChosenPortValue != NULL; }
  AtomInternalPort &chosenInternal() { return *mChosenInternal; }
  bool hasChosenInternal() const { return mChosenInternal != NULL; }
  AtomExternalPort &chosenExternal() { return *mChosenExternal; }
  bool hasChosenExternal() const { return mChosenExternal != NULL; }
  const TimeValue &chosenTime() const { return mChosenTime; }

  // setters
  void setLogger(Logger &logger) { mLogger = &logger; }
  void clearLogger() { mLogger = NULL; }
  void setAsap(bool b) { mAsap = b; }
  void setFirstEnabled(bool b) { mFirstEnabled = b; }
  void setRelaxed(bool b) { mRelaxed = b; }
  void setDisabledTimeSafety(bool b) { mDisabledTimeSafety = b; }
  void setChosenPortValue(PortValue &chosenPortValue) { assert(!hasChosen()); mChosenPortValue = &chosenPortValue; }
  void clearChosenPortValue() { mChosenPortValue = NULL; }
  void setChosenInternal(AtomInternalPort &chosenInternal) { assert(!hasChosen()); mChosenInternal = &chosenInternal; }
  void clearChosenInternal() { mChosenInternal = NULL; }
  void setChosenExternal(AtomExternalPort &chosenExternal) { assert(!hasChosen()); mChosenExternal = &chosenExternal; }
  void clearChosenExternal() { mChosenExternal = NULL; }
  void setChosenTime(const TimeValue &chosenTime) { mChosenTime = chosenTime; }

  void addReserver(Job &job);

  // operations
  void initialize();
  void initializeMutexs() { mResumeReserver.initialize(); }

  BipError &checkTimeSafetyAndResume();
  BipError &checkTimeSafety(const TimeValue &time);
  BipError &resume(const TimeValue &time);

  class ResumeJoiner : public Job::Joiner {
   public:
    // constructors
    ResumeJoiner() : mReserved(NULL) { }
    ResumeJoiner(FastMutexGroup &reserved) : mReserved(&reserved) { }

    // destructor
    virtual ~ResumeJoiner() { }

   protected:
    bool hasReserved() const { return mReserved != NULL; }
    FastMutexGroup &reserved() { return *mReserved; }

    virtual void epilogue();
    FastMutexGroup *mReserved;
  };

 protected:
  // operations
  virtual void realJob();
  BipError &execute();
  virtual void prologue();
  virtual void epilogue();

  void initializeAllExternalPorts();

  void resetIsReady();

  bool mInitialized;
  Atom &mAtom;
  Logger *mLogger;
  
  // reservation mechanisms
  Resource::Writer mWriter;
  Resource::Reserver mResumeReserver;

  PortValue *mChosenPortValue;
  AtomInternalPort *mChosenInternal;
  AtomExternalPort *mChosenExternal;
  TimeValue mChosenTime;

  bool mAsap;
  bool mFirstEnabled;
  bool mRelaxed;
  bool mDisabledTimeSafety;
};

#endif // _BIP_Engine_ExecuteAtomJob_HPP_
