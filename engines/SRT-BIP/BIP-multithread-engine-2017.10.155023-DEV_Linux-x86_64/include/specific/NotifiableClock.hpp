#ifndef _BIP_Engine_NotifiableClock_HPP_
#define _BIP_Engine_NotifiableClock_HPP_

#include <bip-engineiface-config.hpp>
using namespace bipbasetypes;
using namespace biptypes;

#include <chrono>
#include <ctime>

#include <GlobalClock.hpp>

/** \brief Provides the basic interface for implementing the clock used by
 * the scheduler.
 *
 * 
 */
class NotifiableClock : public GlobalClock {
 public:
  NotifiableClock() { }
  virtual ~NotifiableClock() { }

  virtual TimeValue time() const = 0;
  virtual bool wait(const TimeValue &time) = 0;

  void notify();
  void resetNotifications();
  bool isNotified() const { return mIsNotified.load(); }

 protected:
  bool waitForNotifications();
  bool waitForNotificationsFor(const chrono::nanoseconds &delay);

  atomic<bool> mIsNotified;
  atomic<unsigned int> mWaiters;
  mutex mMutex;
  condition_variable mConditionVariable;
};

inline
void NotifiableClock::notify() {
  mIsNotified.store(true);

  if (mWaiters.load() > 0) {
    unique_lock<mutex> lock(mMutex);
    mConditionVariable.notify_all();
  }
}

inline
void NotifiableClock::resetNotifications() {
  mIsNotified.store(false);
}

inline
bool NotifiableClock::waitForNotifications() {
  if (!isNotified()) {
    mWaiters.fetch_add(1);

    unique_lock<mutex> lock(mMutex);

    if (!isNotified()) {
      mConditionVariable.wait(lock);

      assert(isNotified());
    }
  
    mWaiters.fetch_sub(1);
  }

  assert(isNotified());

  return isNotified();
}

inline
bool NotifiableClock::waitForNotificationsFor(const chrono::nanoseconds &delay) {
  if (!isNotified()) {
    mWaiters.fetch_add(1);

    unique_lock<mutex> lock(mMutex);

    if (!isNotified()) {
#ifndef NDEBUG
      cv_status status =
#endif

      mConditionVariable.wait_for(lock, delay);

      assert(isNotified() || status == cv_status::timeout);
    }
  
    mWaiters.fetch_sub(1);
  }

  return isNotified();
}

#endif // _BIP_Engine_NotifiableClock_HPP_
