#ifndef _BIP_Engine_RealTimeClock_HPP_
#define _BIP_Engine_RealTimeClock_HPP_

#include <NotifiableClock.hpp>
#include <TimeValue.hpp>

#include <mutex>

/** \brief Provides the basic interface for implementing the clock used by
 * the scheduler.
 *
 * 
 */
class RealTimeClock : public NotifiableClock {
 public:
  RealTimeClock();
  virtual ~RealTimeClock();

  static void start() { mGetTimeOfDayAtStart = getTimeOfDay(); }

  virtual TimeValue time() const { return getTimeOfDay() - mGetTimeOfDayAtStart; }
  virtual bool wait(const TimeValue &time);

 protected:
  static TimeValue getTimeOfDay();
  static TimeValue mGetTimeOfDayAtStart;

  mutex mMutex;
  condition_variable mConditionVariable;
  bool mNotified;
};

#endif // _BIP_Engine_RealTimeClock_HPP_
