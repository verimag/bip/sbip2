#ifndef _BIP_Engine_Scheduler_HPP_
#define _BIP_Engine_Scheduler_HPP_

// used classes
#include <TimeValue.hpp>

class Interval;

class Scheduler {
 public:
  // constructors
  Scheduler(const TimeValue &time);
  Scheduler(const TimeValue &time, bool asap, bool firstEnabled);


  // destructor
  virtual ~Scheduler();

  // operations
  bool choose(const Interval &interval, bool hasPriority = false);

  // getters
  const TimeValue &plannedTime() const { return mPlannedTime; }

 protected:
  // protected getters
  const TimeValue &time() const { return mTime; }
  bool asap() const { return mAsap; }
  bool firstEnabled() const { return mFirstEnabled; }

  // protected setters
  void setTime(const TimeValue &time) { mTime = time; }

  // protected operations
  TimeValue plan(const Interval &interval) const;
  TimeValue schedulingCriterion(const Interval &interval, const TimeValue &planned) const;

  TimeValue mTime;
  TimeValue mPlannedTime;
  TimeValue mMinCriterion;
  unsigned int mNbMinCriterion;
  bool mAsap;
  bool mFirstEnabled;
};

#endif // _BIP_Engine_Scheduler_HPP_
