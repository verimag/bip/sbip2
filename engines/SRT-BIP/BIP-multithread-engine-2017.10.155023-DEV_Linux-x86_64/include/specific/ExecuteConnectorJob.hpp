#ifndef _BIP_Engine_ExecuteConnectorJob_HPP_
#define _BIP_Engine_ExecuteConnectorJob_HPP_

// inherited classes
#include <Job.hpp>
#include <BipError.hpp>
#include "ReadyQueue.hpp"
#include <Resetable.hpp>
#include <Scheduler.hpp>
#include <TimeValue.hpp>
#include <ExecuteAtomJob.hpp>
#include <Resource.hpp>
#include <UpdateClockJob.hpp>

class Connector;
class Atom;
class Port;
class Logger;
class InteractionValue;
class Atom;

class ExecuteConnectorJob : public Job {
 public:
  // constructors
  ExecuteConnectorJob(Connector &connector);

  // destructor
  virtual ~ExecuteConnectorJob();

  // getters for references
  Connector &connector() const { return mConnector; }
  Logger &logger() { return *mLogger; }
  bool hasLogger() const { return mLogger != NULL; }
  bool asap() const { return mAsap; }
  bool firstEnabled() const { return mFirstEnabled; }
  bool relaxed() const { return mRelaxed; }
  bool disabledTimeSafety() const { return mDisabledTimeSafety; }
  
  // setters
  void setLogger(Logger &logger) { mLogger = &logger; }
  void clearLogger() { mLogger = NULL; }
  void setAsap(bool b) { mAsap = b; }
  void setFirstEnabled(bool b) { mFirstEnabled = b; }
  void setRelaxed(bool b) { mRelaxed = b; }
  void setDisabledTimeSafety(bool b) { mDisabledTimeSafety = b; }

  // operations
  void initialize();
  void initializeMutexs();

 protected:
  // operations
  virtual void realJob();
  virtual void prologue() { mReserver.start(); }
  virtual void epilogue() { mReserver.end(); }

  void unreserveUnusedResources();

  bool tryToExecuteChosenInteraction();
  void executeChosenInteraction();
  BipError &detectCyclesInPriorities() const;

  bool hasChosen() const { assert(mChosenInteraction == NULL || mChosenTime != TimeValue::MIN); return mChosenInteraction != NULL; }
  InteractionValue &chosenInteraction() const { assert(hasChosen()); return *mChosenInteraction; }
  const TimeValue &chosenTime() const { return mChosenTime; }
  void recomputeChoice();
  
  bool hasResumeEagerInteraction() const;
  bool lastAtomHasResume() const;

  vector<Atom *> allAtoms();
  void allAtoms(vector<Atom *> &atoms, const Connector &connector);
  void allAtoms(vector<Atom *> &atoms, const Port &port);

  Connector &mConnector;
  Logger *mLogger;

  // all atoms involved
  vector<Atom *> mAllAtoms;

  // reservation mechanisms
  class ConnectorReserver : public Resource::Reserver {
   public:
    ConnectorReserver(ExecuteConnectorJob &job) : Resource::Reserver(job), mExecuteConnectorJob(job) { }
    virtual ~ConnectorReserver() { }

    virtual void uponReservation(const TimeValue &time) { mExecuteConnectorJob.executeChosenInteraction(); }

   protected:
    ExecuteConnectorJob &mExecuteConnectorJob;
  };
  
  ConnectorReserver mReserver;
  
  ResetableItf mResetChoice;
  InteractionValue *mChosenInteraction;
  TimeValue mChosenTime;
  Interval mTimeSafe;

  bool mAsap;
  bool mFirstEnabled;
  bool mRelaxed;
  bool mDisabledTimeSafety;

  // resume mechanisms
  ExecuteAtomJob::ResumeJoiner mResumeJoiner;

  UpdateClockJob mUpdateClockJob;
};


#endif // _BIP_Engine_ExecuteConnectorJob_HPP_
