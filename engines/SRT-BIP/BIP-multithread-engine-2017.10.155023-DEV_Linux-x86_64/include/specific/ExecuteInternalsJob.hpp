#ifndef _BIP_Engine_ExecuteInternalsJob_HPP_
#define _BIP_Engine_ExecuteInternalsJob_HPP_

// inherited classes
#include <Job.hpp>
#include <Scheduler.hpp>
#include <Resetable.hpp>
#include <ExecuteAtomJob.hpp>
#include <Resource.hpp>
#include <UpdateClockJob.hpp>

class BipError;
class Atom;
class PortValue;
class BipError;
class Logger;
class AtomInternalPort;
class AtomExternalPort;
class TimeValue;

class ExecuteInternalsJob : public Job {
 public:
  // constructors
  ExecuteInternalsJob(Atom &atom);

  // destructor
  virtual ~ExecuteInternalsJob();

  // getters for references
  Atom &atom() const { return mAtom; }
  Logger &logger() { return *mLogger; }
  bool hasLogger() const { return mLogger != NULL; }
  bool asap() const { return mAsap; }
  bool firstEnabled() const { return mFirstEnabled; }
  bool relaxed() const { return mRelaxed; }

  // setters
  void setLogger(Logger &logger) { mLogger = &logger; }
  void clearLogger() { mLogger = NULL; }
  void setAsap(bool b) { mAsap = b; }
  void setFirstEnabled(bool b) { mFirstEnabled = b; }
  void setRelaxed(bool b) { mRelaxed = b; }

  // operations
  void initialize();
  void initializeMutexs() { mReserver.initialize(); }

 protected:
  // operations
  virtual void realJob();
  virtual void prologue() { mReserver.start(); }
  virtual void epilogue() { mReserver.end(); }

  bool tryToExecuteChosenInternal();
  void executeChosenInternal();
  
  bool hasChosen() const { assert(mChosenInternal == NULL || mChosenTime != TimeValue::MIN); return mChosenInternal != NULL; }
  AtomInternalPort &chosenInternal() const { assert(hasChosen()); return *mChosenInternal; }
  const TimeValue &chosenTime() const { return mChosenTime; }
  void recomputeChoice();
  
  bool hasResumeEager() const;

  Atom &mAtom;
  Logger *mLogger;

  // reservation mechanisms
  class AtomReserver : public Resource::Reserver {
   public:
    AtomReserver(ExecuteInternalsJob &job) : Resource::Reserver(job), mExecuteInternalsJob(job) { }
    virtual ~AtomReserver() { }

    virtual void uponReservation(const TimeValue &time) { mExecuteInternalsJob.executeChosenInternal(); }

   protected:
    ExecuteInternalsJob &mExecuteInternalsJob;
  };
  
  AtomReserver mReserver;
  
  ResetableItf mResetChoice;
  AtomInternalPort * mChosenInternal;
  TimeValue mChosenTime;
  Interval mTimeSafe;

  bool mAsap;
  bool mFirstEnabled;
  bool mRelaxed;

  UpdateClockJob mUpdateClockJob;
};

#endif // _BIP_Engine_ExecuteInternalsJob_HPP_
