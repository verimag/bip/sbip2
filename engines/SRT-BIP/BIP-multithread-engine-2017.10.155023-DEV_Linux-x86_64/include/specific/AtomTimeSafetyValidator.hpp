#ifndef _BIP_Engine_AtomTimeSafetyValidator_HPP_
#define _BIP_Engine_AtomTimeSafetyValidator_HPP_

#include <bip-engineiface-config.hpp>
using namespace bipbasetypes;
using namespace biptypes;

#include <Resource.hpp>

class Logger;
class Atom;

class AtomTimeSafetyValidator : public Resource::Validator {
 public:
  // constructors
  AtomTimeSafetyValidator(Atom &atom);

  // destructor
  virtual ~AtomTimeSafetyValidator();

  // getters
  Logger &logger() { return *mLogger; }
  bool hasLogger() const { return mLogger != NULL; }
  Atom &atom() const { return mAtom; }
  const Interval &timeSafe() const { return mTimeSafe; }
  
  // setters
  void setLogger(Logger &logger) { mLogger = &logger; }
  void clearLogger() { mLogger = NULL; }  

  // operations
  virtual void reset(const vector<Resource *> &resources);
  virtual void free(Resource &resource);

  void initialize();
  
 protected:
  void setTimeSafe(const Interval &interval) { mTimeSafe = interval; }
  
  void validate();
  
  Logger *mLogger;
  Atom &mAtom;
  atomic<bool> mError;
  atomic<unsigned int> mMissingResources;
  Interval mTimeSafe;
};

#endif // _BIP_Engine_AtomTimeSafetyValidator_HPP_
