#ifndef _BIP_Engine_ExecuteExternalsJob_HPP_
#define _BIP_Engine_ExecuteExternalsJob_HPP_

// inherited classes
#include <Job.hpp>
#include <Scheduler.hpp>
#include <Resetable.hpp>
#include <Resource.hpp>
#include <UpdateClockJob.hpp>

class BipError;
class Atom;
class PortValue;
class BipError;
class Logger;
class ExecuteConnectorJob;
class AtomInternalPort;
class AtomExternalPort;
class TimeValue;

class ExecuteExternalsJob : public Job {
 public:
  // constructors
  ExecuteExternalsJob(Atom &atom);

  // destructor
  virtual ~ExecuteExternalsJob();

  // getters for references
  Atom &atom() const { return mAtom; }
  Logger &logger() { return *mLogger; }
  bool hasLogger() const { return mLogger != NULL; }
  bool asap() const { return mAsap; }
  bool firstEnabled() const { return mFirstEnabled; }
  bool relaxed() const { return mRelaxed; }

  // setters
  void setWakeUpTime(const TimeValue &time) { mWakeUpTime = time; }
  void setLogger(Logger &logger) { mLogger = &logger; }
  void clearLogger() { mLogger = NULL; }
  void setAsap(bool b) { mAsap = b; }
  void setFirstEnabled(bool b) { mFirstEnabled = b; }
  void setRelaxed(bool b) { mRelaxed = b; }

  // operations
  void initialize();
  void initializeMutexs() { mReserver.initialize(); }

 protected:
  // operations
  virtual void realJob();
  virtual void prologue() { mReserver.start(); }
  virtual void epilogue() { mReserver.end(); }

  bool tryToExecuteChosenExternal();
  void executeChosenExternal();

  bool hasChosen() const { assert(mChosenExternal == NULL || mChosenTime != TimeValue::MIN); return mChosenExternal != NULL; }
  AtomExternalPort &chosenExternal() const { assert(hasChosen()); return *mChosenExternal; }
  const TimeValue &chosenTime() const { return mChosenTime; }
  void recomputeChoice();

  bool hasResumeEager() const;
  
  Atom &mAtom;
  Logger *mLogger;

  // reservation mechanisms
  class AtomReserver : public Resource::Reserver {
   public:
    AtomReserver(ExecuteExternalsJob &job) : Resource::Reserver(job), mExecuteExternalsJob(job) { }
    virtual ~AtomReserver() { }

    virtual void uponReservation(const TimeValue &time) { mExecuteExternalsJob.executeChosenExternal(); }

   protected:
    ExecuteExternalsJob &mExecuteExternalsJob;
  };
  
  AtomReserver mReserver;

  ResetableItf mResetChoice;
  AtomExternalPort * mChosenExternal;
  TimeValue mChosenTime;
  Interval mTimeSafe;

  bool mAsap;
  bool mFirstEnabled;
  bool mRelaxed;

  UpdateClockJob mUpdateClockJob;
};

#endif // _BIP_Engine_ExecuteExternalsJob_HPP_
