#ifndef _BIP_Engine_Atom_HPP_
#define _BIP_Engine_Atom_HPP_

// inherited classes
#include <AtomItf.hpp>
#include "AtomExportPort.hpp"
#include "AtomExportData.hpp"
#include "AtomInternalPort.hpp"
#include "Component.hpp"
#include "ExecuteAtomJob.hpp"
#include "ExecuteInternalsJob.hpp"
#include "ExecuteExternalsJob.hpp"
#include <Initializable.hpp>
#include <ModelClock.hpp>
#include "Resource.hpp"
#include <AtomTimeSafetyValidator.hpp>

class BipError;
class Compound;
class TimeValue;
class Connector;
class CycleInPriorities;

class Atom : public virtual ComponentItf, public Component, public AtomItf {
 public:
  // constructors
  Atom(const string &name);
  Atom(const string &name, const bool &initialHasResume);

  // destructor
  virtual ~Atom();

  virtual BipError &execute(PortValue &portValue, const TimeValue &time) = 0;
  virtual BipError &execute(AtomExternalPort &port, const TimeValue &time) = 0;
  virtual string toString() const { return "-"; };

  // specific
  BipError &execute(AtomInternalPort &internalPort, const TimeValue &time);
  void allNotReady();
  void allReady();
  BipError &checkExternals();
  BipError &recomputeExternals();

  const ExecuteAtomJob &executeJob() const { return mExecuteJob; }
  ExecuteAtomJob &executeJob() { return mExecuteJob; }
  const ExecuteInternalsJob &executeInternalsJob() const { return mExecuteInternalsJob; }
  ExecuteInternalsJob &executeInternalsJob() { return mExecuteInternalsJob; }
  const ExecuteExternalsJob &executeExternalsJob() const { return mExecuteExternalsJob; }
  ExecuteExternalsJob &executeExternalsJob() { return mExecuteExternalsJob; }
  AtomTimeSafetyValidator &timeSafetyValidator() { return mTimeSafetyValidator; }
  ResetableItf &invariantAndTimeReset() const { return mInvariantAndTimeReset; }
  const Resource &resource() const { return mResource; }
  Resource &resource() { return mResource; }
  Resource &resumeResource() { return mResumeResource; }

  Resetable<vector<AtomInternalPort *>, Atom> &internals() const { return mInternals; }
  ResetableItf &externalsReset() const { return mExternalsReset; }
  const vector<AtomExternalPort *> &externals() const { return mExternals; }

  const ModelClock &modelClock() const { return mExecuteJob.modelClock(); }
  TimeValue time() const { return mExecuteJob.modelClock().time(); }

  bool hasResume() const { return mHasResume; }
  void setHasResume(bool b) { mHasResume = b; }
  bool hadResume() const { return mHadResume; }
  void setHadResume(bool b) { mHadResume = b; }

 protected:
  // references accessors
  void addInternalPort(AtomInternalPort &internalPort) {
    mInternalPorts[internalPort.name()] = &internalPort;
    internalPort.setHolder(*this);

    // dependencies
    mInternals.dependsOn(internalPort.timingConstraint());
  }

  void addExternalPort(AtomExternalPort &externalPort) {
    mExternalPorts[externalPort.name()] = &externalPort;
    externalPort.setHolder(*this);
  }

  void addPort(AtomExportPort &port) {
    mPorts[port.name()] = &port;
    port.setHolder(*this);
    Component::addPort(port);

    if (port.hasEarlyUpdate()) {
      port.reset().addResource(port.resource());
    }
    else {
      port.reset().addResource(mResource);
    }
  }

  void addData(AtomExportData &data) {
    mData[data.name()] = &data;
    data.setHolder(*this);
    Component::addData(data);

    if (data.hasEarlyUpdate()) {
      data.reset().addResource(data.resource());
    }
    else {
      data.reset().addResource(mResource);
    }
  }

  // specific
  void recomputeInternals(vector<AtomInternalPort *> &internals) const;

  mutable ResetableItf mInvariantAndTimeReset;
  mutable Resetable<vector<AtomInternalPort *>, Atom> mInternals;

  mutable ResetableItf mExternalsReset;
  vector<AtomExternalPort *> mExternals;
  vector<AtomExternalPort *> mWaiting;
  vector<AtomExternalPort *> mUnexpected;

  ExecuteAtomJob mExecuteJob;
  ExecuteInternalsJob mExecuteInternalsJob;
  ExecuteExternalsJob mExecuteExternalsJob;
  AtomTimeSafetyValidator mTimeSafetyValidator;
  
  Resource mResource;
  Resource mResumeResource;

  bool mHasResume;
  bool mHadResume;

  friend ostream& operator<<(ostream &o, const Atom &atom);
  friend ostream& operator<<(ostream &o, const Atom *atom);
};

#endif // _BIP_Engine_Atom_HPP_
