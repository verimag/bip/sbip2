#ifndef _BIP_Engine_Clock_HPP_
#define _BIP_Engine_Clock_HPP_

// inherited classes
#include <ClockItf.hpp>
#include <TimeValue.hpp>
#include <GlobalClock.hpp>
#include <Atom.hpp>

class Clock : public ClockItf {
 public:
  // constructors
  Clock();
  Clock(Atom &atom);
  Clock(const Clock &clock);

  // destructor
  virtual ~Clock();

  // operations
  virtual TimeValue time() const;
  virtual double speed() const { return mSpeed; }
  virtual void resetTo(const TimeValue &time);
  virtual void setSpeed(const double &speed);


 protected:
  Atom &atom() const { return *mAtom; }
  bool hasAtom() const { return mAtom != NULL; }
  void setAtom(Atom &atom) { mAtom = &atom; }

  const GlobalClock &modelClock() const { assert(hasAtom()); return atom().modelClock(); }

  TimeValue mResetTime;
  TimeValue mResetTo;
  double mSpeed;

  Atom *mAtom;
};

inline Clock::Clock(const Clock &clock) :
  mResetTime(clock.mResetTime),
  mResetTo(clock.mResetTo),
  mSpeed(clock.mSpeed) {
}

inline
TimeValue Clock::time() const {
  return ((modelClock().time() - mResetTime))*speed() + mResetTo;
}

inline
void Clock::resetTo(const TimeValue &time) {
  mResetTime = modelClock().time();
  mResetTo = time;
}

inline
void Clock::setSpeed(const double &speed) {
  resetTo(time());

  mSpeed = speed;
}


#endif // _BIP_Engine_Clock_HPP_
