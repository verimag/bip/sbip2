#ifndef _BIP_Engine_ModelClock_HPP_
#define _BIP_Engine_ModelClock_HPP_

#include <GlobalClock.hpp>
#include <TimeValue.hpp>

/** \brief Model is used to represent the global time elapsed in the model.
 * It should only give values meeting the semantics of the model.
 *
 */
class ModelClock : public GlobalClock {
 public:
  ModelClock();
  ModelClock(GlobalClock &platformClock);
  virtual ~ModelClock();

  virtual TimeValue time() const { return mTime; }
  virtual bool wait(const TimeValue &newTime);
  void update();

  void setPlatformClock(GlobalClock &platformClock) { assert(mPlatformClock == NULL); mPlatformClock = &platformClock; }

 protected:
  GlobalClock &platformClock() const { assert(mPlatformClock != NULL); return *mPlatformClock; }
  void setTime(const TimeValue &time);

  GlobalClock *mPlatformClock;
  TimeValue mTime;
};

inline
bool ModelClock::wait(const TimeValue &newTime) {
  // new time
  setTime(newTime);

  // wait for platform clock to reach target time value
  return platformClock().wait(time());
}

inline
void ModelClock::update() {
  setTime(platformClock().time());
}

inline
void ModelClock::setTime(const TimeValue &time) {
  mTime = time;
}

#endif // _BIP_Engine_ModelClock_HPP_
