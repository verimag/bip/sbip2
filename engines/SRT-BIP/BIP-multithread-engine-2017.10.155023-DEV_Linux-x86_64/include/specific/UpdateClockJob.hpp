#ifndef _BIP_Engine_UpdateClockJob_HPP_
#define _BIP_Engine_UpdateClockJob_HPP_

// inherited classes
#include <Job.hpp>
#include <Resource.hpp>
#include <TimeValue.hpp>

class ModelClock;

class UpdateClockJob : public Job {
 public:
  // constructors
  UpdateClockJob();

  // destructor
  virtual ~UpdateClockJob();

  // operations
  void initialize();

  // getters
  const TimeValue &time() const { return mTime; }
  
  // setters
  void setTime(const TimeValue &time) { mTime = time; }

 protected:
  // operations
  virtual void realJob();
  virtual void prologue() { mWriter.start(); }
  virtual void epilogue() { mWriter.end(); }
  
  // reservation mechanisms
  Resource::Writer mWriter;

  // attributes
  TimeValue mTime;
};

#endif // _BIP_Engine_UpdateClockJob_HPP_
