#ifndef _BIP_Engine_TimeSafetyViolationErrorItf_HPP_
#define _BIP_Engine_TimeSafetyViolationErrorItf_HPP_

#include <bip-engineiface-config.hpp>
using namespace bipbasetypes;
using namespace biptypes;

// inherited classes
#include "BipErrorItf.hpp"

// used classes
#include "BipError.hpp"
#include "TimeValue.hpp"

class Compound;

class TimeSafetyViolationErrorItf : public virtual BipErrorItf {
 public:
  // destructor
  virtual ~TimeSafetyViolationErrorItf();

  // getters for references
  const TimeValue &time() const { return mTime; }
  const Compound &compound() const { return mCompound; }

 protected:
  // protected constructors
  TimeSafetyViolationErrorItf(const TimeValue &time, const Compound &compound);

  // references
  TimeValue mTime;
  const Compound &mCompound;
};

#endif // _BIP_Engine_TimeSafetyViolationErrorItf_HPP_
