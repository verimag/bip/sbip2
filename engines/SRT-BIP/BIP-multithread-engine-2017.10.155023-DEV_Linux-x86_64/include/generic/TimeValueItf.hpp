#ifndef _BIP_Engine_TimeValueItf_HPP_
#define _BIP_Engine_TimeValueItf_HPP_

#include <bip-engineiface-config.hpp>
using namespace bipbasetypes;
using namespace biptypes;

// used classes
class TimeValue;


class TimeValueItf {
 public:
  // destructor
  virtual ~TimeValueItf();

  // operations
  virtual TimeValue &operator=(const TimeValue &value) = 0;
  virtual bool operator<(const TimeValue &value) const = 0;
  virtual bool operator>(const TimeValue &value) const = 0;
  virtual bool operator<=(const TimeValue &value) const = 0;
  virtual bool operator>=(const TimeValue &value) const = 0;
  virtual bool operator==(const TimeValue &value) const = 0;
  virtual bool operator!=(const TimeValue &value) const = 0;
  virtual TimeValue &operator+=(const TimeValue &value) = 0;
  virtual TimeValue &operator-=(const TimeValue &value) = 0;

 protected:
  // protected constructors
  TimeValueItf();
};

#endif // _BIP_Engine_TimeValueItf_HPP_
