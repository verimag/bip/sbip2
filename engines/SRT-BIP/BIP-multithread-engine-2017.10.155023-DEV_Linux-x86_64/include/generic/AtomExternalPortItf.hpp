#ifndef _BIP_Engine_AtomExternalPortItf_HPP_
#define _BIP_Engine_AtomExternalPortItf_HPP_

#include <bip-engineiface-config.hpp>
using namespace bipbasetypes;
using namespace biptypes;

// referenced classes
class Atom;
class PortValue;
class Interval;
class TimeValue;


class AtomExternalPortItf {
 public:
  // destructor
  virtual ~AtomExternalPortItf();

  // operations
  virtual void notify() = 0;
  virtual TimeValue time() = 0;
  virtual void initialize() = 0;
  virtual bool hasEvent() const = 0;
  virtual void popEvent() = 0;
  virtual void purgeEvents() = 0;
  virtual TimeValue eventTime() const = 0;

  // getters for references
  Interval &interval() const { return *mInterval; }
  bool hasInterval() const { return mInterval != NULL; }

  // getters for attributes
  const string &name() const { return mName; }
  const EventConsumptionPolicy &policy() const { return mPolicy; }
  const bool &waiting() const { return mWaiting; }
  Urgency urgency() const { return mUrgency; }
  const bool &hasResume() const { return mHasResume; }

  // setters for references
  void setInterval(Interval &interval) { mInterval = &interval; }
  void clearInterval() { mInterval = NULL; }

  // setters for attributes
  void setWaiting(const bool &waiting) { mWaiting = waiting; }
  void setUrgency(Urgency urgency) { mUrgency = urgency; }
  void setHasResume(const bool &resume) { mHasResume = resume; }

  // opposites accessors
  Atom &holder() const { return *mHolder; }
  bool hasHolder() const { return mHolder != NULL; }
  void setHolder(Atom &atom) { mHolder = &atom; }

 protected:
  // protected constructors
  AtomExternalPortItf(const string &name, const EventConsumptionPolicy &policy);

  // attributes
  const string mName;
  const EventConsumptionPolicy mPolicy;
  bool mWaiting;
  Urgency mUrgency;
  bool mHasResume;

  // references
  Interval *mInterval;

  // opposites
  Atom *mHolder;
};

#endif // _BIP_Engine_AtomExternalPortItf_HPP_
