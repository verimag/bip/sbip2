#ifndef _BIP_Engine_LauncherItf_HPP_
#define _BIP_Engine_LauncherItf_HPP_

#include <bip-engineiface-config.hpp>
using namespace bipbasetypes;
using namespace biptypes;

class Component;

class LauncherItf {
 public:
  // destructor
  virtual ~LauncherItf();

  // getters for references
  Component &root() { return mRoot; }

  // operations
  virtual int initialize() = 0;
  virtual int launch() = 0;
  
 protected:
  // protected constructors
  LauncherItf(int argc, char **argv, Component &root);

  // attributes
  int mArgc;
  char **mArgv;
  Component &mRoot;
};

#endif // _BIP_Engine_LauncherItf_HPP_
