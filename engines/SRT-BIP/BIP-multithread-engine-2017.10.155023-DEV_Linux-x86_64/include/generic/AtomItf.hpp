#ifndef _BIP_Engine_AtomItf_HPP_
#define _BIP_Engine_AtomItf_HPP_

#include <bip-engineiface-config.hpp>
using namespace bipbasetypes;
using namespace biptypes;

// inherited classes
#include "ComponentItf.hpp"

// used classes
#include "AtomExportData.hpp"
#include "AtomExportPort.hpp"
#include "AtomInternalPort.hpp"
#include "AtomExternalPort.hpp"

#include "Component.hpp"

// referenced classes
class BipError;
class PortValue;
class TimeValue;

class Component;

class AtomItf : public virtual ComponentItf {
 public:
  // destructor
  virtual ~AtomItf();

  // operations
  virtual BipError &initialize() = 0;
  virtual BipError &execute(PortValue &portValue, const TimeValue &time) = 0;
  virtual BipError &execute(AtomExternalPort &port, const TimeValue &time) = 0;
  virtual string toString() const = 0;
  virtual const Interval &invariant() const = 0;
  virtual bool hasInvariant() const = 0;
  virtual BipError &resume(const TimeValue &time) = 0;
  virtual const Interval &resume() const = 0;

  // getters for references
  const map<string, AtomInternalPort *> &internalPorts() const { return mInternalPorts; }
  bool hasInternalPorts() const { return !mInternalPorts.empty(); }
  const map<string, AtomExternalPort *> &externalPorts() const { return mExternalPorts; }
  bool hasExternalPorts() const { return !mExternalPorts.empty(); }
  const map<string, AtomExportPort *> &ports() const { return mPorts; }
  bool hasPorts() const { return !mPorts.empty(); }
  const map<string, AtomExportData *> &data() const { return mData; }
  bool hasData() const { return !mData.empty(); }
  
  // getters for attributes
  const bool &initialHasResume() const { return mInitialHasResume; }

protected:
  // protected constructors
  AtomItf(const string &name);
  AtomItf(const string &name, const bool &initialHasResume);

  // protected getters for references
  map<string, AtomInternalPort *> &internalPorts() { return mInternalPorts; }
  map<string, AtomExternalPort *> &externalPorts() { return mExternalPorts; }
  map<string, AtomExportPort *> &ports() { return mPorts; }
  map<string, AtomExportData *> &data() { return mData; }

  // protected setters for references
  virtual void addInternalPort(AtomInternalPort &internalPort) = 0;
  virtual void addExternalPort(AtomExternalPort &externalPort) = 0;
  virtual void addPort(AtomExportPort &port) = 0;
  virtual void addData(AtomExportData &data) = 0;

  // attributes
  const bool mInitialHasResume;

  // references
  map<string, AtomInternalPort *> mInternalPorts;
  map<string, AtomExternalPort *> mExternalPorts;
  map<string, AtomExportPort *> mPorts;
  map<string, AtomExportData *> mData;

  // specific rights for deploying the system
  friend Component* deploy(int argc, char** argv);
};

#endif // _BIP_Engine_AtomItf_HPP_
