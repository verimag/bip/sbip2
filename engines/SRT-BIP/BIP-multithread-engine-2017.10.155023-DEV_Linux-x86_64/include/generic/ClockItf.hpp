#ifndef _BIP_Engine_ClockItf_HPP_
#define _BIP_Engine_ClockItf_HPP_

#include <bip-engineiface-config.hpp>
using namespace bipbasetypes;
using namespace biptypes;

// used classes
class Clock;
class Atom;

#include <TimeValue.hpp>


class ClockItf {
 public:
  // destructor
  virtual ~ClockItf();

  // operations
  virtual TimeValue time() const = 0;
  virtual double speed() const = 0;
  virtual void resetTo(const TimeValue &time) = 0;
  virtual void setSpeed(const double &speed) = 0;

 protected:
  // protected constructors
  ClockItf();
  ClockItf(Atom &atom);
};

#endif // _BIP_Engine_ClockItf_HPP_
