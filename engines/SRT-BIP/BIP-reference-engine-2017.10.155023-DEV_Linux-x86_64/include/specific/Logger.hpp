#ifndef _BIP_Engine_Logger_HPP_
#define _BIP_Engine_Logger_HPP_

#include <bip-engineiface-config.hpp>

using namespace bipbasetypes;
using namespace biptypes;

#include <string>
#include <iostream>
#include <sstream>

class Engine;
class BipError;
class TimeValue;
class InteractionValue;
class AtomInternalPort;
class AtomExternalPort;
class Compound;

/** \brief Represent a node in a tree of strings
 */
class Logger {
 public:
  // constructor and destructor
  Logger(Engine &engine, ostream &outoutStream, bool verbose, unsigned int limit);
  virtual ~Logger();
    
  // operations
  bool reachedLimit() const { return mLimit != 0 && mState >= mLimit; }
  void log(const string &s);
  void log(const BipError &error);
  void logTimeSafetyViolation(const TimeValue &timeSafetyViolation);
  void logEnabled();
  void logEnabledExternals();
  void log(const InteractionValue &interaction, const TimeValue &time);
  void log(const AtomInternalPort &port, const TimeValue &time);
  void log(const AtomExternalPort &port, const TimeValue &time);
  void logDeadlock() { if (verbose()) { begin(); outputStream() << "state #" << mState << ": deadlock!"; end(); } }
      
 protected:
  // operations
  void begin() { outputStream() << "[BIP ENGINE]: "; }
  void newLine() { outputStream() << endl << "[BIP ENGINE]: "; }
  void end() { outputStream() << endl; }
  void newState() { ++mState; }

  void displayViolatedTimingConstraints(const TimeValue &timeSafetyViolation, const Compound &top);
  void displayViolatedInvariants(const TimeValue &timeSafetyViolation, const Compound &top);
  void displayViolatedResume(const TimeValue &timeSafetyViolation, const Compound &top);
 
  // getters
  const Engine &engine() const { return mEngine; }
  bool verbose() const { return mVerbose; }
  ostream &outputStream() { return mOutputStream; }
 
  const Engine &mEngine;
  ostream &mOutputStream;
  bool mVerbose;
  unsigned int mLimit;
  unsigned int mState;
 };
 

#endif // _BIP_Engine_Logger_HPP_
