#ifndef _BIP_Engine_Explorer_HPP_
#define _BIP_Engine_Explorer_HPP_

#include "Scheduler.hpp"
#include "Compound.hpp"
#include "Engine.hpp"
#include <State.hpp>
#include <TimeValue.hpp>

class BipError;

/** \brief Compute all execution sequences of a model.
 */
class Explorer : public Scheduler {
 public:
  Explorer(Engine &engine, const TimeValue &granularity, bool verbose = true, unsigned int limit = 0);
  virtual ~Explorer();

  virtual BipError &initialize();
  virtual BipError &run();

  bool verbose() const { return mVerbose; }
  unsigned int limit() const { return mLimit; }
  const TimeValue &granularity() const { return mGranularity; }


 protected:
  unsigned int dfsExplore(vector<State> &states, vector<State> & deadlocks, vector<State> &timelocks, vector<State> &errors, unsigned int size);

  void handleError(BipError &error);

  bool equals(char *state1, char *state2, size_t size);

  TimeValue mGranularity;
  bool mVerbose;
  unsigned int mLimit;
  unsigned int nbErrors;
};

#endif // _BIP_Engine_Explorer_HPP_
