#ifndef _BIP_Engine_InteractionValue_HPP_
#define _BIP_Engine_InteractionValue_HPP_

// inherited classes
#include <InteractionValueItf.hpp>
#include "Interaction.hpp"
#include <Interval.hpp>
#include <TimingConstraint.hpp>
#include <AtomInternalPort.hpp>

class BipError;
class TimeValue;

class InteractionValue : public InteractionValueItf {
 public:
  // constructors
  InteractionValue();

  // destructor
  virtual ~InteractionValue();

  // specific
  const Connector &connector() const { return interaction().connector(); }
  const vector<Port *> &ports() const { return interaction().ports(); }
  BipError &execute(const TimeValue &time);

  string getDistribution() const;
  AtomInternalPort *getStochasticPort() const;

  bool isAllDefined() const;
  bool isEnabled() const;
  bool isDominatedLocallyBy(const InteractionValue &interaction) const;
  bool hasResume() const;

  void applyDomination(TimingConstraint &constraint) const;
  void applyLocalDomination(TimingConstraint &constraint) const;
  void applyPrioritiesDomination(TimingConstraint &constraint) const { interaction().applyPrioritiesDomination(constraint); }

  void inheritDominatedUrgencies(TimingConstraint &constrain) const;
  void inheritLocalDominatedUrgencies(TimingConstraint &constraint) const;
  void inheritPrioritiesDominatedUrgencies(TimingConstraint &constraint) const { interaction().inheritPrioritiesDominatedUrgencies(constraint); }

  bool operator==(const InteractionValue &interaction) const { return *this <= interaction && interaction <= *this; }
  bool operator!=(const InteractionValue &interaction) const { return ! (*this == interaction); }

  Interval interval() const { return timingConstraint().interval(); }
  TimingConstraint timingConstraint() const;
  TimingConstraint timingConstraintAfterPriorities() const;

 protected:
  bool operator<=(const InteractionValue &interaction) const;
  bool operator< (const InteractionValue &interaction) const { return *this <= interaction && *this != interaction; }

  friend ostream& operator<<(ostream &, const InteractionValue&);
  friend ostream& operator<<(ostream &, const InteractionValue*);
};

#endif // _BIP_Engine_InteractionValue_HPP_
