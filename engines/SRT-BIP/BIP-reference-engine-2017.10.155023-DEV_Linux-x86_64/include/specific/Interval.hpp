#ifndef _BIP_Engine_Interval_HPP_
#define _BIP_Engine_Interval_HPP_

// inherited classes
#include <IntervalItf.hpp>

class Interval : public IntervalItf {
 public:
  // constructors
  Interval();
  Interval(const Interval &interval);
  Interval(bool b);
  Interval(const TimeValue &left, const TimeValue &right);
  Interval(const TimeValue &left, const TimeValue &right, bool leftOpen, bool rightOpen);

  // destructor
  virtual ~Interval();

  // operations
  virtual Interval &operator=(const Interval &interval);
  Interval &operator=(bool b);

  // specific
  bool empty() const;
  bool in(const TimeValue& time) const;
  TimeValue size() const;
  TimeValue next(const TimeValue &time) const;
  Interval urgent(Urgency urgency) const;
  Interval wait(const TimeValue &time, Urgency urgency) const;
  TimeValue shift(const TimeValue &time) const { assert(TimeValue::ZERO <= time && time <= size()); return left() + time; }
  TimeValue random(const TimeValue &granularity) const;

  bool operator==(const Interval &interval);
  bool operator!=(const Interval &interval);

  Interval &operator&=(const Interval &interval);
  Interval &operator+=(const TimeValue &time);
  Interval &operator-=(const TimeValue &time);

  Interval operator&&(const Interval &interval) const { Interval ret = *this; ret &= interval; return ret; }
  Interval operator&&(bool b) const { if (b) return Interval(*this); else return Interval(TimeValue::MAX, TimeValue::MIN); }
  Interval operator||(bool b) const { if (b) return Interval(TimeValue::MIN, TimeValue::MAX); else return Interval(*this); }

  Interval operator-() const;
};

inline bool Interval::operator==(const Interval &interval) {
  return left() == interval.left() &&
    right() == interval.right() &&
    leftOpen() == interval.leftOpen() &&
    rightOpen() == interval.rightOpen();
}

inline bool Interval::operator!=(const Interval &interval) {
  return !(*this == interval);
}

std::ostream &operator<<(std::ostream &o, const Interval &interval);

Interval operator&&(bool b, const Interval &interval);
Interval operator||(bool b, const Interval &interval);

#endif // _BIP_Engine_Interval_HPP_
