#ifndef _BIP_Engine_Component_HPP_
#define _BIP_Engine_Component_HPP_

// inherited classes
#include <ComponentItf.hpp>
#include <Interval.hpp>

class InteractionValue;
class AtomInternalPort;
class AtomExternalPort;
class BipError;

class Component : public virtual ComponentItf {
 public:
  // constructors
  Component(const string &name, const ComponentType &type);

  // destructor
  virtual ~Component();

  // specific operations
  virtual vector<AtomInternalPort *> internals() const = 0;
  virtual vector<AtomExternalPort *> allExternals() const = 0;
  virtual const TimeValue &time() const = 0;
  BipError &updateUpperLevels();
  string fullName() const;
};

#endif // _BIP_Engine_Component_HPP_
