#ifndef _BIP_Engine_ReferenceEngine_HPP_
#define _BIP_Engine_ReferenceEngine_HPP_

#include "Engine.hpp"
#include "Compound.hpp"
#include "ModelClock.hpp"
#include "Interval.hpp"
#include "TimingConstraint.hpp"

#include <bip-engineiface-config.hpp>

class InteractionValue;
class AtomInternalPort;
class BipError;

/** \brief Direct implementation of the BIP semantics, without any
 * optimization.
 *
 * It can be used as a reference for testing other engines, or as a
 * starting point for understanding the BIP semantics.
 */
class ReferenceEngine : public Engine {
 public:
  ReferenceEngine(Compound &top, ModelClock &modelClock, bool disableMaximalProgress = false);
  virtual ~ReferenceEngine();

  virtual TimeValue time() const { return modelClock().time(); }
  virtual BipError &resume(const TimeValue &time);
  virtual void wait(const TimeValue &time);

  virtual BipError& initialize();
  virtual BipError& execute(InteractionValue &interaction, const TimeValue &time);
  virtual BipError& execute(AtomInternalPort &internal, const TimeValue &time);
  virtual BipError& execute(AtomExternalPort &external, const TimeValue &time);
  virtual BipError& checkExternals();
  virtual const vector<InteractionValue *> &interactions() const { return mInteractions; }
  virtual const vector<AtomInternalPort *> &internals() const { return mInternals; }
  virtual const vector<AtomExternalPort *> &externals() const { return mExternals; }
  virtual const vector<AtomExternalPort *> &waiting() const { return mWaiting; }
  
  virtual Interval interval(const InteractionValue &interaction) const;
  virtual Interval interval(const AtomInternalPort &internal) const;
  virtual Interval interval(const AtomExternalPort &external) const;

  virtual State getState() const;
  virtual void setState(const State &state);

  bool disableMaximalProgress() const { return mDisableMaximalProgress; }

 protected:
  /**
   * \brief Update interactions and internals w.r.t. to the current 
   * state of the system.
   */
  BipError &update();

  /**
   * \brief Compute the set of interactions enabled at the current state.
   */
  void computeEnabledInteractions();

  /**
   * \brief Compute the set of internal ports enabled at the current state.
   */
  void computeEnabledInternals();

  void computeWaitAndResumeIntervals();

  void computeInteractionsInternalsExternals();

  bool hasEarlyEvent(AtomExternalPort &port) const;

  /**
   * \brief Returns the set of interactions enabled at the current state.
   */
  vector<InteractionValue *> &interactions() { return mInteractions; }
  vector<InteractionValue *> &enabledInteractions() { return mEnabledInteractions; }

  /**
   * \brief Returns the set of internal ports enabled at the current state.
   */
  vector<AtomInternalPort *> &internals() { return mInternals; }
  vector<AtomInternalPort *> &enabledInternals() { return mEnabledInternals; }
  /**
   * \brief Returns the set of exernal ports enabled at the current state.
   */
  vector<AtomExternalPort *> &externals() { return mExternals; }
  vector<AtomExternalPort *> &enabledExternals() { return mEnabledExternals; }

  /**
   * \brief Returns the set of exernal ports waiting at the current state.
   */
  vector<AtomExternalPort *> &waiting() { return mWaiting; }

  /**
   * \brief Returns the set of exernal ports with unexpected events at the current state.
   */
  vector<AtomExternalPort *> &unexpected() { return mUnexpected; }

  const ModelClock &modelClock() const { return mModelClock; }
  ModelClock &modelClock() { return mModelClock; }

  /**
   * \brief Set of interactions enabled at the current state.
   */
  vector<InteractionValue *> mInteractions;
  vector<InteractionValue *> mEnabledInteractions;

  /**
   * \brief Set of internal ports enabled at the current state.
   */
  vector<AtomInternalPort *> mInternals;
  vector<AtomInternalPort *> mEnabledInternals;

  /**
   * \brief Set of external ports enabled at the current state.
   */
  vector<AtomExternalPort *> mExternals;
  vector<AtomExternalPort *> mEnabledExternals;

  /**
   * \brief Set of external ports waiting at the current state.
   */
  vector<AtomExternalPort *> mWaiting;

  /**
   * \brief Set of external ports with unexpected events at the current state.
   */
  vector<AtomExternalPort *> mUnexpected;


  ModelClock &mModelClock;

  bool mDisableMaximalProgress;
};

#endif // _BIP_Engine_ReferenceEngine_HPP_
