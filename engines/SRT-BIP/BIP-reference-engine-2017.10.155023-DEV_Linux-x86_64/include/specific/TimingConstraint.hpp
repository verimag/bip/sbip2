#ifndef _BIP_Engine_TimingConstraint_HPP_
#define _BIP_Engine_TimingConstraint_HPP_

// inherited classes
#include <TimeValue.hpp>
#include <Interval.hpp>

class TimingConstraint {
 public:
  // constructor
  TimingConstraint();
  TimingConstraint(const TimingConstraint &constraint);
  TimingConstraint(Urgency urgency);
  TimingConstraint(Urgency urgency, const TimeValue &left, const TimeValue &right);
  TimingConstraint(Urgency urgency, const Interval &interval);

  // destructor
  virtual ~TimingConstraint();

  // getters for attributes
  const Interval &interval() const { return mInterval; }
  Urgency urgency() const { return mUrgency; }

  // setters for attributes
  void setUrgency(Urgency urgency) { mUrgency = urgency; }

  // operations
  bool empty() const { return interval().empty(); }
  bool in(const TimeValue &time) const { return interval().in(time); }
  TimeValue size() const { return interval().size(); }
  TimeValue next(const TimeValue &time) const { return interval().next(time); }
  Interval urgent() const { return interval().urgent(urgency()); }
  Interval wait(const TimeValue &time) const { return interval().wait(time, urgency()); }
  TimeValue shift(const TimeValue &time) const { return interval().shift(time); }
  TimeValue random(const TimeValue &granularity) const;

  TimingConstraint &operator=(const TimingConstraint &constraint);
  TimingConstraint &operator=(const Interval &i) { mInterval = i; return *this; }

  TimingConstraint &operator&=(const Interval &i) { mInterval &= i; return *this; }
  TimingConstraint &operator&=(const TimingConstraint &constraint);

  void applyPriority(const TimingConstraint &constraint, const Interval &invariant, const TimeValue &time);
  void applyUrgencyInheritance(const TimingConstraint &constraint, const Interval &invariant, const TimeValue &time);

 protected:
  // attributes
  Urgency mUrgency;
  Interval mInterval;
};

std::ostream &operator<<(std::ostream &o, const TimingConstraint &constraint);

#endif // _BIP_Engine_TimingConstraint_HPP_
