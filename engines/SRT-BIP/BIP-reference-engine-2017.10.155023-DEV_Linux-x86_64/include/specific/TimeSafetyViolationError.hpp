#ifndef _BIP_Engine_TimeSafetyViolationError_HPP_
#define _BIP_Engine_TimeSafetyViolationError_HPP_

// inherited classes
#include <TimeSafetyViolationErrorItf.hpp>
#include "BipError.hpp"


class TimeSafetyViolationError : public virtual BipErrorItf, public BipError, public TimeSafetyViolationErrorItf {
 public:
  // constructors
  TimeSafetyViolationError(const TimeValue &time);

  // destructor
  virtual ~TimeSafetyViolationError();
};

#endif // _BIP_Engine_TimeSafetyViolationError_HPP_
