#ifndef _BIP_Engine_SimulationClock_HPP_
#define _BIP_Engine_SimulationClock_HPP_

#include <GlobalClock.hpp>
#include <TimeValue.hpp>

/** \brief Provides the basic interface for implementing the clock used by
 * the scheduler.
 *
 * 
 */
class SimulationClock : public GlobalClock {
 public:
  SimulationClock();
  virtual ~SimulationClock();

  virtual TimeValue time() const { return mTime; }
  virtual bool wait(const TimeValue &time);

 protected:
  TimeValue mTime;
  sigset_t maskSIGBIP;
};

#endif // _BIP_Engine_SimulationClock_HPP_
