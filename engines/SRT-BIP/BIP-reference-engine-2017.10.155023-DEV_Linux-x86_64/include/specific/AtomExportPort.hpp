#ifndef _BIP_Engine_AtomExportPort_HPP_
#define _BIP_Engine_AtomExportPort_HPP_

// inherited classes
#include <AtomExportPortItf.hpp>
#include "Port.hpp"
#include "PortValue.hpp"

class BipError;

class AtomExportPort : public virtual PortItf, public virtual Port, public AtomExportPortItf {
 public:
  // constructors
  AtomExportPort(const string &name);
  AtomExportPort(const string &name, bool hasEarlyUpdate);

  // destructor
  virtual ~AtomExportPort();

  // operations
  virtual bool isReady() const { return false; }
  virtual void setReady() { }

  // specific
  virtual bool hasResumeFor(PortValue &value);
  string getDistribution(PortValue &value) ;
  AtomInternalPort *getStochasticPort(PortValue &value);
};

#endif // _BIP_Engine_AtomExportPort_HPP_
