#ifndef _BIP_Engine_Interaction_HPP_
#define _BIP_Engine_Interaction_HPP_

// inherited classes
#include <InteractionItf.hpp>

class Priority;
class BipError;
class TimingConstraint;

class Interaction : public InteractionItf {
 public:
  // constructors
  Interaction(const Connector &connector);

  // destructor
  virtual ~Interaction();

  // operations
  virtual void recycle() {}

  // specific opertations
  void release();
  virtual bool isEnabled() const;

  void applyPrioritiesDomination(TimingConstraint &constraint) const;
  void applyLocalDomination(TimingConstraint &constraint) const;



  void inheritPrioritiesDominatedUrgencies(TimingConstraint &constraint) const;
  void inheritLocalDominatedUrgencies(TimingConstraint &constraint) const;

  vector<Priority *> dominatingPriorities() const;
  vector<Priority *> dominatedPriorities() const;
  BipError &detectCycleInPriorities() const;

  unsigned int index(const Port &port) const;
  bool contains(const Port &port) const { return index(port) != ports().size(); }

  bool isDominatedLocallyBy(const Interaction &interaction) const;

  friend ostream& operator<<(ostream &, const Interaction&);
  friend ostream& operator<<(ostream &, const Interaction*);

 protected:
  BipError &detectCycleInPriorities(const vector<const Interaction *> &path) const;
};

#endif // _BIP_Engine_Interaction_HPP_
