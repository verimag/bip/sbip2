#ifndef _BIP_Engine_AtomExternalPort_HPP_
#define _BIP_Engine_AtomExternalPort_HPP_

// inherited classes
#include <AtomExternalPortItf.hpp>

#include <Interval.hpp>
#include <TimingConstraint.hpp>

class AtomExternalPort : public AtomExternalPortItf {
 public:
  // constructors
  AtomExternalPort(const string &name, const EventConsumptionPolicy &policy);

  // destructor
  virtual ~AtomExternalPort();

  // operations
  virtual void notify();
  virtual TimeValue time();

  // specific
  TimingConstraint timingConstraint() const;
  bool hasExpectedEvent() const;
  bool hasOutdatedEvent() const;
};

#endif // _BIP_Engine_AtomExternalPort_HPP_
