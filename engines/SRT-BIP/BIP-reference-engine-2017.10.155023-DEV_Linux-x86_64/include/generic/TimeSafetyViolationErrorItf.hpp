#ifndef _BIP_Engine_TimeSafetyViolationErrorItf_HPP_
#define _BIP_Engine_TimeSafetyViolationErrorItf_HPP_

#include <bip-engineiface-config.hpp>
using namespace bipbasetypes;
using namespace biptypes;

// inherited classes
#include "BipErrorItf.hpp"

// used classes
#include "BipError.hpp"
#include "TimeValue.hpp"


class TimeSafetyViolationErrorItf : public virtual BipErrorItf {
 public:
  // destructor
  virtual ~TimeSafetyViolationErrorItf();

  // getters for references
  const TimeValue &time() const { return mTime; }

 protected:
  // protected constructors
  TimeSafetyViolationErrorItf(const TimeValue &time);

  // references
  TimeValue mTime;
};

#endif // _BIP_Engine_TimeSafetyViolationErrorItf_HPP_
