#ifndef _BIP_Engine_IntervalItf_HPP_
#define _BIP_Engine_IntervalItf_HPP_

#include <bip-engineiface-config.hpp>
using namespace bipbasetypes;
using namespace biptypes;

// used classes
#include "TimeValue.hpp"

// referenced classes
class Interval;


class IntervalItf {
 public:
  // destructor
  virtual ~IntervalItf();

  // operations
  virtual Interval &operator=(const Interval &interval) = 0;

  // getters for references
  TimeValue &left() { return mLeft; }
  const TimeValue &left() const { return mLeft; }
  TimeValue &right() { return mRight; }
  const TimeValue &right() const { return mRight; }

  // getters for attributes
  bool leftOpen() const { return mLeftOpen; }
  bool rightOpen() const { return mRightOpen; }

  // setters for references
  void setLeft(const TimeValue &left) { mLeft = left; }
  void setRight(const TimeValue &right) { mRight = right; }

  // setters for attributes
  void setLeftOpen(bool leftOpen) { mLeftOpen = leftOpen; }
  void setRightOpen(bool rightOpen) { mRightOpen = rightOpen; }

 protected:
  // protected constructors
  IntervalItf(const TimeValue &left, const TimeValue &right);
  IntervalItf(const TimeValue &left, const TimeValue &right, bool leftOpen, bool rightOpen);

  // attributes
  bool mLeftOpen;
  bool mRightOpen;

  // references
  TimeValue mLeft;
  TimeValue mRight;
};

#endif // _BIP_Engine_IntervalItf_HPP_
