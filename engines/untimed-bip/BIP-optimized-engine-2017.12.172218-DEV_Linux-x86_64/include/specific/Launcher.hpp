#ifndef _BIP_Engine_Launcher_HPP_
#define _BIP_Engine_Launcher_HPP_

// inherited classes
#include <LauncherItf.hpp>

class Engine;
class Scheduler;
class Logger;
class Compound;

class Launcher : public LauncherItf {
 public:
  // constructors
  Launcher(int argc, char **argv, Component &root);

  // operations
  virtual int initialize();
  virtual int launch();

  // destructor
  virtual ~Launcher();

 protected:
  // specific
  void printHelp(const string &bipExecutableName);

  Engine *mEngine;
  Scheduler *mScheduler;
  Logger *mLogger;
};

#endif // _BIP_Engine_Launcher_HPP_
