#ifndef _BIP_Engine_UnexpectedEventError_HPP_
#define _BIP_Engine_UnexpectedEventError_HPP_

// inherited classes
#include <UnexpectedEventErrorItf.hpp>
#include "BipError.hpp"

class UnexpectedEventError : public virtual BipErrorItf, public BipError, public UnexpectedEventErrorItf {
 public:
  // constructors
  UnexpectedEventError(Atom &atom, AtomExternalPort &port);

  // destructor
  virtual ~UnexpectedEventError();
};

#endif // _BIP_Engine_UnexpectedEventError_HPP_
