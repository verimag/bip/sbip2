#ifndef _BIP_Engine_ReferenceEngine_HPP_
#define _BIP_Engine_ReferenceEngine_HPP_

#include "Engine.hpp"
#include "Compound.hpp"

#include <bip-engineiface-config.hpp>

class InteractionValue;
class AtomInternalPort;
class AtomExternalPort;
class BipError;

/** \brief Direct implementation of the BIP semantics, without any
 * optimization.
 *
 * It can be used as a reference for testing other engines, or as a
 * starting point for understanding the BIP semantics.
 */
class ReferenceEngine : public Engine {
 public:
  ReferenceEngine(Compound &top);
  ReferenceEngine(Compound &top, bool disableFairness);
  virtual ~ReferenceEngine();

  virtual BipError& initialize();
  virtual BipError& execute(InteractionValue &interaction);
  virtual BipError& execute(AtomInternalPort &internal);
  virtual BipError& execute(AtomExternalPort &external);
  virtual BipError& checkExternals();
  virtual const vector<InteractionValue *> &interactions() const { return mInteractions; }
  virtual const vector<AtomInternalPort *> &internals() const { return mInternals; }
  virtual const vector<AtomExternalPort *> &externals() const { return mExternals; }
  virtual const vector<AtomExternalPort *> &waiting() const { return mWaiting; }

  virtual State getState() const;
  virtual void setState(const State &state);

  bool disableFairness() const { return mDisableFairness; }


 protected:
  /**
   * \brief Update interactions and internals w.r.t. to the current 
   * state of the system.
   */
  BipError &update();

  BipError &checkUnexpected(AtomExternalPort &port);

  /**
   * \brief Returns the set of interactions enabled at the current state.
   */
  vector<InteractionValue *> &interactions() { return mInteractions; }
  /**
   * \brief Returns the set of internal ports enabled at the current state.
   */
  vector<AtomInternalPort *> &internals() { return mInternals; }

  /**
   * \brief Returns the set of exernal ports enabled at the current state.
   */
  vector<AtomExternalPort *> &externals() { return mExternals; }

  /**
   * \brief Returns the set of exernal ports waiting at the current state.
   */
  vector<AtomExternalPort *> &waiting() { return mWaiting; }

  /**
   * \brief Returns the set of exernal ports with unexpected events at the current state.
   */
  vector<AtomExternalPort *> &unexpected() { return mUnexpected; }

  /**
   * \brief Set of internal ports enabled at the current state.
   */
  vector<InteractionValue *> mInteractions;

  /**
   * \brief Set of internal ports enabled at the current state.
   */
  vector<AtomInternalPort *> mInternals;

  /**
   * \brief Set of external ports enabled at the current state.
   */
  vector<AtomExternalPort *> mExternals;

  /**
   * \brief Set of external ports waiting at the current state.
   */
  vector<AtomExternalPort *> mWaiting;

  /**
   * \brief Set of external ports which are not enabled nor waiting.
   */
  vector<AtomExternalPort *> mUnexpected;


  bool mDisableFairness;
};

#endif // _BIP_Engine_ReferenceEngine_HPP_
