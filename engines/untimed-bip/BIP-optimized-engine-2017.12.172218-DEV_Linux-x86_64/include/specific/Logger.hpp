#ifndef _BIP_Engine_Logger_HPP_
#define _BIP_Engine_Logger_HPP_

#include <bip-engineiface-config.hpp>

using namespace bipbasetypes;
using namespace biptypes;

#include <string>
#include <iostream>
#include <sstream>

#include "BipError.hpp"
#include <NonDeterministicPetriNetError.hpp>
#include <NonOneSafePetriNetError.hpp>
#include <CycleInPrioritiesError.hpp>
#include <CycleInAtomPrioritiesError.hpp>
#include <AtomInvariantViolationError.hpp>
#include <UnexpectedEventError.hpp>
#include "Connector.hpp"
#include "InteractionValue.hpp"
#include "Atom.hpp"
#include "AtomInternalPort.hpp"
#include "AtomExternalPort.hpp"


/** \brief Represent a node in a tree of strings
 */
class Logger {
 public:
  // constructor and destructor
  Logger(ostream &outoutStream, bool verbose, unsigned int limit);
  virtual ~Logger();
    
  // operations
  unsigned int state() const { return mState; }
  bool reachedLimit() const { return mLimit != 0 && mState >= mLimit; }
  void log(const string &s);
  void log(const BipError &error);
  void log(const vector<InteractionValue *> &interactions, const vector<AtomInternalPort *> &internals, const vector<AtomExternalPort *> &externals);
  void log(const InteractionValue &interaction, const vector<InteractionValue *> &interactions, const vector<AtomInternalPort *> &internals, const vector<AtomExternalPort *> &externals);
  void log(const AtomInternalPort &port, const vector<InteractionValue *> &interactions, const vector<AtomInternalPort *> &internals, const vector<AtomExternalPort *> &externals);
  void log(const AtomExternalPort &port, const vector<InteractionValue *> &interactions, const vector<AtomInternalPort *> &internals, const vector<AtomExternalPort *> &externals);
  void logDeadlock() { if (verbose()) { begin(); outputStream() << "state #" << mState << ": deadlock!"; end(); } }
      
 protected:
  // operations
  void begin() { outputStream() << "[BIP ENGINE]: "; }
  void newLine() { outputStream() << endl << "[BIP ENGINE]: "; }
  void end() { outputStream() << endl; }
  void newState() { ++mState; }
 
  // getters
  bool verbose() const { return mVerbose; }
  ostream &outputStream() { return mOutputStream; }
 
  ostream &mOutputStream;
  bool mVerbose;
  unsigned int mLimit;
  unsigned int mState;
};


#endif // _BIP_Engine_Logger_HPP_
