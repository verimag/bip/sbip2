#ifndef _BIP_Engine_AtomInvariantViolationErrorItf_HPP_
#define _BIP_Engine_AtomInvariantViolationErrorItf_HPP_

#include <bip-engineiface-config.hpp>
using namespace bipbasetypes;
using namespace biptypes;

// inherited classes
#include "BipErrorItf.hpp"

// used classes
#include "BipError.hpp"

// referenced classes
class Atom;


class AtomInvariantViolationErrorItf : public virtual BipErrorItf {
 public:
  // destructor
  virtual ~AtomInvariantViolationErrorItf();

  // getters for references
  Atom &atom() const { return mAtom; }

  // getters for attributes
  const string &invariantName() const { return mInvariantName; }

 protected:
  // protected constructors
  AtomInvariantViolationErrorItf(const string &invariantName, Atom &atom);

  // attributes
  const string mInvariantName;

  // references
  Atom &mAtom;
};

#endif // _BIP_Engine_AtomInvariantViolationErrorItf_HPP_
