#ifndef _BIP_Engine_UnexpectedEventErrorItf_HPP_
#define _BIP_Engine_UnexpectedEventErrorItf_HPP_

#include <bip-engineiface-config.hpp>
using namespace bipbasetypes;
using namespace biptypes;

// inherited classes
#include "BipErrorItf.hpp"

// used classes
#include "BipError.hpp"

// referenced classes
class Atom;
class AtomExternalPort;


class UnexpectedEventErrorItf : public virtual BipErrorItf {
 public:
  // destructor
  virtual ~UnexpectedEventErrorItf();

  // getters for references
  Atom &atom() const { return mAtom; }
  AtomExternalPort &port() const { return mPort; }

 protected:
  // protected constructors
  UnexpectedEventErrorItf(Atom &atom, AtomExternalPort &port);

  // references
  Atom &mAtom;
  AtomExternalPort &mPort;
};

#endif // _BIP_Engine_UnexpectedEventErrorItf_HPP_
