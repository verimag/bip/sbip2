#ifndef _BIP_Engine_AtomExternalPortItf_HPP_
#define _BIP_Engine_AtomExternalPortItf_HPP_

#include <bip-engineiface-config.hpp>
using namespace bipbasetypes;
using namespace biptypes;

// referenced classes
class Atom;
class PortValue;


class AtomExternalPortItf {
 public:
  // destructor
  virtual ~AtomExternalPortItf();

  // operations
  virtual void notify() = 0;
  virtual void initialize() = 0;
  virtual bool hasEvent() const = 0;
  virtual void popEvent() = 0;
  virtual void purgeEvents() = 0;

  // getters for attributes
  const string &name() const { return mName; }
  const EventConsumptionPolicy &policy() const { return mPolicy; }
  const bool &waiting() const { return mWaiting; }

  // setters for attributes
  void setWaiting(const bool &waiting) { mWaiting = waiting; }

  // opposites accessors
  Atom &holder() const { return *mHolder; }
  bool hasHolder() const { return mHolder != NULL; }
  void setHolder(Atom &atom) { mHolder = &atom; }

 protected:
  // protected constructors
  AtomExternalPortItf(const string &name, const EventConsumptionPolicy &policy);

  // attributes
  const string mName;
  const EventConsumptionPolicy mPolicy;
  bool mWaiting;

  // opposites
  Atom *mHolder;
};

#endif // _BIP_Engine_AtomExternalPortItf_HPP_
