#ifndef _BIP_Engine_ExecuteConnectorJob_HPP_
#define _BIP_Engine_ExecuteConnectorJob_HPP_

// inherited classes
#include <Job.hpp>
#include <BipError.hpp>
#include "ReadyQueue.hpp"
#include "Resource.hpp"

class Connector;
class Atom;
class Port;
class Logger;
class ExecuteAtomJob;
class InteractionValue;

class ExecuteConnectorJob : public Job {
 public:
  // constructors
  ExecuteConnectorJob(Connector &connector);

  // destructor
  virtual ~ExecuteConnectorJob();

  // getters for references
  Connector &connector() const { return mConnector; }
  Logger &logger() { return *mLogger; }
  bool hasLogger() const { return mLogger != NULL; }
  
  // setters
  void setLogger(Logger &logger) { mLogger = &logger; }
  void clearLogger() { mLogger = NULL; }

  // operations
  void initialize();
  void initializeMutexs();

 protected:
  // operations
  virtual void realJob();
  virtual void prologue() { mReserver.start(); }

  void logChosenInteraction();
  void unreserveUnusedResources();

  vector<Atom *> allAtoms();
  void allAtoms(vector<Atom *> &atoms, const Connector &connector);
  void allAtoms(vector<Atom *> &atoms, const Port &port);

  Connector &mConnector;
  Logger *mLogger;

  // all atoms involved
  vector<Atom *> mAllAtoms;

  // reservation mechanisms
  class ConnectorReserver : public Resource::Reserver {
   public:
    ConnectorReserver(ExecuteConnectorJob &job) : Resource::Reserver(job), mExecuteConnectorJob(job) { }
    virtual ~ConnectorReserver() { }

    virtual void uponReservation() { mExecuteConnectorJob.logChosenInteraction(); }

   protected:
    ExecuteConnectorJob &mExecuteConnectorJob;
  };
  
  ConnectorReserver mReserver;

  // chosen interaction
  InteractionValue *mChosenInteraction;
};

#endif // _BIP_Engine_ExecuteConnectorJob_HPP_
