#ifndef _BIP_Engine_Executer_HPP_
#define _BIP_Engine_Executer_HPP_

#include "StringTree.hpp"
#include "ReadyQueue.hpp"
#include "Resource.hpp"

class BipError;
class Atom;
class Component;
class Connector;
class Compound;
class Port;
class InteractionValue;
class ConnectorExportPort;
class CompoundExportPort;
class AtomInternalPort;
class AtomExportPort;
class Logger;
class Thread;
class Job;

/** \brief Compute execution sequences in which interactions
 * are chosen randomly.
 */
class Executer {
 public:
  Executer(Compound &top, Logger &logger, unsigned int nbThreads, bool randomSeed = true, unsigned int seed = 0);
  virtual ~Executer();

  BipError &initialize();
  BipError &run();
  bool debug() const { return mDebug; }
  bool interactive() const { return mInteractive; }
  bool randomSeed() const { return mRandomSeed; }
  unsigned int seed() const { return mSeed; }
  unsigned int limit() const { return mLimit; }

  static ReadyQueue<Job> jobs;

 protected:
  void initializeReserver(const Compound &compound);
  void initializeThreads();
  void startThreads();
  void stopThreads();
  void print();
  StringTree print(const ConnectorExportPort &port);
  StringTree print(const CompoundExportPort &port);
  StringTree print(const AtomInternalPort &port);
  StringTree print(const AtomExportPort &port);
  StringTree print(const InteractionValue &port);
  StringTree print(const Connector &connector);
  StringTree print(const Atom &atom);
  StringTree print(const Compound &compound);
  StringTree print(const Component &component);

  Logger &logger() { return mLogger; }

  /** \return Root component.
   */
  Compound& top() { return mTop; }

  Compound &mTop;

  Logger &mLogger;
  bool mDebug;
  bool mInteractive;
  unsigned int mNbThreads;
  bool mRandomSeed;
  unsigned int mSeed;
  unsigned int mLimit;

  vector<Thread *> mThreads;
  Resource::Reserver mReserver;
};

#endif // _BIP_Engine_Executer_HPP_
