#ifndef _BIP_Engine_ExecuteAtomJob_HPP_
#define _BIP_Engine_ExecuteAtomJob_HPP_

// inherited classes
#include <Job.hpp>
#include <BipError.hpp>
#include "PortValue.hpp"
#include "AtomInternalPort.hpp"
#include "ReadyQueue.hpp"
#include "Resource.hpp"

class Atom;
class PortValue;
class BipError;
class Logger;
class ExecuteConnectorJob;
class AtomInternalPort;
class AtomExternalPort;

class ExecuteAtomJob : public Job {
 public:
  // constructors
  ExecuteAtomJob(Atom &atom);

  // destructor
  virtual ~ExecuteAtomJob();

  // getters for references
  Atom &atom() const { return mAtom; }
  PortValue &portValue() const { return *mPortValue; }
  bool hasPortValue() const { return mPortValue != NULL; }
  Logger &logger() { return *mLogger; }
  bool hasLogger() const { return mLogger != NULL; }
  Resource::Writer &writer() { return mWriter; }

  // setters
  void setPortValue(PortValue &portValue) { mPortValue = &portValue; }
  void clearPortValue() { mPortValue = NULL; }
  void setLogger(Logger &logger) { mLogger = &logger; }
  void clearLogger() { mLogger = NULL; }

  // operations
  void initialize();
  void initializeMutexs() { mAllReserver.initialize(); mPortsDataReserver.initialize(); }

 protected:
  // operations
  virtual void realJob();
  virtual void prologue();
  virtual void epilogue();

  BipError &executeAllExternalPortsAndInternalTransitions();
  void recomputeInternals();
  BipError &recomputeExternals();
  BipError &checkExternals();
  BipError &checkUnexpected(AtomExternalPort &port);
  void initializeAllExternalPorts();

  BipError &initializeAtom();
  BipError &execute(PortValue &portValue);
  BipError &execute(AtomInternalPort &port);
  BipError &execute(AtomExternalPort &port);
  BipError &update();

  void reserveAll();
  void reserve();
  void free();

  void resetIsReady();

  bool mInitialized;
  Atom &mAtom;
  PortValue *mPortValue;
  Logger *mLogger;

  vector<AtomInternalPort *> mNonExportedPorts;

  vector<AtomInternalPort *> mInternals;
  vector<AtomExternalPort *> mExternals;
  vector<AtomExternalPort *> mWaiting;
  vector<AtomExternalPort *> mUnexpected;

  Resource::Writer mWriter;
  Resource::Reserver mAllReserver;
  Resource::Reserver mPortsDataReserver;
};

#endif // _BIP_Engine_ExecuteAtomJob_HPP_
