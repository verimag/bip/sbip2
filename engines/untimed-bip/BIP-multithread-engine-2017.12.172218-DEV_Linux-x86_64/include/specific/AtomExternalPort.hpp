#ifndef _BIP_Engine_AtomExternalPort_HPP_
#define _BIP_Engine_AtomExternalPort_HPP_

// inherited classes
#include <AtomExternalPortItf.hpp>

class ExecuteConnectorJob;

class AtomExternalPort : public AtomExternalPortItf {
 public:
  // constructors
  AtomExternalPort(const string &name, const EventConsumptionPolicy &policy);

  // destructor
  virtual ~AtomExternalPort();

  // operations
  virtual void notify();
};

#endif // _BIP_Engine_AtomExternalPort_HPP_
