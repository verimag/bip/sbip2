#ifndef _BIP_Engine_Atom_HPP_
#define _BIP_Engine_Atom_HPP_

// inherited classes
#include <AtomItf.hpp>
#include "AtomExportPort.hpp"
#include "AtomExportData.hpp"
#include "AtomInternalPort.hpp"
#include "Component.hpp"
#include "ExecuteAtomJob.hpp"
#include "Resource.hpp"

class BipError;
class Compound;

class Atom : public virtual ComponentItf, public Component, public AtomItf {
 public:
  // constructors
  Atom(const string &name);

  // destructor
  virtual ~Atom();

  virtual BipError &execute(PortValue &portValue) = 0;
  virtual BipError &execute(AtomExternalPort &port) = 0;
  virtual string toString() const { return "-"; };

  // specific
  BipError &execute(AtomInternalPort &internalPort);
  void allNotReady();
  void allReady();

  ExecuteAtomJob &executeJob() { return mExecuteJob; }
  Resource &resource() { return mResource; }

 protected:
  // references accessors
  void addInternalPort(AtomInternalPort &internalPort) {
    mInternalPorts[internalPort.name()] = &internalPort;
    internalPort.setHolder(*this);
  }

  void addExternalPort(AtomExternalPort &externalPort) {
    mExternalPorts[externalPort.name()] = &externalPort;
    externalPort.setHolder(*this);
  }

  void addPort(AtomExportPort &port) {
    mPorts[port.name()] = &port;
    port.setHolder(*this);
    Component::addPort(port);

    if (port.hasEarlyUpdate()) {
      port.reset().addResource(port.resource());
    }
    else {
      port.reset().addResource(mResource);
    }
  }

  void addData(AtomExportData &data) {
    mData[data.name()] = &data;
    data.setHolder(*this);
    Component::addData(data);

    if (data.hasEarlyUpdate()) {
      data.reset().addResource(data.resource());
    }
    else {
      data.reset().addResource(mResource);
    }
  }

  // specific
  ExecuteAtomJob mExecuteJob;
  Resource mResource;

  friend ostream& operator<<(ostream &o, const Atom &atom);
  friend ostream& operator<<(ostream &o, const Atom *atom);
};

#endif // _BIP_Engine_Atom_HPP_
