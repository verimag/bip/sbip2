#ifndef _BIP_Engine_CycleInPriorities_HPP_
#define _BIP_Engine_CycleInPriorities_HPP_

#include <bip-engineiface-config.hpp>
using namespace bipbasetypes;
using namespace biptypes;

#include <Resetable.hpp>
#include <Resource.hpp>

class Priority;
class AtomExportData;
class Connector;
class Interaction;
class Logger;

class CycleInPriorities : public Resource::Validator {
 public:
  // constructors
  CycleInPriorities(const vector<Priority *> &priorities);
  CycleInPriorities(const CycleInPriorities &cycle);

  // destructor
  virtual ~CycleInPriorities();

  // getters for references
  Logger &logger() { return *mLogger; }
  bool hasLogger() const { return mLogger != NULL; }
  const vector<Priority *> &priorities() const { return mOrderedPriorities; }

  // setters
  void setLogger(Logger &logger) { mLogger = &logger; }
  void clearLogger() { mLogger = NULL; }  

  // operations
  virtual void reset(const vector<Resource *> &resources);
  virtual void free(Resource &resource);
  void validate();

  void initialize();
  bool allGuardsTrue() const;
  vector<const Interaction *> interactions() const;
  bool operator<(const CycleInPriorities &cycle) const { return mPriorities < cycle.mPriorities; }
  
 protected:
  Interaction &getLowInteraction(const Priority &priority, const Priority &lowerPriority) const;
  Interaction &getHighInteraction(const Priority &priority, const Priority &higherPriority) const;
  Interaction &getMatchingWildcard(const Connector &connector1, const Connector &connector2) const;
  Interaction &getMaximalInteraction(const Connector &connector) const;

  Logger *mLogger;
  vector<Priority *> mOrderedPriorities;
  set<Priority *> mPriorities;
  atomic<bool> mError;
};

#endif // _BIP_Engine_CycleInPriorities_HPP_
