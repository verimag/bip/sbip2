#ifndef _BIP_Engine_AtomExportData_HPP_
#define _BIP_Engine_AtomExportData_HPP_

// inherited classes
#include <AtomExportDataItf.hpp>
#include "Data.hpp"
#include "Resetable.hpp"

class AtomExportData : public virtual DataItf, public virtual Data, public AtomExportDataItf {
 public:
  // constructors
  AtomExportData(const string &name);
  AtomExportData(const string &name, bool hasEarlyUpdate);

  // destructor
  virtual ~AtomExportData();

  // operations
  virtual bool isReady() const { return mIsReady.load(); }
  virtual void setReady();

  // specific
  virtual ResetableItf &reset() { return mReset; }
  Resource &resource() { return mResource; }
  void unsetReady() { mIsReady.store(false); }

 protected:
  // specific
  ResetableItf mReset;
  Resource mResource;
  atomic<bool> mIsReady;
};

inline void AtomExportData::setReady() {
  bool oldIsReady = mIsReady.exchange(true);

  if (!oldIsReady) {
    if (isReset()) {
      reset().resetDependent();
    }

    if (hasEarlyUpdate()) {
      mResource.free();
    }
  }
}

#endif // _BIP_Engine_AtomExportData_HPP_
