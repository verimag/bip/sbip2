#ifndef _BIP_Engine_Job_HPP_
#define _BIP_Engine_Job_HPP_

#include "BipError.hpp"
#include "FastMutex.hpp"
#include "ReadyQueue.hpp"

class ResetableItf;
class Thread;

class Job {
 public:
  // constructors
  Job(ReadyQueue<Job> &readyQueue, bool highPriority = false);

  // destructor
  virtual ~Job();

  // getters
  bool isRestarted() const { return mIsRestarted; }

  // operations
  void dependsOn(ResetableItf &resetable);

  void execute();

  void restart();

  void preventEnqueuing() { mPreventEnqueuing.fetch_add(1); }
  void unpreventEnqueuing();

  const ReadyQueue<Job> &readyQueue() const { return mReadyQueue; }
  const vector<Thread *> &threads() const { return mReadyQueue.threads(); }

 protected:
  // protected operations
  virtual void realJob() {
    // dummy job by default
  }

  virtual void prologue() { }
  virtual void epilogue() { }

  void enqueue();

  // protected getters
  ReadyQueue<Job> &mReadyQueue;
  bool mHighPriority;

  atomic<bool> mIsRestarted;
  atomic<bool> mIsEnqueued;
  atomic<unsigned int> mPreventEnqueuing;

  // (b)locking mechanisms
  FastMutex mExecuting;
};

inline void Job::restart() {
  if (!mIsRestarted.exchange(true)) {
    // restarted jobs require enqueuing at some point
#ifdef NDEBUG
    mIsEnqueued.store(false);
#else
    bool oldIsEnqueued = mIsEnqueued.exchange(false);
#endif

    assert(oldIsEnqueued == true);
    
    // enqueue if no before job has been restarted
    if (mPreventEnqueuing.load() == 0) {
      enqueue();
    }
  }
}

inline void Job::unpreventEnqueuing() {
  unsigned int oldPreventEnqueuing = mPreventEnqueuing.fetch_sub(1);

  assert(oldPreventEnqueuing > 0);

  if (oldPreventEnqueuing - 1 == 0) {
    enqueue();
  }
}

#endif // _BIP_Engine_Job_HPP_
