#ifndef _BIP_Engine_RandomScheduler_HPP_
#define _BIP_Engine_RandomScheduler_HPP_

#include "Scheduler.hpp"
#include "StringTree.hpp"

class Engine;
class Logger;
class BipError;
class Atom;
class Component;
class Connector;
class Compound;
class InteractionValue;
class ConnectorExportPort;
class CompoundExportPort;
class AtomInternalPort;
class AtomExternalPort;
class AtomExportPort;

/** \brief Compute execution sequences in which interactions
 * are chosen randomly.
 */
class RandomScheduler : public Scheduler {
 public:
  RandomScheduler(Engine &engine, Logger &logger, bool debug = false, bool interactive = false, bool randomSeed = true, unsigned int seed = 0);
  virtual ~RandomScheduler();

  virtual BipError &initialize();
  virtual BipError &run();

  bool debug() const { return mDebug; }
  bool interactive() const { return mInteractive; }
  bool randomSeed() const { return mRandomSeed; }
  unsigned int seed() const { return mSeed; }

  static void notify();


 protected:
  bool deadlock() const;
  void waitForNotifications();
  static pid_t pid;
  static volatile unsigned int nbNotifications;
  static void handleSignals(int signum);
  void print();
  StringTree print(const ConnectorExportPort &port);
  StringTree print(const CompoundExportPort &port);
  StringTree print(const AtomInternalPort &port);
  StringTree print(const AtomExternalPort &port);
  StringTree print(const AtomExportPort &port);
  StringTree print(const InteractionValue &port);
  StringTree print(const Connector &connector);
  StringTree print(const Atom &atom);
  StringTree print(const Compound &compound);
  StringTree print(const Component &component);

  bool newNotifications() { return mNbTreatedNotifications != nbNotifications; }
  void updateTreatedNotifications() { mNbTreatedNotifications = nbNotifications; }

  bool checkIsSerializeEnabled();
  string parse(const string &cmdline, size_t &index);

  Logger &logger() const { return mLogger; }

  Logger &mLogger;
  bool mDebug;
  bool mInteractive;
  bool mRandomSeed;
  unsigned int mSeed;

  unsigned int mNbTreatedNotifications;
};

#endif // _BIP_Engine_RandomScheduler_HPP_
