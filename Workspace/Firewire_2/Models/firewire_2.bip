@cpp(include="stdio.h")
package firewire_2
	
	/// Constant variables definition 
	const data int rc_fast_max = 85
	const data int rc_fast_min = 76

	const data int rc_slow_max = 167
	const data int rc_slow_min = 159

	const data int delay = 30
	const data int delay_min = 5

	/// Extern functions	
	extern function printf(string,int)
	
	/// Port types
	port type Port()
	port type ePort(int id_wire)
		
	
	/// Connector type definitions
	connector type Connect(Port snd, Port rcv)
	 	define snd rcv
	end

	connector type ConnectSnd(ePort snd, ePort channel)
	 	define snd channel
			on snd channel provided(snd.id_wire == channel.id_wire)
	end

	connector type ConnectRcv(ePort channel , ePort rcv)
	 	define channel rcv 
			on channel rcv down {rcv.id_wire = channel.id_wire;}
	end

	/// Atom type for nodes
	atom type Node(int n)
		data int s=0
		data int c=0
		data int id_channel = 0
		data bool contention = false
		export port ePort snd_ack(id_channel)
		export port Port snd_req()
		export port ePort rcv_req(id_channel)
		export port Port rcv_ack()
		port Port slow()
		port Port fast()
		port Port wait()
		port Port slave()
		port Port leader()


		// Instantiate clocks
		clock x unit nanosecond

		// locations
		place s0, s1, s2, s3, s4, s5, s6, s7, s8


		
		initial to s0 do{s = 0;}

		on rcv_req from s0 to s1
			provided (n-c>1)
			delayable 
			do{s=1; c=c+1; x=0;}

		on slow from s0 to s2
			provided (n-c==1 && x==0)
			delayable 
			do{s=2; x=0; contention = false;  }

		on fast from s0 to s3
			provided (n-c==1 && x==0)
			delayable 
			do{s=3; x=0; contention = false;  }

		on snd_ack from s1 to s0
			provided(x==0)
			do{s=0; x=0;}

		on wait from s2 to s4
			provided (x<=rc_slow_max && x>=rc_slow_min)
			delayable 
			do{s=4; x=0;}

		on rcv_req from s2 to s6
			delayable 
			do{s=6;  c=c+1; x=0;}

		on wait from s3 to s4
			provided (x<=rc_fast_max && x>=rc_fast_min)
			delayable 
			do{s=4; x=0;}

		on rcv_req from s3 to s6
			delayable 
			do{s=6; c=c+1; x=0;}

		on snd_req from s4 to s5			
			provided(x==0)
			delayable 
			do{s=5; }

		on rcv_req from s4 to s6
			delayable 
			do{s=6;}
	
		on rcv_ack from s5 to s8
			delayable 
			do{s=8;}

		on rcv_req from s5 to s0
			delayable 
			do{s=0; contention = true; x=0;}

		on snd_ack from s6 to s7
			provided(x==0)
			delayable 
			do{s=7; }

		on leader from s7 to s7
			delayable 
			do{s=7;}

		on slave from s8 to s8
			delayable 
			do{s=8;}

	end

	atom type Channel(int id)

		data int id_channel = id
		export port ePort snd_ack( id_channel)
		export port Port snd_req()
		export port ePort rcv_req( id_channel)
		export port Port rcv_ack()
		port Port wait()
		

		// Instantiate clocks
		clock x unit nanosecond

		// locations
		place s0, s1, s2, s3, s4


		initial to s0 do{}
		
		on snd_req from s0 to s1
			delayable 
			do{x=0;}

		on snd_req from s1 to s1
			delayable 
			do{x=0;}

		
		on snd_ack from s1 to s2
			delayable 
			do{x=0;}
		
		on rcv_req from s1 to s0
			provided(x<=delay && x>=delay_min)
			delayable 
			do{x=0;}

		on snd_ack from s0 to s2
			delayable 
			do{x=0;}
		
		on rcv_ack from s2 to s0
			provided(x<=delay && x>=delay_min)
			delayable 
			do{x=0;}

//		on wait from s1 to s1			
//			provided(x>0)
//			delayable 
//			do{x=0;}

//		on wait from s2 to s2		
//			provided(x>0)
//			delayable 
//			do{x=0;}

			

	end


	compound type Compound()
		// Instantiate Atomic components
		component Node node1(1)
		component Node node2(1)
		component Channel wire12(1)
		component Channel wire21(1)

		// Compose them through Send-receive interaction
		connector Connect con_snd_req1(node1.snd_req, wire12.snd_req)
		connector Connect con_snd_req2(node2.snd_req, wire21.snd_req)
		connector ConnectSnd con_snd_ack1(node1.snd_ack, wire12.snd_ack)
		connector ConnectSnd con_snd_ack2(node2.snd_ack, wire21.snd_ack)

		connector ConnectRcv con_rcv_req1(wire21.rcv_req, node1.rcv_req)
		connector ConnectRcv con_rcv_req2(wire12.rcv_req, node2.rcv_req)
		connector Connect con_rcv_ack1(node1.rcv_ack, wire21.rcv_ack)
		connector Connect con_rcv_ack2(node2.rcv_ack, wire12.rcv_ack)
	end

end
